/*****************************************************************************
 * vim:tw=78
 *
 * Copyright (C) 2021 Richard Hacker (lerichi at gmx dot net),
 *                    Florian Pose (fp at igh dot de),
 *                    Bjarne von Horn (vh at igh dot de).
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

/** @file */

#ifndef PDCOM5_SELECTOR_H
#define PDCOM5_SELECTOR_H

#include "SizeTypeInfo.h"

#include <memory>
#include <pdcom5_export.h>
#include <vector>

namespace PdCom {
namespace impl {
struct Selector;
}  // namespace impl
class Variable;


/** Selector base class for creating views on multidimensional data.
 *
 * Developers have to derive from impl::Selector to define new views.
 */
struct PDCOM5_PUBLIC Selector
{
    std::shared_ptr<const impl::Selector> impl_;

    Selector();
    Selector(std::shared_ptr<const impl::Selector> impl) :
        impl_(std::move(impl))
    {}

    SizeInfo getViewSizeInfo(const Variable &variable) const;
};

/** Selects one scalar out of a vector or matrix.
 *
 * The index is in Row major format, ((layer, )row, column).
 */
struct PDCOM5_PUBLIC ScalarSelector : Selector
{
    ScalarSelector(std::vector<int> indices);
};


}  // namespace PdCom

#endif  // PDCOM5_SELECTOR_H
