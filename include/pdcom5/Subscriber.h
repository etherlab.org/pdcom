/*****************************************************************************
 *
 * Copyright (C) 2021 Richard Hacker (lerichi at gmx dot net),
 *                    Florian Pose (fp at igh dot de),
 *                    Bjarne von Horn (vh at igh dot de).
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more .
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

/** @file */

#ifndef PDCOM5_SUBSCRIBER_H
#define PDCOM5_SUBSCRIBER_H

#include <chrono>
#include <functional>
#include <pdcom5/Exception.h>
#include <pdcom5_export.h>

namespace PdCom {

namespace impl {
class Subscription;
}

class Subscription;

/// Tag for event-based subscription.
constexpr struct event_mode_tag
{
} event_mode;

/// Tag for poll-based subscription.
constexpr struct poll_mode_tag
{
} poll_mode;

/** Transmission mode for subscriptions.
 *
 * This class specifies whether a Subscriber should get new values
 * periodically, event-based or by polling only. Therefore, it is passed to
 * the Subscribers' constructor. All Subscriptions registered with that
 * subscriber will share the given transmission mode.
 */
class PDCOM5_PUBLIC Transmission
{
    double interval_;

    static constexpr double checkInterval(double d)
    {
        return d <= 0 ? throw PdCom::InvalidArgument(
                       "period must be greater than zero")
                      : d;
    }

  public:
    constexpr double getInterval() const noexcept { return interval_; }
    template <typename T, typename R>
    constexpr Transmission(std::chrono::duration<T, R> d) :
        interval_(checkInterval(
                std::chrono::duration_cast<std::chrono::duration<double>>(d)
                        .count()))
    {}
    constexpr Transmission(event_mode_tag) noexcept : interval_(0) {}
    constexpr Transmission(poll_mode_tag) noexcept : interval_(-1) {}
    bool operator==(const Transmission &o) const noexcept
    {
        return o.interval_ == interval_;
    }

    static constexpr Transmission fromDouble(double d)
    {
        return d == 0
                ? Transmission(event_mode)
                : (d == -1 ? Transmission(poll_mode)
                           : Transmission(std::chrono::duration<double>(d)));
    }
};

/** Base class for receiving notifications.
 *
 * This class is in charge of passing notifications from the library to client
 * code. Make sure that the subscriber outlives all assigned subscriptions.
 * Otherwise use-after-free bugs will occur, so be careful.
 *
 * The Subscriber class is also used to group subscriptions with the same
 * transmission mode. The newValues method is called everytime, after all
 * active subscriptions got their values updated for one realtime cycle.
 *
 * \example advanced_example.cpp
 */
class PDCOM5_PUBLIC Subscriber
{
    Transmission td_;
    friend class impl::Subscription;

  public:
    explicit Subscriber(const Transmission &td) noexcept : td_(td) {}
    Subscriber(Subscriber const &) = delete;
    Subscriber(Subscriber &&)      = delete;
    Subscriber &operator=(Subscriber const &) = delete;
    Subscriber &operator=(Subscriber &&) = delete;

    const Transmission &getTransmission() const noexcept { return td_; }

  private:
    /** Callback for an updated subscription state.
     * @param subscription Subscription which was updated.
     */
    virtual void stateChanged(PdCom::Subscription const &subscription) = 0;

    /** Callback for new values.
     *
     * The epoch of \p time_ns depends on the clock selected in pdserv.
     *
     * Called after all active subscriptions got their values updated for one
     * realtime cycle.
     *
     * @param time_ns Time since epoch in nanoseconds.
     */
    virtual void newValues(std::chrono::nanoseconds time_ns) = 0;

  protected:
    ~Subscriber() = default;
};

}  // namespace PdCom

namespace std {

/** Hash implementation for PdCom::Transmission.
 *
 * Allows to use PdCom::Transmission as a key in std::unordered_map.
 */
template <>
struct hash<PdCom::Transmission>
{
    size_t operator()(PdCom::Transmission t) const
            noexcept(__cplusplus >= 201703L)
    {
        return std::hash<double>()(t.getInterval());
    }
};

}  // namespace std

#endif  // PDCOM5_SUBSCRIBER_H
