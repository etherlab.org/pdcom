/*****************************************************************************
 * vim:tw=78
 *
 * Copyright (C) 2021 Bjarne von Horn (vh at igh dot de).
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

/** @file */

#ifndef PDCOM5_POSIXPROCESS_H
#define PDCOM5_POSIXPROCESS_H

#include <chrono>
#include <memory>
#include <pdcom5_export.h>
#include <string>

namespace PdCom {


/** Wrapper around POSIX socket.
 *
 * You can derive from this class to use POSIX socket with your Process
 * instance.
 *
 */

class PDCOM5_PUBLIC PosixProcess
{
  public:
    /** Constructor.
     *
     * \param host Host, can be a hostname or an IP(v6) address.
     * \param port Port
     * \throws ConnectionFailed Connection could not be established.
     */
    PosixProcess(const char *host, unsigned short port);

    /** Constructor
     * \param fd File descriptor, assumed to be connected, will be closed by
     * dtor.
     */
    explicit PosixProcess(int fd);


    virtual ~PosixProcess();

    PosixProcess(PosixProcess &&o)     = delete;
    PosixProcess(PosixProcess const &) = delete;

    PosixProcess &operator=(PosixProcess &&o) = delete;
    PosixProcess &operator=(PosixProcess const &) = delete;

  protected:
    /// File descriptior of the socket.
    int fd_;

    /** Buffered Wrapper for write().
     *
     * This loops around write(), so no short writes can occur. It uses an
     * internal buffer which can be flushed used posixFlush().
     * If the buffer is not big enough, it will be flushed and the content of
     * \p buf is written directly to the socket using posixWriteDirect().
     *
     * If the socket works in nonblocking mode, it will be select()ed with a
     * timeout set by setWriteTimeout() until everything is written.
     *
     * \param buf Buffer to send.
     * \param count Number of bytes to send.
     * \throws WriteFailure write() to socket fails.
     */
    void posixWriteBuffered(const char *buf, size_t count);

    /** Unbuffered Wrapper for write().
     *
     * This loops around write(), so no short writes can occur.
     * No internal buffer is used. If you used posixWriteBuffered() before,
     * you'll have to flush() the internal buffer. Otherwise your data will be
     * corrupted.
     *
     * If the socket works in nonblocking mode, it will be select()ed with a
     * timeout set by setWriteTimeout() until everything is written.
     *
     * \param buf Buffer to send.
     * \param count Number of bytes to send.
     * \throws WriteFailure write() to socket fails.
     */
    void posixWriteDirect(const char *buf, size_t count);


    /** Wrapper for read().
     *
     * \param buf Buffer to read into.
     * \param count Size of the buffer in bytes.
     * \return Number of bytes read, 0 for EOF or -EAGAIN or -EINTR.
     * \throws ReadFailure read() from socket fails.
     */
    int posixRead(char *buf, int count);

    /** Flush internal buffer to socket.
     *
     * If the socket works in nonblocking mode, it will be select()ed with a
     * timeout set by setWriteTimeout() until everything is written.
     *
     * \throws WriteFailure write() to socket fails.
     */
    void posixFlush();

    /** Timeout for posixWriteDirect() in non-blocking mode.
     *
     * \param ms Timeout.
     */
    void setWriteTimeout(std::chrono::milliseconds ms);

    /** Establish a new connection.
     *
     * The internal buffer will be cleared, too.
     *
     * \param host Host, can be a hostname or an IP(v6) address.
     * \param port Port
     * \throws ConnectionFailed Connection could not be established.
     */
    void reconnect(const char *host, unsigned short port);

  private:
    class Impl;
    std::shared_ptr<Impl> impl_;
};

}  // namespace PdCom

#endif  // PDCOM5_POSIXPROCESS_H
