/*****************************************************************************
 * vim:tw=78
 *
 * Copyright (C) 2021 Richard Hacker (lerichi at gmx dot net),
 *                    Florian Pose (fp at igh dot de),
 *                    Bjarne von Horn (vh at igh dot de).
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

/** @file */

#ifndef PDCOM5_VARIABLE_H
#define PDCOM5_VARIABLE_H

#include "DataDeserializer.h"
#include "Future.h"
#include "Selector.h"
#include "SizeTypeInfo.h"
#include "details.h"

#include <chrono>
#include <istream>
#include <memory>
#include <ostream>
#include <pdcom5_export.h>
#include <stdint.h>
#include <string>
#include <vector>

namespace PdCom {
namespace impl {
class Variable;
}
struct Exception;
class Process;
class VariablePollResult;

/** PdCom Variable interface.
 *
 * This class represents a variable (signal or parameter). It contains
 * information about the datatype and the shape of a variable.
 *
 * If you default-construct a Variable instance, it does not contain any
 * information. Calling any other member function than empty() is forbidden. To
 * get a valid variable instance, call Process::find().
 *
 * setValue() returns a Future, which takes up to two callbacks to notify about
 * the parameter update operation. If the server does not support these
 * notifications, the Future is empty and no callbacks can be passed to it.
 *
 */
class PDCOM5_PUBLIC Variable
{
    friend class impl::Variable;

    explicit Variable(std::weak_ptr<const impl::Variable> pimpl) :
        pimpl_(std::move(pimpl))
    {}

  public:
    using PollFuture =
            Future<PdCom::Exception const &,
                   VariablePollResult,
                   std::chrono::nanoseconds>;

    using SetValueFuture = Future<PdCom::Exception const &>;


    /** Constructs an empty variable.
     */
    Variable() = default;

    /** Write to a variable.
     *
     * This function is also asynchronous, so the server might still use the old
     * value when this function returns. This overload is for any container of
     * arithmetic types (e.g. std::vector<int>).
     *
     * \c T must be a contiguous container (must have a size() method and a
     * value_type typedef).
     *
     * \param data Data.
     * \param selector Optional selector.
     *
     * \return empty Future if server does not support variable update feedback.
     */
    template <typename T>
    typename std::enable_if<!std::is_arithmetic<T>::value, SetValueFuture>::type
    setValue(T const &data, const Selector &selector = {nullptr}) const
    {
        static_assert(
                std::is_same<
                        decltype(data[0]),
                        typename std::add_lvalue_reference<
                                const typename T::value_type>::type>::value,
                "Index operator does not return a lvalue reference of an "
                "integral");
        static_assert(
                details::is_contiguous<T>::value,
                "Container must be contiguous");
        return setValue(
                &data[0],
                details::TypeInfoTraits<typename T::value_type>::type_info.type,
                data.size(), selector);
    }
    /** Write to a variable.
     *
     * This function is also asynchronous, so the server might still use the old
     * value when this function returns.
     * This overload is for any arithmetic type (int, double, etc.).
     * The variable must not be empty!
     *
     * \param data Data.
     * \param selector Optional selector.
     *
     * \return empty Future if server does not support variable update feedback.
     */
    template <typename T>
    typename std::enable_if<std::is_arithmetic<T>::value, SetValueFuture>::type
    setValue(T const &data, const Selector &selector = {nullptr}) const
    {
        return setValue(
                &data, details::TypeInfoTraits<T>::type_info.type, 1, selector);
    }

    /** Write to a variable.
     *
     * This function is also asynchronous, so the server might still use the old
     * value when this function returns.
     * This overload is for any arithmetic type (int, double, etc.).
     * The variable must not be empty!
     *
     * \param data Data.
     * \param selector Optional selector.
     *
     * \return empty Future if server does not support variable update feedback.
     */
    template <typename T, size_t M, size_t N>
    SetValueFuture
    setValue(const T (&data)[M][N], const Selector &selector = {nullptr}) const
    {
        return setValue(
                data, details::TypeInfoTraits<T>::type_info.type, M * N,
                selector);
    }

    /** Write to a variable.
     *
     * This function is also asynchronous, so the server might still use the old
     * value when this function returns.
     * The variable must not be empty!
     *
     * \param src Pointer to one or more values.
     * \param src_type Type of the source.
     * \param count Number of values.
     * \param selector Optional selector.
     *
     * \return empty Future if server does not support variable update feedback.
     */
    SetValueFuture setValue(
            const void *src,
            TypeInfo::DataType src_type,
            size_t count,
            const Selector &selector = {nullptr}) const;

    /** Write to a variable.
     *
     * This function is also asynchronous, so the server might still use the old
     * value when this function returns.
     * The variable must not be empty!
     *
     * \param src Pointer to one or more values.
     * \param src_type Type of the source.
     * \param count Number of values.
     * \param offset Optional offset in the destination.
     *
     * \return empty Future if server does not support variable update feedback.
     */
    PdCom::Variable::SetValueFuture setValue(
            const void *src,
            TypeInfo::DataType src_type,
            size_t count,
            size_t offset) const;

    /** Get details about the variable type.
     * The variable must not be empty!
     * @return Variable type info.
     */
    TypeInfo getTypeInfo() const;
    /** Get details about the variable shape.
     * The variable must not be empty!
     * @return Variable shape info.
     */
    SizeInfo getSizeInfo() const;
    /** The Path of the variable.
     * The variable must not be empty!
     * @return The full path.
     */
    std::string getPath() const;
    /** The name of the variable.
     * The variable must not be empty!
     * @return the name, without the path components.
     */
    std::string getName() const;
    /** The alias of the variable, if set.
     * The variable must not be empty!
     * @return the alias.
     */
    std::string getAlias() const;
    /** The task id of the variable.
     * The variable must not be empty!
     * @return the task id.
     */
    int getTaskId() const;

    /** @return whether the variable is writeable.
     */
    bool isWriteable() const;
    /** @return sample time, 0 for parameters.
     */
    std::chrono::duration<double> getSampleTime() const;


    /** Checks whether this instance is empty.
     *
     * Default-constructed variables are empty per default,
     * calling any other member function than empty() will crash.
     * \return false if this instance is alive.
     */
    bool empty() const noexcept { return (pimpl_.expired()); }

    /** Read a variable without subscription.
     *
     * This method returns a Future. To get the actual result,
     * set a callback function using Future::then().
     * You can use Lambdas to capture local variables in the callback.
     * The callback gets two arguments: a VariablePollResult instance and a
     * timestamp. Make sure to keep the Future until it is resolved, otherwise
     * the poll request will be dropped. So this is not fire-and-forget.
     *
     * \code
     * auto handle = ...;
     * const auto future = var.poll().then([&handle](VariablePollResult res,
     *          std::chrono::nanoseconds ts)
     * {
     *      double val;
     *      res.getValue(val);
     *      handle.processValue(val);
     * });
     * \endcode
     *
     */
    PollFuture poll() const;

    /** Get the assigned Process.
     * \return The Process.
     */
    Process *getProcess() const;

  private:
    std::weak_ptr<const impl::Variable> pimpl_;
};

/** Result of Variable::poll()
 *
 * This is a container for a Variable value.
 * The data can be extracted with one of the many getValue() overloads.
 *
 */
class PDCOM5_PUBLIC VariablePollResult :
    public DataDeserializer<VariablePollResult>
{
    std::vector<char> data_;
    Variable variable_;

  public:
    explicit VariablePollResult(Variable var) :
        data_(var.getSizeInfo().totalElements()
              * var.getTypeInfo().element_size),
        variable_(var)
    {}

    const void *getData() const noexcept { return data_.data(); }
    void *getData() noexcept { return data_.data(); }
    Variable getVariable() const noexcept { return variable_; }
};

}  // namespace PdCom

#endif  // PDCOM5_VARIABLE_H
