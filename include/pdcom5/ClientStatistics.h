/*****************************************************************************
 *
 * Copyright (C) 2022 Bjarne von Horn (vh at igh dot de).
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

/** @file */

#ifndef PDCOM5_CLIENTSTATISTICS_H
#define PDCOM5_CLIENTSTATISTICS_H

#include <chrono>
#include <cstddef>
#include <string>

namespace PdCom {

/** Statistics about a connected client.
 *
 * Can be fetched using Process::getClientStatistics().
 *
 */
struct ClientStatistics
{
    std::string name_; /**< Text as specified using the name attribute in
                         `<remote_host/>`. */
    std::string application_name_;            /**< Text as specified using the
                                                applicationname attribute
                                                in `<remote_host/>`.*/
    std::size_t received_bytes_;              /**< Received bytes. */
    std::size_t sent_bytes_;                  /**< Transmitted bytes. */
    std::chrono::nanoseconds connected_time_; /**< Time when client
                                                connected. */
};

}  // namespace PdCom


#endif  // PDCOM5_CLIENTSTATISTICS_H
