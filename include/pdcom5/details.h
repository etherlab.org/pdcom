/*****************************************************************************
 * vim:tw=78
 *
 * Copyright (C) 2021 Richard Hacker (lerichi at gmx dot net),
 *                    Florian Pose (fp at igh dot de),
 *                    Bjarne von Horn (vh at igh dot de).
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#ifndef PDCOM5_DETAILS_H
#define PDCOM5_DETAILS_H

/** @file */

#include "SizeTypeInfo.h"

#include <array>
#include <cstddef>
#include <cstdint>
#include <pdcom5_export.h>
#include <string>
#include <type_traits>
#include <vector>

namespace PdCom { namespace details {

template <typename T>
struct TypeInfoTraits
{};
template <>
struct TypeInfoTraits<bool>
{
    static constexpr TypeInfo type_info = {
            TypeInfo::boolean_T, "bool", sizeof(bool)};
};
template <>
struct TypeInfoTraits<char>
{
    static constexpr TypeInfo type_info = {
            TypeInfo::char_T, "char", sizeof(char)};
};
template <>
struct TypeInfoTraits<uint8_t>
{
    static constexpr TypeInfo type_info = {
            TypeInfo::uint8_T, "uint8_t", sizeof(uint8_t)};
};
template <>
struct TypeInfoTraits<int8_t>
{
    static constexpr TypeInfo type_info = {
            TypeInfo::int8_T, "int8_t", sizeof(int8_t)};
};
template <>
struct TypeInfoTraits<uint16_t>
{
    static constexpr TypeInfo type_info = {
            TypeInfo::uint16_T, "uint16_t", sizeof(uint16_t)};
};
template <>
struct TypeInfoTraits<int16_t>
{
    static constexpr TypeInfo type_info = {
            TypeInfo::int16_T, "int16_t", sizeof(int16_t)};
};
template <>
struct TypeInfoTraits<uint32_t>
{
    static constexpr TypeInfo type_info = {
            TypeInfo::uint32_T, "uint32_t", sizeof(uint32_t)};
};
template <>
struct TypeInfoTraits<int32_t>
{
    static constexpr TypeInfo type_info = {
            TypeInfo::int32_T, "int32_t", sizeof(int32_t)};
};
template <>
struct TypeInfoTraits<uint64_t>
{
    static constexpr TypeInfo type_info = {
            TypeInfo::uint64_T, "uint64_t", sizeof(uint64_t)};
};
template <>
struct TypeInfoTraits<int64_t>
{
    static constexpr TypeInfo type_info = {
            TypeInfo::int64_T, "int64_t", sizeof(int64_t)};
};
template <>
struct TypeInfoTraits<double>
{
    static constexpr TypeInfo type_info = {
            TypeInfo::double_T, "double", sizeof(double)};
};
template <>
struct TypeInfoTraits<float>
{
    static constexpr TypeInfo type_info = {
            TypeInfo::single_T, "float", sizeof(float)};
};

template <TypeInfo::DataType dtype>
struct DataTypeTraits
{};
template <>
struct DataTypeTraits<TypeInfo::boolean_T>
{
    using value_type = bool;
};
template <>
struct DataTypeTraits<TypeInfo::char_T>
{
    using value_type = char;
};
template <>
struct DataTypeTraits<TypeInfo::uint8_T>
{
    using value_type = uint8_t;
};
template <>
struct DataTypeTraits<TypeInfo::int8_T>
{
    using value_type = int8_t;
};
template <>
struct DataTypeTraits<TypeInfo::uint16_T>
{
    using value_type = uint16_t;
};
template <>
struct DataTypeTraits<TypeInfo::int16_T>
{
    using value_type = int16_t;
};
template <>
struct DataTypeTraits<TypeInfo::uint32_T>
{
    using value_type = uint32_t;
};
template <>
struct DataTypeTraits<TypeInfo::int32_T>
{
    using value_type = int32_t;
};
template <>
struct DataTypeTraits<TypeInfo::uint64_T>
{
    using value_type = uint64_t;
};
template <>
struct DataTypeTraits<TypeInfo::int64_T>
{
    using value_type = int64_t;
};
template <>
struct DataTypeTraits<TypeInfo::double_T>
{
    using value_type = double;
};
template <>
struct DataTypeTraits<TypeInfo::single_T>
{
    using value_type = float;
};

template <class T, class /* sfinae */ = void>
struct is_contiguous : std::false_type
{};

template <class T>
struct is_contiguous<std::vector<T>> : std::true_type
{};

template <>
struct is_contiguous<std::vector<bool>> : std::false_type
{};

template <class T, size_t N>
struct is_contiguous<std::array<T, N>> : std::true_type
{};

template <class Char, class Alloc, class Traits>
struct is_contiguous<std::basic_string<Char, Traits, Alloc>> : std::true_type
{};


/** Data Conversion Matrix.
 *
 * This method converts an array of one numeric type to another.
 *
 * Essentially does
 * @code
 * for (i=0; i < nelem; ++i)
 *    dst[i] = src[i+offset]
 * @endcode
 *
 * @param dst Destination array, must not be null.
 * @param dst_type Type of the destination array.
 * @param src Source array, must not be null.
 * @param src_type Type of the source array.
 * @param nelem Number of elements to copy.
 * @param offset Optional offset of the first element to copy.
 */
void PDCOM5_PUBLIC copyData(
        void *dst,
        TypeInfo::DataType dst_type,
        const void *src,
        TypeInfo::DataType src_type,
        size_t nelem,
        size_t offset = 0);

}}  // namespace PdCom::details

#endif  // PDCOM5_DETAILS_H
