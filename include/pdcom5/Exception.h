/*****************************************************************************
 *
 * Copyright (C) 2021 Bjarne von Horn (vh at igh dot de).
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more .
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

/** @file */

#ifndef PDCOM5_EXCEPTION_H
#define PDCOM5_EXCEPTION_H

#include <pdcom5_export.h>
#include <stdexcept>
#include <string>

namespace PdCom {

struct PDCOM5_PUBLIC Exception : std::runtime_error
{
    using std::runtime_error::runtime_error;
};

struct PDCOM5_PUBLIC InternalError : Exception
{
    InternalError() : Exception("Internal error, please file a bug report") {}
    InternalError(const std::string &msg) :
        Exception("Internal error, please file a bug report: " + msg)
    {}
};

struct PDCOM5_PUBLIC InvalidArgument : Exception
{
    using Exception::Exception;
};

struct PDCOM5_PUBLIC ConnectionFailed : Exception
{
    ConnectionFailed() : Exception("Connection failed") {}
};

struct PDCOM5_PUBLIC EmptyVariable : Exception
{
    EmptyVariable() : Exception("Variable is empty") {}
};

struct PDCOM5_PUBLIC InvalidSubscription : Exception
{
    InvalidSubscription() : Exception("invalid subscription") {}
};

struct PDCOM5_PUBLIC LoginRequired : Exception
{
    LoginRequired() : Exception("Login is required") {}
};

struct PDCOM5_PUBLIC NotConnected : Exception
{
    NotConnected() : Exception("Not connected") {}
};

struct PDCOM5_PUBLIC ProcessGoneAway : Exception
{
    ProcessGoneAway() : Exception("Process has gone away") {}
};

struct PDCOM5_PUBLIC ProtocolError : Exception
{
    using Exception::Exception;
};

struct PDCOM5_PUBLIC ReadFailure : Exception
{
    int errno_;
    ReadFailure(int e) :
        Exception("Read failure, errno: " + std::to_string(e)), errno_(e)
    {}
};

struct PDCOM5_PUBLIC TlsError : Exception
{
    TlsError(std::string what, int err_code) :
        Exception(std::move(what)), err_code_(err_code)
    {}
    int err_code_;
};

struct PDCOM5_PUBLIC WriteFailure : Exception
{
    int errno_;
    using Exception::Exception;
    WriteFailure(int e) :
        Exception("Write failure, errno: " + std::to_string(e)), errno_(e)
    {}
};


}  // namespace PdCom


#endif  // PDCOM5_EXCEPTION_H
