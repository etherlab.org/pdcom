/*****************************************************************************
 *
 * Copyright (C) 2021 Bjarne von Horn (vh at igh dot de).
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#ifndef PDCOM5_SASL_H
#define PDCOM5_SASL_H

#include <pdcom5_export.h>

namespace PdCom {
namespace impl {
class Process;
}

/** SASL Interface for PdCom.
 *
 * With the help of this interface, a SASL library can communicate with the
 * server and perfom authentification.
 *
 * The authentification class needs to be registered in the process with
 * Process::setAuthManager().
 *
 */
class PDCOM5_PUBLIC Sasl
{
  public:
    Sasl();
    Sasl(Sasl const &) = delete;
    Sasl(Sasl &&) noexcept;
    Sasl &operator=(Sasl &&) noexcept;
    Sasl &operator=(Sasl const &) = delete;

  protected:
    /** Perform SASL login step.
     *
     * @param mech SASL mechanism
     * @param clientData Base64 encoded SASL output data to server
     *
     * Setting both \p mech and \p clientData to NULL will initate the
     * login process.
     *
     * Every call to login() is answered by a loginReply(), unless
     * login is not supported. When login is mandatory, loginReply()
     * will be called automatically.
     *
     * @return false if login is not supported
     */
    bool loginStep(const char *mech, const char *clientData);
    /** Logout from server
     */
    void logout();
    ~Sasl();


  private:
    friend impl::Process;
    std::weak_ptr<impl::Process> process_;

    /** SASL server reply to login()
     *
     * loginReply() may be called without calling login() in the case
     * where login is mandatory. In this case, it is called before
     * Process::connected().
     *
     * @param mechlist Space separated list of supported mechanisms
     * @param serverData Base64 encoded SASL data from server
     * @param finished
     * \parblock
     *   &gt; 0: Login successful\n
     *   0:      Login process not finished; another login() step is required\n
     *   &lt; 0: Login failed
     * \endparblock
     */
    virtual void
    loginReply(const char *mechlist, const char *serverData, int finished) = 0;
};
}  // namespace PdCom

#endif  // PDCOM5_SASL_H
