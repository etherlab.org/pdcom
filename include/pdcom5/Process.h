/*****************************************************************************
 * vim:tw=78
 *
 * Copyright (C) 2021 Richard Hacker (lerichi at gmx dot net),
 *                    Florian Pose (fp at igh dot de),
 *                    Bjarne von Horn (vh at igh dot de).
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

/** @file */

#ifndef PDCOM5_PROCESS_H
#define PDCOM5_PROCESS_H

#include "ClientStatistics.h"
#include "Variable.h"

#include <chrono>
#include <memory>
#include <pdcom5_export.h>
#include <stddef.h>
#include <stdint.h>
#include <string>
#include <vector>

namespace PdCom {
namespace impl {
class Process;
}

class SecureProcess;
class Variable;
class Sasl;
class Subscription;
class Subscriber;
class MessageManagerBase;

/** Base class for PdCom protocol handler
 *
 * This is the base class to interact with real time process server.  The
 * PdCom protocol ist implemented using this class.
 *
 * For socket input and output, the library completely relies on a derived
 * class where read(), write(), flush() and connected() methods are
 * reimplemented.
 *
 * When data is available for reading, call asyncData() which in turn
 * calls the reimplemented read() method.
 *
 * When the protocol is initialized, the reimplemented connected() method
 * is called.
 *
 * After connected(), the following commands can be issued:
 *
 *  * list(): list a directory; returns result in listReply()
 *  * find(): find a variable; returns result in findReply()
 *  * ping(): ping the server; returns result in pingReply()
 *
 * All these commands are non-blocking asynchronous calls and either
 * return the result immediately with the corresponding reply methods or
 * issue a command to the server using excessive (!) calls to write().
 * Data should be written to a buffer to optimize network communication.
 * To flush the buffer to wire, flush() is issued by the library when
 * required.
 *
 * The server may query presence of the user by issuing an alive() call.
 * Using this call, certain actions could be undertaken by the server if
 * the user is not active any more.
 *
 */
class PDCOM5_PUBLIC Process
{
    friend class impl::Process;
    friend class SecureProcess;

  public:
    /** Constructor */
    Process();
    Process(Process &&)      = delete;
    Process(Process const &) = delete;
    Process &operator=(Process &&) = delete;
    Process &operator=(Process const &) = delete;

  protected:
    /** Destructor
     *
     * The destructor cleans up all internally allocated structures
     */
    virtual ~Process();

  public:
    /** Remote process name string */
    std::string name() const;

    /** Remote process version string */
    std::string version() const;

    /** Library entry point for new data.
     *
     * Calling this method tells the library that new data has arrived
     * from the server and is waiting to be processed.
     *
     * The library prepares an input buffer and then calls the
     * reimplemented read() virtual method to read incoming data.
     *
     * This method can throw many exceptions, especially protocol_error and all
     * exceptions which are thrown in the callbacks.
     */
    void asyncData();

    /** Call delayed callbacks.
     *
     * This method is used to call queued callbacks,
     * for example Subscriber::stateChanged() after a Subscription has been
     * created. It is also called twice by asyncData(), so usually you don't
     * have to bother with it.
     */
    void callPendingCallbacks();

    /** Send a broadcast message to the server and other clients.
     *
     * @param message Broadcast message.
     * @param attr Xml tag name, can be text or action.
     */
    void
    broadcast(const std::string &message, const std::string &attr = "text");

    struct SubscriptionInfo
    {
        const PdCom::Subscription *subscription;
        const PdCom::Subscriber *subscriber;
        PdCom::Variable variable;

        SubscriptionInfo(
                const PdCom::Subscription *subscription,
                const PdCom::Subscriber *subscriber,
                PdCom::Variable variable) :
            subscription(subscription),
            subscriber(subscriber),
            variable(variable)
        {}
    };

    std::vector<SubscriptionInfo> getActiveSubscriptions() const;

  protected:
    /** Reset communications and clean up internal buffers */
    void reset();

    /** Name of application user application.
     *
     * The application name is transferred to the server to be able to
     * identify the clients more easily.
     *
     * \return a descriptive name of your application.
     */
    virtual std::string applicationName() const { return "pdcom5"; }

    /** Host name of remote server.
     *
     * Reimplement this method to return the remote server host name
     * this library connects to. This is especially important in
     * multi-hosted TLS environments, where multiple hosts resolv to
     * the same IP address. TLS needs to know the original server host
     * name.
     *
     * \return server host name
     */
    virtual std::string hostname() const { return {}; }

    /** Read data from server
     *
     * Reimplement this method to transfer data from the server to
     * the library. This method is called within the call to
     * asyncData().
     *
     * Essentially this method is a little wrapper around your
     * socket's `%read()` function:
     * \code
     *   int MyProcess::read(char *buf, size_t count)
     *   {
     *       return ::read(this->socket_fd, buf, count);
     *   }
     * \endcode
     *
     * The method must return the number of bytes read, which may of
     * coarse be less than \p count or even 0. Return values of
     * &lt;=&nbsp;0 are not interpreted by the protocol handler.
     *
     * @param buf data destination
     * @param count buffer size
     *
     * \return number of bytes read, 0 on connection close, -EAGAIN on when no
     * data is available or <0 on error.
     */
    virtual int read(char *buf, int count) = 0;

    /** Write data to server
     *
     * Reimplement this method to transfer data from the library to
     * the server. This method is called when any library
     * operation requires data to be sent to the server.
     *
     * Note: the library makes many calls to write(), so use
     * buffered output otherwise you're in for performance problems!
     *
     * Essentially this method is a little wrapper around your
     * socket's `%write()` function:
     * \code
     * void MyProcess::write(const char *buf, size_t count)
     * {
     *     if (count != ::fwrite(buf, 1, count, this->socket_file)) {
     *         // react to errors, set flags, etc
     *     }
     * }
     * \endcode
     *
     * Note: the library does not have an internal buffer and expects
     * that all data is sent. If your implementation might send less
     * than \p count, it is your responsibility to buffer the data and
     * send it later.
     *
     * @param buf data to be sent
     * @param count number of bytes to send
     */
    virtual void write(const char *buf, size_t count) = 0;

    /** Flush unsent data in output buffer
     *
     * Reimplement this method to flush data in the output buffer.
     *
     * This method tells the user that it is time to flush the
     * output buffer to the wire. The library only expects that data
     * is sent to the server within this call.
     *
     * Essentially this method is a little wrapper around your
     * socket's `fflush()` function:
     * \code
     * void MyProcess::flush()
     * {
     *     if (::fflush(this->socket_file)) {
     *         // react to errors
     *     }
     * }
     * \endcode
     */
    virtual void flush() = 0;

    /** Protocol initialization completed
     *
     * This is a signal emitted by the library to indicate that
     * protocol initialization has been completed and that library
     * operations can be performed thereafter.
     *
     * Reimplement this method to get the signal.
     *
     * Absolutely NO process operations other than asyncData(),
     * and Sasl::login() (and then only due to a previous
     * loginReply() are permitted before this signal has been
     * emitted.
     */
    virtual void connected() = 0;

    /** List a directory path
     *
     * A process command to return all variables and directories
     * within a directory path. The \p path parameter has typical
     * unix character, with forward slashes '/' separating
     * directories.
     *
     * listReply() must be reimplemented to receive the reply
     * to this call.
     *
     * If the directory is cached (for instance a previous call to
     * a similar path, or an entire server listing has been performed),
     * listReply() is called within the context of this call and no
     * server query is performed.
     *
     * If uncached, the library sends a server query and returns
     * immediately. Later on during asyncData(), the virtual method
     * listReply(), is called when the server's reply is
     * processed.
     *
     * As a special case, an empty string (std::string()) for \p path
     * will let the server list all its variables in one go. This
     * possibility must be used with caution, as it can cause heavy
     * network traffic.
     *
     * @param path directory path
     *
     * \return
     *     true if the path was cached
     */
    bool list(const std::string &path = "");

    /** Reply to list() call
     *
     * You must reimplement this method to receive replies to list()
     * calls.
     *
     * Note that a variable can have the same path as a directory!
     * An example is a vector variable with atomized elements.
     *
     * Replies are in strict order of list() calls.
     *
     * @param variables List of variables.
     * @param dirs List of directories.
     */
    virtual void
    listReply(std::vector<Variable> variables, std::vector<std::string> dirs);

    /** Find a variable with a corresponding path
     *
     * If the path search is known (be it successful or unsuccessful),
     * the variable is returned in the call to the reimplemented
     * virtual findReply() method immediately and the method returns
     * true;
     *
     * If unsuccessful, the command is sent to the server to and the
     * call returns immediately with false. Later on during
     * asyncData(), findReply() is called when the server's reply is
     * processed.
     *
     * @param path path of variable to find
     *
     * \return true if path is found immediately (cached)
     */
    bool find(const std::string &path);

    /** Reply to find()
     *
     * This virtual method is called within the context of asyncData()
     * when the server's reply to a variable discovery is processed.
     *
     * findReply()ies are called in strict order of find()
     *
     * @param variable Variable, empty if variable was not found.
     */
    virtual void findReply(const Variable &variable);

    /** @brief Request client statistics from the server.
     *
     * Override clientStatisticsReply() to process the reply.
     */
    void getClientStatistics();

    /// @brief Reply for getClientStatistics().
    /// @param statistics The stats for all connected clients.
    virtual void
    clientStatisticsReply(std::vector<ClientStatistics> statistics);

    /** Ping server */
    void ping();

    /** Ping reply
     *
     * You must reimplement this method to receive the reply to a
     * ping() call.
     */
    virtual void pingReply() {}

    /** Test from process whether client is alive.
     *
     * In some cases the server may want to know whether the client
     * is still alive. Default implementation is to return true.
     * Reimplement this if you wish to control presence
     *
     * \return true to indicate user presence
     */
    virtual bool alive() { return true; }

    /** Register a SASL handler.
     *
     * A previous registered handler will be unregistered.
     * Note that the registered handler has to outlive the process.
     * Passing a \c nullptr will unregister the handler.
     */
    void setAuthManager(Sasl *);

    /** Get the current SASL handler.
     *
     * \return The current SASL handler, may be NULL.
     */
    Sasl *getAuthManager() const;

    /** Register a Message handler.
     *
     * A previous registered handler will be unregistered.
     * Note that the registered handler has to outlive the process.
     * Passing a \c nullptr will unregister the handler.
     */
    void setMessageManager(MessageManagerBase *);

    /** Recieve a broadcast message from other clients.
     *
     * \p time_ns and \p user will be filled in by the server.
     * If the sending client is not authenticated, \p user will be \c anonymous.
     *
     * @param message Message
     * @param attr Attribute name, can be action or text.
     * @param time_ns Time since epoch, will be filled in by the server.
     * @param user Client which sent the broadcast, will be filled in by the
     * server.
     */
    virtual void broadcastReply(
            const std::string &message,
            const std::string &attr,
            std::chrono::nanoseconds time_ns,
            const std::string &user);

  private:
    std::shared_ptr<impl::Process> pimpl;
    explicit Process(std::shared_ptr<impl::Process> impl);
};
}  // namespace PdCom

#endif  // PDCOM5_PROCESS_H
