/*****************************************************************************
 * vim:tw=78
 *
 * Copyright (C) 2021 Richard Hacker (lerichi at gmx dot net),
 *                    Florian Pose (fp at igh dot de),
 *                    Bjarne von Horn (vh at igh dot de).
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

/** @file */

#ifndef PDCOM5_SUBSCRIPTION_H
#define PDCOM5_SUBSCRIPTION_H

#include "DataDeserializer.h"
#include "Exception.h"
#include "Selector.h"
#include "Variable.h"
#include "details.h"

#include <memory>
#include <pdcom5_export.h>
#include <string>
#include <vector>

namespace PdCom {

namespace impl {
class Subscription;
}  // namespace impl

class Process;
class Variable;
class Subscriber;

/** PdCom Subscription interface.
 *
 * This class represents a subscription to a variable. It is updated either
 * on a periodic base, on an external event (e.g. the value has changed) or as
 * requested with poll(). The type of transmission can be selected by
 * creating an appropriate Subscriber object.
 *
 * To cancel a subscription, you have to delete the corresponding subscription
 * instance.
 *
 * Calling getState() is always allowed, the other member functions need the
 * subscription to be Active.
 */
class PDCOM5_PUBLIC Subscription : public DataDeserializer<Subscription>
{
  public:
    enum class State {
        Invalid = 0,
        Pending,
        Active,
    };

    /// Default constructor for an empty subscription.
    Subscription() = default;
    Subscription(Subscription &&) noexcept;
    Subscription &operator=(Subscription &&) noexcept;

    /** Constructor for a known variable.
     * @param subscriber Subscriber which will recieve the status updates.
     * @param variable The variable to subscribe.
     * @param selector Select parts of multi-dimensional variables.
     * @throw invalid_subscription Subscription is already known to be
     *        invalid.
     */
    Subscription(
            Subscriber &subscriber,
            const Variable &variable,
            const Selector &selector = {});

    /** Constructor for an unknown variable.
     * @param subscriber Subscriber which will recieve the status updates.
     * @param process The process.
     * @param path Path of the variable to subscribe.
     * @param selector Select parts of multi-dimensional variables.
     * @throw invalid_subscription Subscription is already known to be
     *        invalid.
     */
    Subscription(
            Subscriber &subscriber,
            Process &process,
            const std::string &path,
            const Selector &selector = {});

    /** Poll values from the server.
     *
     * The subscription has to be \c Active.
     */
    void poll();

    /** Get the data Pointer.
     *
     * The subscription has to be \c Active.
     *
     * \return Pointer to the internal storage.
     */
    const void *getData() const;

    /** Access the subscribed variable.
     *
     * The subscription must not be empty().
     * \return The subscribed variable.
     */
    Variable getVariable() const;

    /** Print the value(s).
     *
     * \param os Stream to print into.
     * \param delimiter Delimiter which separates the values.
     */
    void print(std::ostream &os, char delimiter) const;

    /** Check whether the subscription is default-constructed.
     * \return false The subscription is populated.
     */
    bool empty() const noexcept { return !(pimpl); }

    /** Get the current state.
     * \return The state.
     */
    State getState() const noexcept { return state_; }

    /** Get the assigned Process.
     * \return The Process.
     */
    Process *getProcess() const;

    /** Get the path of the subscribed variable.
     *
     * This also works in Pending and Invalid state.
     * \return Path of the subscribed variable.
     */
    std::string getPath() const;

  private:
    friend impl::Subscription;

    std::shared_ptr<impl::Subscription> pimpl = {};

    State state_ = State::Invalid;
};

}  // namespace PdCom

#endif  // PDCOM5_SUBSCRIPTION_H
