/*****************************************************************************
 * vim:tw=78
 *
 * Copyright (C) 2022 Bjarne von Horn (vh at igh dot de).
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#ifndef PDCOM5_FUTURE_H
#define PDCOM5_FUTURE_H

#include <functional>
#include <memory>
#include <pdcom5_export.h>

namespace PdCom {
namespace impl {
template <class Exception, class... Result>
struct Promise;
}

/** Callback management handle.
 *
 * This class can save up to two callbacks for an asynchronous operation.
 * When the result of the asynchronous operation arrives,
 * the callback set by then() is called.
 * On error, handle_exception() is invoked.
 *
 * Please note that this Future does not support fire-and-forget.
 * The Future should be kept until one of the callback was called.
 * If you're not interested in the result anymore, you can delete the future.
 * In this case, no callbacks are run.
 */
template <class Exception, class... Result>
class PDCOM5_PUBLIC Future
{
  public:
    using ResolveFn = std::function<void(Result...)>;
    using RejectFn  = std::function<void(Exception)>;

    /// Default Constructor.
    Future() = default;
    Future(std::shared_ptr<impl::Promise<Exception, Result...>> impl) noexcept :
        impl_(std::move(impl))
    {}
    Future(Future &&)      = default;
    Future(const Future &) = delete;
    Future &operator=(Future &&) = default;
    Future &operator=(const Future &) = delete;

    /** Set continuation callback.
     *
     * \param resolve The callback to be invoked with the result.
     * \return const Reference to \c this.
     */
    const Future &then(ResolveFn resolve) const &;
    /** Set continuation callback.
     *
     * \param resolve The callback to be invoked with the result.
     * \return a new Future.
     */
    Future then(ResolveFn resolve) &&;
    /** Set error handling callback
     *
     * \param reject The callback to be invoked on error.
     * \return const Reference to \c this.
     */
    const Future &handle_exception(RejectFn reject) const &;
    /** Set error handling callback
     *
     * \param reject The callback to be invoked on error.
     * \return a new Future.
     */
    Future handle_exception(RejectFn reject) &&;

    /**
     * \return true if the Future was default-constructed.
     */
    bool empty() const noexcept { return !impl_.operator bool(); }

    /// Hash support, e.g. for \c std::unordered_set
    struct Hash
    {
        std::size_t operator()(Future const &future) const
        {
            return std::hash<
                    std::shared_ptr<impl::Promise<Exception, Result...>>>()(
                    future.impl_);
        }
    };

    /// Less compare support, e.g. for \c std::set
    struct Less
    {
        bool operator()(const Future &lhs, const Future &rhs) const noexcept
        {
            return std::owner_less<
                    std::shared_ptr<impl::Promise<Exception, Result...>>>()(
                    lhs.impl_, rhs.impl_);
        }
    };

    /// Equal comparsion.
    bool operator==(const Future &other) const noexcept
    {
        return other.impl_ == impl_;
    }

  private:
    std::shared_ptr<impl::Promise<Exception, Result...>> impl_;
};
}  // namespace PdCom

namespace std {
template <class Exception, class... Result>
struct hash<PdCom::Future<Exception, Result...>> :
    PdCom::Future<Exception, Result...>::Hash
{};

template <class Exception, class... Result>
struct less<PdCom::Future<Exception, Result...>> :
    PdCom::Future<Exception, Result...>::Less
{};

}  // namespace std


#endif  // PDCOM5_FUTURE_H
