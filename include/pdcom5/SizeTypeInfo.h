/*****************************************************************************
 *
 * Copyright (C) 2021 Richard Hacker (lerichi at gmx dot net),
 *                    Florian Pose (fp at igh dot de),
 *                    Bjarne von Horn (vh at igh dot de).
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#ifndef PDCOM5_SIZETYPEINFO_H
#define PDCOM5_SIZETYPEINFO_H

#include <cstddef>
#include <vector>

namespace PdCom {

/** Type of a Variable */
struct TypeInfo
{
    enum DataType {
        DataTypeBegin = 0,
        boolean_T     = DataTypeBegin,
        uint8_T,
        int8_T,
        uint16_T,
        int16_T,
        uint32_T,
        int32_T,
        uint64_T,
        int64_T,
        double_T,
        single_T,
        char_T,
        DataTypeEnd  // keep this as the very last one!
    };

    DataType type;
    /** name of the correspondig c type (double, char, ...) */
    const char *ctype;
    /** Size of one element in bytes */
    size_t element_size;
    constexpr TypeInfo(
            DataType type,
            const char *ctype,
            size_t element_size) noexcept :
        type(type), ctype(ctype), element_size(element_size)
    {}
};

/** Size of a Variable */
class SizeInfo : public std::vector<unsigned int>
{
  public:
    using std::vector<unsigned int>::vector;

    bool isScalar() const { return size() == 1 && (*this)[0] == 1; }
    bool isVector() const { return size() == 1 && (*this)[0] > 1; }
    bool is2DMatrix() const { return size() == 2; }
    std::size_t dimension() const noexcept
    {
        return size() == 1 && (*this)[0] == 1 ? 0 : size();
    }

    std::size_t totalElements() const
    {
        if (empty())
            return 0;
        std::size_t ans = 1;
        for (auto i : *this)
            ans *= i;
        return ans;
    }
    static SizeInfo Scalar() { return {1U}; }
    static SizeInfo RowVector(unsigned int rows) { return {rows}; }
    static SizeInfo Matrix(unsigned int rows, unsigned int cols)
    {
        return {{rows, cols}};
    }
    static SizeInfo
    Matrix(unsigned int layers, unsigned int rows, unsigned int cols)
    {
        return {{layers, rows, cols}};
    }

    unsigned int layers() const
    {
        if (empty())
            return 0;
        return size() >= 3 ? operator[](size() - 3) : 1;
    }

    unsigned int rows() const
    {
        if (empty())
            return 0;
        return size() >= 2 ? operator[](size() - 2) : 1;
    }

    unsigned int columns() const { return empty() ? 0 : back(); }
};
}  // namespace PdCom


#endif  // PDCOM5_SIZETYPEINFO_H
