/*****************************************************************************
 * vim:tw=78
 *
 * Copyright (C) 2022 Richard Hacker (lerichi at gmx dot net),
 *                    Florian Pose (fp at igh dot de),
 *                    Bjarne von Horn (vh at igh dot de).
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

/** @file */

#ifndef PDCOM5_DATADESERIALIZER_H
#define PDCOM5_DATADESERIALIZER_H

#include "Exception.h"
#include "details.h"

#include <cstdint>
#include <string>
#include <type_traits>
#include <vector>

namespace PdCom {
/** Data Deserialisation helper.
 *
 * This class is used to extract the values from the buffer
 * as requested by the library user.
 * It is expected that Derived has at least a getData() member
 * which returns a pointer to the buffer and a getVariable()
 * member which returns the corresponding variable.
 *
 */

template <class Derived>
struct DataDeserializer
{
    /** Copy the values into a custom buffer.
     *
     * Up to \p buf.size() values are converted into the type of the buffer.
     * This overload accepts a container like \c std::vector , but also
     * non-contiguous containers like \c std::list are supported.
     * A member typedef \c value_type and a size() member are expected.
     *
     * \param dest Custom buffer.
     */
    template <typename T>
    typename std::enable_if<!std::is_arithmetic<T>::value, void>::type
    getValue(T &dest, size_t offset = 0) const;

    /** Copy the values into a custom buffer.
     *
     * \param dest Custom buffer.
     */
    template <typename T>
    typename std::enable_if<std::is_arithmetic<T>::value, void>::type
    getValue(T &dest, size_t offset = 0) const
    {
        const auto var = static_cast<const Derived &>(*this).getVariable();
        const auto total_elements = var.getSizeInfo().totalElements();
        if (offset >= total_elements)
            throw InvalidArgument(
                    "offset too large, must be less than "
                    + std::to_string(total_elements));
        details::copyData(
                &dest, details::TypeInfoTraits<T>::type_info.type,
                static_cast<const Derived &>(*this).getData(),
                var.getTypeInfo().type, 1, offset);
    }

    template <typename T, size_t M, size_t N>
    void getValue(T (&dest)[M][N]) const
    {
        const auto var = static_cast<const Derived &>(*this).getVariable();
        if (var.getSizeInfo().totalElements() != M * N)
            throw InvalidArgument(
                    "Size mismatch between destination and source");
        details::copyData(
                dest, details::TypeInfoTraits<T>::type_info.type,
                static_cast<const Derived &>(*this).getData(),
                var.getTypeInfo().type, M * N);
    }
    template <typename T, size_t N>
    void getValue(T (&dest)[N], size_t offset = 0) const
    {
        const auto var = static_cast<const Derived &>(*this).getVariable();
        const auto total_elements = var.getSizeInfo().totalElements();
        if (offset >= total_elements)
            throw InvalidArgument(
                    "offset too large, must be less than "
                    + std::to_string(total_elements));
        details::copyData(
                dest, details::TypeInfoTraits<T>::type_info.type,
                static_cast<const Derived &>(*this).getData(),
                var.getTypeInfo().type,
                std::min<size_t>(N, total_elements - offset), offset);
    }
    template <typename T>
    T getValue() const
    {
        T ans;
        this->getValue(ans);
        return ans;
    }
};

template <class Derived>
template <typename T>
inline typename std::enable_if<!std::is_arithmetic<T>::value, void>::type
DataDeserializer<Derived>::getValue(T &dest, size_t offset) const
{
    const auto var = static_cast<const Derived &>(*this).getVariable();
    const auto total_elements = var.getSizeInfo().totalElements();
    static_assert(
            std::is_same<
                    decltype(dest[0]),
                    typename std::add_lvalue_reference<
                            typename T::value_type>::type>::value,
            "Index operator does not return a lvalue reference of an "
            "integral");
    if (offset >= total_elements)
        throw InvalidArgument("Offset too large");
    const auto count = std::min<size_t>(dest.size(), total_elements - offset);
    const auto src_type = var.getTypeInfo();
    const auto dst_type =
            details::TypeInfoTraits<typename T::value_type>::type_info.type;

    const auto src = static_cast<const Derived &>(*this).getData();
    if (!details::is_contiguous<T>::value) {
        for (unsigned int i = 0; i < count; ++i) {
            details::copyData(
                    &dest[i], dst_type, src, src_type.type, 1, offset + i);
        }
    }
    else {
        details::copyData(
                &dest[0], dst_type, src, src_type.type, count, offset);
    }
}

}  // namespace PdCom


#endif  // PDCOM5_DATADESERIALIZER_H
