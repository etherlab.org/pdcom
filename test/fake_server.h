/*****************************************************************************
 * vim:tw=78
 *
 * Copyright (C) 2021 Bjarne von Horn (vh at igh dot de).
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#pragma once

#include "../src/msrproto/expat_wrapper.h"
#include "../src/msrproto/expat_wrapper_impl.h"
#include "fake_variable.h"

#include <algorithm>
#include <gtest/gtest.h>
#include <list>
#include <numeric>
#include <optional>
#include <ostream>
#include <pdcom5/MessageManagerBase.h>
#include <pdcom5/Process.h>
#include <queue>
#include <sstream>
#include <string>
#include <unordered_map>


using PdCom::impl::MsrProto::ExpatWrapper;
class XmlAttr;


namespace Request {
struct RemoteHost
{
    std::string id;
};

struct ReadChannel
{
    std::string id, path;
};

struct ReadVariable
{
    std::string id;
    enum AddressMode {
        None,
        NameSet,
        IndexSet,
    } address_mode;
    std::string name;
    unsigned int index;
    bool short_output, hex, is_parameter;
};

struct ParameterSubscription
{
    std::string id;
    bool hex /*,  monitor */;
    std::vector<int> parameters;
};

struct ParameterUnsubscribe
{
    std::string id;
    /* bool monitor; */
    std::vector<int> parameters;
};

struct SignalSubscription
{
    std::string id, coding;
    bool event, sync;
    std::vector<int> channels;
    unsigned int blocksize;
    std::optional<unsigned int> reduction, precision;
    unsigned int group;
};

struct SignalUnsubscribe
{
    std::string id;
    std::vector<int> channels;
    unsigned int group;
};
}  // namespace Request

class FakeProcess :
    public PdCom::Process,
    protected ExpatWrapper<FakeProcess, false>
{
  protected:
    int read(char *buf, int count) override;
    void write(const char *buf, size_t count) override
    {
        if (throw_at_write_)
            throw PdCom::WriteFailure(ECONNRESET);
        if (throw_at_write_gt_)
            std::for_each(buf, buf + count, [](char c) {
                if (c == '>')
                    throw PdCom::WriteFailure(ECONNRESET);
            });
        if (!parse(buf, count))
            throw std::runtime_error("parse() failed");
        written_bytes_ += count;
    }
    void flush() override {}
    void connected() override;

    // callbacks for xml parser
    friend class ExpatWrapper<FakeProcess, false>;
    void startElement(const char *name, const char **atts);
    void endElement(const char *name);
    void xmlError(const char *) {}

  public:
    enum class ParserState {
        Startup,
        WaitForRemoteHost,
        Idle,
    };
    using ContainerPtr = std::unique_ptr<FakeVariableContainerInterface>;


    FakeProcess(ContainerPtr _signals, ContainerPtr _parameters) :
        PdCom::Process(),
        ExpatWrapper(),
        signals(std::move(_signals)),
        parameters(std::move(_parameters))
    {}

    ::testing::AssertionResult
    startup(bool enable_xsap = true, bool enable_group = true);

    ::testing::AssertionResult answer_rk_rp_requests(
            int expected_rk_found,
            int expected_rk_notfound,
            int expected_rp_found,
            int expected_rp_notfound,
            int break_after_count = -1);

    ::testing::AssertionResult answer_signal_subscription_requests(
            int expected_event,
            int expected_periodic);


    auto getReadParamRequests() const
    {
        return std::count_if(
                read_variable_requests.begin(), read_variable_requests.end(),
                [](const Request::ReadVariable &req) {
                    return req.is_parameter;
                });
    }

    auto getReadChannelRequests() const
    {
        return std::count_if(
                read_variable_requests.begin(), read_variable_requests.end(),
                [](const Request::ReadVariable &req) {
                    return !req.is_parameter;
                });
    }

    auto getSignalUnsubscribeRequests() const
    {
        return signal_unsubscribe_requests.size();
    }


    ::testing::AssertionResult
    answer_signal_unsubscribe_requests(int expected_channel_count);
    ::testing::AssertionResult
    answer_parameter_subscription_requests(int expected_event);
    ::testing::AssertionResult
    answer_parameter_unsubscribe_requests(int expected_parameter_count);

    /// sends an invalid xml tag
    void send_garbage()
    {
        read_buffer << "<do\xf0\x90\x80\x80/>";
        flush_read_buffer();
    }

    void send_eof()
    {
        flush_read_buffer();
        asyncData();
    }

    struct MessageDetails
    {
        std::string text;
        PdCom::LogLevel level;
    };

    // if text == nullopt, then <reset>
    void send_message(
            const std::string &name,
            int index,
            unsigned int seq,
            std::optional<MessageDetails> const &details,
            std::optional<std::string> const &id = std::nullopt);

    void flush_read_buffer();

    class SendDataManager
    {
        FakeProcess &p_;
        const char *child_tag_name_;
        const unsigned int group_;
        std::unordered_map<int, std::vector<char>> variable_cache_;
        const bool send_close_tag_;

      public:
        SendDataManager(
                FakeProcess &p,
                bool event,
                double time,
                unsigned int group,
                bool send_close_tag = true) :
            p_(p),
            child_tag_name_(event ? "E" : "F"),
            group_(group),
            send_close_tag_(send_close_tag)
        {
            p_.read_buffer << "<data time=\"" << time << "\" group=\"" << group
                           << "\">";
        }
        void addData(const std::vector<int> &signals = {});
        void addTime(std::vector<uint64_t> const &timestamps = {});
        ~SendDataManager() noexcept(false);
        unsigned int blocksize() const;
    };

    using GroupSubscription =
            std::unordered_map<int /* channel */, Request::SignalSubscription>;

    std::vector<unsigned int> subscribed_group_ids;
    std::ostringstream read_buffer;

    void clear_server_side()
    {
        subscribed_group_ids.clear();
        group_id_count.clear();
        read_buffer.clear();


        remote_host_requests            = {};
        read_variable_requests          = {};
        signal_subscription_requests    = {};
        signal_unsubscribe_requests     = {};
        parameter_subscription_requests = {};
        parameter_unsubscribe_requests  = {};
        signal_subscriptions            = {};
        parameter_subscriptions         = {};
        ExpatWrapper<FakeProcess, false>::reset();
    }

    size_t written_bytes_   = 0;
    bool throw_at_write_    = false;
    bool throw_at_write_gt_ = false;

    std::vector<PdCom::Message> active_messages_;

    bool has_no_group_subscriptions() const
    {
        for (const auto &kv : group_id_count)
            if (kv.second)
                return false;
        return true;
    }

    unsigned getBlocksize(unsigned group, int channel) const
    {
        return signal_subscriptions.at(group).at(channel).blocksize;
    }

  protected:
    // does not flush, does nothing if id is empty
    void send_ack(const std::string &id);

    ParserState state = ParserState::Startup;
    std::queue<Request::RemoteHost> remote_host_requests;
    std::list<Request::ReadVariable> read_variable_requests;
    std::queue<Request::SignalSubscription> signal_subscription_requests;
    std::queue<Request::SignalUnsubscribe> signal_unsubscribe_requests;
    std::queue<Request::ParameterSubscription> parameter_subscription_requests;
    std::queue<Request::ParameterUnsubscribe> parameter_unsubscribe_requests;
    std::unordered_map<unsigned int /* group */, GroupSubscription>
            signal_subscriptions;

    std::unordered_map<int /* param */, bool /* hex */> parameter_subscriptions;
    bool groups_enabled_ = true;

    ContainerPtr signals, parameters;

  private:
    void handle_read_kanaele(const XmlAttr &);
    void handle_read_parameter(const XmlAttr &);
    void handle_wp(const XmlAttr &);
    void handle_xsad(const XmlAttr &);
    void handle_xsod(const XmlAttr &);
    void handle_xsap(const XmlAttr &);
    void handle_xsop(const XmlAttr &);
    void handle_ping(const XmlAttr &);
    void handle_message_history(const XmlAttr &);
    void handle_read_statistics(const XmlAttr &);

    std::unordered_map<unsigned int /* id*/, int /* count */> group_id_count;
};
