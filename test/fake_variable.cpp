/*****************************************************************************
 * vim:tw=78
 *
 * Copyright (C) 2021 Bjarne von Horn (vh at igh dot de).
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#include "fake_variable.h"

#include "fake_utils.h"

#include <iomanip>
#include <openssl/evp.h>

std::string FakeVariableBase::toXmlTagImpl(
        const std::string &id,
        const char *tag_name,
        bool short_output,
        const std::map<std::string, std::string> &extra_attrs) const
{
    std::map<std::string, std::string> attrs = extra_attrs;

    attrs["name"]  = name;
    attrs["index"] = std::to_string(index);
    if (!id.empty())
        attrs["id"] = id;
    if (!short_output) {
        attrs["typ"]      = typ;
        attrs["datasize"] = std::to_string(datasize);
        attrs["bufsize"]  = std::to_string(bufsize);
        attrs["task"]     = std::to_string(task);
        if (orientation) {
            attrs["orientation"] = orientation;
            attrs["anz"]         = std::to_string(anz);
            if (rnum > 0 and cnum > 0) {
                attrs["rnum"] = std::to_string(rnum);
                attrs["cnum"] = std::to_string(cnum);
            }
        }
    }
    std::ostringstream os;
    os << XmlTag {tag_name, std::move(attrs), {}};
    return os.str();
}

std::string FakeVariableBase::valuesToHex(const void *data, size_t byte_size)
{
    std::ostringstream os;
    os << std::hex << std::setfill('0') << std::uppercase;
    auto s         = reinterpret_cast<const unsigned char *>(data);
    const auto end = s + byte_size;
    for (; s != end; ++s) {
        os << std::setw(2) << static_cast<unsigned int>(*s);
    }
    return os.str();
}

std::string FakeVariableBase::valuesToBase64(const void *data, size_t byte_size)
{
    const std::size_t required_len =
            byte_size / 3 * 4 + static_cast<bool>(byte_size % 3) * 4 + 1;
    std::string ans;
    ans.resize(required_len);
    const int s = EVP_EncodeBlock(
            reinterpret_cast<unsigned char *>(&ans[0]),
            reinterpret_cast<const unsigned char *>(data), byte_size);
    if (s <= 0)
        throw std::runtime_error("base64 encode failed");
    ans.resize(s);
    return ans;
}
