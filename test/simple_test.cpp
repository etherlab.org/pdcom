/*****************************************************************************
 * vim:tw=78
 *
 * Copyright (C) 2021 Bjarne von Horn (vh at igh dot de).
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#include "fake_server.h"

#include <array>
#include <cmath>
#include <condition_variable>
#include <cstring>
#include <deque>
#include <fcntl.h>
#include <gtest/gtest.h>
#include <iostream>
#include <iterator>
#include <mutex>
#include <netdb.h>
#include <netinet/in.h>
#include <numeric>
#include <pdcom5/Exception.h>
#include <pdcom5/PosixProcess.h>
#include <pdcom5/Process.h>
#include <pdcom5/Subscriber.h>
#include <pdcom5/Subscription.h>
#include <signal.h>
#include <sys/socket.h>
#include <thread>
#include <vector>

using PdCom::InvalidSubscription;
using PdCom::ProtocolError;
using PdCom::Subscription;

class MyProcess : public FakeProcess
{
  public:
    std::vector<PdCom::Variable> found_vars;
    std::vector<std::vector<PdCom::ClientStatistics>>
            client_statistics_replies_;
    int recieved_pings_ = 0;

    static constexpr size_t VectorSize = 5;

    using VectorType       = std::array<int, VectorSize>;
    using DoubleVectorType = std::array<double, VectorSize>;
    // 3x2 Matrix
    static constexpr size_t MatrixRows    = 3;
    static constexpr size_t MatrixColumns = 2;
    using MatrixType = std::array<std::array<short, MatrixColumns>, MatrixRows>;
    static constexpr MatrixType param02Init = {{{{1, 2}}, {{3, 4}}, {{5, 6}}}};

    using SignalContainer = FakeVariableContainer<
            FakeSignal,
            unsigned char,
            double,
            int,
            VectorType,
            DoubleVectorType>;
    using FirstSignalT       = FakeSignal<unsigned char>;
    using SecondSignalT      = FakeSignal<double>;
    using FourthSignalT      = FakeSignal<VectorType>;
    using FifthSignalT       = FakeSignal<DoubleVectorType>;
    using Param2Type         = FakeParameter<MatrixType>;
    using ParameterContainer = FakeVariableContainer<
            FakeParameter,
            unsigned char,
            MatrixType,
            int>;

    static std::unique_ptr<SignalContainer> makeSignals()
    {
        return std::unique_ptr<SignalContainer> {new SignalContainer(
                {"/signal1", 1000, 1, 100, 'a'},
                {"/signal2", 1000, 1, 100, 7.0}, {"/signal3", 1000, 1, 100, 42},
                {"/signal4", 1000, 1, 100, {{4, 5, 6, 7, 8}}},
                {"/signal5", 1000, 1, 100, {{4.0, 5.0, 6.0, 7.0, 8.0}}})};
    }

    static std::unique_ptr<ParameterContainer> makeParams()
    {
        return std::unique_ptr<ParameterContainer>(new ParameterContainer(
                {"/params/param01", 1000, 1, 0x01, 0},
                {"/params/param02", 1000, 1, 0x01, param02Init},
                {"/params/param03", 1000, 1, 0x01, 4711}));
    }

    MyProcess() : FakeProcess(makeSignals(), makeParams()) {}

    using Process::find;
    using Process::getClientStatistics;
    using Process::list;
    using Process::ping;
    using Process::reset;
    using Process::setMessageManager;

    void findReply(const PdCom::Variable &v) override
    {
        found_vars.emplace_back(v);
    }

    void clientStatisticsReply(
            std::vector<PdCom::ClientStatistics> statistics) override
    {
        client_statistics_replies_.push_back(std::move(statistics));
    }

    auto &accessSignals() { return *signals; }
    auto &accessParameters() { return *parameters; }

    void pingReply() override { ++recieved_pings_; }

    template <class... Args>
    bool setValue(const PdCom::Variable &var, Args &&...args)
    {
        bool callback_called = false;
        callback_called      = true;
        // TODO(igh-vh) implement parameter setValue notification in pdserv
        EXPECT_TRUE(var.setValue(std::forward<Args>(args)...).empty());
        /*
        auto future =
                var.setValue(std::forward<Args>(args)...)
                        .then([&callback_called]() { callback_called = true; });
        */
        flush_read_buffer();
        return callback_called;
    }
};


class MySubscriber : public PdCom::Subscriber
{
  public:
    using PdCom::Subscriber::Subscriber;
    struct notification
    {
        PdCom::Subscription const *object_;
        PdCom::Subscription::State state_;

        bool operator==(notification const &o) const noexcept
        {
            return o.object_ == object_ && o.state_ == state_;
        }
    };
    std::vector<notification> state_changed_notifications_;
    int newValues_count_ = 0;
    void stateChanged(PdCom::Subscription const &s) override
    {
        state_changed_notifications_.push_back({&s, s.getState()});
    }
    void newValues(std::chrono::nanoseconds /*time_ns*/) override
    {
        ++newValues_count_;
    }
};

class MyTsSubscriber : public MySubscriber
{
  public:
    using MySubscriber::MySubscriber;

    std::vector<std::chrono::nanoseconds> timeStamps_;

    void newValues(std::chrono::nanoseconds ts) override
    {
        MySubscriber::newValues(ts);
        timeStamps_.push_back(ts);
    }
};

class MyBlockSubscriber : public MySubscriber
{
  public:
    PdCom::Subscription sub1_, sub2_;

    std::vector<unsigned char> sub1_values_;
    std::vector<double> sub2_values_;
    std::vector<std::chrono::nanoseconds> ts_;
    MyBlockSubscriber(
            PdCom::Transmission const &t,
            PdCom::Subscription sub1,
            PdCom::Subscription sub2) :
        MySubscriber(t), sub1_(std::move(sub1)), sub2_(std::move(sub2))
    {}

    int newValues_count_ = 0;
    void newValues(std::chrono::nanoseconds time_ns) override
    {
        ++newValues_count_;
        ts_.push_back(time_ns);
        sub1_values_.resize(sub1_values_.size() + 1);
        sub1_.getValue(sub1_values_.back());
        sub2_values_.resize(sub2_values_.size() + 1);
        sub2_.getValue(sub2_values_.back());
    }
};

class MyProcessTest : public ::testing::Test
{
  protected:
    void SetUp() override
    {
        my_process.reset(new MyProcess());
        ASSERT_TRUE(my_process->startup());
    }

    static std::unique_ptr<MyProcess> my_process;

    static void _doRemoveEventSubscriptionAtDataTagParsingTime(void (
            *cbk)(unsigned int gid, std::unique_ptr<PdCom::Subscription> &));
};

class MyMessageManager : public PdCom::MessageManagerBase
{
  public:
    std::vector<PdCom::Message> unsolicited_, replies_, active_;

    using MessageManagerBase::activeMessages;
    using MessageManagerBase::getMessage;

    MyMessageManager() : MessageManagerBase() {}
    void processMessage(PdCom::Message message) override
    {
        unsolicited_.push_back(std::move(message));
    }
    void getMessageReply(PdCom::Message message) override
    {
        replies_.push_back(std::move(message));
    }
    void activeMessagesReply(std::vector<PdCom::Message> messageList) override
    {
        active_ = messageList;
    }
};

std::unique_ptr<MyProcess> MyProcessTest::my_process;

TEST_F(MyProcessTest, ServerDetails)
{
    EXPECT_STREQ(my_process->name().c_str(), "PdServ Test");
    EXPECT_STREQ(my_process->version().c_str(), "1.234");
}

TEST_F(MyProcessTest, Ping)
{
    my_process->ping();
    EXPECT_EQ(my_process->recieved_pings_, 1);
}

TEST_F(MyProcessTest, ClientStatistics)
{
    my_process->getClientStatistics();
    my_process->flush_read_buffer();
    ASSERT_EQ(my_process->client_statistics_replies_.size(), 1)
            << "one clientStatisticsReply reply";
    EXPECT_EQ(my_process->client_statistics_replies_[0].size(), 1)
            << "with one statistics item";
    auto stat = my_process->client_statistics_replies_[0][0];
    EXPECT_EQ(stat.name_, "127.0.0.1:37290");
    EXPECT_EQ(stat.application_name_, "unknown");
    EXPECT_EQ(stat.received_bytes_, 17);
    EXPECT_EQ(stat.sent_bytes_, 269);
    EXPECT_NEAR(stat.connected_time_.count(), 1670877353.739869 * 1e9, 1e3);
}

TEST_F(MyProcessTest, EventSubscriptonWithUnknownSignal)
{
    {
        MySubscriber sub(PdCom::event_mode);
        Subscription s1 {sub, *my_process, "/signal1"};
        EXPECT_THROW(s1.getData(), InvalidSubscription);
        EXPECT_TRUE(s1.getVariable().empty());
        {
            std::ostringstream os;
            EXPECT_THROW(s1.print(os, ';'), InvalidSubscription);
        }
        EXPECT_THROW(s1.poll(), InvalidSubscription)
                << "poll() on a pending Subscription should throw";
        EXPECT_THROW(s1.getData(), InvalidSubscription);
        const Subscription s2 {sub, *my_process, "/signal1"};
        {
            // we're creating one instance on the heap and destroy it
            // immediately, to get a change to spot use-after-free during
            // subscription with the help of ASan.
            std::unique_ptr<Subscription> use_after_free_1 {
                    new Subscription(sub, *my_process, "/signal1")};
            EXPECT_EQ(
                    use_after_free_1->getState(),
                    PdCom::Subscription::State::Pending)
                    << "State is Pending before callback were called";
            my_process->callPendingCallbacks();
            EXPECT_EQ(sub.state_changed_notifications_.size(), 3)
                    << "1x Pending per subscription";
            for (const auto &i : sub.state_changed_notifications_)
                EXPECT_EQ(i.state_, PdCom::Subscription::State::Pending);
        }
        EXPECT_EQ(my_process->getReadChannelRequests(), 1)
                << "Discovery of yet unknown variables should be grouped";
        EXPECT_EQ(my_process->getReadParamRequests(), 1);
        ASSERT_TRUE(my_process->answer_rk_rp_requests(1, 0, 0, 1));
        EXPECT_EQ(sub.newValues_count_, 0)
                << "sub.newValues() was never called";
        ASSERT_TRUE(my_process->answer_signal_subscription_requests(1, 0));
        ASSERT_EQ(my_process->subscribed_group_ids.size(), 1)
                << "Exactly one group id is used for the subscriptions";
        const unsigned int gid = my_process->subscribed_group_ids.front();
        {
            MyProcess::SendDataManager sm(*my_process, true, 47.11, gid);
            sm.addTime();
            sm.addData();
        }
        ASSERT_EQ(sub.state_changed_notifications_.size(), 5);
        EXPECT_EQ(
                sub.state_changed_notifications_[3].state_,
                PdCom::Subscription::State::Active)
                << "Subscribtion 1 has active state";
        EXPECT_EQ(
                sub.state_changed_notifications_[4].state_,
                PdCom::Subscription::State::Active)
                << "Subscribtion 2 has active state";
        EXPECT_NE(
                sub.state_changed_notifications_[3].object_,
                sub.state_changed_notifications_[4].object_)
                << "status was updated for both subscriptions";

        auto v = s1.getVariable();
        ASSERT_EQ(v.getTypeInfo().type, PdCom::TypeInfo::uint8_T)
                << "Variable is unsigned char";
        ASSERT_TRUE(v.getSizeInfo().isScalar()) << "Variable is a scalar";
        EXPECT_EQ(sub.newValues_count_, 2)
                << "sub.newValues() was called exactly once for each "
                   "subscription";
        const auto value = s1.getValue<std::array<unsigned char, 1>>();
        ASSERT_EQ(value[0], 'a') << "Value mismatch";

        {
            sub.state_changed_notifications_.resize(0);
            const Subscription s3 {sub, *my_process, "/signal1"};
            ASSERT_EQ(sub.state_changed_notifications_.size(), 0)
                    << "Subscription 3 recieved no status callbacks after "
                       "construction";
            my_process->callPendingCallbacks();
            ASSERT_EQ(sub.state_changed_notifications_.size(), 1)
                    << "Subscription 3 recieved exactly one status callback "
                       "after callPendingCallbacks()";
            ASSERT_EQ(
                    sub.state_changed_notifications_[0].state_,
                    PdCom::Subscription::State::Active)
                    << "Subscribtion 3 has active state immediately";
        }
        ASSERT_TRUE(my_process->answer_signal_unsubscribe_requests(0));
        {
            // we're creating one instance on the heap and destroy it
            // immediately, to get a change to spot use-after-free after
            // subscription with the help of ASan.
            std::unique_ptr<Subscription> use_after_free_2 {
                    new Subscription(sub, *my_process, "/signal1")};
        }
        auto &sig1 = dynamic_cast<typename MyProcess::FirstSignalT &>(
                *my_process->accessSignals().at("/signal1"));
        sig1.data_container = '5';
        {
            MyProcess::SendDataManager sm(*my_process, true, 47.11, gid);
            sm.addTime();
            sm.addData();
        }

        EXPECT_EQ(sub.newValues_count_, 4)
                << "sub.newValues() was called exactly twice per subscription";
        const auto value2 = s1.getValue<unsigned char>();
        ASSERT_EQ(value2, '5') << "Value mismatch";
        {
            sig1.data_container  = 'I';
            sub.newValues_count_ = 0;
            s1.poll();
            ASSERT_TRUE(my_process->answer_rk_rp_requests(1, 0, 0, 0));
            ASSERT_EQ(sub.newValues_count_, 1);
            std::array<unsigned char, 1> value3;
            s1.getValue(value3);
            ASSERT_EQ(value3[0], 'I') << "Value mismatch after poll";
        }
    }
    ASSERT_TRUE(my_process->answer_signal_unsubscribe_requests(1));

    EXPECT_TRUE(my_process->has_no_group_subscriptions());
}

TEST_F(MyProcessTest, EventSubscriptonWithKnownSignal)
{
    EXPECT_FALSE(my_process->find("/signal1"))
            << "unknown signal cannot be cached";
    EXPECT_EQ(my_process->getReadChannelRequests(), 1)
            << "Discovery of yet unknown variables should be grouped";
    EXPECT_EQ(my_process->getReadParamRequests(), 1);
    ASSERT_TRUE(my_process->answer_rk_rp_requests(1, 0, 0, 1));
    ASSERT_EQ(my_process->found_vars.size(), 1) << "Found one signal";
    const auto v = std::move(my_process->found_vars[0]);
    EXPECT_EQ(v.getProcess(), my_process.get());
    my_process->found_vars.clear();
    ASSERT_FALSE(v.empty()) << "Variable must have a valid pimpl instance";
    ASSERT_EQ(v.getTypeInfo().type, PdCom::TypeInfo::uint8_T)
            << "Variable is unsigned char";
    ASSERT_TRUE(v.getSizeInfo().isScalar()) << "Variable is a scalar";
    ASSERT_TRUE(my_process->answer_signal_subscription_requests(0, 0))
            << "No subscription requests so far";
    EXPECT_FALSE(v.isWriteable()) << "Signal is not writeable";

    {
        // we put the mailbox on the heap for ASan
        std::unique_ptr<MySubscriber> sub {new MySubscriber(PdCom::event_mode)};
        Subscription s1 {*sub, v};
        EXPECT_EQ(s1.getPath(), "/signal1") << "Path Accessor in pending state";

        EXPECT_EQ(sub->newValues_count_, 0)
                << "sub->newValues() was never called";
        ASSERT_TRUE(my_process->answer_signal_subscription_requests(1, 0));
        ASSERT_EQ(my_process->subscribed_group_ids.size(), 1)
                << "Exactly one group id is used for the subscriptions";
        EXPECT_EQ(s1.getPath(), "/signal1") << "Path Accessor in active state";
        const unsigned int gid = my_process->subscribed_group_ids.front();
        {
            MyProcess::SendDataManager sm(*my_process, true, 47.11, gid);
            sm.addTime();
            sm.addData();
        }
        ASSERT_EQ(sub->state_changed_notifications_.size(), 2)
                << "Subscription 1 recieved exactly two status callback";
        ASSERT_EQ(
                sub->state_changed_notifications_[1].state_,
                PdCom::Subscription::State::Active)
                << "Subscribtion 1 has active state";

        Subscription s2 {*sub, *my_process, "/signal1"};
        ASSERT_EQ(sub->state_changed_notifications_.size(), 2)
                << "No more callbacks before callPendingCallbacks()";
        my_process->callPendingCallbacks();
        ASSERT_EQ(sub->state_changed_notifications_.size(), 3)
                << "Subscription 2 recieved exactly one status callback";
        ASSERT_EQ(
                sub->state_changed_notifications_[2].state_,
                PdCom::Subscription::State::Active)
                << "Subscribtion 2 has active state";
        ASSERT_TRUE(my_process->answer_signal_subscription_requests(0, 0))
                << "No new subscription requests, as it is already present";
    }
    ASSERT_TRUE(my_process->answer_signal_unsubscribe_requests(1));
    EXPECT_TRUE(my_process->has_no_group_subscriptions());
}

template <bool with_xsap = true>
static void _dotestMultipleParameterEventSubscription(MyProcess *my_process)
{
    const auto flush_setValue = [my_process]() {
        my_process->flush_read_buffer();
        if (!with_xsap) {
            EXPECT_TRUE(my_process->answer_rk_rp_requests(0, 0, 1, 0))
                    << "<pu> is processed";
        }
    };
    struct MyS : MySubscriber, PdCom::Subscription
    {
        MyS(const PdCom::Variable &v) :
            MySubscriber(PdCom::event_mode), Subscription(*this, v)
        {}
    };

    const std::array<const char *, 2> params {
            "/params/param01", "/params/param03"};
    for (const auto i : params) {
        EXPECT_FALSE(my_process->find(i));
        ASSERT_TRUE(my_process->answer_rk_rp_requests(0, 1, 1, 0));
    }
    ASSERT_EQ(my_process->found_vars.size(), 2);
    const auto p1 = my_process->found_vars[0], p2 = my_process->found_vars[1];
    auto &p1_rt = dynamic_cast<FakeParameter<unsigned char> &>(
            *my_process->accessParameters().at(params[0]));
    auto &p2_rt = dynamic_cast<FakeParameter<int> &>(
            *my_process->accessParameters().at(params[1]));
    my_process->found_vars.clear();
    ASSERT_FALSE(p1.empty() or p2.empty());
    {
        MyS s1 {p1};
        ASSERT_TRUE(
                my_process->answer_parameter_subscription_requests(with_xsap));
        ASSERT_EQ(s1.getState(), PdCom::Subscription::State::Active)
                << "Subscription 1 is active";
        const unsigned char p1_v1 = 'Z', p1_v2 = 'F', p1_v3 = 'K', p1_v4 = 'O';
        ASSERT_TRUE(my_process->setValue(p1, p1_v1));
        EXPECT_EQ(p1_rt.data_container, p1_v1)
                << "setValue() parameter 1 mismatch";
        flush_setValue();
        ASSERT_EQ(s1.newValues_count_, 1);
        ASSERT_EQ(
                s1.getVariable().getTypeInfo().type, PdCom::TypeInfo::uint8_T);
        EXPECT_EQ(*reinterpret_cast<const unsigned char *>(s1.getData()), p1_v1)
                << "getData() parameter 1 mismatch";
        auto a = std::numeric_limits<unsigned long>::max();
        s1.getValue(a);

        EXPECT_EQ(a, p1_v1) << "getValue() parameter 1 mismatch";
        {
            MyS s2 {p2};
            ASSERT_TRUE(my_process->answer_parameter_subscription_requests(
                    with_xsap));
            ASSERT_EQ(s2.getState(), PdCom::Subscription::State::Active)
                    << "Subscription 2 is active";
            ASSERT_TRUE(my_process->setValue(p1, p1_v2));
            flush_setValue();
            ASSERT_EQ(s1.newValues_count_, 2);
            ASSERT_EQ(s2.newValues_count_, 0)
                    << "Subscription 2 is unaffected by updating parameter 1";
            s1.getValue(a);
            EXPECT_EQ(a, p1_v2) << "getValue() parameter 1 mismatch";
            const int p2_v1 = 999999;
            ASSERT_TRUE(my_process->setValue(p2, p2_v1 * 1.0));
            EXPECT_EQ(p2_rt.data_container, p2_v1)
                    << "setValue() parameter 2 mismatch";
            flush_setValue();
            ASSERT_EQ(s1.newValues_count_, 2)
                    << "Subscription 1 is unaffected by updating parameter 2";
            ASSERT_EQ(s2.newValues_count_, 1);
            ASSERT_EQ(
                    s2.getVariable().getTypeInfo().type,
                    PdCom::TypeInfo::int32_T);
            EXPECT_EQ(*reinterpret_cast<const int *>(s2.getData()), p2_v1)
                    << "getData() parameter 2 mismatch";
            long a2;
            s2.getValue(a2);
            EXPECT_EQ(a2, p2_v1) << "getValue() parameter 2 mismatch";
            s1.getValue(a);
            EXPECT_EQ(a, p1_v2) << "parameter 1 is not modified";
        }
        ASSERT_TRUE(
                my_process->answer_parameter_unsubscribe_requests(with_xsap));
        s1.getValue(a);
        EXPECT_EQ(a, p1_v2) << "parameter 1 is not modified";
        ASSERT_TRUE(my_process->setValue(p2, 77));
        flush_setValue();
        ASSERT_EQ(s1.newValues_count_, 2)
                << "Subscription 1 is unaffected by updating parameter 2";
        s1.getValue(a);
        EXPECT_EQ(a, p1_v2) << "parameter 1 is not modified";
        s1.poll();
        ASSERT_TRUE(my_process->answer_rk_rp_requests(0, 0, 1, 0))
                << "Poll failed";
        s1.getValue(a);
        EXPECT_EQ(a, p1_v2) << "parameter 1 is not modified in rt process";

        {
            s1.newValues_count_ = 0;
            // subscribe same parameter as s1
            MyS s2 {p1};
            ASSERT_TRUE(my_process->answer_parameter_subscription_requests(0));
            ASSERT_EQ(s2.getState(), PdCom::Subscription::State::Active)
                    << "Subscription 2 is active";

            ASSERT_TRUE(my_process->setValue(p1, p1_v3));
            flush_setValue();
            ASSERT_EQ(s1.newValues_count_, 1);
            ASSERT_EQ(s2.newValues_count_, 1)
                    << "s1 and s2 got parameter 1 update";
            s1.getValue(a);
            EXPECT_EQ(a, p1_v3)
                    << "getValue() subscription 1 parameter 1 mismatch";
            a = 0;
            s2.getValue(a);
            EXPECT_EQ(a, p1_v3)
                    << "getValue() subscription 2 parameter 1 mismatch";

            {
                MyS s3 {p2};
                ASSERT_TRUE(my_process->answer_parameter_subscription_requests(
                        with_xsap));
                ASSERT_EQ(s3.getState(), PdCom::Subscription::State::Active)
                        << "Subscription 3 is active";
                ASSERT_TRUE(my_process->setValue(p1, p1_v4));
                flush_setValue();
                ASSERT_EQ(s1.newValues_count_, 2);
                ASSERT_EQ(s2.newValues_count_, 2)
                        << "s1 and s2 got parameter 1 update";
                ASSERT_EQ(s3.newValues_count_, 0) << "s3 stays unaffected";
                s1.getValue(a);
                EXPECT_EQ(a, p1_v4)
                        << "getValue() subscription 1 parameter 1  mismatch";
                s2.getValue(a);
                EXPECT_EQ(a, p1_v4)
                        << "getValue() subscription 1 parameter 1  mismatch";

                const int p2_v2 = 7777;
                ASSERT_TRUE(my_process->setValue(p2, p2_v2));
                flush_setValue();
                ASSERT_EQ(s1.newValues_count_, 2) << "s1 stays unaffected";
                ASSERT_EQ(s2.newValues_count_, 2) << "s2 stays unaffected";
                ASSERT_EQ(s3.newValues_count_, 1) << "s3 got update";
                long a2;
                s3.getValue(a2);
                EXPECT_EQ(a2, p2_v2) << "getValue() parameter 2 mismatch";
                s1.getValue(a);
                EXPECT_EQ(a, p1_v4) << "parameter 1 is not modified";
                s2.getValue(a);
                EXPECT_EQ(a, p1_v4) << "parameter 1 is not modified";
            }
        }
    }
    ASSERT_TRUE(
            my_process->answer_parameter_unsubscribe_requests(2 * with_xsap));
    EXPECT_TRUE(my_process->has_no_group_subscriptions());
}

void testImmediateResubscriptionEvent(
        bool const callCancelSubscriptions,
        bool const use_xsap)
{
    MyProcess my_process;
    ASSERT_TRUE(my_process.startup(use_xsap));

    EXPECT_FALSE(my_process.find("/params/param01"))
            << "unknown parameter cannot be cached";
    ASSERT_TRUE(my_process.answer_rk_rp_requests(0, 1, 1, 0));
    ASSERT_EQ(my_process.found_vars.size(), 1) << "Found one parameter";
    auto v = std::move(my_process.found_vars[0]);

    const auto work = [&](bool expect_subscribe = true) {
        MySubscriber sub(PdCom::event_mode);
        Subscription s1 {sub, v};
        if (use_xsap and expect_subscribe) {
            EXPECT_EQ(s1.getState(), Subscription::State::Pending);
            ASSERT_TRUE(my_process.answer_parameter_subscription_requests(1));
        }
        EXPECT_EQ(s1.getState(), Subscription::State::Active);
        v.setValue(10);
        my_process.flush_read_buffer();
        if (not use_xsap) {
            ASSERT_TRUE(my_process.answer_rk_rp_requests(0, 0, 1, 0))
                    << "<pu> is processed correctly";
        }
        EXPECT_EQ(sub.newValues_count_, 1) << "Received exactly one new value";
        EXPECT_EQ(
                sub.state_changed_notifications_.size(),
                1 + (use_xsap and expect_subscribe))
                << "stateChanged() called once (no xsap) or twice (xsap w/ "
                   "cancel).";
    };

    work();
    if (callCancelSubscriptions) {
        my_process.callPendingCallbacks();
        if (use_xsap) {
            ASSERT_TRUE(my_process.answer_parameter_unsubscribe_requests(1));
        }
        work();
    }
    else {
        work(false);
    }
}

TEST_F(MyProcessTest, ImmediateResubscriptionEventNoCallbacks)
{
    testImmediateResubscriptionEvent(false, false);
}


TEST_F(MyProcessTest, ImmediateResubscriptionEventCallbacks)
{
    testImmediateResubscriptionEvent(true, false);
}

TEST_F(MyProcessTest, DISABLED_ImmediateResubscriptionEventNoCallbacksXsap)
{
    testImmediateResubscriptionEvent(false, true);
}


TEST_F(MyProcessTest, DISABLED_ImmediateResubscriptionEventCallbacksXsap)
{
    testImmediateResubscriptionEvent(true, true);
}


void testImmediateResubscriptionPeriodic(
        MyProcess &my_process,
        bool const callCancelSubscriptions)
{
    constexpr std::chrono::duration<double> dflt_time {0.2};
    EXPECT_FALSE(my_process.find("/signal1"))
            << "unknown signal cannot be cached";
    ASSERT_TRUE(my_process.answer_rk_rp_requests(1, 0, 0, 1));
    ASSERT_EQ(my_process.found_vars.size(), 1) << "Found one signal";
    const auto v = std::move(my_process.found_vars[0]);

    unsigned int gid = 0;
    const auto work  = [&]() {
        MySubscriber sub(dflt_time);
        Subscription s1 {sub, v};
        EXPECT_EQ(s1.getState(), Subscription::State::Pending);
        EXPECT_EQ(sub.newValues_count_, 0)
                << "sub.newValues() was never called";
        ASSERT_TRUE(my_process.answer_signal_subscription_requests(0, 1));
        EXPECT_EQ(s1.getState(), Subscription::State::Active);
        ASSERT_FALSE(my_process.subscribed_group_ids.empty());
        gid = my_process.subscribed_group_ids.back();
        {
            MyProcess::SendDataManager sm(my_process, false, 47.11, gid);
            sm.addTime();
            sm.addData();
        }

        EXPECT_EQ(sub.newValues_count_, 1) << "Received exactly one new value";
        EXPECT_EQ(sub.state_changed_notifications_.size(), 2)
                << "stateChanged() called twice.";
    };
    work();
    if (callCancelSubscriptions)
        my_process.callPendingCallbacks();

    // unsubscribing without callPendingCallbacks() can be a side effect
    if (callCancelSubscriptions
        or my_process.getSignalUnsubscribeRequests() != 0) {
        ASSERT_TRUE(my_process.answer_signal_unsubscribe_requests(1));
        // make the exact same subscription
        work();
    }
    else {
        // make the exact same subscription
        MySubscriber sub(dflt_time);
        Subscription s1 {sub, v};
        EXPECT_EQ(s1.getState(), Subscription::State::Active)
                << "Subscription is active immediately";
        ASSERT_TRUE(my_process.answer_signal_unsubscribe_requests(0))
                << "no subscription is canceled";
        ASSERT_TRUE(my_process.answer_signal_subscription_requests(0, 0))
                << "No subscription is added, too";
        {
            MyProcess::SendDataManager sm(my_process, false, 47.11, gid);
            sm.addTime();
            sm.addData();
        }

        EXPECT_EQ(sub.newValues_count_, 1) << "Received exactly one new value";
        EXPECT_EQ(sub.state_changed_notifications_.size(), 1)
                << "stateChanged() called Once.";
    }
}

TEST_F(MyProcessTest, ImmediateResubscriptionPeriodicNoCallbacks)
{
    testImmediateResubscriptionPeriodic(*my_process, false);
}


TEST_F(MyProcessTest, ImmediateResubscriptionPeriodicCallbacks)
{
    testImmediateResubscriptionPeriodic(*my_process, true);
}

TEST_F(MyProcessTest, DISABLED_MultipleParameterEventSubscription)
{
    _dotestMultipleParameterEventSubscription(my_process.get());
}

TEST_F(MyProcessTest, PendingSubscriptionReadEOF)
{
    MySubscriber sub(PdCom::event_mode);
    size_t written_bytes_so_far = 0;

    const auto work = [&]() {
        Subscription s1 {sub, *my_process, "/signal1"};
        ASSERT_EQ(my_process->getReadChannelRequests(), 1);

        written_bytes_so_far = my_process->written_bytes_;
        my_process->send_eof();
        ASSERT_EQ(sub.state_changed_notifications_.size(), 2)
                << "Subscription 1 recieved exactly two status callback";
        ASSERT_EQ(
                sub.state_changed_notifications_.front().state_,
                PdCom::Subscription::State::Pending);
        ASSERT_EQ(
                sub.state_changed_notifications_.back().state_,
                PdCom::Subscription::State::Invalid);
        EXPECT_EQ(s1.getState(), PdCom::Subscription::State::Invalid)
                << "Subscription is invalid right after EOF";
        EXPECT_THROW(s1.poll(), InvalidSubscription);
        EXPECT_THROW(s1.getData(), InvalidSubscription);
        EXPECT_TRUE(s1.getVariable().empty());
        std::ostringstream os;
        EXPECT_THROW(s1.print(os, ';'), InvalidSubscription);

        sub.state_changed_notifications_.clear();
        PdCom::Subscription s2 {sub, *my_process, "/signal1"};
        my_process->callPendingCallbacks();
        ASSERT_EQ(sub.state_changed_notifications_.size(), 1);
        EXPECT_EQ(
                sub.state_changed_notifications_.back().state_,
                PdCom::Subscription::State::Pending)
                << "New Subscription after EOF will be registered for "
                   "reconnect";
    };
    work();
    EXPECT_EQ(written_bytes_so_far, my_process->written_bytes_)
            << "No more data sent to server after EOF";
    sub.state_changed_notifications_.clear();
    my_process->clear_server_side();
    ASSERT_TRUE(my_process->startup()) << "Reconnect fake server";
    work();
    EXPECT_EQ(written_bytes_so_far, my_process->written_bytes_)
            << "No more data sent to server after EOF";
}


TEST_F(MyProcessTest, EventSubscriptonReadEOF)
{
    MySubscriber sub(PdCom::event_mode);
    PdCom::Variable v;
    size_t written_bytes_so_far = 0;

    const auto work = [&]() {
        EXPECT_FALSE(my_process->find("/signal1"))
                << "unknown signal cannot be cached";
        ASSERT_TRUE(my_process->answer_rk_rp_requests(1, 0, 0, 1));
        ASSERT_EQ(my_process->found_vars.size(), 1) << "Found one signal";
        const auto v = std::move(my_process->found_vars[0]);
        my_process->found_vars.clear();
        ASSERT_FALSE(v.empty()) << "Variable must have a valid pimpl instance";

        Subscription s1 {sub, v};

        ASSERT_TRUE(my_process->answer_signal_subscription_requests(1, 0));
        ASSERT_EQ(my_process->subscribed_group_ids.size(), 1)
                << "Exactly one group id is used for the subscriptions";
        const unsigned int gid = my_process->subscribed_group_ids.front();
        ASSERT_EQ(sub.state_changed_notifications_.size(), 2)
                << "Subscription 1 recieved exactly two status callback";
        ASSERT_EQ(
                sub.state_changed_notifications_[1].state_,
                PdCom::Subscription::State::Active)
                << "Subscribtion 1 has active state";
        sub.state_changed_notifications_.clear();

        // connection is lost right when receiving a <data> packet
        my_process->read_buffer << "<data time=\"47.11\" group=\"" << gid
                                << "\">\r\n";

        written_bytes_so_far = my_process->written_bytes_;
        my_process->send_eof();
        ASSERT_EQ(sub.state_changed_notifications_.size(), 1)
                << "Subscription 1 recieved exactly one status callback";
        ASSERT_EQ(
                sub.state_changed_notifications_.back().state_,
                PdCom::Subscription::State::Invalid)
                << "Subscribtion 1 has invalid state right after EOF";
        EXPECT_EQ(s1.getState(), PdCom::Subscription::State::Invalid);
        EXPECT_THROW(s1.poll(), InvalidSubscription);
        EXPECT_THROW(s1.getData(), InvalidSubscription);
        EXPECT_TRUE(s1.getVariable().empty());
        std::ostringstream os;
        EXPECT_THROW(s1.print(os, ';'), InvalidSubscription);
        EXPECT_TRUE(v.empty());

        EXPECT_THROW((PdCom::Subscription {sub, v}), PdCom::EmptyVariable);

        sub.state_changed_notifications_.clear();
        PdCom::Subscription s2 {sub, *my_process, "/signal1"};
        my_process->callPendingCallbacks();
        ASSERT_EQ(sub.state_changed_notifications_.size(), 1);
        EXPECT_EQ(
                sub.state_changed_notifications_.back().state_,
                PdCom::Subscription::State::Pending)
                << "New Subscription after EOF will be registered for "
                   "reconnect";
    };
    work();
    EXPECT_EQ(written_bytes_so_far, my_process->written_bytes_)
            << "No more data sent to server after EOF";
    sub.state_changed_notifications_.clear();
    my_process->clear_server_side();
    ASSERT_TRUE(my_process->startup()) << "Reconnect fake server";
    work();
    EXPECT_EQ(written_bytes_so_far, my_process->written_bytes_)
            << "No more data sent to server after EOF";
}

TEST_F(MyProcessTest, PeriodicSubscriptonReadEOF)
{
    PdCom::Variable v;
    MySubscriber sub(std::chrono::milliseconds {100});
    size_t written_bytes_so_far = 0;

    const auto work = [&]() {
        EXPECT_FALSE(my_process->find("/signal1"))
                << "unknown signal cannot be cached";
        ASSERT_TRUE(my_process->answer_rk_rp_requests(1, 0, 0, 1));
        ASSERT_EQ(my_process->found_vars.size(), 1) << "Found one signal";
        v = std::move(my_process->found_vars[0]);
        my_process->found_vars.clear();
        ASSERT_FALSE(v.empty()) << "Variable must have a valid pimpl instance";
        Subscription s1 {sub, v};

        ASSERT_TRUE(my_process->answer_signal_subscription_requests(0, 1));
        ASSERT_EQ(my_process->subscribed_group_ids.size(), 1)
                << "Exactly one group id is used for the subscriptions";
        const unsigned int gid = my_process->subscribed_group_ids.front();
        ASSERT_EQ(sub.state_changed_notifications_.size(), 2)
                << "Subscription 1 recieved exactly two status callback";
        ASSERT_EQ(
                sub.state_changed_notifications_[1].state_,
                PdCom::Subscription::State::Active)
                << "Subscribtion 1 has active state";
        sub.state_changed_notifications_.clear();

        // connection is lost right when receiving a <data> packet
        my_process->read_buffer << "<data time=\"47.11\" group=\"" << gid
                                << "\">\r\n";

        written_bytes_so_far = my_process->written_bytes_;
        my_process->send_eof();
        ASSERT_EQ(sub.state_changed_notifications_.size(), 1)
                << "Subscription 1 recieved exactly one status callback";
        ASSERT_EQ(
                sub.state_changed_notifications_.back().state_,
                PdCom::Subscription::State::Invalid)
                << "Subscribtion 1 has invalid state right after EOF";
        EXPECT_EQ(s1.getState(), PdCom::Subscription::State::Invalid);
        EXPECT_THROW(s1.poll(), InvalidSubscription);
        EXPECT_THROW(s1.getData(), InvalidSubscription);
        EXPECT_TRUE(s1.getVariable().empty());
        std::ostringstream os;
        EXPECT_THROW(s1.print(os, ';'), InvalidSubscription);
        EXPECT_TRUE(v.empty());

        EXPECT_THROW((PdCom::Subscription {sub, v}), PdCom::EmptyVariable);

        sub.state_changed_notifications_.clear();
        PdCom::Subscription s2 {sub, *my_process, "/signal1"};
        my_process->callPendingCallbacks();
        ASSERT_EQ(sub.state_changed_notifications_.size(), 1);
        EXPECT_EQ(
                sub.state_changed_notifications_.back().state_,
                PdCom::Subscription::State::Pending)
                << "New Subscription after EOF will be registered for "
                   "reconnect";
    };
    work();
    EXPECT_EQ(written_bytes_so_far, my_process->written_bytes_)
            << "No more data sent to server after EOF";

    sub.state_changed_notifications_.clear();
    my_process->clear_server_side();
    ASSERT_TRUE(my_process->startup()) << "Reconnect fake server";
    work();
    EXPECT_EQ(written_bytes_so_far, my_process->written_bytes_)
            << "No more data sent to server after EOF";
}

TEST_F(MyProcessTest, PollSubscriptonReadEOF)
{
    PdCom::Variable v;
    MySubscriber sub(PdCom::poll_mode);
    size_t written_bytes_so_far = 0;

    const auto work = [&]() {
        EXPECT_FALSE(my_process->find("/params/param01"))
                << "unknown parameter cannot be cached";
        ASSERT_TRUE(my_process->answer_rk_rp_requests(0, 1, 1, 0));
        ASSERT_EQ(my_process->found_vars.size(), 1) << "Found one parameter";
        v = std::move(my_process->found_vars[0]);
        my_process->found_vars.clear();
        ASSERT_FALSE(v.empty()) << "Variable must have a valid pimpl instance";
        Subscription s1 {sub, v};

        EXPECT_NO_THROW(s1.poll());
        ASSERT_EQ(my_process->getReadParamRequests(), 1) << "poll() worked";

        written_bytes_so_far = my_process->written_bytes_;
        my_process->send_eof();
        ASSERT_GT(sub.state_changed_notifications_.size(), 1);
        ASSERT_EQ(
                sub.state_changed_notifications_.back().state_,
                PdCom::Subscription::State::Invalid)
                << "Subscribtion 1 has invalid state right after EOF";
        EXPECT_EQ(s1.getState(), PdCom::Subscription::State::Invalid);
        EXPECT_THROW(s1.poll(), InvalidSubscription);
        EXPECT_THROW(s1.getData(), InvalidSubscription);
        EXPECT_TRUE(s1.getVariable().empty());
        std::ostringstream os;
        EXPECT_THROW(s1.print(os, ';'), InvalidSubscription);
        EXPECT_TRUE(v.empty());
        EXPECT_THROW(v.setValue(4711), PdCom::EmptyVariable);

        EXPECT_THROW((PdCom::Subscription {sub, v}), PdCom::EmptyVariable);

        sub.state_changed_notifications_.clear();
        PdCom::Subscription s2 {sub, *my_process, "/params/param01"};
        my_process->callPendingCallbacks();
        ASSERT_EQ(sub.state_changed_notifications_.size(), 1);
        EXPECT_EQ(
                sub.state_changed_notifications_.back().state_,
                PdCom::Subscription::State::Pending)
                << "New Subscription after EOF will be registered for "
                   "reconnect";
    };

    work();
    EXPECT_EQ(written_bytes_so_far, my_process->written_bytes_)
            << "No more data sent to server after EOF";
    sub.state_changed_notifications_.clear();
    my_process->clear_server_side();
    ASSERT_TRUE(my_process->startup()) << "Reconnect fake server";
    work();
    EXPECT_EQ(written_bytes_so_far, my_process->written_bytes_)
            << "No more data sent to server after EOF";
}

TEST_F(MyProcessTest, EOFDuringWrite)
{
    EXPECT_FALSE(my_process->find("/params/param01"));
    my_process->throw_at_write_ = true;
    EXPECT_THROW(my_process->find("/signal1"), PdCom::WriteFailure)
            << "EOF results in an Exception";
    EXPECT_THROW(my_process->find("/signal2"), PdCom::WriteFailure)
            << "EOF results in an Exception";
}

// The closing tag > is written in a dtor in pdcom, we should also test it.
TEST_F(MyProcessTest, EOFDuringWriteGtTag)
{
    EXPECT_FALSE(my_process->find("/params/param01"));
    my_process->throw_at_write_gt_ = true;
    EXPECT_THROW(my_process->find("/signal1"), PdCom::WriteFailure)
            << "EOF results in an Exception";
    EXPECT_THROW(my_process->find("/signal2"), PdCom::WriteFailure)
            << "EOF results in an Exception";
}

void testDuringUnsibscribe(bool do_throw, MyProcess *my_process)
{
    MySubscriber sub(std::chrono::milliseconds {100});
    size_t written_bytes_so_far = 0;

    EXPECT_FALSE(my_process->find("/signal1"))
            << "unknown signal cannot be cached";
    ASSERT_TRUE(my_process->answer_rk_rp_requests(1, 0, 0, 1));
    ASSERT_EQ(my_process->found_vars.size(), 1) << "Found one signal";
    auto v = std::move(my_process->found_vars[0]);
    my_process->found_vars.clear();
    ASSERT_FALSE(v.empty()) << "Variable must have a valid pimpl instance";
    try {
        struct callCallbacksOnLeave
        {
            MyProcess *process;
            ~callCallbacksOnLeave()
            {
                try {
                    process->callPendingCallbacks();
                }
                catch (...) {
                }
            }
            callCallbacksOnLeave(MyProcess *p) : process(p) {}
        } const cl(my_process);
        Subscription s1 {sub, v};
        ASSERT_TRUE(my_process->answer_signal_subscription_requests(0, 1));
        ASSERT_EQ(s1.getState(), PdCom::Subscription::State::Active);

        Subscription s2 {sub, *my_process, "/signal2"};
        ASSERT_TRUE(my_process->answer_rk_rp_requests(1, 0, 0, 1));
        ASSERT_TRUE(my_process->answer_signal_subscription_requests(0, 1));
        ASSERT_EQ(s2.getState(), PdCom::Subscription::State::Active);


        written_bytes_so_far = my_process->written_bytes_;

        if (do_throw) {
            my_process->throw_at_write_gt_ = true;
            throw std::runtime_error("My bad");
        }
        else {
            my_process->throw_at_write_ = true;
        }
    }
    catch (...) {
    }
    if (!do_throw) {
        EXPECT_EQ(written_bytes_so_far, my_process->written_bytes_)
                << "No more data sent to server after EOF";
    }
    else {
        written_bytes_so_far = my_process->written_bytes_;
    }
    try {
        my_process->find("/ZZZZunknownsignal");
        ADD_FAILURE() << "Further writing does not lead to an exception";
    }
    catch (...) {
    }
    EXPECT_EQ(written_bytes_so_far, my_process->written_bytes_)
            << "No more data sent to server after EOF";
}


TEST_F(MyProcessTest, EOFDuringUnsubscribe)
{
    testDuringUnsibscribe(false, my_process.get());
}

TEST_F(MyProcessTest, EOFDuringUnwinding)
{
    testDuringUnsibscribe(true, my_process.get());
}

TEST_F(MyProcessTest, PeriodicSubscriptonWithUnknownSignal)
{
    constexpr std::chrono::duration<double> dflt_time {0.2};
    std::unique_ptr<MySubscriber> sub {new MySubscriber(dflt_time)};
    {
        Subscription s1 {*sub, *my_process, "/signal1"};
        Subscription s2 {*sub, *my_process, "/signal1"};
        EXPECT_EQ(s1.getState(), PdCom::Subscription::State::Pending);
        EXPECT_EQ(s2.getState(), PdCom::Subscription::State::Pending);
        {
            // we're creating one instance on the heap and destroy it
            // immediately, to get a change to spot use-after-free during
            // subscription with the help of ASan.
            std::unique_ptr<Subscription> use_after_free_1 {
                    new Subscription(*sub, *my_process, "/signal1")};


            my_process->callPendingCallbacks();
            EXPECT_EQ(sub->state_changed_notifications_.size(), 3)
                    << "1x Pending per subscription";
        }
        EXPECT_EQ(my_process->getReadChannelRequests(), 1)
                << "Discovery of yet unknown variables should be grouped";

        EXPECT_EQ(my_process->getReadParamRequests(), 1);
        ASSERT_TRUE(my_process->answer_rk_rp_requests(1, 0, 0, 1));

        EXPECT_EQ(sub->newValues_count_, 0)
                << "sub.newValues() was never called";
        ASSERT_TRUE(my_process->answer_signal_subscription_requests(0, 1));
        ASSERT_EQ(my_process->subscribed_group_ids.size(), 1)
                << "One group id per subscription";

        EXPECT_EQ(sub->newValues_count_, 0)
                << "sub.newValues() was never called";
        ASSERT_EQ(sub->state_changed_notifications_.size(), 5)
                << "Subscription state does not change before <data> has "
                   "arrived";
        ASSERT_EQ(
                sub->state_changed_notifications_[3].state_,
                PdCom::Subscription::State::Active)
                << "Subscribtion 1 has active state";
        ASSERT_EQ(
                sub->state_changed_notifications_[4].state_,
                PdCom::Subscription::State::Active)
                << "Subscribtion 2 has active state";

        const int gid = my_process->subscribed_group_ids.front();
        {
            MyProcess::SendDataManager sm(*my_process, false, 47.11, gid);
            sm.addTime();
            sm.addData();
        }

        auto v = s1.getVariable();
        ASSERT_EQ(v.getTypeInfo().type, PdCom::TypeInfo::uint8_T)
                << "Variable is unsigned char";
        ASSERT_TRUE(v.getSizeInfo().isScalar()) << "Variable is a scalar";
        EXPECT_EQ(sub->newValues_count_, 1)
                << "sub.newValues() was called exactly once";
        std::array<unsigned char, 1> value;
        s1.getValue(value);
        EXPECT_EQ(value[0], 'a') << "S1 Value mismatch";
        s2.getValue(value);
        EXPECT_EQ(value[0], 'a') << "S2 Value mismatch";

        sub->state_changed_notifications_.clear();
        Subscription s3 {*sub, *my_process, "/signal1"};
        EXPECT_EQ(s3.getState(), PdCom::Subscription::State::Active);
        ASSERT_EQ(sub->state_changed_notifications_.size(), 0)
                << "No more callbacks before callPendingCallbacks()";
        my_process->callPendingCallbacks();
        ASSERT_EQ(sub->state_changed_notifications_.size(), 1)
                << "Subscription 3 recieved exactly one status callback";
        ASSERT_EQ(
                sub->state_changed_notifications_.back().state_,
                PdCom::Subscription::State::Active)
                << "Subscribtion 3 has active state";
        {
            Subscription s4 {*sub, *my_process, "/signal1"};
            ASSERT_EQ(sub->state_changed_notifications_.size(), 1)
                    << "No more callbacks before callPendingCallbacks()";
            my_process->callPendingCallbacks();
            ASSERT_EQ(sub->state_changed_notifications_.size(), 2)
                    << "Subscription 4 recieved exactly one status callback";
            ASSERT_EQ(
                    sub->state_changed_notifications_.back().state_,
                    PdCom::Subscription::State::Active)
                    << "Subscribtion 3 has active state";
        }
        EXPECT_TRUE(my_process->answer_signal_unsubscribe_requests(0))
                << "Subscription (on the server side) still there";
        {
            // we're creating one instance on the heap and destroy it
            // immediately, to get a change to spot use-after-free after
            // subscription with the help of ASan.
            std::unique_ptr<Subscription> use_after_free_2 {
                    new Subscription(*sub, *my_process, "/signal1")};
        }
        sub->newValues_count_ = 0;
        dynamic_cast<typename MyProcess::FirstSignalT &>(
                *my_process->accessSignals().at("/signal1"))
                .data_container = '5';
        {
            MyProcess::SendDataManager sm(*my_process, false, 47.11, gid);
            sm.addTime();
            sm.addData();
        }

        EXPECT_EQ(sub->newValues_count_, 1)
                << "sub.newValues() was called exactly once";
        s1.getValue(value);
        EXPECT_EQ(value[0], '5') << "Value mismatch";
    }
    EXPECT_TRUE(my_process->answer_signal_unsubscribe_requests(1))
            << "server-side subscription is canceled, because all "
               "subscriptions are gone";
    EXPECT_TRUE(my_process->has_no_group_subscriptions());
}


TEST_F(MyProcessTest, PeriodicSubscriptonWithKnownSignal)
{
    constexpr std::chrono::duration<double> dflt_time {0.2};
    EXPECT_FALSE(my_process->find("/signal1"))
            << "unknown signal cannot be cached";

    EXPECT_EQ(my_process->getReadChannelRequests(), 1);
    EXPECT_EQ(my_process->getReadParamRequests(), 1);
    ASSERT_TRUE(my_process->answer_rk_rp_requests(1, 0, 0, 1));
    ASSERT_EQ(my_process->found_vars.size(), 1) << "Found one signal";
    const auto v = std::move(my_process->found_vars[0]);
    my_process->found_vars.clear();
    ASSERT_FALSE(v.empty()) << "Variable must have a valid pimpl instance";
    ASSERT_EQ(v.getTypeInfo().type, PdCom::TypeInfo::uint8_T)
            << "Variable is unsigned char";
    ASSERT_TRUE(v.getSizeInfo().isScalar()) << "Variable is a scalar";
    unsigned int gid = 0;
    {
        std::unique_ptr<MySubscriber> sub {new MySubscriber(dflt_time)};
        std::unique_ptr<MySubscriber> sub2 {new MySubscriber(2 * dflt_time)};
        Subscription s1 {*sub, v};

        EXPECT_EQ(sub->newValues_count_, 0)
                << "sub.newValues() was never called";
        ASSERT_TRUE(my_process->answer_signal_subscription_requests(0, 1));
        ASSERT_EQ(my_process->subscribed_group_ids.size(), 1)
                << "Exactly one group id is used for the subscriptions";
        gid = my_process->subscribed_group_ids.front();
        {
            MyProcess::SendDataManager sm(*my_process, false, 47.11, gid);
            sm.addTime();
            sm.addData();
        }

        ASSERT_EQ(sub->state_changed_notifications_.size(), 2)
                << "Subscription 1 recieved exactly two status callbacks";
        EXPECT_EQ(
                sub->state_changed_notifications_.front().state_,
                PdCom::Subscription::State::Pending);
        ASSERT_EQ(
                sub->state_changed_notifications_[1].state_,
                PdCom::Subscription::State::Active)
                << "Subscribtion 1 has active state";

        Subscription s2 {*sub, *my_process, "/signal1"};
        ASSERT_TRUE(my_process->answer_signal_subscription_requests(0, 0))
                << "No extra <xsad>, because variable and period is the same";
        my_process->callPendingCallbacks();
        ASSERT_EQ(sub->state_changed_notifications_.size(), 3)
                << "Subscription 2 recieved exactly one status callback";
        ASSERT_EQ(
                sub->state_changed_notifications_.back().state_,
                PdCom::Subscription::State::Active)
                << "Subscribtion 2 has active state immediately";

        const auto s1_old_val_count = sub->newValues_count_;
        Subscription s3 {*sub2, v};
        EXPECT_EQ(sub2->newValues_count_, 0)
                << "sub2.newValues() was never called";
        ASSERT_TRUE(my_process->answer_signal_subscription_requests(0, 1));
        ASSERT_EQ(my_process->subscribed_group_ids.size(), 2);
        ASSERT_NE(
                my_process->subscribed_group_ids[0],
                my_process->subscribed_group_ids[1])
                << "Different group ids for different intervals";
        const unsigned int gid2 = my_process->subscribed_group_ids.back();
        {
            MyProcess::SendDataManager sm(*my_process, false, 47.11, gid2);
            sm.addTime();
            sm.addData();
        }
        EXPECT_EQ(sub2->newValues_count_, 1) << "S3 got new values";
        ASSERT_EQ(sub->newValues_count_, s1_old_val_count)
                << "S1 newValuesCount stays unaffected";
    }
    // transmit one extra <data> packet to detect use-after-free
    const auto extra_data = [&]() {
        MyProcess::SendDataManager sm(*my_process, false, 47.11, gid);
        sm.addTime({1});
        sm.addData();
    };
    EXPECT_NO_THROW(extra_data())
            << "Sending extra <data> does not cause an exception";
    EXPECT_TRUE(my_process->answer_signal_unsubscribe_requests(2));
    EXPECT_NO_THROW(extra_data())
            << "Sending extra <data> does not cause an exception";
    EXPECT_TRUE(my_process->has_no_group_subscriptions());
}


TEST_F(MyProcessTest, PeriodicSubscriptonWithinDataTag)
{
    constexpr std::chrono::duration<double> dflt_time {0.2};
    EXPECT_FALSE(my_process->find("/signal1"))
            << "unknown signal cannot be cached";

    EXPECT_EQ(my_process->getReadChannelRequests(), 1);
    EXPECT_EQ(my_process->getReadParamRequests(), 1);
    ASSERT_TRUE(my_process->answer_rk_rp_requests(1, 0, 0, 1));
    ASSERT_EQ(my_process->found_vars.size(), 1) << "Found one signal";
    const auto v = std::move(my_process->found_vars[0]);
    my_process->found_vars.clear();
    ASSERT_FALSE(v.empty()) << "Variable must have a valid pimpl instance";
    unsigned int gid = 0;
    {
        std::unique_ptr<MySubscriber> sub {new MySubscriber(dflt_time)};
        Subscription s1 {*sub, v};

        EXPECT_EQ(sub->newValues_count_, 0)
                << "sub.newValues() was never called";
        ASSERT_TRUE(my_process->answer_signal_subscription_requests(0, 1));
        ASSERT_EQ(s1.getState(), PdCom::Subscription::State::Active);
        ASSERT_EQ(my_process->subscribed_group_ids.size(), 1)
                << "Exactly one group id is used for the subscriptions";
        gid = my_process->subscribed_group_ids.front();
        {
            MyProcess::SendDataManager sm(
                    *my_process, false, 47.11, gid, false);
            sm.addTime();
            sm.addData();
        }

        Subscription s2 {*sub, v};
        my_process->read_buffer << "</data>";
        my_process->flush_read_buffer();
        EXPECT_EQ(my_process->getReadChannelRequests(), 0);
        EXPECT_EQ(my_process->getReadParamRequests(), 0);
        ASSERT_TRUE(my_process->answer_rk_rp_requests(0, 0, 0, 0));
        ASSERT_TRUE(my_process->answer_signal_subscription_requests(0, 0));
        ASSERT_EQ(s2.getState(), PdCom::Subscription::State::Active);
    }
}


TEST_F(MyProcessTest, PeriodicSubscriptonAtDataTagEnd)
{
    class Sub : public MySubscriber
    {
      public:
        Subscription sub2_;
        PdCom::Variable var_;

        Sub(std::chrono::duration<double> period, PdCom::Variable var) :
            MySubscriber(period), var_(var)
        {}

        void newValues(std::chrono::nanoseconds time_ns) override
        {
            sub2_ = Subscription {*this, var_};
            MySubscriber::newValues(time_ns);
        }
    };
    constexpr std::chrono::duration<double> dflt_time {0.2};
    EXPECT_FALSE(my_process->find("/signal1"))
            << "unknown signal cannot be cached";

    EXPECT_EQ(my_process->getReadChannelRequests(), 1);
    EXPECT_EQ(my_process->getReadParamRequests(), 1);
    ASSERT_TRUE(my_process->answer_rk_rp_requests(1, 0, 0, 1));
    ASSERT_EQ(my_process->found_vars.size(), 1) << "Found one signal";
    const auto v = std::move(my_process->found_vars[0]);
    my_process->found_vars.clear();
    ASSERT_FALSE(v.empty()) << "Variable must have a valid pimpl instance";
    unsigned int gid = 0;
    {
        std::unique_ptr<Sub> sub {new Sub(dflt_time, v)};
        Subscription s1 {*sub, v};

        EXPECT_EQ(sub->newValues_count_, 0)
                << "sub.newValues() was never called";
        ASSERT_TRUE(my_process->answer_signal_subscription_requests(0, 1));
        ASSERT_EQ(s1.getState(), PdCom::Subscription::State::Active);
        ASSERT_EQ(my_process->subscribed_group_ids.size(), 1)
                << "Exactly one group id is used for the subscriptions";
        gid = my_process->subscribed_group_ids.front();
        {
            MyProcess::SendDataManager sm(
                    *my_process, false, 47.11, gid, false);
            sm.addTime();
            sm.addData();
        }

        my_process->read_buffer << "</data>";
        // we now create another subscription while iterating through all active
        // subscriptions. STL container love that.
        my_process->flush_read_buffer();
        EXPECT_GE(sub->newValues_count_, 1)
                << "newValues() called at least once";
        EXPECT_EQ(my_process->getReadChannelRequests(), 0);
        EXPECT_EQ(my_process->getReadParamRequests(), 0);
        ASSERT_TRUE(my_process->answer_rk_rp_requests(0, 0, 0, 0));
        ASSERT_TRUE(my_process->answer_signal_subscription_requests(0, 0));
        ASSERT_EQ(sub->sub2_.getState(), PdCom::Subscription::State::Active);
    }
}

TEST_F(MyProcessTest, PollNaN)
{
    auto &sig5 = dynamic_cast<typename MyProcess::FifthSignalT &>(
            *my_process->accessSignals().at("/signal5"));

    constexpr size_t index        = 2;
    sig5.data_container.at(index) = -NAN;
    const auto expected           = sig5.data_container;

    MySubscriber subscriber {PdCom::poll_mode};
    PdCom::Subscription subscription {subscriber, *my_process, "/signal5"};
    ASSERT_TRUE(my_process->answer_rk_rp_requests(1, 0, 0, 1));
    EXPECT_EQ(subscription.getState(), PdCom::Subscription::State::Active);
    subscription.poll();
    ASSERT_TRUE(my_process->answer_rk_rp_requests(1, 0, 0, 0));
    MyProcess::DoubleVectorType v;
    subscription.getValue(v);
    // NaN != NaN
    EXPECT_NE(v.at(index), v.at(index));
    for (unsigned int i = 0; i < v.size(); ++i) {
        if (i == index)
            continue;
        EXPECT_EQ(v[i], expected[i])
                << "other entries should be unaffected, index " << i;
    }
}

TEST_F(MyProcessTest, PeriodicSubscriptonWithMultipleUnknownSignal)
{
    constexpr std::chrono::duration<double> dflt_time {0.2};
    auto &sig1 = dynamic_cast<typename MyProcess::FirstSignalT &>(
            *my_process->accessSignals().at("/signal1"));
    auto &sig2 = dynamic_cast<typename MyProcess::SecondSignalT &>(
            *my_process->accessSignals().at("/signal2"));
    int gid                 = 0;
    const auto check_values = [](Subscription const &s1, Subscription const &s2,
                                 unsigned char const exp_1, double const exp_2,
                                 const char *msg1 = "Value mismatch",
                                 const char *msg2 = "Value mismatch") {
        unsigned char act_1 = 0;
        double act_2        = 0.0;
        s1.getValue(act_1);
        s2.getValue(act_2);
        EXPECT_EQ(act_1, exp_1) << msg1;
        EXPECT_EQ(act_2, exp_2) << msg2;
    };

    {
        std::unique_ptr<MySubscriber> sub {new MySubscriber(dflt_time)};
        Subscription s1 {*sub, *my_process, "/signal1"};
        Subscription s2 {*sub, *my_process, "/signal2"};
        EXPECT_EQ(s1.getState(), PdCom::Subscription::State::Pending);
        EXPECT_EQ(s2.getState(), PdCom::Subscription::State::Pending);
        EXPECT_EQ(my_process->getReadChannelRequests(), 2);
        EXPECT_EQ(my_process->getReadParamRequests(), 2);
        ASSERT_TRUE(my_process->answer_rk_rp_requests(2, 0, 0, 2));

        EXPECT_EQ(sub->newValues_count_, 0)
                << "sub.newValues() was never called";
        ASSERT_TRUE(my_process->answer_signal_subscription_requests(0, 2));
        ASSERT_EQ(my_process->subscribed_group_ids.size(), 2)
                << "One group id for both subscriptions";
        ASSERT_EQ(
                my_process->subscribed_group_ids[0],
                my_process->subscribed_group_ids[1])
                << "Group id must be the same for one interval";
        EXPECT_EQ(sub->newValues_count_, 0)
                << "sub.newValues() was never called";
        ASSERT_EQ(sub->state_changed_notifications_.size(), 4)
                << "All status updates were recieved, without calling "
                   "callPendingCallbacks()";
        for (int i = 0; i < 2; ++i) {
            EXPECT_EQ(
                    sub->state_changed_notifications_[i].state_,
                    PdCom::Subscription::State::Pending)
                    << "Subscribtion " << i << " has Pending state";
            EXPECT_EQ(
                    sub->state_changed_notifications_[i + 2].state_,
                    PdCom::Subscription::State::Active)
                    << "Subscribtion " << i << " has active state";
        }
        gid = my_process->subscribed_group_ids.front();
        {
            MyProcess::SendDataManager sm(*my_process, false, 47.11, gid);
            sm.addTime();
            sm.addData();
        }

        EXPECT_EQ(sub->newValues_count_, 1)
                << "sub.newValues() was called once";

        auto v1 = s1.getVariable();
        ASSERT_EQ(v1.getTypeInfo().type, PdCom::TypeInfo::uint8_T)
                << "Variable is unsigned char";
        ASSERT_TRUE(v1.getSizeInfo().isScalar()) << "Variable is a scalar";

        auto v2 = s2.getVariable();
        ASSERT_EQ(v2.getTypeInfo().type, PdCom::TypeInfo::double_T)
                << "Variable is double";
        ASSERT_TRUE(v2.getSizeInfo().isScalar()) << "Variable is a scalar";
        check_values(s1, s2, 'a', 7.0);

        const unsigned char sig1_v1 = '5';
        const unsigned char sig1_v2 = 'C';
        const unsigned char sig1_v3 = '/';
        const unsigned char sig1_v4 = '=';
        const double sig2_v1        = 10.0;
        sig1.data_container         = sig1_v1;
        sig2.data_container         = sig2_v1;
        {
            MyProcess::SendDataManager sm(*my_process, false, 47.11, gid);
            sm.addTime();
            sm.addData();
        }

        EXPECT_EQ(sub->newValues_count_, 2)
                << "sub.newValues() was called again after <data>";
        check_values(s1, s2, sig1_v1, sig2_v1);
        {
            std::unique_ptr<MySubscriber> sub2 {
                    new MySubscriber(2 * dflt_time)};
            const auto old_bn_count = sub->newValues_count_;
            Subscription s11 {*sub2, *my_process, "/signal1"};
            Subscription s22 {*sub2, *my_process, "/signal2"};
            ASSERT_TRUE(my_process->answer_signal_subscription_requests(0, 2));
            ASSERT_EQ(my_process->subscribed_group_ids.size(), 4);
            ASSERT_NE(
                    my_process->subscribed_group_ids.front(),
                    my_process->subscribed_group_ids.back())
                    << "Group id must be the different for two intervals";
            sig1.data_container = sig1_v2;
            {
                MyProcess::SendDataManager sm(
                        *my_process, false, 47.11,
                        my_process->subscribed_group_ids.back());
                sm.addTime();
                sm.addData();
            }
            EXPECT_EQ(sub2->newValues_count_, 1)
                    << "bn2.newValues was called once";
            check_values(s11, s22, sig1_v2, sig2_v1);
            EXPECT_EQ(sub->newValues_count_, old_bn_count)
                    << "other block notification must not be changed";
            check_values(
                    s1, s2, sig1_v1, sig2_v1,
                    "Subscription 1 of subscriber 1 is not affected",
                    "Subscription 2 of subscriber 1 is not affected");
            {
                MySubscriber sub3 {dflt_time};
                Subscription s33 {sub3, *my_process, "/signal1"};
                EXPECT_EQ(s33.getState(), PdCom::Subscription::State::Active)
                        << "Subscription to signal 1 is active immediately";
                ASSERT_EQ(my_process->subscribed_group_ids.size(), 4)
                        << "number of signal subscriptions has not changed";
                sig1.data_container = sig1_v3;
                // sending update for subscriber 1 and 3 (which have the same
                // group)
                {
                    MyProcess::SendDataManager sm(
                            *my_process, false, 47.11,
                            my_process->subscribed_group_ids.front());
                    sm.addTime();
                    sm.addData();
                }
                EXPECT_EQ(sub3.newValues_count_, 1);
                {
                    unsigned char c = 0;
                    s33.getValue(c);
                    EXPECT_EQ(c, sig1_v3) << "Value mismatch";
                }
                check_values(
                        s11, s22, sig1_v2, sig2_v1,
                        "Subscription 1 of subscriber 2 is not affected",
                        "Subscription 2 of subscriber 2 is not affected");
                check_values(s1, s2, sig1_v3, sig2_v1);
            }
        }
        EXPECT_TRUE(my_process->answer_signal_unsubscribe_requests(2));
        {
            std::unique_ptr<MySubscriber> sub2 {new MySubscriber(dflt_time)};
            Subscription s11 {*sub2, *my_process, "/signal1"};
            Subscription s22 {*sub2, *my_process, "/signal3"};
            EXPECT_EQ(s11.getState(), PdCom::Subscription::State::Active)
                    << "Subscription to signal 1 is active immediately";
            EXPECT_EQ(s22.getState(), PdCom::Subscription::State::Pending)
                    << "Subscription to signal 3 is pending, as it's still "
                       "unknown";
            EXPECT_EQ(my_process->getReadChannelRequests(), 1);
            EXPECT_EQ(my_process->getReadParamRequests(), 1);
            ASSERT_TRUE(my_process->answer_rk_rp_requests(1, 0, 0, 1));
            ASSERT_TRUE(my_process->answer_signal_subscription_requests(0, 1));
            EXPECT_EQ(s22.getState(), PdCom::Subscription::State::Active)
                    << "Subscription to signal 3 is now active";
            const auto num_groups =
                    std::set<unsigned>(
                            my_process->subscribed_group_ids.begin(),
                            my_process->subscribed_group_ids.end())
                            .size();
            ASSERT_EQ(num_groups, 2);
            ASSERT_EQ(
                    my_process->subscribed_group_ids.front(),
                    my_process->subscribed_group_ids.back())
                    << "signal with same interval is added to same group";
            sig1.data_container = sig1_v4;
            {
                MyProcess::SendDataManager sm(
                        *my_process, false, 47.11,
                        my_process->subscribed_group_ids.back());
                sm.addTime();
                sm.addData();
            }
            EXPECT_EQ(sub2->newValues_count_, 1)
                    << "bn2.newValues was called once";
        }
        EXPECT_TRUE(my_process->answer_signal_unsubscribe_requests(1))
                << "Signal 3 was unsubscribed";
        s2 = Subscription();
        EXPECT_TRUE(my_process->answer_signal_unsubscribe_requests(1))
                << "Signal 2 was unsubscribed";
        sub->newValues_count_ = 0;
        {
            MyProcess::SendDataManager sm(
                    *my_process, false, 47.11,
                    my_process->subscribed_group_ids.back());
            sm.addTime();
            sm.addData();
        }
        ASSERT_EQ(sub->newValues_count_, 1) << "only one signal left";
    }
    // transmit one extra <data> packet to detect use-after-free
    const auto extra_data = [&]() {
        MyProcess::SendDataManager sm(*my_process, false, 47.11, gid);
        sm.addTime({1});
        sm.addData();
    };
    EXPECT_NO_THROW(extra_data())
            << "Sending extra <data> does not cause an exception";
    EXPECT_TRUE(my_process->answer_signal_unsubscribe_requests(1));
    EXPECT_NO_THROW(extra_data())
            << "Sending extra <data> does not cause an exception";
    EXPECT_TRUE(my_process->has_no_group_subscriptions());
}


TEST_F(MyProcessTest, PeriodicBlockSubscriptonWithMultipleUnknownSignal)
{
    auto &sig1 = dynamic_cast<typename MyProcess::FirstSignalT &>(
            *my_process->accessSignals().at("/signal1"));
    auto &sig2 = dynamic_cast<typename MyProcess::SecondSignalT &>(
            *my_process->accessSignals().at("/signal2"));
    constexpr std::chrono::duration<double> dflt_time {0.001};
    std::unique_ptr<MyBlockSubscriber> const sub {
            new MyBlockSubscriber(dflt_time, {}, {})};
    Subscription &s1 {sub->sub1_}, &s2 {sub->sub2_};
    s1 = {*sub, *my_process, "/signal1"};
    EXPECT_EQ(s1.getState(), PdCom::Subscription::State::Pending)
            << "Move assignment operator copies state";
    {
        Subscription tmp {*sub, *my_process, "/signal2"};
        Subscription tmp2(std::move(tmp));
        EXPECT_EQ(tmp.getState(), PdCom::Subscription::State::Invalid)
                << "Moved-from object is invalid";
        EXPECT_EQ(tmp2.getState(), PdCom::Subscription::State::Pending)
                << "Move constructor copies state";
        s2 = std::move(tmp2);
        EXPECT_EQ(tmp2.getState(), PdCom::Subscription::State::Invalid)
                << "Moved-from object is invalid";
    }
    EXPECT_EQ(my_process->getReadChannelRequests(), 2);
    EXPECT_EQ(my_process->getReadParamRequests(), 2);
    ASSERT_TRUE(my_process->answer_rk_rp_requests(2, 0, 0, 2));

    EXPECT_EQ(sub->ts_.size(), 0) << "sub.newValues() was never called";
    ASSERT_TRUE(my_process->answer_signal_subscription_requests(0, 2));
    ASSERT_EQ(my_process->subscribed_group_ids.size(), 2)
            << "One group id for both subscriptions";
    ASSERT_EQ(
            my_process->subscribed_group_ids[0],
            my_process->subscribed_group_ids[1])
            << "Group id must be the same for one block";
    EXPECT_EQ(sub->newValues_count_, 0) << "sub.newValues() was never called";
    ASSERT_EQ(sub->state_changed_notifications_.size(), 4)
            << "All status updates were recieved, without calling "
               "callPendingCallbacks()";
    for (int i = 0; i < 2; ++i) {
        EXPECT_EQ(
                sub->state_changed_notifications_[i].state_,
                PdCom::Subscription::State::Pending)
                << "Subscribtion " << i << " has Pending state";
        EXPECT_EQ(
                sub->state_changed_notifications_[i + 2].state_,
                PdCom::Subscription::State::Active)
                << "Subscribtion " << i << " has active state";
    }
    const int gid          = my_process->subscribed_group_ids.front();
    unsigned int blocksize = [&]() {
        MyProcess::SendDataManager sm(*my_process, false, 47.11, gid);
        // time vector goes from 1 to blocksize
        sm.addTime();
        const unsigned int blocksize = sm.blocksize();
        for (unsigned int i = 0; i < sm.blocksize(); ++i) {
            // filling the variables with some monotonic data
            sig1.data_container = 'a' + i;
            sig2.data_container = -0.5 + i;

            sm.addData();
        }
        return blocksize;
    }();
    ASSERT_GT(blocksize, 1) << "We want to test grouped value transmission";


    ASSERT_EQ(sub->ts_.size(), blocksize)
            << "sub.newValues() was called " << blocksize << " times";

    for (unsigned int i = 0; i < blocksize; ++i) {
        EXPECT_EQ(sub->ts_[i], std::chrono::nanoseconds(i + 1))
                << "Timestamp mismatch";
        EXPECT_EQ(sub->sub1_values_[i], 'a' + i);
        EXPECT_EQ(sub->sub2_values_[i], -0.5 + i);
    }

    auto v1 = s1.getVariable();
    ASSERT_EQ(v1.getTypeInfo().type, PdCom::TypeInfo::uint8_T)
            << "Variable 1 is unsigned char";
    ASSERT_TRUE(v1.getSizeInfo().isScalar()) << "Variable 1 is a scalar";
    auto v2 = s2.getVariable();
    ASSERT_EQ(v2.getTypeInfo().type, PdCom::TypeInfo::double_T)
            << "Variable 2 is double";
    ASSERT_TRUE(v1.getSizeInfo().isScalar()) << "Variable 2 is a scalar";

    std::array<unsigned char, 1> value;
    s1.getValue(value);
    EXPECT_EQ(value[0], sub->sub1_values_.back()) << "Signal value stays valid";

    double val2 = 0.0;
    s2.getValue(val2);
    EXPECT_EQ(val2, sub->sub2_values_.back()) << "Signal value stays valid";
    s1 = {};
    s2 = {};

    EXPECT_TRUE(my_process->answer_signal_unsubscribe_requests(2));
    EXPECT_TRUE(my_process->has_no_group_subscriptions());
}


TEST_F(MyProcessTest, PeriodicSubscriptonWithMultipleKnownSignal)
{
    constexpr std::chrono::duration<double> dflt_time {0.2};
    EXPECT_FALSE(my_process->find("/signal1"))
            << "unknown signal cannot be cached";
    EXPECT_FALSE(my_process->find("/signal2"))
            << "unknown signal cannot be cached";

    EXPECT_EQ(my_process->getReadChannelRequests(), 2);
    EXPECT_EQ(my_process->getReadParamRequests(), 2);
    ASSERT_TRUE(my_process->answer_rk_rp_requests(2, 0, 0, 2));
    ASSERT_EQ(my_process->found_vars.size(), 2) << "Found both signals";
    const auto v1 = std::move(my_process->found_vars[0]),
               v2 = std::move(my_process->found_vars[1]);
    my_process->found_vars.clear();
    ASSERT_FALSE(v1.empty()) << "Variable must have a valid pimpl instance";
    ASSERT_FALSE(v2.empty()) << "Variable must have a valid pimpl instance";
    ASSERT_EQ(v1.getTypeInfo().type, PdCom::TypeInfo::uint8_T)
            << "Variable is unsigned char";
    ASSERT_TRUE(v1.getSizeInfo().isScalar()) << "Variable is a scalar";
    ASSERT_EQ(v2.getTypeInfo().type, PdCom::TypeInfo::double_T)
            << "Variable is double";
    ASSERT_TRUE(v2.getSizeInfo().isScalar()) << "Variable is a scalar";
    unsigned int gid = 0;

    {
        std::unique_ptr<MySubscriber> sub {new MySubscriber(dflt_time)};
        std::unique_ptr<MySubscriber> sub2 {new MySubscriber(2 * dflt_time)};
        Subscription s1 {*sub, v1};
        Subscription s2 {*sub, v2};

        EXPECT_EQ(sub->newValues_count_, 0)
                << "sub.newValues() was never called";
        ASSERT_TRUE(my_process->answer_signal_subscription_requests(0, 2));
        ASSERT_EQ(my_process->subscribed_group_ids.size(), 2);
        ASSERT_EQ(
                my_process->subscribed_group_ids[0],
                my_process->subscribed_group_ids[1])
                << "Group id must equal for one subscriber";
        gid = my_process->subscribed_group_ids.front();
        {
            MyProcess::SendDataManager sm(*my_process, false, 47.11, gid);
            sm.addTime();
            sm.addData();
        }

        ASSERT_EQ(sub->state_changed_notifications_.size(), 4)
                << "two callbacks per subscription";
        ASSERT_EQ(
                sub->state_changed_notifications_[2].state_,
                PdCom::Subscription::State::Active)
                << "Subscribtion 1 has active state";
        ASSERT_EQ(
                sub->state_changed_notifications_[3].state_,
                PdCom::Subscription::State::Active)
                << "Subscribtion 2 has active state";

        EXPECT_EQ(sub->newValues_count_, 1)
                << "sub.newValues() was called once";

        auto v1 = s1.getVariable();
        ASSERT_EQ(v1.getTypeInfo().type, PdCom::TypeInfo::uint8_T)
                << "Variable is unsigned char";
        ASSERT_TRUE(v1.getSizeInfo().isScalar()) << "Variable is a scalar";
        std::array<unsigned char, 1> value;
        s1.getValue(value);
        EXPECT_EQ(value[0], 'a') << "Value mismatch";

        auto v2 = s2.getVariable();
        ASSERT_EQ(v2.getTypeInfo().type, PdCom::TypeInfo::double_T)
                << "Variable is double";
        ASSERT_TRUE(v2.getSizeInfo().isScalar()) << "Variable is a scalar";
        double val2 = 0.0;
        s2.getValue(val2);
        EXPECT_EQ(val2, 7.0) << "Value mismatch";
    }
    // transmit one extra <data> packet to detect use-after-free
    const auto extra_data = [&]() {
        MyProcess::SendDataManager sm(*my_process, false, 47.11, gid);
        sm.addTime({1});
        sm.addData();
    };
    EXPECT_NO_THROW(extra_data())
            << "Sending extra <data> does not cause an exception";
    EXPECT_TRUE(my_process->answer_signal_unsubscribe_requests(2));
    EXPECT_NO_THROW(extra_data())
            << "Sending extra <data> does not cause an exception";
    EXPECT_TRUE(my_process->has_no_group_subscriptions());
}

TEST_F(MyProcessTest,
       PeriodicSubscriptonWithMultipleKnownSignalAndPrematureDataBlock)
{
    constexpr std::chrono::duration<double> dflt_time {0.2};
    EXPECT_FALSE(my_process->find("/signal1"))
            << "unknown signal cannot be cached";
    EXPECT_FALSE(my_process->find("/signal2"))
            << "unknown signal cannot be cached";

    EXPECT_EQ(my_process->getReadChannelRequests(), 2);
    EXPECT_EQ(my_process->getReadParamRequests(), 2);
    ASSERT_TRUE(my_process->answer_rk_rp_requests(2, 0, 0, 2));
    ASSERT_EQ(my_process->found_vars.size(), 2) << "Found both signals";
    const auto v1 = std::move(my_process->found_vars[0]),
               v2 = std::move(my_process->found_vars[1]);
    my_process->found_vars.clear();
    ASSERT_FALSE(v1.empty()) << "Variable must have a valid pimpl instance";
    ASSERT_FALSE(v2.empty()) << "Variable must have a valid pimpl instance";
    ASSERT_EQ(v1.getTypeInfo().type, PdCom::TypeInfo::uint8_T)
            << "Variable is unsigned char";
    EXPECT_EQ(v1.getPath(), "/signal1");
    EXPECT_EQ(v1.getName(), "signal1");
    EXPECT_EQ(v1.getTaskId(), 1);
    ASSERT_TRUE(v1.getSizeInfo().isScalar()) << "Variable is a scalar";
    ASSERT_EQ(v2.getTypeInfo().type, PdCom::TypeInfo::double_T)
            << "Variable is double";
    ASSERT_TRUE(v2.getSizeInfo().isScalar()) << "Variable is a scalar";
    unsigned int gid = 0;

    {
        std::unique_ptr<MySubscriber> sub {new MySubscriber(dflt_time)};
        Subscription s1 {*sub, v1};

        EXPECT_EQ(sub->newValues_count_, 0)
                << "sub.newValues() was never called";
        ASSERT_TRUE(my_process->answer_signal_subscription_requests(0, 1));
        ASSERT_EQ(sub->state_changed_notifications_.size(), 2)
                << "Subscription 1 recieved exactly two status callbacks";
        ASSERT_EQ(
                sub->state_changed_notifications_[1].state_,
                PdCom::Subscription::State::Active)
                << "Subscribtion 1 has active state";
        Subscription s2 {*sub, v2};
        EXPECT_EQ(s2.getState(), PdCom::Subscription::State::Pending)
                << "Subscription 2 is still pending";
        ASSERT_EQ(my_process->subscribed_group_ids.size(), 1);
        gid                          = my_process->subscribed_group_ids.front();
        auto send_premature_data_tag = [&]() {
            MyProcess::SendDataManager sm(*my_process, false, 47.11, gid);
            sm.addTime({4711});
            sm.addData({0});
        };
        ASSERT_NO_THROW(send_premature_data_tag())
                << "Sending a <data> packet with only the first signal, as "
                   "<xsad> has not reached the server yet";

        ASSERT_TRUE(my_process->answer_signal_subscription_requests(0, 1));
        ASSERT_EQ(my_process->subscribed_group_ids.size(), 2);
        ASSERT_EQ(
                my_process->subscribed_group_ids[0],
                my_process->subscribed_group_ids[1])
                << "Group id must equal for one subscriber";

        ASSERT_EQ(sub->state_changed_notifications_.size(), 4)
                << "Two callbacks per subscription";
        ASSERT_EQ(
                sub->state_changed_notifications_[3].state_,
                PdCom::Subscription::State::Active)
                << "Subscribtion 2 has active state";

        EXPECT_EQ(sub->newValues_count_, 1)
                << "sub.newValues() was called once";

        auto v1 = s1.getVariable();
        ASSERT_EQ(v1.getTypeInfo().type, PdCom::TypeInfo::uint8_T)
                << "Variable is unsigned char";
        ASSERT_TRUE(v1.getSizeInfo().isScalar()) << "Variable is a scalar";
        std::array<unsigned char, 1> value;
        s1.getValue(value);
        EXPECT_EQ(value[0], 'a') << "Value mismatch";


        dynamic_cast<typename MyProcess::FirstSignalT &>(
                *my_process->accessSignals().at("/signal1"))
                .data_container = 'Y';
        dynamic_cast<typename MyProcess::SecondSignalT &>(
                *my_process->accessSignals().at("/signal2"))
                .data_container = -7.5;

        EXPECT_THROW(v1.setValue('J'), PdCom::InvalidArgument)
                << "Signal is read-only";

        {
            MyProcess::SendDataManager sm(*my_process, false, 47.11, gid);
            sm.addTime();
            sm.addData();
        }

        EXPECT_EQ(sub->newValues_count_, 2)
                << "sub.newValues() was called another time";

        s1.getValue(value);
        EXPECT_EQ(value[0], 'Y') << "Value mismatch";

        auto v2 = s2.getVariable();
        ASSERT_EQ(v2.getTypeInfo().type, PdCom::TypeInfo::double_T)
                << "Variable is double";
        ASSERT_TRUE(v2.getSizeInfo().isScalar()) << "Variable is a scalar";
        double val2 = 0.0;
        s2.getValue(val2);
        EXPECT_EQ(val2, -7.5) << "Value mismatch";
    }
    // transmit one extra <data> packet to detect use-after-free
    const auto extra_data = [&]() {
        MyProcess::SendDataManager sm(*my_process, false, 47.11, gid);
        sm.addTime({1});
        sm.addData();
    };
    EXPECT_NO_THROW(extra_data())
            << "Sending extra <data> does not cause an exception";
    EXPECT_TRUE(my_process->answer_signal_unsubscribe_requests(2));
    EXPECT_NO_THROW(extra_data())
            << "Sending extra <data> does not cause an exception";
    EXPECT_TRUE(my_process->has_no_group_subscriptions());
}

TEST_F(MyProcessTest, ChangePeriodWhileDataOnTheFly)
{
    /* In this testcase, the period of a subscription is changed, but at the
     * time the period is updated on the client side one <data> element arrives
     * from the server.
     * See also https://gitlab.com/etherlab.org/testmanager/-/issues/25
     */
    constexpr std::chrono::duration<double> t1 {0.1}, t2 {0.01};
    EXPECT_FALSE(my_process->find("/signal1"))
            << "unknown signal cannot be cached";

    ASSERT_TRUE(my_process->answer_rk_rp_requests(1, 0, 0, 1));
    ASSERT_EQ(my_process->found_vars.size(), 1);
    const auto var = my_process->found_vars.front();
    int gid;

    {
        MySubscriber sub1 {t1};
        Subscription s1 {sub1, var};

        ASSERT_TRUE(my_process->answer_signal_subscription_requests(0, 1));
        ASSERT_EQ(my_process->subscribed_group_ids.size(), 1);
        gid = my_process->subscribed_group_ids.front();
        ASSERT_EQ(s1.getState(), Subscription::State::Active);
        {
            MyProcess::SendDataManager sm(*my_process, false, 47.11, gid);
            sm.addTime({1});
            sm.addData();
        }

        EXPECT_EQ(sub1.newValues_count_, 1);
    }

    {
        MySubscriber sub2 {t2};
        Subscription s1 {sub2, var};

        // one <data> for the old subscription is still on the wire
        ASSERT_NO_THROW({
            MyProcess::SendDataManager sm(*my_process, false, 47.11, gid);
            sm.addTime({1});
            sm.addData();
        });

        ASSERT_TRUE(my_process->answer_signal_unsubscribe_requests(1));
        ASSERT_TRUE(my_process->answer_signal_subscription_requests(0, 1));
        ASSERT_EQ(s1.getState(), Subscription::State::Active);
        EXPECT_EQ(sub2.newValues_count_, 0)
                << "Subscription with other period does not receive <data> tag "
                   "for already unsubscribed subscription";

        gid = my_process->subscribed_group_ids.back();
        unsigned blocksize;
        ASSERT_NO_THROW({
            MyProcess::SendDataManager sm(*my_process, false, 47.74, gid);
            blocksize = sm.blocksize();
            std::vector<uint64_t> ts;
            for (unsigned i = 0; i < blocksize; ++i)
                ts.push_back(2 * i);
            sm.addTime(ts);
            for (unsigned i = 0; i < blocksize; ++i)
                sm.addData();
        });
        EXPECT_EQ(sub2.newValues_count_, blocksize)
                << "Receive one <data> with sufficient data blocks";
    }
}

TEST_F(MyProcessTest, SubscriptonWithKnownSignalArray)
{
    auto &sig = dynamic_cast<MyProcess::FourthSignalT &>(
            *my_process->accessSignals().at("/signal4"));
    constexpr std::chrono::duration<double> dflt_time {0.2};
    EXPECT_FALSE(my_process->find("/signal4"))
            << "unknown signal cannot be cached";

    EXPECT_EQ(my_process->getReadChannelRequests(), 1);
    EXPECT_EQ(my_process->getReadParamRequests(), 1);
    ASSERT_TRUE(my_process->answer_rk_rp_requests(1, 0, 0, 1));
    ASSERT_EQ(my_process->found_vars.size(), 1) << "Found signal";
    const auto v1 = std::move(my_process->found_vars[0]);
    my_process->found_vars.clear();
    ASSERT_FALSE(v1.empty()) << "Variable must have a valid pimpl instance";
    ASSERT_EQ(v1.getTypeInfo().type, PdCom::TypeInfo::int32_T)
            << "Variable is integer";
    ASSERT_TRUE(v1.getSizeInfo().isVector()) << "Variable is a Vector";
    EXPECT_FALSE(v1.getSizeInfo().isScalar());
    EXPECT_FALSE(v1.getSizeInfo().is2DMatrix());
    ASSERT_EQ(v1.getSizeInfo()[0], sig.data_container.size()) << "Dimension";
    unsigned int gid = 0;
    {
        std::unique_ptr<MySubscriber> sub {new MySubscriber(dflt_time)};
        Subscription s1 {*sub, v1};
        Subscription s2 {*sub, v1, PdCom::ScalarSelector({2})};
        EXPECT_THROW(
                (Subscription {*sub, v1, PdCom::ScalarSelector({4711})}),
                PdCom::InvalidArgument)
                << "Invalid index for ScalarSelector";

        EXPECT_EQ(sub->newValues_count_, 0)
                << "sub.newValues() was never called";

        ASSERT_TRUE(my_process->answer_signal_subscription_requests(0, 1));
        ASSERT_EQ(sub->state_changed_notifications_.size(), 4)
                << "Two callbacks per subscription";
        ASSERT_EQ(
                sub->state_changed_notifications_[0].state_,
                PdCom::Subscription::State::Pending);
        ASSERT_EQ(
                sub->state_changed_notifications_[1].state_,
                PdCom::Subscription::State::Pending);
        ASSERT_EQ(
                sub->state_changed_notifications_[2].state_,
                PdCom::Subscription::State::Active);
        ASSERT_EQ(
                sub->state_changed_notifications_[3].state_,
                PdCom::Subscription::State::Active);
        {
            MyProcess::VectorType value = {};
            s1.getValue(value);
            for (const auto v : value)
                EXPECT_EQ(v, 0) << "Subscription value is zero initialized";
        }
        ASSERT_EQ(my_process->subscribed_group_ids.size(), 1);
        gid = my_process->subscribed_group_ids.front();
        {
            MyProcess::SendDataManager sm(*my_process, false, 47.11, gid);
            sm.addTime();
            sm.addData();
        }

        EXPECT_EQ(sub->newValues_count_, 1)
                << "sub.newValues() was called once";

        MyProcess::VectorType value = {};
        s1.getValue(value);
        for (unsigned int i = 0; i < value.size(); ++i)
            EXPECT_EQ(value[i], i + 4) << "Value mismatch periodic";
        int v = 4711;
        s2.getValue(v);
        EXPECT_EQ(v, 6) << "ScalarSelector value mismatch periodic";

        sig.data_container = {{10, 11, 12, 13, 14}};
        s1.poll();
        ASSERT_TRUE(my_process->answer_rk_rp_requests(1, 0, 0, 0));

        s1.getValue(value);
        for (unsigned int i = 0; i < value.size(); ++i)
            EXPECT_EQ(value[i], i + 10) << "Value mismatch poll";
        {
            std::ostringstream os;
            s1.print(os, ' ');
            EXPECT_EQ(os.str(), "[10 11 12 13 14]") << "Vector print failed";
        }
        {
            double val2[MyProcess::VectorSize] = {1, 2, 3, 4, 5};
            s1.getValue(val2);
            for (unsigned int i = 0; i < MyProcess::VectorSize; ++i)
                EXPECT_EQ(val2[i], i + 10) << "Value mismatch c array";
        }
        {
            static_assert(MyProcess::VectorSize == 5, "");
            double val2[5]      = {1, 2, 3, 4, 5};
            const size_t offset = 2;
            s1.getValue(val2, 2);
            for (unsigned int i = 0; i < 3; ++i) {
                EXPECT_EQ(val2[i], i + 10 + offset)
                        << "Value mismatch c array with offset";
            }
            EXPECT_EQ(val2[3], 4) << "data is unmodified";
            EXPECT_EQ(val2[4], 5) << "data is unmodified";
        }
        {
            std::deque<float> val3;
            val3.resize(3);
            ASSERT_EQ(val3.size(), 3);
            const size_t offset = 1;
            s1.getValue(val3, offset);
            for (unsigned int i = 0; i < 3; ++i) {
                EXPECT_EQ(val3[i], i + 10 + offset)
                        << "Value mismatch map with offset";
            }
            EXPECT_EQ(val3.size(), 3) << "no element was added";
            EXPECT_THROW(s1.getValue(val3, 500), PdCom::InvalidArgument)
                    << "offset is too large";
        }

        s2.getValue(v);
        EXPECT_EQ(v, 6) << "second subscription is unaffected by poll";


        sig.data_container = {{20, 21, 22, 23, 24}};
        s2.poll();
        ASSERT_TRUE(my_process->answer_rk_rp_requests(1, 0, 0, 0));

        s1.getValue(value);
        for (unsigned int i = 0; i < value.size(); ++i)
            EXPECT_EQ(value[i], i + 10)
                    << "First subscription is unaffected by poll";

        s2.getValue(v);
        EXPECT_EQ(v, 22) << "ScalarSelector value mismatch poll";
    }
    // transmit one extra <data> packet to detect use-after-free
    const auto extra_data = [&]() {
        MyProcess::SendDataManager sm(*my_process, false, 47.11, gid);
        sm.addTime({1});
        sm.addData();
    };
    EXPECT_NO_THROW(extra_data())
            << "Sending extra <data> does not cause an exception";
    EXPECT_TRUE(my_process->answer_signal_unsubscribe_requests(1));
    EXPECT_NO_THROW(extra_data())
            << "Sending extra <data> does not cause an exception";
    EXPECT_TRUE(my_process->has_no_group_subscriptions());
}
template <bool with_xsap = true>
static void _doSubscriptonWithKnownParameterArray(MyProcess *my_process)
{
    using CArrayT = short[MyProcess::MatrixRows][MyProcess::MatrixColumns];

    auto &param = dynamic_cast<FakeParameter<MyProcess::MatrixType> &>(
            *my_process->accessParameters().at("/params/param02"));
    EXPECT_FALSE(my_process->find("/params/param02"));

    EXPECT_EQ(my_process->getReadChannelRequests(), 1);
    EXPECT_EQ(my_process->getReadParamRequests(), 1);
    ASSERT_TRUE(my_process->answer_rk_rp_requests(0, 1, 1, 0));
    ASSERT_EQ(my_process->found_vars.size(), 1) << "Found signal";
    const auto p1 = std::move(my_process->found_vars[0]);
    ASSERT_FALSE(p1.empty());
    my_process->found_vars.clear();
    EXPECT_FALSE(p1.getSizeInfo().isVector());
    EXPECT_FALSE(p1.getSizeInfo().isScalar());
    ASSERT_TRUE(p1.getSizeInfo().is2DMatrix()) << "Parameter is a matrix";
    ASSERT_EQ(p1.getSizeInfo().dimension(), 2);
    ASSERT_EQ(p1.getSizeInfo()[0], param.data_container.size())
            << "Row count mismatch";
    ASSERT_EQ(p1.getSizeInfo()[1], param.data_container[0].size())
            << "Column count mismatch";

    MySubscriber sub {PdCom::event_mode};
    {
        Subscription s1 {sub, p1};
        EXPECT_THROW(
                (Subscription {sub, p1, PdCom::ScalarSelector({1})}),
                PdCom::InvalidArgument)
                << "ScalarSelector expects two dimensions for Matrix";
        EXPECT_THROW(
                (Subscription {
                        sub, p1,
                        PdCom::ScalarSelector({MyProcess::MatrixRows, 0})}),
                PdCom::InvalidArgument)
                << "Row out of range";
        EXPECT_THROW(
                (Subscription {
                        sub, p1,
                        PdCom::ScalarSelector({0, MyProcess::MatrixColumns})}),
                PdCom::InvalidArgument)
                << "Column out of range";
        Subscription s2 {sub, p1, PdCom::ScalarSelector({0, 1})};

        ASSERT_TRUE(
                my_process->answer_parameter_subscription_requests(with_xsap));
        s1.poll();
        s2.poll();
        ASSERT_TRUE(my_process->answer_rk_rp_requests(0, 0, 1, 0))
                << "Polling both subscriptions is grouped";
        ASSERT_EQ(sub.newValues_count_, 2);
        {
            CArrayT buf;
            s1.getValue(buf);
            for (unsigned row = 0; row < MyProcess::MatrixRows; ++row)
                for (unsigned col = 0; col < MyProcess::MatrixColumns; ++col)
                    EXPECT_EQ(buf[row][col], MyProcess::param02Init[row][col])
                            << "Value mismatch";
            short buf2;
            s2.getValue(buf2);
            EXPECT_EQ(buf2, MyProcess::param02Init[0][1])
                    << "ScalarSelector value mismatch";
        }
        sub.newValues_count_ = 0;
        {
            CArrayT const send_buf = {{11, 12}, {13, 14}, {15, 16}};
            ASSERT_TRUE(my_process->setValue(p1, send_buf));
            my_process->flush_read_buffer();
            ASSERT_TRUE(my_process->answer_rk_rp_requests(0, 0, !with_xsap, 0));
            ASSERT_EQ(sub.newValues_count_, 2);
            CArrayT buf;
            s1.getValue(buf);
            for (unsigned row = 0; row < MyProcess::MatrixRows; ++row)
                for (unsigned col = 0; col < MyProcess::MatrixColumns; ++col)
                    EXPECT_EQ(buf[row][col], send_buf[row][col])
                            << "Value mismatch";
            short buf2;
            s2.getValue(buf2);
            EXPECT_EQ(buf2, send_buf[0][1]) << "ScalarSelector value mismatch";
        }
    }
}


TEST_F(MyProcessTest, DISABLED_SubscriptonWithKnownParameterArray)
{
    _doSubscriptonWithKnownParameterArray(my_process.get());
}

TEST_F(MyProcessTest, RemoveEventSubscriptionAtDataTagParsingTime)
{
    constexpr std::chrono::duration<double> dflt_time {0.2};
    EXPECT_FALSE(my_process->find("/signal1"))
            << "unknown signal cannot be cached";

    EXPECT_EQ(my_process->getReadChannelRequests(), 1);
    EXPECT_EQ(my_process->getReadParamRequests(), 1);
    ASSERT_TRUE(my_process->answer_rk_rp_requests(1, 0, 0, 1));
    ASSERT_EQ(my_process->found_vars.size(), 1) << "Found one signals";
    const auto v1 = std::move(my_process->found_vars[0]);
    my_process->found_vars.clear();
    ASSERT_FALSE(v1.empty()) << "Variable must have a valid pimpl instance";
    ASSERT_EQ(v1.getTypeInfo().type, PdCom::TypeInfo::uint8_T)
            << "Variable is unsigned char";
    ASSERT_TRUE(v1.getSizeInfo().isScalar()) << "Variable is a scalar";
    unsigned int gid = 0;


    MySubscriber sub(PdCom::event_mode), sub2(2 * dflt_time);
    std::unique_ptr<Subscription> s1 {new Subscription(sub, v1)},
            s2 {new Subscription(sub2, v1)};

    EXPECT_EQ(sub.newValues_count_, 0) << "sub.newValues() was never called";
    EXPECT_EQ(sub2.newValues_count_, 0) << "sub2.newValues() was never called";
    ASSERT_TRUE(my_process->answer_signal_subscription_requests(1, 1));

    ASSERT_EQ(sub.state_changed_notifications_.size(), 2)
            << "Subscription 1 recieved exactly two status callbacks";
    ASSERT_EQ(
            sub.state_changed_notifications_.back().state_,
            PdCom::Subscription::State::Active)
            << "Subscribtion 1 has active state";
    ASSERT_EQ(sub2.state_changed_notifications_.size(), 2)
            << "Subscription 2 recieved exactly two status callbacks";
    ASSERT_EQ(
            sub2.state_changed_notifications_.back().state_,
            PdCom::Subscription::State::Active)
            << "Subscribtion 2 has active state";
    ASSERT_EQ(my_process->subscribed_group_ids.size(), 2);
    gid = my_process->subscribed_group_ids.front();
    {
        MyProcess::SendDataManager sm(*my_process, true, 47.11, gid);
        s1.reset();
        my_process->flush_read_buffer();
        sm.addTime();
        sm.addData();
    }

    EXPECT_EQ(sub.newValues_count_, 0) << "sub.newValues() was not called";
    const unsigned gid2 = my_process->subscribed_group_ids.back();
    {
        MyProcess::SendDataManager sm(*my_process, false, 47.11, gid2);
        sm.addTime();
        sm.addData();
    }

    EXPECT_EQ(sub2.newValues_count_, 1) << "ProtocolHandler still works";
}

void MyProcessTest::_doRemoveEventSubscriptionAtDataTagParsingTime(
        void (*cbk)(unsigned int gid, std::unique_ptr<PdCom::Subscription> &))
{
    constexpr std::chrono::duration<double> dflt_time {0.2};
    EXPECT_FALSE(my_process->find("/signal1"))
            << "unknown signal cannot be cached";

    EXPECT_EQ(my_process->getReadChannelRequests(), 1);
    EXPECT_EQ(my_process->getReadParamRequests(), 1);
    ASSERT_TRUE(my_process->answer_rk_rp_requests(1, 0, 0, 1));
    ASSERT_EQ(my_process->found_vars.size(), 1) << "Found one signals";
    const auto v1 = std::move(my_process->found_vars[0]);
    my_process->found_vars.clear();
    ASSERT_FALSE(v1.empty()) << "Variable must have a valid pimpl instance";
    unsigned int gid = 0;


    MySubscriber sub(dflt_time), sub2(2 * dflt_time);
    std::unique_ptr<Subscription> s1 {new Subscription(sub, v1)},
            s2 {new Subscription(sub2, v1)};

    EXPECT_EQ(sub.newValues_count_, 0) << "sub.newValues() was never called";
    EXPECT_EQ(sub2.newValues_count_, 0) << "sub2.newValues() was never called";
    ASSERT_TRUE(my_process->answer_signal_subscription_requests(0, 2));

    ASSERT_EQ(sub.state_changed_notifications_.size(), 2)
            << "Subscription 1 recieved exactly two status callbacks";
    ASSERT_EQ(
            sub.state_changed_notifications_.back().state_,
            PdCom::Subscription::State::Active)
            << "Subscribtion 1 has active state";
    ASSERT_EQ(sub2.state_changed_notifications_.size(), 2)
            << "Subscription 2 recieved exactly two status callbacks";
    ASSERT_EQ(
            sub2.state_changed_notifications_.back().state_,
            PdCom::Subscription::State::Active)
            << "Subscribtion 2 has active state";
    ASSERT_EQ(my_process->subscribed_group_ids.size(), 2);
    gid = my_process->subscribed_group_ids.front();

    cbk(gid, s1);
    EXPECT_EQ(sub.newValues_count_, 0) << "sub.newValues() was not called";
    const unsigned gid2 = my_process->subscribed_group_ids.back();
    {
        MyProcess::SendDataManager sm(*my_process, false, 47.11, gid2);
        sm.addTime();
        sm.addData();
    }

    EXPECT_EQ(sub2.newValues_count_, 1) << "ProtocolHandler still works";
}

TEST_F(MyProcessTest, RemoveEventSubscriptionAtDataTagParsingTime1)
{
    _doRemoveEventSubscriptionAtDataTagParsingTime(
            [](unsigned int gid, std::unique_ptr<PdCom::Subscription> &s1) {
                MyProcess::SendDataManager sm(*my_process, true, 47.11, gid);
                my_process->flush_read_buffer();
                s1.reset();
                sm.addTime();
                sm.addData();
            });
}
TEST_F(MyProcessTest, RemoveEventSubscriptionAtDataTagParsingTime2)
{
    _doRemoveEventSubscriptionAtDataTagParsingTime(
            [](unsigned int gid, std::unique_ptr<PdCom::Subscription> &s1) {
                s1.reset();
                MyProcess::SendDataManager sm(*my_process, true, 47.11, gid);
                my_process->flush_read_buffer();
                sm.addTime();
                sm.addData();
            });
}

TEST_F(MyProcessTest, RemoveEventSubscriptionAtDataTagParsingTime3)
{
    _doRemoveEventSubscriptionAtDataTagParsingTime(
            [](unsigned int gid, std::unique_ptr<PdCom::Subscription> &s1) {
                MyProcess::SendDataManager sm(*my_process, true, 47.11, gid);
                sm.addTime();
                sm.addData();
                my_process->flush_read_buffer();
                s1.reset();
            });
}

// this test cancels a event-based subscription with
// incoming events still incoming
TEST_F(MyProcessTest, ReplaceEventSubscription)
{
    MySubscriber sub_poll {PdCom::poll_mode};
    PdCom::Subscription s1;
    struct FancySubscriber : PdCom::Subscriber
    {
        MySubscriber *poll_subscriber;
        PdCom::Subscription *s1;
        int i = 0;

        FancySubscriber(
                MySubscriber *poll_subscriber,
                PdCom::Subscription *s1) :
            PdCom::Subscriber(PdCom::event_mode),
            poll_subscriber(poll_subscriber),
            s1(s1)
        {}

        void stateChanged(const PdCom::Subscription &) {}

        void newValues(std::chrono::nanoseconds)
        {
            i++;
            if (i == 2) {
                *s1 = PdCom::Subscription {*poll_subscriber, s1->getVariable()};
                s1->poll();
            }
        }
    } sub_event {&sub_poll, &s1};

    s1         = {sub_event, *my_process, "/signal1"};
    auto &sig1 = dynamic_cast<typename MyProcess::FirstSignalT &>(
            *my_process->accessSignals().at("/signal1"));
    EXPECT_TRUE(my_process->answer_rk_rp_requests(1, 0, 0, 1));
    ASSERT_TRUE(my_process->answer_signal_subscription_requests(1, 0));
    ASSERT_EQ(s1.getState(), PdCom::Subscription::State::Active);
    ASSERT_EQ(my_process->subscribed_group_ids.size(), 1);
    const auto gid = my_process->subscribed_group_ids.back();
    for (int i = 0; i < 4; ++i) {
        sig1.data_container = i;
        MyProcess::SendDataManager sm(*my_process, true, 47.11, gid);
        sm.addTime();
        sm.addData({sig1.index});
    }
    my_process->flush_read_buffer();
    EXPECT_TRUE(my_process->answer_signal_unsubscribe_requests(1));
    EXPECT_TRUE(my_process->answer_rk_rp_requests(1, 0, 0, 0));
    EXPECT_EQ(sub_event.i, 2) << "Event newData was called only two times";
}

TEST_F(MyProcessTest, InvalidEventSubscription)
{
    MySubscriber sub {PdCom::event_mode};
    PdCom::Subscription s1 {sub, *my_process, "/unknown/param"};
    EXPECT_TRUE(my_process->answer_rk_rp_requests(0, 1, 0, 1));
    EXPECT_EQ(s1.getState(), PdCom::Subscription::State::Invalid);
    ASSERT_EQ(sub.state_changed_notifications_.size(), 2);
    EXPECT_EQ(
            sub.state_changed_notifications_.front().state_,
            PdCom::Subscription::State::Pending);
    EXPECT_EQ(
            sub.state_changed_notifications_.back().state_,
            PdCom::Subscription::State::Invalid);
}

TEST_F(MyProcessTest, VariableInvalidAfterReset)
{
    EXPECT_FALSE(my_process->find("/signal1"))
            << "unknown signal cannot be cached";

    EXPECT_EQ(my_process->getReadChannelRequests(), 1);
    EXPECT_EQ(my_process->getReadParamRequests(), 1);
    ASSERT_TRUE(my_process->answer_rk_rp_requests(1, 0, 0, 1));
    ASSERT_EQ(my_process->found_vars.size(), 1) << "Found one signal";

    const auto var = my_process->found_vars.front();

    MySubscriber sub {PdCom::event_mode};
    Subscription s1 {sub, var};

    ASSERT_TRUE(my_process->answer_signal_subscription_requests(1, 0));

    my_process->reset();

    EXPECT_THROW(var.getPath(), PdCom::EmptyVariable)
            << "Variable becomes invalid after resetting Process";
}

TEST_F(MyProcessTest, DISABLED_KnownParameterEvent)
{
    EXPECT_FALSE(my_process->find("/params/param01"));
    EXPECT_TRUE(my_process->answer_rk_rp_requests(0, 1, 1, 0));
    ASSERT_EQ(my_process->found_vars.size(), 1) << "Parameter has been found";
    ASSERT_FALSE(my_process->found_vars.back().empty());
    EXPECT_TRUE(my_process->found_vars.back().isWriteable())
            << "Parameter is writeable";
    {
        std::unique_ptr<MySubscriber> sub {new MySubscriber(PdCom::event_mode)};
        Subscription s1 {*sub, my_process->found_vars[0]};
        EXPECT_EQ(s1.getState(), PdCom::Subscription::State::Pending);
        EXPECT_TRUE(my_process->answer_parameter_subscription_requests(1));
        EXPECT_TRUE(my_process->answer_signal_subscription_requests(0, 0));
        const unsigned char v = 'H';
        ASSERT_EQ(sub->state_changed_notifications_.size(), 2);
        EXPECT_EQ(
                sub->state_changed_notifications_.back().state_,
                PdCom::Subscription::State::Active);
        ASSERT_TRUE(my_process->setValue(my_process->found_vars[0], v))
                << "parameter.setValue() failed";
        my_process->flush_read_buffer();
        EXPECT_EQ(sub->newValues_count_, 1);
        unsigned char p;
        s1.getValue(p);
        EXPECT_EQ(v, p);
        {
            auto &p1 = dynamic_cast<FakeParameter<unsigned char> &>(
                    *my_process->accessParameters().at("/params/param01"));
            p1.data_container = 'T';
            s1.poll();
            ASSERT_TRUE(my_process->answer_rk_rp_requests(0, 0, 1, 0));
            EXPECT_EQ(sub->newValues_count_, 2);
            s1.getValue(p);
            EXPECT_EQ(p, 'T') << "Poll value mismatch";
        }
    }
    EXPECT_TRUE(my_process->answer_parameter_unsubscribe_requests(1));
    {
        std::unique_ptr<MySubscriber> sub {new MySubscriber(PdCom::event_mode)};
        {
            Subscription s1 {*sub, my_process->found_vars[0]};
            EXPECT_TRUE(my_process->answer_parameter_subscription_requests(1));
            EXPECT_TRUE(my_process->answer_signal_subscription_requests(0, 0));
        }
        const unsigned char v = 'U';
        ASSERT_TRUE(my_process->setValue(my_process->found_vars[0], v))
                << "Set parameter with not yet unsubscribed subscription does "
                   "not throw";

        EXPECT_TRUE(my_process->answer_parameter_unsubscribe_requests(1));
    }
    {
        using namespace std::chrono_literals;
        MySubscriber sub2 {{100ms}};
        const auto t = [&]() {
            [[maybe_unused]] Subscription s2 {sub2, my_process->found_vars[0]};
        };
        EXPECT_THROW(t(), InvalidSubscription)
                << "Parameter can't be subscribed periodically";
    }
}

TEST_F(MyProcessTest, VariablePollThenCallback)
{
    EXPECT_THROW(PdCom::Variable().poll(), PdCom::EmptyVariable);

    EXPECT_FALSE(my_process->find("/signal1"))
            << "unknown signal cannot be cached";

    EXPECT_EQ(my_process->getReadChannelRequests(), 1);
    EXPECT_EQ(my_process->getReadParamRequests(), 1);
    ASSERT_TRUE(my_process->answer_rk_rp_requests(1, 0, 0, 1));
    ASSERT_EQ(my_process->found_vars.size(), 1) << "Found one signal";

    const auto var       = my_process->found_vars.front();
    bool callback_called = false;

    const auto future =
            var.poll()
                    .then([&](PdCom::VariablePollResult res,
                              std::chrono::nanoseconds) {
                        callback_called = true;

                        char c = 0;
                        res.getValue(c);
                        EXPECT_EQ(c, 'a') << "correct value was polled";
                    })
                    .handle_exception([](const PdCom::Exception &) {
                        ADD_FAILURE()
                                << "exception handler was launched unexpected";
                    });

    EXPECT_TRUE(my_process->answer_rk_rp_requests(1, 0, 0, 0));
    EXPECT_TRUE(callback_called) << ".then() callback was called";
}

TEST_F(MyProcessTest, VariablePollExceptionCallback)
{
    EXPECT_THROW(PdCom::Variable().poll(), PdCom::EmptyVariable);

    EXPECT_FALSE(my_process->find("/signal1"))
            << "unknown signal cannot be cached";

    EXPECT_EQ(my_process->getReadChannelRequests(), 1);
    EXPECT_EQ(my_process->getReadParamRequests(), 1);
    ASSERT_TRUE(my_process->answer_rk_rp_requests(1, 0, 0, 1));
    ASSERT_EQ(my_process->found_vars.size(), 1) << "Found one signals";

    const auto var       = my_process->found_vars.front();
    bool callback_called = false;

    const auto future =
            var.poll()
                    .then([](PdCom::VariablePollResult,
                             std::chrono::nanoseconds) {
                        ADD_FAILURE() << ".then() callback was called";
                    })
                    .handle_exception([&](const PdCom::Exception &) {
                        callback_called = true;
                    });
    // currently, handle_exception() is only called when a server is destroyed
    my_process.reset();
    EXPECT_TRUE(callback_called) << ".handle_exception() callback was called";
}


TEST(AutoReconnect, PeriodicSubscription)
{
    MyProcess p;

    auto &sig1 = dynamic_cast<typename MyProcess::FirstSignalT &>(
            *p.accessSignals().at("/signal1"));
    MySubscriber sub(std::chrono::milliseconds(100));
    Subscription s1 {sub, p, "/signal1"};
    p.callPendingCallbacks();
    EXPECT_EQ(s1.getState(), PdCom::Subscription::State::Pending);
    for (int i = 0; i < 3; ++i) {
        ASSERT_TRUE(p.startup()) << "Startup on run " << i;
        sub.newValues_count_ = 0;
        p.callPendingCallbacks();
        EXPECT_EQ(s1.getState(), PdCom::Subscription::State::Pending);
        EXPECT_TRUE(p.answer_rk_rp_requests(1, 0, 0, 1));
        ASSERT_TRUE(p.answer_signal_subscription_requests(0, 1));
        ASSERT_EQ(p.subscribed_group_ids.size(), 1);
        EXPECT_EQ(s1.getState(), PdCom::Subscription::State::Active);
        {
            FakeProcess::SendDataManager sm {
                    p, false, 47.11, p.subscribed_group_ids.back()};
            sig1.data_container = 186 + i;
            sm.addTime();
            sm.addData();
        }
        ASSERT_EQ(sub.newValues_count_, 1);
        unsigned char c = 0;
        s1.getValue(c);
        EXPECT_EQ(c, 186 + i);
        p.send_eof();
        EXPECT_EQ(s1.getState(), PdCom::Subscription::State::Invalid);
        p.clear_server_side();
    }
}

TEST(AutoReconnect, EOFDuringWrite)
{
    MyProcess p;

    auto &sig1 = dynamic_cast<typename MyProcess::FirstSignalT &>(
            *p.accessSignals().at("/signal1"));
    ASSERT_TRUE(p.startup()) << "Startup 1";
    MySubscriber sub(std::chrono::milliseconds(100));
    MySubscriber sub2 {std::chrono::milliseconds(200)};
    Subscription s1 {sub, p, "/signal1"};
    Subscription s2 {sub2, p, "/signal1"};
    p.callPendingCallbacks();
    EXPECT_EQ(s1.getState(), PdCom::Subscription::State::Pending);
    sub.newValues_count_ = 0;
    p.callPendingCallbacks();
    EXPECT_EQ(s1.getState(), PdCom::Subscription::State::Pending);
    EXPECT_TRUE(p.answer_rk_rp_requests(1, 0, 0, 1));
    ASSERT_TRUE(p.answer_signal_subscription_requests(0, 2));
    ASSERT_EQ(p.subscribed_group_ids.size(), 2);
    EXPECT_EQ(s1.getState(), PdCom::Subscription::State::Active);
    EXPECT_EQ(s2.getState(), PdCom::Subscription::State::Active);
    {
        FakeProcess::SendDataManager sm {
                p, false, 47.11, p.subscribed_group_ids.front()};
        sig1.data_container = 186;
        sm.addTime();
        sm.addData();
    }
    ASSERT_EQ(sub.newValues_count_, 1);
    unsigned char c = 0;
    s1.getValue(c);
    EXPECT_EQ(c, 186);
    p.throw_at_write_ = true;
    EXPECT_THROW(s1.poll(), PdCom::WriteFailure);
    p.reset();
    p.clear_server_side();
    p.throw_at_write_ = false;
    ASSERT_TRUE(p.startup()) << "Startup 2";
    sub.newValues_count_ = 0;
    p.callPendingCallbacks();
    EXPECT_EQ(s1.getState(), PdCom::Subscription::State::Pending);
    EXPECT_EQ(s2.getState(), PdCom::Subscription::State::Pending);
}

TEST(PdServWithoutXsap, EventSubscriptonWithKnownSignal)
{
    MyProcess p;
    ASSERT_TRUE(p.startup(false)) << "Startup server without xsap support";
    EXPECT_FALSE(p.find("/params/param01"));
    EXPECT_TRUE(p.answer_rk_rp_requests(0, 1, 1, 0));
    ASSERT_EQ(p.found_vars.size(), 1) << "Parameter has been found";
    ASSERT_FALSE(p.found_vars.back().empty());
    {
        std::unique_ptr<MySubscriber> sub {new MySubscriber(PdCom::event_mode)};
        Subscription s1 {*sub, p.found_vars[0]};
        EXPECT_EQ(s1.getState(), PdCom::Subscription::State::Active);
        p.callPendingCallbacks();
        EXPECT_TRUE(p.answer_parameter_subscription_requests(0));
        EXPECT_TRUE(p.answer_signal_subscription_requests(0, 0));
        const unsigned char v = 'H';
        ASSERT_EQ(sub->state_changed_notifications_.size(), 1);
        EXPECT_EQ(
                sub->state_changed_notifications_.back().state_,
                PdCom::Subscription::State::Active);
        ASSERT_TRUE(p.setValue(p.found_vars[0], v))
                << "parameter.setValue() failed";
        p.flush_read_buffer();
        EXPECT_TRUE(p.answer_rk_rp_requests(0, 0, 1, 0))
                << "pdcom5 requests update for parameter";

        EXPECT_EQ(sub->newValues_count_, 1) << "new value has arrived";
        unsigned char c;
        s1.getValue(c);
        EXPECT_EQ(v, c);
        {
            auto &p1 = dynamic_cast<FakeParameter<unsigned char> &>(
                    *p.accessParameters().at("/params/param01"));
            p1.data_container = 'T';
            s1.poll();
            ASSERT_TRUE(p.answer_rk_rp_requests(0, 0, 1, 0));
            EXPECT_EQ(sub->newValues_count_, 2);
            s1.getValue(c);
            EXPECT_EQ(c, 'T') << "Poll value mismatch";
        }
    }
    EXPECT_TRUE(p.answer_parameter_unsubscribe_requests(0)) << "No xsop";
    EXPECT_TRUE(p.answer_signal_unsubscribe_requests(0)) << "No xsod";
    {
        using namespace std::chrono_literals;
        MySubscriber sub2 {{100ms}};
        const auto t = [&]() {
            [[maybe_unused]] Subscription s2 {sub2, p.found_vars[0]};
        };
        EXPECT_THROW(t(), InvalidSubscription)
                << "Parameter can't be subscribed periodically";
    }
}


TEST(PdServWithoutXsap, MultipleEventSubscriptonWithKnownSignal)
{
    MyProcess p;
    ASSERT_TRUE(p.startup(false)) << "Startup server without xsap support";
    _dotestMultipleParameterEventSubscription<false>(&p);
}


TEST(PdServWithoutXsap, SubscriptonWithKnownParameterArray)
{
    MyProcess p;
    ASSERT_TRUE(p.startup(false)) << "Startup server without xsap support";
    _doSubscriptonWithKnownParameterArray<false>(&p);
}

TEST_F(MyProcessTest, CanceledPoll)
{
    EXPECT_FALSE(my_process->find("/params/param01"));
    EXPECT_TRUE(my_process->answer_rk_rp_requests(0, 1, 1, 0));
    ASSERT_EQ(my_process->found_vars.size(), 1) << "Parameter has been found";
    ASSERT_FALSE(my_process->found_vars.back().empty());
    MySubscriber sub2(PdCom::poll_mode);
    {
        Subscription s1 {sub2, my_process->found_vars.back()};
        EXPECT_TRUE(my_process->answer_parameter_subscription_requests(0));
        EXPECT_TRUE(my_process->answer_signal_subscription_requests(0, 0));
        my_process->callPendingCallbacks();
        ASSERT_EQ(sub2.state_changed_notifications_.size(), 1);
        EXPECT_EQ(
                sub2.state_changed_notifications_.back().state_,
                PdCom::Subscription::State::Active);
        s1.poll();
    }
    EXPECT_TRUE(my_process->answer_parameter_unsubscribe_requests(0));
    EXPECT_TRUE(my_process->answer_rk_rp_requests(0, 0, 1, 0));
}

TEST_F(MyProcessTest, Print)
{
    dynamic_cast<typename MyProcess::SecondSignalT &>(
            *my_process->accessSignals().at("/signal2"))
            .data_container = -7.5;
    EXPECT_FALSE(my_process->find("/signal2"));
    EXPECT_TRUE(my_process->answer_rk_rp_requests(1, 0, 0, 1));
    ASSERT_EQ(my_process->found_vars.size(), 1) << "Parameter has been found";
    ASSERT_FALSE(my_process->found_vars.back().empty());
    MySubscriber sub2(PdCom::poll_mode);

    Subscription s1 {sub2, my_process->found_vars.back()};
    EXPECT_TRUE(my_process->answer_parameter_subscription_requests(0));
    EXPECT_TRUE(my_process->answer_signal_subscription_requests(0, 0));
    my_process->callPendingCallbacks();
    ASSERT_EQ(sub2.state_changed_notifications_.size(), 1);
    EXPECT_EQ(
            sub2.state_changed_notifications_.back().state_,
            PdCom::Subscription::State::Active);
    s1.poll();

    ASSERT_TRUE(my_process->answer_rk_rp_requests(1, 0, 0, 0));
    ASSERT_EQ(sub2.newValues_count_, 1);
    double d = 6.0;
    s1.getValue(d);
    EXPECT_EQ(d, -7.5);
    std::ostringstream os;
    s1.print(os, ';');
    EXPECT_EQ(os.str(), "-7.5");
}

TEST_F(MyProcessTest, DISABLED_UnknownParameterEvent)
{
    std::unique_ptr<MySubscriber> sub {new MySubscriber(PdCom::event_mode)};
    {
        Subscription s1 {*sub, *my_process, "/params/param01"};
        EXPECT_EQ(s1.getState(), PdCom::Subscription::State::Pending);
        EXPECT_TRUE(my_process->answer_rk_rp_requests(0, 1, 1, 0));
        EXPECT_TRUE(my_process->answer_parameter_subscription_requests(1));
        EXPECT_TRUE(my_process->answer_signal_subscription_requests(0, 0));
        const unsigned char v = 'H';
        ASSERT_EQ(sub->state_changed_notifications_.size(), 2);
        EXPECT_EQ(
                sub->state_changed_notifications_.back().state_,
                PdCom::Subscription::State::Active);
        ASSERT_TRUE(my_process->setValue(s1.getVariable(), v))
                << "parameter.setValue() failed";
        my_process->flush_read_buffer();
        EXPECT_EQ(sub->newValues_count_, 1);
        unsigned char p;
        s1.getValue(p);
        EXPECT_EQ(v, p);
    }
    EXPECT_TRUE(my_process->answer_parameter_unsubscribe_requests(1));
}

TEST_F(MyProcessTest, InvalidXml)
{
    EXPECT_THROW(my_process->send_garbage(), ProtocolError);
}

TEST_F(MyProcessTest, MessageSubsystemUnsolicited)
{
    MyMessageManager mm;
    const FakeProcess::MessageDetails md {
            "Critical Error", PdCom::LogLevel::Critical};
    my_process->setMessageManager(&mm);
    my_process->send_message("message1", 0, 4711, {md});
    my_process->flush_read_buffer();
    EXPECT_TRUE(mm.active_.empty()) << "activeMessagesReply() was not called";
    EXPECT_TRUE(mm.replies_.empty()) << "getMessageReply() was not called";
    ASSERT_EQ(mm.unsolicited_.size(), 1) << "One message received";
    EXPECT_EQ(mm.unsolicited_.front().index, 0);
    EXPECT_EQ(mm.unsolicited_.front().seqNo, 4711);
    EXPECT_EQ(mm.unsolicited_.front().text, md.text);
    EXPECT_EQ(mm.unsolicited_.front().level, md.level);
    EXPECT_EQ(mm.unsolicited_.front().path, "message1");
    mm.unsolicited_.clear();
}

TEST_F(MyProcessTest, MessageSubsystemActive)
{
    using namespace std::chrono_literals;
    my_process->active_messages_ = {
            {3123, PdCom::LogLevel::Critical, "/message01", 10ms,
             "Awesome Text", 1},
            {3120, PdCom::LogLevel::Info, "/message02", 10ms, "Awesome Text",
             1},
            {3110, PdCom::LogLevel::Reset, "/message03", 10ms, "", 1},
    };

    MyMessageManager mm;
    my_process->setMessageManager(&mm);

    // do the test twice to make sure that the vector is cleared
    for (int j = 0; j < 2; ++j) {
        mm.activeMessages();
        my_process->flush_read_buffer();
        EXPECT_TRUE(mm.unsolicited_.empty()) << "no unsolicited messages";
        EXPECT_TRUE(mm.replies_.empty()) << "getMessageReply() was not called";
        ASSERT_EQ(mm.active_.size(), my_process->active_messages_.size())
                << "All active messages received";
        for (unsigned int i = 0; i < mm.active_.size(); ++i) {
            EXPECT_EQ(
                    mm.active_[i].index, my_process->active_messages_[i].index);
            EXPECT_EQ(
                    mm.active_[i].level, my_process->active_messages_[i].level);
            EXPECT_EQ(mm.active_[i].path, my_process->active_messages_[i].path);
            EXPECT_EQ(
                    mm.active_[i].seqNo, my_process->active_messages_[i].seqNo);
            EXPECT_EQ(mm.active_[i].text, my_process->active_messages_[i].text);
        }
        mm.active_.clear();
    }
}
TEST_F(MyProcessTest, MessageSubsystemGetKnownMessage)
{
    using namespace std::chrono_literals;
    my_process->active_messages_ = {
            {3123, PdCom::LogLevel::Critical, "/message01", 10ms,
             "Awesome Text", 1},
            {3120, PdCom::LogLevel::Info, "/message02", 10ms, "Awesome Text",
             1},
            {3110, PdCom::LogLevel::Reset, "/message03", 10ms, "", 1},
    };

    MyMessageManager mm;
    my_process->setMessageManager(&mm);
    mm.getMessage(3120);
    my_process->flush_read_buffer();
    EXPECT_TRUE(mm.unsolicited_.empty()) << "no unsolicited messages";
    EXPECT_TRUE(mm.active_.empty()) << "activeMessagesReply() was not called";
    ASSERT_EQ(mm.replies_.size(), 1) << "Exactly one message returned";

    EXPECT_EQ(mm.replies_.front().index, my_process->active_messages_[1].index);
    EXPECT_EQ(mm.replies_.front().level, my_process->active_messages_[1].level);
    EXPECT_EQ(mm.replies_.front().path, my_process->active_messages_[1].path);
    EXPECT_EQ(mm.replies_.front().seqNo, my_process->active_messages_[1].seqNo);
    EXPECT_EQ(mm.replies_.front().text, my_process->active_messages_[1].text);
}

TEST_F(MyProcessTest, MessageSubsystemGetUnknownMessage)
{
    using namespace std::chrono_literals;
    my_process->active_messages_ = {
            {3123, PdCom::LogLevel::Critical, "/message01", 10ms,
             "Awesome Text", 1},
            {3120, PdCom::LogLevel::Info, "/message02", 10ms, "Awesome Text",
             1},
            {3110, PdCom::LogLevel::Reset, "/message03", 10ms, "", 1},
    };

    MyMessageManager mm;
    my_process->setMessageManager(&mm);
    const auto seq = 4711U;
    mm.getMessage(seq);
    my_process->flush_read_buffer();
    EXPECT_TRUE(mm.unsolicited_.empty()) << "no unsolicited messages";
    EXPECT_TRUE(mm.active_.empty()) << "activeMessagesReply() was not called";
    ASSERT_EQ(mm.replies_.size(), 1) << "Exactly one message returned";
    EXPECT_EQ(mm.replies_.front().path, "")
            << "Path is empty, because message cound not be found";
    EXPECT_EQ(mm.replies_.front().seqNo, seq);
    /* rest of message is unspecified */
}

TEST_F(MyProcessTest, ActiveMessageList)
{
    MySubscriber event_subscriber {PdCom::event_mode};
    MySubscriber periodic_subscriber(std::chrono::seconds {1});

    Subscription s1 {event_subscriber, *my_process, "/signal1"},
            s2 {event_subscriber, *my_process, "/params/param01"},
            s3 {periodic_subscriber, *my_process, "/signal1"};

    EXPECT_TRUE(my_process->answer_rk_rp_requests(1, 1, 1, 1));
    EXPECT_TRUE(my_process->answer_signal_subscription_requests(1, 1));
    // disabled until xsap is back
    // EXPECT_TRUE(my_process->answer_parameter_subscription_requests(1));

    EXPECT_TRUE(my_process->find("/signal1"));
    EXPECT_TRUE(my_process->find("/params/param01"));
    ASSERT_EQ(my_process->found_vars.size(), 2);

    EXPECT_EQ(s1.getState(), Subscription::State::Active);
    EXPECT_EQ(s2.getState(), Subscription::State::Active);
    EXPECT_EQ(s3.getState(), Subscription::State::Active);

    const auto list = my_process->getActiveSubscriptions();
    const std::array<PdCom::Process::SubscriptionInfo, 3> expected {{
            {&s1, &event_subscriber, my_process->found_vars[0]},
            {&s2, &event_subscriber, my_process->found_vars[1]},
            {&s3, &periodic_subscriber, my_process->found_vars[0]},
    }};

    ASSERT_EQ(list.size(), 3) << "three active subscriptions";
    for (const auto &expected_sub : expected) {
        const auto it = std::find_if(
                list.begin(), list.end(),
                [&expected_sub](const PdCom::Process::SubscriptionInfo &si) {
                    return si.subscriber == expected_sub.subscriber
                            && expected_sub.subscription == si.subscription
                            && expected_sub.variable.getPath()
                            == si.variable.getPath();
                });
        EXPECT_NE(it, list.end())
                << "Subscription on Path " << expected_sub.variable.getPath()
                << " not listed";
    }
}

void _test_not_connected(MyProcess &p)
{
    using PdCom::NotConnected;
    MySubscriber s(PdCom::event_mode);
    EXPECT_THROW(p.find(""), NotConnected);
    EXPECT_THROW(p.list(""), NotConnected);
    EXPECT_THROW(p.name(), NotConnected);
    EXPECT_THROW(p.version(), NotConnected);
    EXPECT_THROW(p.ping(), NotConnected);
    EXPECT_THROW(p.broadcast("test"), NotConnected);
    const auto t = [&]() {
        [[maybe_unused]] Subscription ms(s, p, "/signal1");
    };
    EXPECT_NO_THROW(t());
    EXPECT_NO_THROW(p.callPendingCallbacks());
}


TEST(NotConnected, ActionsBeforeConnected)
{
    MyProcess p;
    _test_not_connected(p);
}

TEST(NotConnected, ActionsAfterReset)
{
    MyProcess p;
    ASSERT_TRUE(p.startup());
    p.reset();
    _test_not_connected(p);
}

template <void (*reset_fn)(std::unique_ptr<MyProcess> &)>
void _test_write_parameter()
{
    std::unique_ptr<MyProcess> p {new MyProcess()};
    ASSERT_TRUE(p->startup());
    EXPECT_FALSE(p->find("/params/param01"));
    ASSERT_TRUE(p->answer_rk_rp_requests(0, 1, 1, 0));
    ASSERT_EQ(p->found_vars.size(), 1);
    const auto var  = p->found_vars.back();
    unsigned char c = 1;
    EXPECT_TRUE(p->setValue(var, c));
    reset_fn(p);
    EXPECT_THROW(var.setValue(c), PdCom::EmptyVariable);
}

void delete_process(std::unique_ptr<MyProcess> &p)
{
    p.reset();
}
void reset_process(std::unique_ptr<MyProcess> &p)
{
    p->reset();
}

TEST(ServerDeleted, WriteParameter)
{
    _test_write_parameter<delete_process>();
}

TEST(ServerReset, WriteParameter)
{
    _test_write_parameter<reset_process>();
}

TEST(DefaultConstructed, DefaultSubscription)
{
    using PdCom::InvalidSubscription;
    PdCom::Subscription s;
    std::ostringstream os;
    EXPECT_THROW(s.poll(), InvalidSubscription);
    EXPECT_THROW(s.getData(), InvalidSubscription);
    EXPECT_THROW(s.getVariable(), InvalidSubscription);
    EXPECT_THROW(s.print(os, ';'), InvalidSubscription);
    EXPECT_EQ(s.getState(), PdCom::Subscription::State::Invalid);
    EXPECT_TRUE(s.empty());
}

TEST(DefaultConstructed, DefaultVariable)
{
    using PdCom::EmptyVariable;
    const PdCom::Variable v;
    const unsigned char c = 'L';
    EXPECT_THROW(v.setValue(c), EmptyVariable);
    EXPECT_THROW(v.getTaskId(), EmptyVariable);
    EXPECT_THROW(v.getTypeInfo(), EmptyVariable);
    EXPECT_THROW(v.getSizeInfo(), EmptyVariable);
    EXPECT_THROW(v.getPath(), EmptyVariable);
    EXPECT_THROW(v.getAlias(), EmptyVariable);
    EXPECT_THROW(v.getName(), EmptyVariable);
    EXPECT_TRUE(v.empty());
}

TEST(CallAsyncDataFromCallback, CallAsyncDataFromCallback)
{
    struct P : FakeProcess
    {
        P() : FakeProcess(MyProcess::makeSignals(), MyProcess::makeParams()) {}
        void connected() override { asyncData(); }
    } p;
    EXPECT_THROW(p.startup(), PdCom::InvalidArgument);
}


TEST(CallResetFromCallback, CallResetFromCallback)
{
    struct P : FakeProcess
    {
        P() : FakeProcess(MyProcess::makeSignals(), MyProcess::makeParams()) {}
        void connected() override { find("/signal1"); }
        void findReply(PdCom::Variable const &) override
        {
            PdCom::Process::reset();
        }
    } p;
    EXPECT_TRUE(p.startup());
    EXPECT_THROW(p.answer_rk_rp_requests(1, 0, 0, 1), PdCom::InvalidArgument);
}

TEST(OverwriteSubscriptionInCallback, OverwriteSubscriptionInCallback)
{
    struct P : FakeProcess, MySubscriber
    {
        P() :
            FakeProcess(MyProcess::makeSignals(), MyProcess::makeParams()),
            MySubscriber(PdCom::event_mode)
        {}
        void connected() override
        {
            FakeProcess::connected();
            sub_ = Subscription(*this, *this, "/signal1");
        }

        PdCom::Subscription sub_ = {};
    } p;
    EXPECT_TRUE(p.startup());

    ASSERT_TRUE(p.answer_rk_rp_requests(1, 0, 0, 1));
    ASSERT_TRUE(p.answer_signal_subscription_requests(1, 0));
}

class Pipe
{
    int fds_[2] = {};

  public:
    Pipe()
    {
        if (pipe(fds_) != 0)
            throw std::runtime_error("could not create pipe");
    }

    int rx() const { return fds_[0]; }
    int tx() const { return fds_[1]; }

    ~Pipe()
    {
        close(fds_[0]);
        close(fds_[1]);
    }
};


struct MyPosixProcess : PdCom::PosixProcess
{
    using PosixProcess::posixFlush;
    using PosixProcess::PosixProcess;
    using PosixProcess::posixRead;
    using PosixProcess::posixWriteBuffered;
    using PosixProcess::posixWriteDirect;
    using PosixProcess::setWriteTimeout;
};

class SignalBlocker
{
    void (*const old_sig_)(int);
    const int sig_;

  public:
    explicit SignalBlocker(int sig) : old_sig_(signal(sig, SIG_IGN)), sig_(sig)
    {
        if (old_sig_ == SIG_ERR)
            throw std::runtime_error("Could not block signal");
    }
    ~SignalBlocker() { signal(sig_, old_sig_); }
};

namespace {
bool bind_v6(int fd, unsigned short port)
{
    addrinfo *res    = nullptr;
    addrinfo hint    = {};
    hint.ai_flags    = AI_NUMERICHOST | AI_PASSIVE;
    hint.ai_family   = AF_INET6;
    hint.ai_socktype = SOCK_STREAM;
    hint.ai_protocol = IPPROTO_TCP;


    if (getaddrinfo("::1", nullptr, &hint, &res) != 0)
        return false;
    reinterpret_cast<sockaddr_in6 *>(res->ai_addr)->sin6_port = htons(port);

    const bool ans = bind(fd, res->ai_addr, res->ai_addrlen) == 0;
    freeaddrinfo(res);
    return ans;
}
bool bind_v4(int fd, unsigned short port)
{
    const sockaddr_in addr {AF_INET, htons(port), htonl(INADDR_LOOPBACK), {}};
    return bind(fd, reinterpret_cast<const sockaddr *>(&addr), sizeof(addr))
            == 0;
}


class PortListener
{
    const int fd_;

  public:
    PortListener(unsigned short port, bool use_v6) :
        fd_(socket(use_v6 ? AF_INET6 : AF_INET, SOCK_STREAM, 0))
    {
        const auto do_bind = use_v6 ? bind_v6 : bind_v4;
        const std::string err_msg =
                "Listening to port " + std::to_string(port) + " failed.";
        if (fd_ != -1) {
            const int optval = 1;
            if (!setsockopt(
                        fd_, SOL_SOCKET, SO_REUSEADDR, &optval,
                        sizeof(optval))) {
                if (do_bind(fd_, port) and !listen(fd_, 5)) {
                    return;
                }
            }
            perror(err_msg.c_str());
            ::close(fd_);
        }
        throw std::runtime_error(err_msg);
    }

    class Connection
    {
        const int fd_;

      public:
        explicit Connection(int fd) : fd_(fd) {}

        template <size_t N>
        bool write(const char (&d)[N])
        {
            return ::write(fd_, d, N) == N;
        }

        ~Connection() { ::close(fd_); }
    };

    Connection accept()
    {
        const int ans = ::accept(fd_, nullptr, nullptr);
        if (ans == -1)
            throw std::runtime_error("accept() failed");
        return Connection {ans};
    }

    ~PortListener() { ::close(fd_); }
};

void doTestConnectAndRead(const char *host, bool use_v6)
{
    constexpr unsigned short port = 2345;
    {
        PortListener pb(port, use_v6);
        MyPosixProcess proc {host, port};
        const char s[] = "hello!";
        {
            auto c = pb.accept();
            c.write(s);
            char r[sizeof(s)];
            EXPECT_EQ((proc.posixRead(r, sizeof(r))), sizeof(s));
            EXPECT_STREQ(r, s);
        }
        char buf;
        EXPECT_EQ((proc.posixRead(&buf, 1)), 0)
                << "read() should return 0 on EOF";
    }
    EXPECT_THROW((MyPosixProcess {host, port}), PdCom::ConnectionFailed);
}

}  // namespace

TEST(PosixProcessTest, WriteLargeDatasetBuffered)
{
    constexpr size_t max_size = 1024;

    using T = long;

    const Pipe pipes;
    std::string error   = {};
    bool worker_running = false;
    std::condition_variable cv;
    std::mutex mut;
    std::unique_lock<std::mutex> lck(mut);
    std::thread worker(
            [&error, &cv, &mut, &worker_running](int fd) {
                MyPosixProcess p {fd};
                std::array<T, max_size> write_buffer;
                std::iota(write_buffer.begin(), write_buffer.end(), 1);
                {
                    std::unique_lock<std::mutex> lck(mut);
                    worker_running = true;
                    cv.notify_all();
                }
                const size_t chunk1_size = 12;
                try {
                    p.posixWriteBuffered(
                            reinterpret_cast<const char *>(
                                    write_buffer.begin()),
                            chunk1_size * sizeof(T));
                    p.posixWriteBuffered(
                            reinterpret_cast<const char *>(
                                    write_buffer.begin() + chunk1_size),
                            (max_size - chunk1_size) * sizeof(T));
                    p.posixFlush();
                }
                catch (const std::exception &e) {
                    error = e.what();
                }
            },
            pipes.tx());
    // wait for worker to boot
    cv.wait(lck, [&worker_running] { return worker_running; });
    std::array<T, max_size> read_buffer;
    char *begin     = reinterpret_cast<char *>(read_buffer.begin());
    char *const end = reinterpret_cast<char *>(read_buffer.end());
    while (begin != end) {
        const auto res = read(pipes.rx(), begin, end - begin);
        if (res <= 0) {
            worker.join();
            ASSERT_GT(res, 0) << "Read from pipe failed";
        }
        begin += res;
    }
    worker.join();
    ASSERT_EQ(error, "") << "Exception occured in worker";
    for (unsigned int i = 0; i < max_size; ++i) {
        ASSERT_EQ(read_buffer[i], i + 1) << "Data corruption";
    }
}


TEST(PosixProcessTest, WriteLargeDatasetNonBlocking)
{
    constexpr size_t max_size = 1024 * 512;

    using T = long;

    const Pipe pipes;

    std::string error   = {};
    bool worker_running = false;
    std::condition_variable cv;
    std::mutex mut;
    std::unique_lock<std::mutex> lck(mut);
    std::thread worker(
            [&error, &cv, &mut, &worker_running](int fd) {
                int flags = fcntl(fd, F_GETFL, 0);
                if (flags < 0) {
                    error = "set NONBLOCK";
                    return;
                }
                flags |= O_NONBLOCK;
                if (fcntl(fd, F_SETFL, flags) == -1) {
                    error = "set NONBLOCK";
                    return;
                }
                MyPosixProcess p {fd};
                p.setWriteTimeout(std::chrono::milliseconds {10});
                std::vector<T> write_buffer(max_size, 0);
                std::iota(write_buffer.begin(), write_buffer.end(), 1);
                {
                    std::unique_lock<std::mutex> lck(mut);
                    worker_running = true;
                    cv.notify_all();
                }
                try {
                    p.posixWriteDirect(
                            reinterpret_cast<const char *>(write_buffer.data()),
                            max_size * sizeof(T));
                }
                catch (const std::exception &e) {
                    error = e.what();
                }
            },
            pipes.tx());
    // wait for worker to boot
    std::vector<T> read_buffer(max_size, 0);
    cv.wait(lck, [&worker_running] { return worker_running; });
    char *begin = reinterpret_cast<char *>(read_buffer.data());
    char *const end =
            reinterpret_cast<char *>(read_buffer.data() + read_buffer.size());
    while (begin != end) {
        const auto res =
                read(pipes.rx(), begin, std::min<size_t>(end - begin, 128));
        if (res <= 0) {
            worker.join();
            ASSERT_GT(res, 0) << "Read from pipe failed";
        }
        begin += res;
    }
    worker.join();
    ASSERT_EQ(error, "") << "Exception occured in worker";
    for (unsigned int i = 0; i < max_size; ++i) {
        ASSERT_EQ(read_buffer[i], i + 1) << "Data corruption";
    }
}

TEST(PosixProcessTest, EOFOnWrite)
{
    constexpr char hello[] = "Hello!";
    const Pipe pipes;

    SignalBlocker const sb {SIGPIPE};

    MyPosixProcess p(pipes.tx());
    p.posixWriteDirect(hello, sizeof(hello));


    char buf[4];
    ASSERT_EQ((read(pipes.rx(), buf, sizeof(buf))), sizeof(buf));
    close(pipes.rx());
    try {
        p.posixWriteDirect(hello, sizeof(hello));
        ADD_FAILURE() << "writing on closed connection does not throw";
    }
    catch (const PdCom::WriteFailure &w) {
        ASSERT_EQ(w.errno_, EPIPE);
    }
    catch (...) {
        ADD_FAILURE()
                << "writing on closed connection throws incorrect exception";
    }
}

TEST(PosixProcessTest, TimeoutOnWrite)
{
    constexpr size_t max_size = 1024 * 512;

    using T = long;

    const Pipe pipes;
    int flags = fcntl(pipes.tx(), F_GETFL, 0);
    ASSERT_GE(flags, 0);
    flags |= O_NONBLOCK;
    ASSERT_NE(fcntl(pipes.tx(), F_SETFL, flags), -1);

    SignalBlocker const sb {SIGPIPE};
    std::vector<T> write_buffer(max_size, 0);
    std::iota(write_buffer.begin(), write_buffer.end(), 1);
    MyPosixProcess p(pipes.tx());
    p.setWriteTimeout(std::chrono::milliseconds {1});

    ::timespec t1 = {}, t2 = {};
    ASSERT_EQ((clock_gettime(CLOCK_MONOTONIC, &t1)), 0);
    try {
        p.posixWriteDirect(
                reinterpret_cast<const char *>(write_buffer.data()),
                write_buffer.size() * sizeof(T));
        ADD_FAILURE() << "Timeout does not throw exception";
    }
    catch (const PdCom::WriteFailure &w) {
        ASSERT_EQ((clock_gettime(CLOCK_MONOTONIC, &t2)), 0);
        ASSERT_EQ(w.errno_, ETIMEDOUT);
        ::timespec t3 = {t2.tv_sec - t1.tv_sec, t2.tv_nsec - t1.tv_nsec};
        if (t3.tv_nsec < 0) {
            t3.tv_sec -= 1;
            t3.tv_nsec += 1e9;
        }
        ASSERT_EQ(t3.tv_sec, 0)
                << "setWriteTimeout() may failed, as it took too long to throw";
        ASSERT_GE(t3.tv_nsec, 0);
        EXPECT_LT(t3.tv_nsec, 1e8)
                << "setWriteTimeout() may failed, as it took too long to throw";
    }
    catch (...) {
        ADD_FAILURE() << "Timeout throws incorrect exception";
    }
}

TEST(PosixProcessTest, NonBlockingRead)
{
    const Pipe pipes;
    int flags = fcntl(pipes.rx(), F_GETFL, 0);
    ASSERT_GE(flags, 0);
    flags |= O_NONBLOCK;
    ASSERT_NE(fcntl(pipes.rx(), F_SETFL, flags), -1);

    MyPosixProcess p(pipes.rx());
    char buf;
    EXPECT_EQ((p.posixRead(&buf, 1)), -EAGAIN)
            << "Reading on empty PIPE should give -EAGAIN";
}

TEST(PosixProcessTest, OpenConnectionIpv4)
{
    doTestConnectAndRead("127.0.0.1", false);
}

TEST(PosixProcessTest, DISABLED_OpenConnectionIpv6)
{
    doTestConnectAndRead("::1", true);
}

TEST(PosixProcessTest, OpenConnectionIpv4DNS)
{
    doTestConnectAndRead("localhost", false);
}

TEST(PosixProcessTest, DISABLED_OpenConnectionIpv6DNS)
{
    doTestConnectAndRead("localhost", true);
}

class MyLegacyProcessTest : public ::testing::Test
{
  protected:
    void SetUp() override
    {
        my_process.reset(new MyProcess());
        ASSERT_TRUE(my_process->startup(false, false));
    }

    static std::unique_ptr<MyProcess> my_process;
};

std::unique_ptr<MyProcess> MyLegacyProcessTest::my_process;

TEST_F(MyLegacyProcessTest, PeriodicSubscription)
{
    auto &sig1 = dynamic_cast<typename MyProcess::FirstSignalT &>(
            *my_process->accessSignals().at("/signal1"));
    auto &sig2 = dynamic_cast<typename MyProcess::SecondSignalT &>(
            *my_process->accessSignals().at("/signal2"));
    constexpr std::chrono::duration<double> dflt_time {0.01};
    MySubscriber sub1 {dflt_time};

    Subscription s1 {sub1, *my_process, "/signal1"};

    ASSERT_TRUE(my_process->answer_rk_rp_requests(1, 0, 0, 1));
    ASSERT_TRUE(my_process->answer_signal_subscription_requests(0, 1));
    EXPECT_EQ(s1.getState(), Subscription::State::Active);
    {
        MySubscriber sube {PdCom::event_mode};
        EXPECT_THROW(
                (Subscription {sube, *my_process, "/signal1"}),
                PdCom::InvalidSubscription);
    }
    {
        MyProcess::SendDataManager sm {*my_process, false, 47.11, 0};
        ASSERT_GT(sm.blocksize(), 1);
        // Send only one block
        sig1.data_container = 'A';
        sm.addData();
    }
    ASSERT_EQ(sub1.newValues_count_, 1)
            << "less blocks than expected supported";
    EXPECT_EQ(s1.getValue<char>(), sig1.data_container);
    sub1.newValues_count_ = 0;
    unsigned blocksize    = 0;
    {
        MyProcess::SendDataManager sm {*my_process, false, 47.11, 0};
        blocksize = sm.blocksize();
        for (unsigned i = 0; i < blocksize; ++i) {
            sig1.data_container = 'B' + i;
            sm.addData();
        }
    }
    EXPECT_EQ(sub1.newValues_count_, blocksize);
    EXPECT_EQ(s1.getValue<char>(), sig1.data_container);


    MySubscriber sub2 {dflt_time * 2};
    Subscription s2 {sub2, *my_process, "/signal2"};
    ASSERT_TRUE(my_process->answer_rk_rp_requests(1, 0, 0, 1));
    ASSERT_TRUE(my_process->answer_signal_subscription_requests(0, 1));
    EXPECT_EQ(s2.getState(), Subscription::State::Active);
    const auto sign2blocks = my_process->getBlocksize(0, sig2.index);
    ASSERT_LT(sign2blocks, blocksize)
            << "This only makes sense if signal2 has less blocks than signal1. "
               "Otherwise adapt testcase.";
    sub1.newValues_count_ = 0;
    {
        MyProcess::SendDataManager sm {*my_process, false, 47.11, 0};
        for (unsigned i = 0; i < blocksize; ++i) {
            sig1.data_container = 'a' + i;
            if (i < sign2blocks) {
                sig2.data_container = 4711 + i;
                sm.addData({sig1.index, sig2.index});
            }
            else {
                sm.addData({sig1.index});
            }
        }
    }
    EXPECT_EQ(sub2.newValues_count_, sign2blocks)
            << "forcing more blocks than expected";
    EXPECT_EQ(s2.getValue<double>(), sig2.data_container);
}

TEST_F(MyLegacyProcessTest, PeriodicSubscriptionTsCalculation)
{
    auto &sig1 = dynamic_cast<typename MyProcess::FirstSignalT &>(
            *my_process->accessSignals().at("/signal1"));
    constexpr std::chrono::duration<double> dflt_time {0.02};
    MyTsSubscriber sub1 {dflt_time};

    Subscription s1 {sub1, *my_process, "/signal1"};

    ASSERT_TRUE(my_process->answer_rk_rp_requests(1, 0, 0, 1));
    ASSERT_TRUE(my_process->answer_signal_subscription_requests(0, 1));
    EXPECT_EQ(s1.getState(), Subscription::State::Active);
    unsigned blocksize = 0;
    const std::chrono::duration<double> sendtime(1000.0);
    {
        MyProcess::SendDataManager sm {*my_process, false, sendtime.count(), 0};
        blocksize = sm.blocksize();
        ASSERT_GT(blocksize, 1);
        for (unsigned i = 0; i < blocksize; ++i) {
            sig1.data_container = 'B' + i;
            sm.addData();
        }
    }
    EXPECT_EQ(sub1.newValues_count_, blocksize);
    EXPECT_EQ(s1.getValue<char>(), sig1.data_container);
    ASSERT_EQ(sub1.timeStamps_.size(), blocksize);
    // sendtime is expected to be equal to the last ts
    for (unsigned i = 0; i < blocksize; ++i) {
        EXPECT_NEAR(
                std::chrono::duration<double>(
                        sub1.timeStamps_[blocksize - i - 1])
                        .count(),
                (sendtime - i * dflt_time).count(), dflt_time.count() / 100);
    }
}
