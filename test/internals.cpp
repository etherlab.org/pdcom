/*****************************************************************************
 *
 * Copyright (C) 2021 Bjarne von Horn (vh at igh dot de).
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#include <Selector.h>
#include <Variable.h>
#include <array>
#include <gtest/gtest.h>
#include <msrproto/DataConverter.h>
#include <numeric>
#include <pdcom5/Exception.h>
#include <pdcom5/Subscription.h>
#include <pdcom5/details.h>
#include <vector>

static bool readFromBase64(char *dst, const char *src, size_t buflen)
{
    const size_t input_len = strlen(src);
    return PdCom::impl::MsrProto::readFromBase64(dst, src, input_len, buflen);
}


TEST(Internals, Base64)
{
    const auto verify = [](const char *src, const std::vector<char> &expected) {
        char buf[30];
        std::fill(std::begin(buf), std::end(buf), 'Z');
        if (!readFromBase64(buf, src, expected.size()))
            return ::testing::AssertionFailure() << "readFromBase64 failed";
        const char *dst = buf;
        for (const auto c : expected) {
            if (c != *dst) {
                return ::testing::AssertionFailure()
                        << "Value mismatch, got " << *dst << ", expected " << c;
            }
            dst++;
        }
        return ::testing::AssertionSuccess();
    };

    EXPECT_TRUE(verify("AA==", {'\0'}));
    EXPECT_TRUE(verify("AGE=", {'\0', 'a'}));
    EXPECT_TRUE(verify("ZGEA", {'d', 'a', '\0'}));
    EXPECT_TRUE(verify("AQEBAQ==", {1, 1, 1, 1}));
    EXPECT_TRUE(verify("3d3d3Q==", {'\xdd', '\xdd', '\xdd', '\xdd'}));
    char buf[30];
    EXPECT_FALSE(readFromBase64(buf, "A", 1)) << "len(base64) / 4 must be 0";
    EXPECT_FALSE(readFromBase64(buf, "AA", 1)) << "len(base64) / 4 must be 0";
    EXPECT_FALSE(readFromBase64(buf, "AAA", 1)) << "len(base64) / 4 must be 0";
    EXPECT_FALSE(readFromBase64(buf, "AGE=", 4)) << "base64 too short";
    EXPECT_FALSE(readFromBase64(buf, "AAAAAA==", 2)) << "base64 too long";

    using PdCom::impl::MsrProto::Base64Info;
    EXPECT_EQ(Base64Info("QQ==").encodedDataLength_, 1);
    EXPECT_EQ(Base64Info("QUI=").encodedDataLength_, 2);
    EXPECT_EQ(Base64Info("QUJD").encodedDataLength_, 3);
}

class MyVar : public PdCom::impl::Variable
{
  public:
    MyVar(PdCom::SizeInfo size_info, const PdCom::TypeInfo *type_info) :
        Variable(size_info, type_info, {})
    {}
    PdCom::Variable::SetValueFuture
    setValue(const void *, PdCom::TypeInfo::DataType, size_t, size_t)
            const override
    {
        throw PdCom::InvalidArgument("not implemented");
    }
    std::string getPath() const override { return ""; }
    std::string getName() const override { return ""; }
    std::string getAlias() const override { return ""; }
    std::chrono::duration<double> getSampleTime() const override
    {
        return std::chrono::duration<double> {0.0};
    }
    int getTaskId() const override { return 0; }
    bool isWriteable() const override { return false; }
};

TEST(Internals, SizeInfo)
{
    using SI = PdCom::SizeInfo;
    {
        const SI si {};
        EXPECT_FALSE(si.isScalar());
        EXPECT_FALSE(si.isVector());
        EXPECT_FALSE(si.is2DMatrix());
        EXPECT_TRUE(si.empty());
        EXPECT_EQ(si.totalElements(), 0);
        EXPECT_EQ(si.dimension(), 0);
    }
    {
        const SI si {SI::Scalar()};
        EXPECT_TRUE(si.isScalar());
        EXPECT_FALSE(si.isVector());
        EXPECT_FALSE(si.is2DMatrix());
        EXPECT_EQ(si.totalElements(), 1);
        EXPECT_EQ(si.dimension(), 0);
    }
    {
        const SI si {SI::RowVector(3)};
        EXPECT_FALSE(si.isScalar());
        EXPECT_TRUE(si.isVector());
        EXPECT_FALSE(si.is2DMatrix());
        EXPECT_EQ(si.dimension(), 1);
        EXPECT_EQ(si.totalElements(), 3);
        EXPECT_EQ(si[0], 3);
    }
    {
        const SI si {SI::Matrix(3, 5)};
        EXPECT_FALSE(si.isScalar());
        EXPECT_FALSE(si.isVector());
        EXPECT_TRUE(si.is2DMatrix());
        EXPECT_EQ(si.dimension(), 2);
        EXPECT_EQ(si[0], 3);
        EXPECT_EQ(si[1], 5);
        EXPECT_EQ(si.totalElements(), 15);
    }
}


TEST(Internals, ScalarSelector1D)
{
    using T            = int;
    constexpr auto &ti = PdCom::details::TypeInfoTraits<T>::type_info;
    constexpr int rows = 4;
    using PdCom::impl::ScalarSelector;
    EXPECT_THROW(ScalarSelector {{}}, PdCom::InvalidArgument)
            << "At least one index must be provided";
    {
        const auto v =
                std::make_shared<MyVar>(PdCom::SizeInfo::RowVector(rows), &ti);
        const std::array<T, rows> var_vec = {1, 2, 3, 4};
        for (int i = 0; i < rows; ++i) {
            const ScalarSelector sc {{i}};
            EXPECT_EQ(sc.getRequiredSize(*v), sizeof(T));
            const auto cpyf = sc.getCopyFunction(*v);
            T buf           = 4711;
            cpyf(&buf, var_vec.begin());
            EXPECT_EQ(buf, var_vec[i]) << "Access Element at" << i;
        }
        {
            const ScalarSelector sc {{rows}};
            EXPECT_THROW(sc.getCopyFunction(*v), PdCom::InvalidArgument)
                    << "Index out of range";
        }
    }
    {
        const auto scalar_v =
                std::make_shared<MyVar>(PdCom::SizeInfo::Scalar(), &ti);
        {
            const ScalarSelector sc {{1}};
            EXPECT_THROW(sc.getCopyFunction(*scalar_v), PdCom::InvalidArgument)
                    << "Index out of range";
        }
        const ScalarSelector sc {{0}};
        EXPECT_NO_THROW(sc.getCopyFunction(*scalar_v))
                << "Scalar is like 1x1 vector";
    }
}

TEST(Internals, ScalarSelector2D)
{
    using T            = int;
    constexpr auto &ti = PdCom::details::TypeInfoTraits<T>::type_info;
    constexpr int rows = 3, cols = 4;
    using PdCom::impl::ScalarSelector;

    const auto v =
            std::make_shared<MyVar>(PdCom::SizeInfo::Matrix(rows, cols), &ti);
    const std::array<std::array<T, cols>, rows> var_mat {{
            {1, 2, 3, 4},
            {5, 6, 7, 8},
            {9, 10, 11, 12},
    }};
    for (int i = 0; i < rows; ++i)
        for (int j = 0; j < cols; ++j) {
            const ScalarSelector sc {{i, j}};
            EXPECT_EQ(sc.getRequiredSize(*v), sizeof(T));
            const auto cpyf = sc.getCopyFunction(*v);
            T buf           = 4711;
            cpyf(&buf, var_mat.begin()->begin());
            EXPECT_EQ(buf, var_mat[i][j])
                    << "Access Element at (" << i << "," << j << ")";
        }
    const std::vector<int> indices[] = {{0, 4}, {3, 0}, {3, 4}, {5, 5}};
    for (const auto &index : indices) {
        const ScalarSelector sc {index};
        EXPECT_THROW(sc.getCopyFunction(*v), PdCom::InvalidArgument)
                << "Index out of range";
    }
    {
        const ScalarSelector sc {{1}};
        EXPECT_THROW(sc.getCopyFunction(*v), PdCom::InvalidArgument)
                << "Dimension Error";
    }
    {
        const ScalarSelector sc {{1, 1, 1}};
        EXPECT_THROW(sc.getCopyFunction(*v), PdCom::InvalidArgument)
                << "Dimension Error";
    }
}

TEST(Internals, ScalarSelector3D)
{
    using T              = int;
    constexpr auto &ti   = PdCom::details::TypeInfoTraits<T>::type_info;
    constexpr int layers = 2, rows = 3, cols = 4;
    using PdCom::impl::ScalarSelector;

    const auto v = std::make_shared<MyVar>(
            PdCom::SizeInfo::Matrix(layers, rows, cols), &ti);
    constexpr std::array<std::array<std::array<T, cols>, rows>, layers>
            var_mat = {
                    {{{
                             {1, 2, 3, 4},
                             {5, 6, 7, 8},
                             {9, 10, 11, 12},
                     }},
                     {{
                             {13, 14, 15, 16},
                             {17, 18, 19, 20},
                             {21, 22, 23, 24},
                     }}}};
    for (int l = 0; l < layers; ++l)
        for (int i = 0; i < rows; ++i)
            for (int j = 0; j < cols; ++j) {
                const ScalarSelector sc {{l, i, j}};
                EXPECT_EQ(sc.getRequiredSize(*v), sizeof(T));
                const auto cpyf = sc.getCopyFunction(*v);
                T buf           = 4711;
                cpyf(&buf, var_mat.begin()->begin());
                EXPECT_EQ(buf, var_mat[l][i][j]) << "Access Element at (" << l
                                                 << "," << i << "," << j << ")";
            }
    const std::vector<int> indices[] = {{0, 0, 4}, {0, 3, 0}, {2, 0, 0},
                                        {2, 0, 4}, {2, 3, 4}, {5, 5, 5}};
    for (const auto &index : indices) {
        const ScalarSelector sc {index};
        EXPECT_THROW(sc.getCopyFunction(*v), PdCom::InvalidArgument)
                << "Index out of range";
    }
    {
        const ScalarSelector sc {{1}};
        EXPECT_THROW(sc.getCopyFunction(*v), PdCom::InvalidArgument)
                << "Dimension Error";
    }
    {
        const ScalarSelector sc {{1, 1, 1, 1}};
        EXPECT_THROW(sc.getCopyFunction(*v), PdCom::InvalidArgument)
                << "Dimension Error";
    }
}

namespace {
template <typename Src, typename Dst, size_t N>
void copyVector(Dst (&dst)[N], const Src (&src)[N], size_t offset = 0)
{
    return PdCom::details::copyData(
            dst, PdCom::details::TypeInfoTraits<Dst>::type_info.type, src,
            PdCom::details::TypeInfoTraits<Src>::type_info.type, N - offset,
            offset);
}
template <typename Src, typename Dst>
void copyScalar(Dst &dst, const Src &src)
{
    return PdCom::details::copyData(
            &dst, PdCom::details::TypeInfoTraits<Dst>::type_info.type, &src,
            PdCom::details::TypeInfoTraits<Src>::type_info.type, 1);
}


}  // namespace


TEST(CopyMatrix, CopyMatrix)
{
    {
        const uint8_t src[2] = {47, 11};
        double dst[2];
        copyVector(dst, src);
        EXPECT_EQ(dst[0], 47.0);
        EXPECT_EQ(dst[1], 11);
    }
    {
        const int8_t src[2] = {47, -11};
        double dst[2];
        copyVector(dst, src);
        EXPECT_EQ(dst[0], 47.0);
        EXPECT_EQ(dst[1], -11);
    }
    {
        const uint8_t src[3] = {63, 56, 199};
        double dst[3]        = {28.0, 7.0, 3.0};
        copyVector(dst, src, 1);
        EXPECT_EQ(dst[0], 56) << "63 is left out";
        EXPECT_EQ(dst[1], 199);
        EXPECT_EQ(dst[2], 3.0) << "stays the same";
    }
    {
        int res = 8;
        copyScalar(res, -48.11);
        EXPECT_EQ(res, -48);
    }
    {
        double res = 0.0;
        copyScalar(res, -89.11111111);
        EXPECT_NEAR(res, -89.11111111, 1e-5);
    }
    {
        auto res = std::numeric_limits<unsigned long>::max();
        copyScalar(res, 'U');
        EXPECT_EQ(res, static_cast<unsigned long>('U'))
                << "unsigned char is promoted, top 8 bytes are zeroed out";
    }
    {
        const int8_t src[3] = {47, -11, 86};
        const auto do_copy  = [&src](unsigned offset) {
            double dst;
            PdCom::details::copyData(
                     &dst, PdCom::TypeInfo::DataType::double_T, src,
                     PdCom::TypeInfo::DataType::int8_T, 1, offset);
            return dst;
        };
        EXPECT_EQ(do_copy(0), 47.0);
        EXPECT_EQ(do_copy(1), -11.0);
        EXPECT_EQ(do_copy(2), 86.0);
    }
}
