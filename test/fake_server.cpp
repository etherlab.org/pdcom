/*****************************************************************************
 * vim:tw=78
 *
 * Copyright (C) 2021 Bjarne von Horn (vh at igh dot de).
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#include "fake_server.h"

#include "fake_utils.h"
#include "fake_variable.h"

#include <algorithm>
#include <map>
#include <openssl/evp.h>
#include <unordered_map>
#include <unordered_set>
#include <vector>

static const char initial_server_response_1[] =
        "<connected name=\"MSR\" host=\"lappi\" app=\"PdServ Test\" "
        "appversion=\"1.234\" "
        "version=\"393226\" "
        "features=\"pushparameters,binparameters,eventchannels,statistics,"
        "pmtime,"
        "aic,messages,polite,login,history";

static const char initial_server_response_2[] = "\" endian=\"little\" "
                                                "recievebufsize=\"8192\"/>";


std::ostream &operator<<(std::ostream &os, const XmlTag &tag)
{
    os << "<" << tag.name;
    for (const auto &kv : tag.attributes) {
        os << " " << kv.first << "=\"" << kv.second << "\"";
    }
    if (tag.children.empty()) {
        os << "/>\r\n";
    }
    else {
        os << ">\r\n";
        for (const auto &c : tag.children) {
            os << c;
        }
        os << "</" << tag.name << ">\r\n";
    }
    return os;
}

int FakeProcess::read(char *data, int max_write_bytes)
{
    const auto buf_copy = read_buffer.str();

    const auto copy_bytes = std::min<int>(max_write_bytes, buf_copy.size());
    std::copy(buf_copy.data(), buf_copy.data() + copy_bytes, data);
    read_buffer.str({buf_copy.begin() + copy_bytes, buf_copy.end()});
    return copy_bytes;
}

void FakeProcess::flush_read_buffer()
{
    std::streampos pos = read_buffer.tellp();       // store current location
    read_buffer.seekp(0, std::ios_base::end);       // go to end
    const bool empty = (read_buffer.tellp() == 0);  // check size == 0 ?
    read_buffer.seekp(pos);                         // restore location
    if (!empty)
        asyncData();
}

void FakeProcess::startElement(const char *name, const char **atts)
{
    using FnType = void (FakeProcess::*)(const XmlAttr &);
    const std::unordered_map<std::string, FnType> incoming_request_callbacks = {
            {"rc", &FakeProcess::handle_read_kanaele},
            {"rk", &FakeProcess::handle_read_kanaele},
            {"read_kanaele", &FakeProcess::handle_read_kanaele},
            {"rp", &FakeProcess::handle_read_parameter},
            {"read_parameter", &FakeProcess::handle_read_parameter},
            {"xsad", &FakeProcess::handle_xsad},
            {"xsod", &FakeProcess::handle_xsod},
            {"xsap", &FakeProcess::handle_xsap},
            {"xsop", &FakeProcess::handle_xsop},
            {"wp", &FakeProcess::handle_wp},
            {"write_parameter", &FakeProcess::handle_wp},
            {"ping", &FakeProcess::handle_ping},
            {"message_history", &FakeProcess::handle_message_history},
            {"read_statistics", &FakeProcess::handle_read_statistics},
    };

    switch (state) {
        case ParserState::Startup:
            ASSERT_STREQ(name, "xml");
            state = ParserState::WaitForRemoteHost;
            break;
        case ParserState::WaitForRemoteHost:
            ASSERT_STREQ("remote_host", name) << "Invalid XML Level";
            remote_host_requests.push({XmlAttr(atts).getStringOr("id")});
            state = ParserState::Idle;
            break;
        case ParserState::Idle: {
            const auto fn = incoming_request_callbacks.find(name);
            ASSERT_NE(fn, incoming_request_callbacks.end())
                    << "Tag " << name << " unknown";
            (this->*(fn->second))(XmlAttr(atts));
            break;
        }
        default:
            FAIL() << "unknown parser state " << static_cast<int>(state);
    }
}

void FakeProcess::endElement(const char * /* name */)
{}

::testing::AssertionResult
FakeProcess::startup(bool enable_xsap, bool enable_group)
{
    state           = ParserState::Startup;
    groups_enabled_ = enable_group;
    const char s[]  = "<xml>";
    if (!parse(s, sizeof(s) - 1))
        throw std::runtime_error("init parser failed");
    read_buffer << initial_server_response_1;
    if (enable_xsap)
        read_buffer << ",xsap";
    if (enable_group)
        read_buffer << ",list,group";
    read_buffer << initial_server_response_2;
    flush_read_buffer();
    if (remote_host_requests.size() != 1)
        return ::testing::AssertionFailure() << "did not recieve <remote_host>";
    send_ack(remote_host_requests.front().id);
    remote_host_requests.pop();
    flush_read_buffer();
    if (state != ParserState::Idle)
        return ::testing::AssertionFailure()
                << "connected() callback was not called";
    return ::testing::AssertionSuccess();
}

void FakeProcess::connected()
{
    ASSERT_EQ(state, ParserState::Idle);
}

void FakeProcess::send_ack(const std::string &id)
{
    if (!id.empty())
        read_buffer << XmlTag {"ack", {{"id", id}}, {}};
}

static Request::ReadVariable
parse_readvariable_request(const XmlAttr &atts, bool is_param)
{
    const bool has_name                         = atts.find("name");
    const bool has_index                        = atts.find("index");
    const Request::ReadVariable::AddressMode am = [&]() {
        if (!has_name and !has_index)
            return Request::ReadVariable::None;
        else if (has_name)
            return Request::ReadVariable::NameSet;
        else
            return Request::ReadVariable::IndexSet;
    }();
    return {atts.getStringOr("id"),
            am,
            atts.getStringOr("name"),
            atts.getUIntOr("index"),
            atts.getUIntOr("short") != 0,
            atts.getUIntOr("hex") != 0,
            is_param};
}

void FakeProcess::handle_read_kanaele(const XmlAttr &atts)
{
    read_variable_requests.push_back(parse_readvariable_request(atts, false));
}
void FakeProcess::handle_read_parameter(const XmlAttr &atts)
{
    read_variable_requests.push_back(parse_readvariable_request(atts, true));
}

::testing::AssertionResult FakeProcess::answer_rk_rp_requests(
        int const expected_rk_found,
        int const expected_rk_notfound,
        int const expected_rp_found,
        int const expected_rp_notfound,
        int break_after_count)
{
    int actual_found[2] = {}, actual_notfound[2] = {};
    while (!read_variable_requests.empty() and break_after_count) {
        --break_after_count;
        const auto req  = read_variable_requests.front();
        auto &container = req.is_parameter ? *parameters : *signals;
        if (req.address_mode == Request::ReadVariable::None) {
            for (const auto &var : container) {
                read_buffer << var.toXmlTag(req.id, req.hex, req.short_output);
                ++actual_found[req.is_parameter];
            }
        }
        else {
            FakeVariableBase *const var =
                    req.address_mode == Request::ReadVariable::NameSet
                    ? container.at(req.name.c_str())
                    : container.at(req.index);
            if (var) {
                read_buffer << var->toXmlTag(req.id, req.hex, req.short_output);
                ++actual_found[req.is_parameter];
            }
            else
                ++actual_notfound[req.is_parameter];
        }
        send_ack(req.id);
        read_variable_requests.pop_front();
    }
    flush_read_buffer();
    const bool is_parameter = true;
    if (expected_rk_found != actual_found[!is_parameter]) {
        return ::testing::AssertionFailure()
                << "Mismatch of found channels: expected " << expected_rk_found
                << ", got " << actual_found[!is_parameter];
    }
    if (expected_rk_notfound != actual_notfound[!is_parameter]) {
        return ::testing::AssertionFailure()
                << "Mismatch of missing variables: expected "
                << expected_rk_notfound << ", got "
                << actual_notfound[!is_parameter];
    }
    if (expected_rp_found != actual_found[is_parameter]) {
        return ::testing::AssertionFailure()
                << "Mismatch of found parameters: expected "
                << expected_rp_found << ", got " << actual_found[is_parameter];
    }
    if (expected_rp_notfound != actual_notfound[is_parameter]) {
        return ::testing::AssertionFailure()
                << "Mismatch of missing parameters: expected "
                << expected_rp_notfound << ", got "
                << actual_notfound[is_parameter];
    }
    return ::testing::AssertionSuccess();
}

void FakeProcess::handle_xsad(const XmlAttr &attr)
{
    const Request::SignalSubscription s {
            attr.getStringOr("id"),
            attr.getStringOr("coding"),
            attr.getBoolOr("event"),
            attr.getBoolOr("sync"),
            attr.getUIntList("channels"),
            attr.getUIntOr("blocksize", 1),
            attr.getOptionalUInt("reduction"),
            attr.getOptionalUInt("precision"),
            groups_enabled_ ? attr.getUIntOr("group") : 0U,
    };
    if (s.coding != "Base64")
        throw std::logic_error("Other coding than base64 not implemented");
    for (const int idx : s.channels)
        ASSERT_NE(signals->at(idx), nullptr)
                << "channel " << idx << " not found";

    const std::unordered_set<int> test_set(
            s.channels.begin(), s.channels.end());
    ASSERT_EQ(test_set.size(), s.channels.size())
            << "channels must not contain duplicates";

    ASSERT_TRUE(!s.channels.empty() || s.sync)
            << "channels and/or sync must be in <xsad>";

    signal_subscription_requests.push(s);
}

void FakeProcess::handle_xsod(const XmlAttr &attr)
{
    signal_unsubscribe_requests.push(
            {attr.getStringOr("id"), attr.getUIntList("channels"),
             groups_enabled_ ? attr.getUIntOr("group") : 0U});
}

void FakeProcess::handle_xsap(const XmlAttr &attr)
{
    const Request::ParameterSubscription s {
            attr.getStringOr("id"), attr.getBoolOr("hex"),
            /* attr.getBoolOr("monitor"),  */ attr.getUIntList("parameters")};

    for (const int idx : s.parameters)
        ASSERT_NE(parameters->at(idx), nullptr)
                << "parameter " << idx << " not found";

    const std::unordered_set<int> test_set(
            s.parameters.begin(), s.parameters.end());
    ASSERT_EQ(test_set.size(), s.parameters.size())
            << "parameters must not contain duplicates";

    parameter_subscription_requests.push(s);
}

void FakeProcess::handle_xsop(const XmlAttr &attr)
{
    parameter_unsubscribe_requests.push(
            {attr.getStringOr("id"),
             /* attr.getBoolOr("monitor"),  */ attr.getUIntList("parameters")});
}

::testing::AssertionResult FakeProcess::answer_signal_subscription_requests(
        int const expected_event,
        int const expected_periodic)
{
    int actual_event = 0, actual_periodic = 0;
    while (!signal_subscription_requests.empty()) {
        const auto &r = signal_subscription_requests.front();
        if (r.event) {
            actual_event += r.channels.size();
        }
        else {
            actual_periodic += r.channels.size();
        }

        for (const int c : r.channels) {
            signal_subscriptions[r.group][c] = r;
            subscribed_group_ids.push_back(r.group);
            ++group_id_count[r.group];
        }
        send_ack(r.id);
        signal_subscription_requests.pop();
    }
    flush_read_buffer();
    if (expected_event == actual_event && expected_periodic == actual_periodic)
        return ::testing::AssertionSuccess();
    auto ans = ::testing::AssertionFailure();
    if (expected_event != actual_event)
        ans << "Mismatch of event subscriptions";
    if (expected_periodic != actual_periodic)
        ans << "Mismatch of periodic subscriptions";
    return ans;
}

::testing::AssertionResult
FakeProcess::answer_parameter_subscription_requests(int const expected_event)
{
    int actual_event = 0;
    while (!parameter_subscription_requests.empty()) {
        const auto &r = parameter_subscription_requests.front();
        for (const int p : r.parameters) {
            parameter_subscriptions[p] = r.hex;
            ++actual_event;
        }

        send_ack(r.id);
        parameter_subscription_requests.pop();
    }
    flush_read_buffer();
    if (actual_event != expected_event)
        return ::testing::AssertionFailure()
                << "Mismatch of event subscriptions";
    return ::testing::AssertionSuccess();
}

::testing::AssertionResult FakeProcess::answer_signal_unsubscribe_requests(
        int const expected_channel_count)
{
    callPendingCallbacks();
    int unsubscribed_channels = 0;
    while (!signal_unsubscribe_requests.empty()) {
        const auto &r = signal_unsubscribe_requests.front();

        unsubscribed_channels += r.channels.size();
        if (r.channels.empty()) {
            if ((size_t) group_id_count[r.group]
                != signal_subscriptions[r.group].size())
                return ::testing::AssertionFailure()
                        << "Group id mismatch"
                        << "subscribed group " << group_id_count[r.group]
                        << " times but it contains"
                        << signal_subscriptions[r.group].size()
                        << " Subscriptions";
            signal_subscriptions[r.group].clear();
            group_id_count[r.group] = 0;
        }
        for (const int c : r.channels) {
            if (--group_id_count[r.group] < 0) {
                return ::testing::AssertionFailure()
                        << "Group id mismatch, negative number of "
                           "subscriptions";
            }
            signal_subscriptions[r.group].erase(c);
        }
        send_ack(r.id);

        signal_unsubscribe_requests.pop();
    }
    flush_read_buffer();
    if (unsubscribed_channels != expected_channel_count)
        return ::testing::AssertionFailure()
                << "Mismatch in channels to unsubscribe, got "
                << unsubscribed_channels << ", expected "
                << expected_channel_count;
    return ::testing::AssertionSuccess();
}

::testing::AssertionResult FakeProcess::answer_parameter_unsubscribe_requests(
        int const expected_parameter_count)
{
    callPendingCallbacks();
    int unsubscribed_params = 0;
    while (!parameter_unsubscribe_requests.empty()) {
        const auto &r = parameter_unsubscribe_requests.front();
        if (r.parameters.empty()) {
            unsubscribed_params += parameter_subscriptions.size();
            parameter_subscriptions.clear();
        }
        for (const int p : r.parameters) {
            if (parameter_subscriptions.erase(p))
                ++unsubscribed_params;
        }
        send_ack(r.id);
        parameter_unsubscribe_requests.pop();
    }
    flush_read_buffer();
    if (unsubscribed_params != expected_parameter_count)
        return ::testing::AssertionFailure()
                << "Mismatch in parameters to unsubscribe, got "
                << unsubscribed_params << ", expected "
                << expected_parameter_count;
    return ::testing::AssertionSuccess();
}

void FakeProcess::handle_wp(const XmlAttr &attr)
{
    FakeVariableBase *const p = [&]() {
        if (const auto index = attr.getOptionalUInt("index"))
            return parameters->at(*index);
        else
            return parameters->at(attr.getString("name").c_str());
    }();
    if (attr.find("startindex")) {
        FAIL() << "startindex not yet supported";
    }
    if (const char *hexval = attr.find("hexvalue")) {
        p->fromHex(hexval);
    }
    else if (const char *doubleval = attr.find("value")) {
        p->fromValue(doubleval);
    }
    else {
        FAIL() << "Neigher value nor hexvalue was specified";
        return;
    }
    if (const char *id = attr.find("id"))
        send_ack(id);
    if (!attr.getUIntOr("aic")) {
        read_buffer << "<pu index=\"" << p->index << "\"/>";
    }
    if (const auto s = parameter_subscriptions.find(p->index);
        s != parameter_subscriptions.end()) {
        read_buffer << p->toXmlTag(
                "", true, false, {{"pm", "1"}, {"mtime", "1565627438.417570"}});
    }
}


unsigned int FakeProcess::SendDataManager::blocksize() const
{
    const auto &channels = p_.signal_subscriptions.at(group_);
    if (channels.empty())
        throw std::invalid_argument("no more subscriptions");
    auto it                = channels.begin();
    const unsigned int bsz = it->second.blocksize;
    for (; it != channels.end(); ++it) {
        if (bsz != it->second.blocksize)
            throw std::runtime_error("blocksizes are not identical");
    }
    return bsz;
}

void FakeProcess::SendDataManager::addTime(
        std::vector<uint64_t> const &timestamps)
{
    const auto send = [this](std::vector<uint64_t> const &timestamps) {
        p_.read_buffer << XmlTag {
                "time",
                {{"d",
                  FakeVariableBase::valuesToBase64(
                          timestamps.data(),
                          timestamps.size() * sizeof(uint64_t))}},
                {}};
    };
    if (timestamps.empty()) {
        std::vector<uint64_t> ts(blocksize(), 0);
        std::iota(ts.begin(), ts.end(), 1);
        send(ts);
    }
    else {
        send(timestamps);
    }
}

void FakeProcess::SendDataManager::addData(const std::vector<int> &signals)
{
    const auto &channels = p_.signal_subscriptions.at(group_);

    if (signals.empty())
        for (const auto &kv : channels)
            p_.signals->at(kv.first)->appendToCache(variable_cache_[kv.first]);
    else
        for (const int signal_idx : signals) {
            p_.signals->at(signal_idx)
                    ->appendToCache(variable_cache_[signal_idx]);
        }
}

FakeProcess::SendDataManager::~SendDataManager() noexcept(false)
{
    const auto &channels = p_.signal_subscriptions.at(group_);
    for (const auto &c : variable_cache_) {
        const auto &s = channels.at(c.first);
        if (s.coding != "Base64")
            throw std::logic_error("Other coding than base64 not implemented");
        p_.read_buffer << XmlTag {
                child_tag_name_,
                {{"c", std::to_string(c.first)},
                 {"d",
                  FakeVariableBase::valuesToBase64(
                          c.second.data(), c.second.size())}},
                {}};
    }
    if (send_close_tag_)
        p_.read_buffer << "</data>";
    p_.flush_read_buffer();
}

void FakeProcess::handle_ping(const XmlAttr &)
{
    read_buffer << "<ping/>";
    flush_read_buffer();
}

void FakeProcess::handle_message_history(const XmlAttr &attr)
{
    auto id = attr.getOptionalString("id");

    const auto sndmsg = [this, &id](const PdCom::Message &msg) {
        if (msg.level != PdCom::LogLevel::Reset)
            send_message(
                    msg.path, msg.index, msg.seqNo, {{msg.text, msg.level}},
                    id);
        else
            send_message(msg.path, msg.index, msg.seqNo, std::nullopt, id);
    };

    if (const auto seq = attr.getOptionalUInt("seq")) {
        for (const auto &msg : active_messages_) {
            if (msg.seqNo == seq) {
                sndmsg(msg);
                break;
            }
        }
    }
    else {
        read_buffer << "<message_history>";
        for (const auto &msg : active_messages_) {
            sndmsg(msg);
        }
        read_buffer << "</message_history>";
    }
    if (id)
        send_ack(*id);
}

void FakeProcess::handle_read_statistics(const XmlAttr &)
{
    read_buffer << "<clients><client name=\"127.0.0.1:37290\" "
                   "apname=\"unknown\" countin=\"17\" countout=\"269\" "
                   "connectedtime=\"1670877353.739869\"/></clients>";
}

void FakeProcess::send_message(
        const std::string &name,
        int index,
        unsigned int seq,
        std::optional<MessageDetails> const &details,
        std::optional<std::string> const &id)
{
    if (details) {
        switch (details->level) {
            case PdCom::LogLevel::Emergency:
            case PdCom::LogLevel::Alert:
            case PdCom::LogLevel::Critical:
                read_buffer << "<crit_error";
                break;
            case PdCom::LogLevel::Error:
                read_buffer << "<error";
                break;
            case PdCom::LogLevel::Warn:
                read_buffer << "<warn";
                break;
            case PdCom::LogLevel::Info:
            case PdCom::LogLevel::Debug:
            case PdCom::LogLevel::Trace:
                read_buffer << "<info";
                break;
            default:
                throw std::invalid_argument("invalid level");
        }
        read_buffer << " prio=\"" << static_cast<unsigned int>(details->level)
                    << "\" text=\"" << details->text << "\"";
    }
    else {
        read_buffer << "<reset";
    }

    if (id) {
        read_buffer << " id=\"" << *id << '"';
    }
    read_buffer << " index=\"" << index << "\" seq=\"" << seq << "\" name=\""
                << name << "\" time=\"1565627438.417570\"/>";
}
