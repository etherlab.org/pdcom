/*****************************************************************************
 * vim:tw=78
 *
 * Copyright (C) 2021 Bjarne von Horn (vh at igh dot de).
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#pragma once

#include <cstdint>
#include <cstring>
#include <map>
#include <sstream>
#include <stdexcept>
#include <string>
#include <vector>

template <typename T, typename = void>
struct MsrTypeName
{};

template <>
struct MsrTypeName<uint8_t>
{
    static constexpr char typ[] = "TUCHAR";
};

template <>
struct MsrTypeName<double>
{
    static constexpr char typ[] = "TDBL";
};

template <>
struct MsrTypeName<int>
{
    static constexpr char typ[] = "TINT";
};
template <>
struct MsrTypeName<short>
{
    static constexpr char typ[] = "TSHORT";
};

template <typename T, size_t N>
struct MsrTypeName<std::array<T, N>>
{
    static constexpr auto _gettyp()
    {
        constexpr size_t sz      = sizeof(MsrTypeName<T>::typ) + 5;
        std::array<char, sz> ans = {};
        auto it                  = ans.begin();
        for (auto c : MsrTypeName<T>::typ)
            *it++ = c;
        *(it - 1) = '_';
        *it       = 'L';
        *it++     = 'I';
        *it++     = 'S';
        *it++     = 'T';
        *it       = 0;
        return ans;
    }
    static constexpr auto _typ = _gettyp();
    static constexpr auto typ  = _typ.data();
};

template <typename T, size_t M, size_t N>
struct MsrTypeName<std::array<std::array<T, N>, M>>
{
    static constexpr auto _gettyp()
    {
        constexpr size_t sz      = sizeof(MsrTypeName<T>::typ) + 7;
        std::array<char, sz> ans = {};
        auto it                  = ans.begin();
        for (auto c : MsrTypeName<T>::typ)
            *it++ = c;
        *(it - 1) = '_';
        *it       = 'M';
        *it++     = 'A';
        *it++     = 'T';
        *it++     = 'R';
        *it++     = 'I';
        *it++     = 'X';
        *it       = 0;
        return ans;
    }
    static constexpr auto _typ = _gettyp();
    static constexpr auto typ  = _typ.data();
};

template <typename T, typename = void>
struct DataTypeTraits
{};

template <typename T>
struct DataTypeTraits<
        T,
        std::enable_if_t<
                std::is_integral_v<T> or std::is_floating_point_v<T>>> :
    MsrTypeName<T>
{
    static constexpr size_t row_count    = 0;
    static constexpr size_t column_count = 0;
    static constexpr size_t element_size = sizeof(T);
    static constexpr size_t total_count  = 1;
    using c_data_type                    = T;
};

template <typename T, size_t N>
struct DataTypeTraits<std::array<T, N>> : MsrTypeName<T>
{
    static constexpr size_t row_count    = N;
    static constexpr size_t column_count = 0;
    static constexpr size_t element_size = sizeof(T);
    static constexpr size_t total_count  = N;
    using c_data_type                    = T;
    static_assert(N > 1, "Container must have more than one element");
};

template <typename T, size_t M, size_t N>
struct DataTypeTraits<std::array<std::array<T, N>, M>> :
    MsrTypeName<std::array<std::array<T, N>, M>>
{
    static constexpr size_t row_count    = M;
    static constexpr size_t column_count = N;
    static constexpr size_t element_size = sizeof(T);
    static constexpr size_t total_count  = M * N;
    using c_data_type                    = T;
    static_assert(N > 0 and M > 0, "Container must have at least one element");
    static_assert(
            sizeof(std::array<std::array<T, N>, M>) == M * N * sizeof(T),
            "Array of array is not contiguous (it has padding bytes)");
};

class FakeVariableBase
{
  public:
    const char *name;
    unsigned int datasize, bufsize, task;
    int index = -1;
    const char *typ;
    const char *orientation = nullptr;
    unsigned int anz        = 0;
    unsigned int cnum       = 0;
    unsigned int rnum       = 0;

    constexpr FakeVariableBase(
            const char *name,
            unsigned int datasize,
            unsigned int bufsize,
            unsigned int task,
            const char *typ) :
        name(name), datasize(datasize), bufsize(bufsize), task(task), typ(typ)
    {}

    virtual std::string toBase64() const = 0;
    virtual std::string toXmlTag(
            const std::string &id,
            bool hex,
            bool short_output,
            std::map<std::string, std::string> attrs = {}) const = 0;
    virtual void fromHex(const char *hex_val)                    = 0;
    virtual void fromValue(const char *val)                      = 0;
    virtual void appendToCache(std::vector<char> &cache)         = 0;

    static std::string valuesToHex(const void *data, size_t byte_size);
    static std::string valuesToBase64(const void *data, size_t byte_size);

  protected:
    std::string toXmlTagImpl(
            const std::string &id,
            const char *tag_name,
            bool short_output,
            const std::map<std::string, std::string> &extra_attrs = {}) const;
};

template <typename T>
struct FakeVariable : FakeVariableBase, DataTypeTraits<T>
{
    T data_container;

    using DataTypeTraits<T>::element_size;
    using DataTypeTraits<T>::column_count;
    using DataTypeTraits<T>::row_count;
    using DataTypeTraits<T>::total_count;

    static constexpr size_t total_size =
            total_count * sizeof(typename DataTypeTraits<T>::c_data_type);

    constexpr FakeVariable(
            const char *name,
            unsigned int bufsize,
            unsigned int task,
            T values) :
        FakeVariableBase(
                name,
                element_size,
                bufsize,
                task,
                DataTypeTraits<T>::typ),
        data_container(values)
    {
        if (column_count > 0) {
            anz         = total_count;
            cnum        = column_count;
            rnum        = row_count;
            orientation = "MATRIX_ROW_MAJOR";
        }
        else if (row_count > 0) {
            anz         = total_count;
            orientation = "VECTOR";
        }
    }
    std::string toBase64() const override
    {
        return FakeVariableBase::valuesToBase64(ptr(), total_size);
    }

    void fromHex(const char *hex_val) override
    {
        if (::strlen(hex_val) != 2 * total_size)
            throw std::invalid_argument("invalid hex string");
        auto conv = [](char input) -> unsigned char {
            if (input >= '0' && input <= '9')
                return input - '0';
            if (input >= 'A' && input <= 'F')
                return input - 'A' + 10;
            if (input >= 'a' && input <= 'f')
                return input - 'a' + 10;
            throw std::invalid_argument("invalid hex string");
        };
        auto dst = ptr();
        for (unsigned int i = 0; i < sizeof(T); ++i) {
            dst[i] = conv(hex_val[2 * i]) * 16 + conv(hex_val[2 * i + 1]);
        }
    }

    void fromValue(const char *val) override
    {
        std::istringstream s(val);
        s.exceptions(std::ios_base::badbit | std::ios_base::failbit);
        if constexpr (row_count == 0)
            s >> data_container;
        else
            for (auto &i : data_container)
                if constexpr (column_count == 0)
                    s >> i;
                else
                    for (auto &o : i)
                        s >> o;
    }

    void appendToCache(std::vector<char> &cache) override
    {
        const auto begin = ptr();
        for (auto it = begin; it != begin + total_size; ++it) {
            cache.push_back(*it);
        }
    }

  protected:
    std::string valuesToString() const
    {
        if constexpr (row_count == 0)
            return std::to_string(data_container);
        else {
            std::ostringstream os;
            os.imbue(std::locale {"C"});
            auto print_row =
                    [&os](const std::array<
                            typename DataTypeTraits<T>::c_data_type,
                            !column_count ? row_count : column_count> &row) {
                        auto it = row.begin();
                        os << *it++;
                        for (; it != row.end(); ++it)
                            os << "," << *it;
                    };
            if constexpr (column_count == 0)
                print_row(data_container);
            else {
                auto row_it = data_container.begin();
                print_row(*row_it++);
                for (; row_it != data_container.end(); ++row_it) {
                    os << ",";
                    print_row(*row_it++);
                }
            }
            return os.str();
        }
    }
    std::string toXmlTagImpl(
            const std::string &id,
            const char *tag_name,
            bool hex,
            bool short_output,
            const std::map<std::string, std::string> &extra_attrs) const
    {
        std::map<std::string, std::string> attrs {extra_attrs};
        if (hex)
            attrs["hexvalue"] = valuesToHex(
                    &this->data_container, sizeof(this->data_container));
        else
            attrs["value"] = valuesToString();

        return FakeVariableBase::toXmlTagImpl(
                id, tag_name, short_output, attrs);
    }

    const unsigned char *ptr() const
    {
        if constexpr (row_count == 0)
            return reinterpret_cast<const unsigned char *>(&data_container);
        else
            return reinterpret_cast<const unsigned char *>(
                    data_container.data());
    }
    unsigned char *ptr()
    {
        if constexpr (row_count == 0)
            return reinterpret_cast<unsigned char *>(&data_container);
        else
            return reinterpret_cast<unsigned char *>(data_container.data());
    }
};

template <typename T>
struct FakeParameter : FakeVariable<T>
{
    unsigned int flags;

    constexpr FakeParameter(
            const char *name,
            unsigned int bufsize,
            unsigned int task,
            unsigned int flags,
            T values) :
        FakeVariable<T>(name, bufsize, task, values), flags(flags)
    {}

    std::string toXmlTag(
            const std::string &id,
            bool hex,
            bool short_output,
            std::map<std::string, std::string> attrs) const override
    {
        if (!short_output)
            attrs["flags"] = std::to_string(flags);
        return FakeVariable<T>::toXmlTagImpl(
                id, "parameter", hex, short_output, attrs);
    }
};

template <typename T>
struct FakeSignal : FakeVariable<T>
{
    unsigned int hz;

    constexpr FakeSignal(
            const char *name,
            unsigned int bufsize,
            unsigned int task,
            unsigned int hz,
            T values) :
        FakeVariable<T>(name, bufsize, task, values), hz(hz)
    {}
    std::string toXmlTag(
            const std::string &id,
            bool hex,
            bool short_output,
            std::map<std::string, std::string>) const override
    {
        return FakeSignal<T>::toXmlTagImpl(
                id, "channel", hex, short_output, {{"HZ", std::to_string(hz)}});
    }
};

struct FakeVariableContainerInterface
{
    struct Iterator
    {
        FakeVariableContainerInterface *ptr;
        FakeVariableBase &operator*() const { return *ptr->get(); }
        FakeVariableBase *operator->() const { return ptr->get(); }
        void operator++() { ptr = ptr->next(); }
        bool operator!=(const Iterator &it) const { return ptr != it.ptr; }
    };

    virtual ~FakeVariableContainerInterface()      = default;
    virtual FakeVariableBase *at(const char *)     = 0;
    virtual FakeVariableBase *at(int)              = 0;
    virtual FakeVariableBase *get()                = 0;
    virtual FakeVariableContainerInterface *next() = 0;

    virtual Iterator begin() = 0;
    Iterator end() { return Iterator {nullptr}; }
};

template <template <typename> class Variable, typename... Ts>
class FakeVariableContainer;

template <template <typename> class Variable>
class FakeVariableContainer<Variable> : public FakeVariableContainerInterface
{
  public:
    constexpr FakeVariableContainer(int = 0) {}
    constexpr FakeVariableBase *at(const char *) override { return nullptr; }
    constexpr FakeVariableBase *at(int) override { return nullptr; }
    FakeVariableBase *get() override { return nullptr; }
    FakeVariableContainerInterface *next() override { return nullptr; }
    Iterator begin() override { return {nullptr}; }
    constexpr FakeVariableContainerInterface *getPtrForIterator()
    {
        return nullptr;
    }
};


template <template <typename> class Variable, typename T, typename... Ts>
class FakeVariableContainer<Variable, T, Ts...> :
    public FakeVariableContainer<Variable, Ts...>
{
    Variable<T> my_signal;

  protected:
    constexpr FakeVariableContainer(
            int index,
            Variable<T> const &a,
            Variable<Ts> const &...o) :
        FakeVariableContainer<Variable, Ts...>(index + 1, o...), my_signal(a)
    {
        my_signal.index = index;
    }

  public:
    constexpr FakeVariableContainer(
            Variable<T> const &a,
            Variable<Ts> const &...o) :
        FakeVariableContainer(0, a, o...)
    {}

    constexpr FakeVariableBase *at(const char *name) override
    {
        return !std::strcmp(name, my_signal.name)
                ? &my_signal
                : FakeVariableContainer<Variable, Ts...>::at(name);
    }
    constexpr FakeVariableBase *at(int index) override
    {
        return index == 0
                ? &my_signal
                : FakeVariableContainer<Variable, Ts...>::at(index - 1);
    }
    constexpr FakeVariableBase *get() override { return &my_signal; }

    constexpr FakeVariableContainerInterface *getPtrForIterator()
    {
        return this;
    }

    constexpr FakeVariableContainerInterface *next() override
    {
        return FakeVariableContainer<Variable, Ts...>::getPtrForIterator();
    }

    constexpr FakeVariableContainerInterface::Iterator begin() override
    {
        return {this};
    }
};
