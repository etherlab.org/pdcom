/*****************************************************************************
 * vim:tw=78
 *
 * Copyright (C) 2021 Bjarne von Horn (vh at igh dot de).
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#pragma once

#include <map>
#include <optional>
#include <ostream>
#include <sstream>
#include <string>
#include <vector>


struct XmlTag
{
    std::string name;
    std::map<std::string, std::string> attributes;
    std::vector<XmlTag> children;
};

std::ostream &operator<<(std::ostream &os, const XmlTag &tag);


class XmlAttr
{
    const char **atts;

  public:
    const char *
    find(const std::string &name, bool throw_if_notfound = false) const
    {
        for (const char **a = atts; *a; a += 2) {
            if (name == *a)
                return *(a + 1);
        }
        if (throw_if_notfound)
            throw xml_attribute_not_found(name);
        return nullptr;
    }

    XmlAttr(const char **atts) : atts(atts) {}

    class xml_attribute_not_found : public std::exception
    {
        std::string desc;

      public:
        xml_attribute_not_found(const std::string &name) :
            std::exception(), desc("XML Attribute " + name + " not found")
        {}

        const char *what() const noexcept override { return desc.c_str(); }
    };

    unsigned int getUInt(const std::string &name) const
    {
        return std::stoul(find(name, true));
    }

    unsigned int getUIntOr(const std::string &name, unsigned int v = 0) const
    {
        if (const auto s = find(name, false))
            return std::stoul(s);
        return v;
    }

    bool getBoolOr(const std::string &name, bool v = false) const
    {
        if (const auto s = find(name, false))
            return s == std::string("1");
        return v;
    }

    std::optional<unsigned int> getOptionalUInt(const std::string &name) const
    {
        if (const auto s = find(name, false))
            return {std::stoul(s)};
        return {std::nullopt};
    }

    std::vector<int> getUIntList(const std::string &name) const
    {
        const auto s = find(name, false);
        if (!s || *s == '\0')
            return {};
        std::vector<int> ans;
        std::istringstream is(s);
        const auto push = [&ans, &is]() {
            int i;
            is >> i;
            if (i >= 0)
                ans.push_back(i);
            else
                throw std::invalid_argument(
                        "negative integer in vector of uint");
        };
        is.exceptions(std::ios_base::failbit | std::ios_base::badbit);
        push();
        while (!is.eof()) {
            char c;
            is >> c;
            if (c != ',')
                throw std::invalid_argument("invalid separator");
            if (is.eof())
                throw std::invalid_argument("trailing separator");
            push();
        }
        return ans;
    }

    std::string getString(const std::string &name) const
    {
        return find(name, true);
    }

    std::optional<std::string> getOptionalString(const std::string &name) const
    {
        if (const auto s = find(name, false))
            return {s};
        return std::nullopt;
    }

    std::string
    getStringOr(const std::string &name, const std::string &v = "") const
    {
        if (const auto s = find(name, false))
            return s;
        return v;
    }
};
