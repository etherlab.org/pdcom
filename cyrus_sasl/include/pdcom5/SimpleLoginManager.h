/*****************************************************************************
 * vim:tw=78
 *
 * Copyright (C) 2021 Richard Hacker (lerichi at gmx dot net),
 *                    Florian Pose (fp at igh dot de),
 *                    Bjarne von Horn (vh at igh dot de).
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

/** @file */

#ifndef PDCOM5_SIMPLELOGINMANAGER_H
#define PDCOM5_SIMPLELOGINMANAGER_H

#include "pdcom5-sasl_export.h"

#include <memory>
#include <pdcom5/Sasl.h>
#include <stdexcept>
#include <vector>

extern "C" struct sasl_callback;

namespace PdCom {
namespace impl {
class SimpleLoginManager;
}  // namespace impl

/** \class SimpleLoginManager
 * Wrapper for Cyrus SASL library.
 *
 * This class provides a nice interface to enable simple authentification.
 * All you have to do is overriding some of the callbacks below.
 * First, register your instance to the Process using Process::setAuthManager().
 * After that, call login(). Then, call Process::asyncData() until completed()
 * is called. During this process, you're being asked to enter credentials via
 * the given callbacks.
 *
 * \example sasl_example.cpp
 */

class PDCOM5_SASL_EXPORT SimpleLoginManager : public Sasl
{
    friend class impl::SimpleLoginManager;
    std::unique_ptr<impl::SimpleLoginManager> impl_;
    void loginReply(const char *mechlist, const char *serverData, int finished)
            override;

  public:
    /** Sasl global initialization.
     *
     * Call this at startup of your application to initialize the underlying
     * sasl library.
     *
     * \throws PdCom::Exception Initialization failed.
     */
    static void InitLibrary(const char *plugin_path = nullptr);
    /** Sasl global finalization
     */
    static void FinalizeLibrary();

    /** Constructor.
     *
     * \param remote_host Remote hostname.
     * \param additional_callbacks NULL-terminated list of additional callback
     * handlers.
     */
    SimpleLoginManager(
            const char *remote_host,
            sasl_callback *additional_callbacks = nullptr);
    SimpleLoginManager(SimpleLoginManager &&) noexcept;
    SimpleLoginManager &operator=(SimpleLoginManager &&) noexcept;


    /** Exception for callback cancelation.
     *
     * Throw this in one of the callbacks (like getAuthname()) to cancel the
     * current login step.
     */
    struct Cancel : std::exception
    {};

    /// @brief Result of login operation
    enum class LoginResult {
        Success,
        Error,
        Canceled,
        NoSaslMechanism, /**< No matching SASL Mechanism found on client
                            machine. */
    };

  protected:
    virtual ~SimpleLoginManager();

    /** Perform SASL login step.
     *
     * @param mech SASL mechanism
     * @param clientData Base64 encoded SASL output data to server
     *
     * Setting both \p mech and \p clientData to NULL will initate the
     * login process.
     *
     * Every call to login() is answered by a loginReply(), unless
     * login is not supported. When login is mandatory, loginReply()
     * will be called automatically.
     *
     * @return false if login is not supported
     */
    bool login();

    /** Logout from server
     */
    using Sasl::logout;
    /** Callback to get login name.
     */
    virtual std::string getAuthname() { throw Cancel(); }
    /** Callback to get password.
     */
    virtual std::string getPassword() { throw Cancel(); }
    /** Callback to get realm.
     */
    virtual std::string
    getRealm(const std::vector<const char *> & /* available realms */)
    {
        throw Cancel();
    }
    /** SASL get option callback */
    virtual std::string
    getOption(const char * /*plugin_name*/, const char * /*option*/)
    {
        throw Cancel();
    }
    /** SASL interact callback */
    virtual std::string interact(
            unsigned long /*id*/,
            const char * /*challenge*/,
            const char * /*prompt*/,
            const char * /*default result*/)
    {
        throw Cancel();
    }
    /** Authentification completed callback.
     *
     * \param result Outcome of login operation.
     */
    virtual void completed(LoginResult result) = 0;

    /** Log callback.
     *
     * \param level Log level, 0 to 7. 7 may contain passwords.
     * \param message Message.
     */
    virtual void log(int level, const char *message);
};

}  // namespace PdCom

#endif  // PDCOM5_SIMPLELOGINMANAGER_H
