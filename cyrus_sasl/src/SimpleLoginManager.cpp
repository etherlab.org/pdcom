/*****************************************************************************
 * vim:tw=78
 *
 * Copyright (C) 2021 Richard Hacker (lerichi at gmx dot net),
 *                    Florian Pose (fp at igh dot de),
 *                    Bjarne von Horn (vh at igh dot de).
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#include "SimpleLoginManagerImpl.h"

#include <algorithm>
#include <array>
#include <cstring>
#include <iostream>
#include <new>
#include <pdcom5/Exception.h>
#include <pdcom5/Process.h>
#include <pdcom5/SimpleLoginManager.h>
#include <sasl/sasl.h>
#include <sasl/saslutil.h>
#include <stdexcept>
#include <type_traits>

// #define DEBUG_SASL

using SLM  = PdCom::SimpleLoginManager;
using SLMi = PdCom::impl::SimpleLoginManager;

namespace {
std::string bb64decode(const char *data)
{
    if (!data or !*data)
        return "";
    const auto b64_len      = ::strlen(data);
    unsigned int binary_len = (b64_len + 3) / 4 * 3 + 3;
    std::string ans(binary_len, '0');
    if (sasl_decode64(data, b64_len, &ans[0], binary_len, &binary_len)
        != SASL_OK)
        throw PdCom::Exception("sasl_decode64 failed");
    ans.resize(binary_len);
    return ans;
}
std::string bb64encode(const char *data, size_t len)
{
    unsigned int b64_len = (len + 2) / 3 * 4 + 3;
    std::string ans(b64_len, '0');
    if (sasl_encode64(data, len, &ans[0], b64_len, &b64_len) != SASL_OK)
        throw PdCom::Exception("sasl_encode64 failed");
    ans.resize(b64_len);
    return ans;
}
}  // namespace

SLM::SimpleLoginManager(
        const char *remote_host,
        sasl_callback *additional_callbacks) :
    impl_(new SLMi(this, remote_host, additional_callbacks))
{}

SLM::SimpleLoginManager(SLM &&o) noexcept :
    Sasl(std::move(o)), impl_(std::move(o.impl_))
{
    if (impl_)
        impl_->This_ = this;
}

SLM::~SimpleLoginManager() = default;

SLM &SLM::operator=(SLM &&o) noexcept
{
    if (&o == this)
        return *this;
    static_cast<Sasl &>(*this) = static_cast<Sasl &&>(o);
    std::swap(impl_, o.impl_);

    if (impl_)
        impl_->This_ = this;
    if (o.impl_)
        o.impl_->This_ = &o;
    return *this;
}

bool SLM::login()
{
    if (!impl_)
        throw PdCom::InvalidArgument("Used Moved-from instance!");
    impl_->startNewSession();
    return Sasl::loginStep(nullptr, nullptr);
}

void SLM::log(int /*level*/, const char * /*message*/)
{}

void SLMi::SaslContextDeleter::operator()(sasl_conn_t *c) const
{
    sasl_dispose(&c);
}

struct SLMi::CallbackManager
{
    static int
    getSimple(void *context, int id, const char **result, unsigned *len);
    static int getSecret(
            sasl_conn_t * /*conn*/,
            void *context,
            int id,
            sasl_secret_t **psecret);
    static int getOption(
            void *context,
            const char *plugin_name,
            const char *option,
            const char **result,
            unsigned *len);
    static int getRealm(
            void *context,
            int id,
            const char **availrealms,
            const char **result);
    static int log(void *context, int level, const char *message);
    static std::unique_ptr<sasl_callback[]> make_callbacks(
            SimpleLoginManager *primary_context,
            sasl_callback_t *additional_callbacks);
};
int SLMi::CallbackManager::getSimple(
        void *context,
        int id,
        const char **result,
        unsigned *len)
{
    *result = nullptr;
    if (id != SASL_CB_AUTHNAME)
        return SASL_FAIL;

    SLMi &am = *reinterpret_cast<SLMi *>(context);
    try {
        am.cached_userdata_.emplace_front(am.This_->getAuthname());
        *result = am.cached_userdata_.front().data();
        if (len)
            *len = am.cached_userdata_.front().size();
        return SASL_OK;
    }
    catch (SLM::Cancel const &) {
        am.state_ = SLMi::State::Canceled;
    }
    catch (std::exception const &) {
    }
    return SASL_FAIL;
}

int SLMi::CallbackManager::getSecret(
        sasl_conn_t * /*conn*/,
        void *context,
        int id,
        sasl_secret_t **psecret)
{
    *psecret = nullptr;
    if (id != SASL_CB_PASS)
        return SASL_FAIL;

    SLMi &am = *reinterpret_cast<SLMi *>(context);
    try {
        const auto pw = am.This_->getPassword();
        // allocate buffer of char's and do some placement new
        // yes that's annoying but you really don't want UB here...
        static_assert(
                std::is_trivially_destructible<sasl_secret_t>::value,
                "dtor of sasl_secret_t must be skippable");
        am.cached_password_.reset(new char[sizeof(long) + pw.size()]());
        sasl_secret_t *const secret =
                new (am.cached_password_.get()) sasl_secret_t();
        secret->len = pw.size();
        std::copy(pw.begin(), pw.end(), &secret->data[0]);
        *psecret = secret;
        return SASL_OK;
    }
    catch (SLM::Cancel const &) {
        am.state_ = SLMi::State::Canceled;
    }
    catch (std::exception const &) {
    }
    return SASL_FAIL;
}

int SLMi::CallbackManager::getOption(
        void *context,
        const char *plugin_name,
        const char *option,
        const char **result,
        unsigned *len)
{
    *result  = nullptr;
    SLMi &am = *reinterpret_cast<SLMi *>(context);
    try {
        am.cached_userdata_.emplace_front(
                am.This_->getOption(plugin_name, option));
        if (am.cached_userdata_.front().empty()) {
            am.cached_userdata_.pop_front();
            *result = nullptr;
            if (len)
                *len = 0;
        }
        else {
            *result = am.cached_userdata_.front().data();
            if (len)
                *len = am.cached_userdata_.front().size();
        }
        return SASL_OK;
    }
    catch (SLM::Cancel const &) {
        // do not set state to canceled as it is meant to be for username/pw not
        // supplied
    }
    catch (std::exception const &) {
    }
    return SASL_FAIL;
}


int SLMi::CallbackManager::getRealm(
        void *context,
        int id,
        const char **availrealms,
        const char **result)
{
    if (id != SASL_CB_GETREALM)
        return SASL_FAIL;
    *result  = nullptr;
    SLMi &am = *reinterpret_cast<SLMi *>(context);
    try {
        std::vector<const char *> avail_reams;
        if (availrealms) {
            for (; *availrealms; availrealms++) {
                avail_reams.push_back(*availrealms);
            }
        }
        am.cached_userdata_.emplace_front(am.This_->getRealm(avail_reams));
        *result = am.cached_userdata_.front().data();
        return SASL_OK;
    }
    catch (SLM::Cancel const &) {
        am.state_ = SLMi::State::Canceled;
    }
    catch (std::exception const &) {
    }
    return SASL_FAIL;
}

int SLMi::CallbackManager::log(void *context, int level, const char *message)
{
    const SLMi &am = *reinterpret_cast<const SLMi *>(context);
    try {
        if (am.This_)
            am.This_->log(level, message);
    }
    catch (...) {
        return SASL_FAIL;
    }

    return SASL_OK;
}

std::unique_ptr<sasl_callback[]> SLMi::CallbackManager::make_callbacks(
        SimpleLoginManager *primary_context,
        sasl_callback_t *additional_callbacks)
{
    using cb_t = int (*)();
#ifndef _MSC_VER
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wcast-function-type"
#endif
    const std::array<sasl_callback, 5> primary_callbacks {{
            {SASL_CB_GETOPT, (cb_t) getOption, primary_context},
            {SASL_CB_AUTHNAME, (cb_t) CallbackManager::getSimple,
             primary_context},
            {SASL_CB_PASS, (cb_t) CallbackManager::getSecret, primary_context},
            {SASL_CB_GETREALM, (cb_t) CallbackManager::getRealm,
             primary_context},
            {SASL_CB_LOG, (cb_t) CallbackManager::log, primary_context},
    }};
#ifndef _MSC_VER
#pragma GCC diagnostic pop
#endif
    size_t count = primary_callbacks.size() + 1;
    for (auto i = additional_callbacks; i && i->id != SASL_CB_LIST_END; ++i) {
        ++count;
    }
    std::unique_ptr<sasl_callback[]> ans {new sasl_callback[count]()};
    std::copy(primary_callbacks.begin(), primary_callbacks.end(), ans.get());
    size_t occupied_slots = primary_callbacks.size();
    for (auto i = additional_callbacks; i && i->id != SASL_CB_LIST_END; ++i) {
        const auto current_end = ans.get() + occupied_slots;
        const auto match       = std::find_if(
                      ans.get(), current_end,
                      [i](const sasl_callback_t &e) { return e.id == i->id; });
        if (match == current_end)
            ++occupied_slots;
        *match = *i;
    }
    ans[occupied_slots] = {SASL_CB_LIST_END, nullptr, nullptr};
    return ans;
}

SLMi::SimpleLoginManager(
        SLM *This,
        const char *host,
        sasl_callback *additional_callbacks) :
    This_(This),
    host_(host),
    callbacks_(CallbackManager::make_callbacks(this, additional_callbacks)),
    cached_userdata_(5)
{}

void SLMi::startNewSession()
{
    state_                = SimpleLoginManager::State::Init;
    sasl_conn_t *sasl_ctx = nullptr;
    if (sasl_client_new(
                "pdserv", host_.c_str(), nullptr, nullptr, callbacks_.get(),
                SASL_SUCCESS_DATA, &sasl_ctx)
        != SASL_OK)
        throw PdCom::Exception("sasl client new failed");
    sasl_ctx_.reset(sasl_ctx);
    sasl_ctx = nullptr;
    cached_userdata_.clear();
    cached_password_.reset();
}

void SLM::loginReply(const char *mechlist, const char *serverData, int finished)
{
    if (!impl_) {
        throw PdCom::InvalidArgument("Used Moved-from instance!");
    }

    if (!impl_->sasl_ctx_) {
        impl_->startNewSession();
    }

    if (finished == 0) {
        using State                = SLMi::State;
        sasl_interact_t *interacts = nullptr;
        const char *client_out = nullptr, *mech = nullptr;
        unsigned int client_out_len = 0;
        int res                     = SASL_FAIL;
        if (impl_->state_ == State::Init) {
            impl_->state_ = State::MechRecieved;
            do {
                res = sasl_client_start(
                        impl_->sasl_ctx_.get(), mechlist, &interacts,
                        &client_out, &client_out_len, &mech);
#ifdef DEBUG_SASL
                std::cerr << __func__ << "() sasl_client_start " << res
                          << " mechlist " << mechlist << std::endl;
#endif
                impl_->doInteract(interacts);
            } while (res == SASL_INTERACT);
        }
        else if (impl_->state_ == State::Pending) {
            const std::string s = bb64decode(serverData);
            do {
                res = sasl_client_step(
                        impl_->sasl_ctx_.get(), s.c_str(), s.size(), &interacts,
                        &client_out, &client_out_len);
#ifdef DEBUG_SASL
                std::cerr << __func__ << "() sasl_client_step " << res
                          << std::endl;
#endif
                impl_->doInteract(interacts);
            } while (res == SASL_INTERACT);
        }
        else {
            return;
        }

        switch (res) {
            case SASL_OK:
                impl_->state_ = State::Completed;
                break;
            case SASL_CONTINUE:
                impl_->state_ = State::Pending;
                break;
            case SASL_NOMECH:
                impl_->state_ = State::Error;
                completed(LoginResult::NoSaslMechanism);
                impl_->sasl_ctx_.reset();
                return;
            default:
                if (impl_->state_ == State::Canceled) {
                    completed(LoginResult::Canceled);
                    impl_->startNewSession();
                }
                else {
                    impl_->state_ = State::Error;
                    completed(LoginResult::Error);
                    std::cerr << sasl_errdetail(impl_->sasl_ctx_.get())
                              << std::endl;
                    impl_->sasl_ctx_.reset();
                }
                return;
        }
        if (res == SASL_CONTINUE or res == SASL_OK) {
            const auto s = bb64encode(client_out, client_out_len);
            Sasl::loginStep(mech, s.c_str());
        }
    }
    else if (finished > 0) {
        impl_->sasl_ctx_.reset();
        completed(LoginResult::Success);
    }
    else if (finished < 0) {
        impl_->sasl_ctx_.reset();
        completed(LoginResult::Error);
    }
}

void SLMi::doInteract(sasl_interact_t *interacts)
{
    for (; interacts && interacts->id != SASL_CB_LIST_END; ++interacts) {
        try {
            cached_userdata_.emplace_front(This_->interact(
                    interacts->id, interacts->challenge, interacts->prompt,
                    interacts->defresult));
        }
        catch (SLM::Cancel const &) {
            interacts->result = nullptr;
            continue;
        }
        interacts->result = cached_userdata_.front().data();
        interacts->len    = cached_userdata_.front().size();
    }
}

static char plugin_path_buffer[2049];

static int sasl_get_path(void *, const char **path)
{
    *path = plugin_path_buffer;
    return SASL_OK;
}

#ifndef _MSC_VER
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wcast-function-type"
#endif
static const sasl_callback_t static_callbacks[] = {
        {SASL_CB_GETPATH, (int (*)(void))(sasl_get_path), nullptr},
        {SASL_CB_LIST_END, nullptr, nullptr},
};
#ifndef _MSC_VER
#pragma GCC diagnostic pop
#endif

void SLM::InitLibrary(const char *plugin_path)
{
    const sasl_callback_t *callbacks = nullptr;
    if (plugin_path && *plugin_path) {
        strncpy(plugin_path_buffer, plugin_path,
                sizeof(plugin_path_buffer) - 1);
        plugin_path_buffer[sizeof(plugin_path_buffer) - 1] = 0;

        callbacks = static_callbacks;
    }
    if (sasl_client_init(callbacks) != SASL_OK)
        throw PdCom::Exception("sasl_client_init() failed");
}

void SLM::FinalizeLibrary()
{
    sasl_client_done();
}
