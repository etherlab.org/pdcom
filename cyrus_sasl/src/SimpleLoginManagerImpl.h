/*****************************************************************************
 * vim:tw=78
 *
 * Copyright (C) 2021 Richard Hacker (lerichi at gmx dot net),
 *                    Florian Pose (fp at igh dot de),
 *                    Bjarne von Horn (vh at igh dot de).
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#ifndef PDCOM5_SIMPLELOGINMANAGER_IMPL_H
#define PDCOM5_SIMPLELOGINMANAGER_IMPL_H

extern "C" {
struct sasl_conn;
struct sasl_interact;
struct sasl_callback;
}

#include <forward_list>
#include <memory>
#include <string>

namespace PdCom {
class SimpleLoginManager;

namespace impl {

class SimpleLoginManager
{
  public:
    enum class State {
        Init,
        MechRecieved,
        Pending,
        Completed,
        Error,
        Canceled,
    };

    SimpleLoginManager(
            PdCom::SimpleLoginManager *This,
            const char *host,
            sasl_callback *additional_callbacks);
    void loginReply(const char *mechlist, const char *serverData, int finished);

  private:
    void doInteract(sasl_interact *);
    void startNewSession();

    struct SaslContextDeleter
    {
        void operator()(sasl_conn *) const;
    };

    struct CallbackManager;

    friend PdCom::SimpleLoginManager;
    PdCom::SimpleLoginManager *This_;
    const std::string host_;
    const std::unique_ptr<sasl_callback[]> callbacks_;
    std::unique_ptr<sasl_conn, SaslContextDeleter> sasl_ctx_;
    // pointers to strings need to stay valid on resize, so not using a vector
    // here
    std::forward_list<std::string> cached_userdata_;
    std::unique_ptr<char[]> cached_password_;
    State state_ = State::Init;
};

}  // namespace impl
}  // namespace PdCom

#endif  // PDCOM5_SIMPLELOGINMANAGER_IMPL_H
