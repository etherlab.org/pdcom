##############################################################################
#
#  Copyright (C) 2015-2016  Richard Hacker (lerichi at gmx dot net)
#                           Florian Pose <fp@igh.de>
#
#  This file is part of the PdCom library.
#
#  The PdCom library is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Lesser General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or (at your
#  option) any later version.
#
#  The PdCom library is distributed in the hope that it will be useful, but
#  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
#  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
#  License for more details.
#
#  You should have received a copy of the GNU Lesser General Public License
#  along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

IF (USE_SASL)

    ADD_EXECUTABLE(sasl_example sasl_example.cpp)
    TARGET_LINK_LIBRARIES(sasl_example ${LIBNAME} ${LIBNAME}-sasl)
ENDIF()

IF (GNUTLS_FOUND)
    ADD_EXECUTABLE(gnutls_example gnutls_example.cpp)
    TARGET_LINK_LIBRARIES(gnutls_example PUBLIC ${LIBNAME} ${LIBNAME}-gnutls)
ENDIF()

ADD_EXECUTABLE(advanced_example advanced_example.cpp)
TARGET_LINK_LIBRARIES(advanced_example ${LIBNAME})

ADD_EXECUTABLE(simple_osc_example simple_osc_example.cpp)
TARGET_LINK_LIBRARIES(simple_osc_example ${LIBNAME})
