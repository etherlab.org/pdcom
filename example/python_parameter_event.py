# Copyright (C) 2022 Bjarne von Horn (vh at igh dot de).
#
# This file is part of the PdCom library.
#
# The PdCom library is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# The PdCom library is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
# License for more .
#
# You should have received a copy of the GNU Lesser General Public License
# along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.

import pdcom5
import asyncio
import sys

"""
Monitor "/osc/enable" parameter from pdserv-example-st.
"""


async def main(url):
    process = pdcom5.Process()
    await process.connect(url)
    # Make event-based subscription
    subscription = await process.subscribe(
        pdcom5.Transmission.event_mode,
        "/osc/enable",
    )
    # poll initial value
    value = await subscription.poll()
    while True:
        print("Oscillator is", "enabled" if value else "disabled")
        # wait until new value arrives.
        value, _ = await subscription.read()


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage:", sys.argv[0], "url")
        exit(1)
    asyncio.run(main(sys.argv[1]))
