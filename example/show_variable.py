# Copyright (C) 2022 Bjarne von Horn (vh at igh dot de).
#
# This file is part of the PdCom library.
#
# The PdCom library is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# The PdCom library is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
# License for more .
#
# You should have received a copy of the GNU Lesser General Public License
# along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.

import asyncio
import pdcom5
import sys


async def main(url, name):
    process = pdcom5.Process()
    await process.connect(url)
    var = await process.find(name)
    if var is None:
        print("Variable not found")
        exit(1)
    value, timestamp = await var.poll()
    print("Variable", var.path, "is", value)


if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage:", sys.argv[0], "<url> <variable_name>")
        exit(1)
    asyncio.run(main(sys.argv[1], sys.argv[2]))
