/*****************************************************************************
 *
 * Copyright (C) 2021 Bjarne von Horn (vh at igh dot de).
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

/* PdCom example with authentification
 *
 * This example requires a pdserv instance with authentification.
 * After the connection is established, a login is performed and a list of all
 * variables is fetched.
 *
 */

#include <iostream>
#include <pdcom5/PosixProcess.h>
#include <pdcom5/Process.h>
#include <pdcom5/SimpleLoginManager.h>
#include <pdcom5/Variable.h>


class MyProcess : public PdCom::Process, private PdCom::PosixProcess
{
    int read(char *buf, int count) override { return posixRead(buf, count); }
    void write(const char *buf, size_t count) override
    {
        posixWriteBuffered(buf, count);
    }
    void flush() override { posixFlush(); }

    void listReply(
            std::vector<PdCom::Variable> vars,
            std::vector<std::string> /* directories */) override
    {
        // enumerate all variables
        for (const auto &v : vars)
            std::cout << "Found var " << v.getPath() << "\n";
        running_ = false;
    }

    void connected() override
    {
        // connection is established, but wait for login
        std::cout << "Connected!" << std::endl;
        connected_ = true;
    }

  public:
    MyProcess(const char *host = "127.0.0.1") :
        PdCom::Process(), PdCom::PosixProcess(host, 2345)
    {}

    // Make list() and setAuthManager() public
    using Process::list;
    using Process::setAuthManager;

    bool running_   = true;
    bool connected_ = false;
};

class MyAuthManager : public PdCom::SimpleLoginManager
{
  public:
    MyProcess *p_ = nullptr;
    MyAuthManager() : PdCom::SimpleLoginManager("localhost") {}
    // provide credentials
    std::string getAuthname() override { return "bjarne"; }
    std::string getPassword() override { return "test"; }

    using Sasl::logout;
    using SimpleLoginManager::login;

    void completed(LoginResult result) override
    {
        // authentification has completed.
        std::cout << "completed: " << static_cast<int>(result) << std::endl;
        if (result == LoginResult::Success) {
            // ask server for a list of all variables
            p_->list("");
        }
    }
};

int main()
{
    // initialize the cyrus SASL library
    PdCom::SimpleLoginManager::InitLibrary();
    MyAuthManager am;
    MyProcess p;
    // register our AuthManager
    p.setAuthManager(&am);
    am.p_ = &p;
    // wait for the connection to be established
    while (!p.connected_)
        p.asyncData();
    // start login
    am.login();
    while (p.running_) {
        // perform login, after that process incoming data
        p.asyncData();
    }
    am.logout();
    p.asyncData();
    return 0;
}
