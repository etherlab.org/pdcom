/*****************************************************************************
 *
 * Copyright (C) 2022 Bjarne von Horn (vh at igh dot de).
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

/** Simple PdCom example.
 * This is a tiny user interface for the example of pdserv,
 * which provides an oscillator.
 * We subscribe to its cosine output with 2 Hz,
 * so we get a new value two times per second.
 * In addition, the amplitute can be freezed
 * by setting the parameter /osc/enable to zero.
 * This can be done with the keyboard (keys 'f' and 'u').
 * To quit the application, press 'q'.
 */

#include <cstdio>
#include <iostream>
#include <pdcom5/PosixProcess.h>
#include <pdcom5/Process.h>
#include <pdcom5/Subscriber.h>
#include <pdcom5/Subscription.h>
#include <pdcom5/Variable.h>
#include <termios.h>
#include <unistd.h>

// RAII helper to configure the shell. Nothing to do with PdCom.
class TerminalGuard
{
    termios old_;

  public:
    TerminalGuard()
    {
        tcgetattr(STDIN_FILENO, &old_);
        termios newt = old_;
        newt.c_lflag &= ~(ECHO | ICANON);
        tcsetattr(STDIN_FILENO, TCSANOW, &newt);
    }
    ~TerminalGuard() { tcsetattr(STDIN_FILENO, TCSANOW, &old_); }
};

/* This class combines the Subscriber (which is like a mailbox) with the
 * Subscription (in terms of subscribing to a newspaper). One Subscriber can be
 * used with multiple Subscriptions in more complicated scenarios, though.
 */
class SimpleSubscription : public PdCom::Subscriber, public PdCom::Subscription
{
    unsigned int progress = 0;

    void stateChanged(PdCom::Subscription const & /* me */) override
    {
        // other states are Pending and Active.
        if (getState() == State::Invalid) {
            std::cerr << "Invalid Subscription!" << std::endl;
            exit(-1);
        }
    }

    // when a new value has arrived, update progress bar on shell
    void newValues(std::chrono::nanoseconds /* time_ns */) override
    {
        const char progress_char[] = {'-', '/', '|', '\\'};
        // get value from PdCom
        const int value = getValue<int>();
        // print progress bar and the new value
        std::cout << '\r' << progress_char[progress] << " " << value
                  << "         " << std::flush;
        ++progress;
        if (progress >= sizeof(progress_char))
            progress -= sizeof(progress_char);
    }

  public:
    SimpleSubscription(PdCom::Process &process, const char *path) :
        PdCom::Subscriber(std::chrono::milliseconds(500)),
        PdCom::Subscription(*this, process, path)
    {}
};

/* This is the main entry point for PdCom.
 * As the PdCom::Process does not come with a socket implementation,
 * (i.e. read(), write() and flush() are to be overwritten)
 * we provide one in PosixProcess for convenience.
 * Other socket implementations (like QTCPSocket) can be used, too.
 *
 * When new data has arrived, use Process::asyncData() to load it into PdCom
 * (which will then call read()).
 */
class MyProcess : public PdCom::Process, public PdCom::PosixProcess
{
    // read, write and flush are implemented in PosixProcess.
    // use your own socket implementation if you like.
    int read(char *buf, int count) override
    {
        const int ans = posixRead(buf, count);
        // stop on EOF
        if (ans == 0)
            running_ = false;
        return ans;
    }

    void write(const char *buf, size_t count) override
    {
        posixWriteBuffered(buf, count);
    }
    void flush() override { posixFlush(); }

    // this will be called when the welcome message from pdserv has arrived
    void connected() override
    {
        // connection is established.
        connected_ = true;
    }

    // this is the answer to Process::find()
    void findReply(PdCom::Variable const &result) override
    {
        variable_find_reply_arrived_ = true;
        find_reply_result_           = result;
    }

  public:
    MyProcess(const char *host = "127.0.0.1", unsigned short port = 2345) :
        PdCom::Process(), PdCom::PosixProcess(host, port)
    {}

    // make the fd from PosixProcess available for select() it
    int fileDescriptor() const { return PdCom::PosixProcess::fd_; }

    // make find() public
    using Process::find;

    bool connected_ = false, running_ = true,
         variable_find_reply_arrived_ = false;
    PdCom::Variable find_reply_result_;
};

int main(int argc, char **argv)
{
    // set up console
    const TerminalGuard tg;
    // usage: simple_osc_example <host> <port>
    const char *const host  = argc >= 2 ? argv[1] : "127.0.0.1";
    const unsigned int port = argc >= 3 ? strtoul(argv[2], nullptr, 10) : 2345;
    MyProcess process(host, port);

    // spin while welcome message from server has not arrived
    while (!process.connected_)
        process.asyncData();
    // go find the parameter we need to write
    process.find("/osc/enable");
    // and spin until it arrives
    while (!process.variable_find_reply_arrived_)
        process.asyncData();
    const PdCom::Variable parameter = process.find_reply_result_;
    // and verify it was found
    if (parameter.empty()) {
        std::cerr << "Parameter not found!\n";
        return -1;
    }

    // subscribe to one of the outputs of the oscillator
    // the rate is set to 2Hz in the constructor of Subscriber
    const SimpleSubscription subscription(process, "/osc/cos");

    std::cout << "Connected. Press 'f' to freeze and 'u' to unfreeze the "
                 "amplitude and q to quit."
              << std::endl;

    // to work on input from socket and stdin, we use select().
    fd_set fds;
    const int max_fd = std::max<int>(process.fileDescriptor(), STDIN_FILENO);
    while (process.running_) {
        FD_ZERO(&fds);
        FD_SET(process.fileDescriptor(), &fds);
        FD_SET(STDIN_FILENO, &fds);

        select(max_fd + 1, &fds, NULL, NULL, NULL);

        // process input from stdin
        if (FD_ISSET(STDIN_FILENO, &fds)) {
            char buf;
            std::cin.read(&buf, 1);
            if (buf == 'q') {
                break;
            }

            if (buf == 'u')
                parameter.setValue(true);
            else if (buf == 'f')
                parameter.setValue(false);
        }

        // ask PdCom to process incoming data
        if (FD_ISSET(process.fileDescriptor(), &fds)) {
            process.asyncData();
        }
    }
    return 0;
}
