# Copyright (C) 2022 Bjarne von Horn (vh at igh dot de).
#
# This file is part of the PdCom library.
#
# The PdCom library is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# The PdCom library is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
# License for more .
#
# You should have received a copy of the GNU Lesser General Public License
# along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.


"""
Small example for PdCom Python api.
To run it, start the pdserv-example-st from pdserv.
"""

import asyncio
import pdcom5
import sys
import termios


class TerminalGuard:
    """Configures the console input (no echo, don't wait for enter)"""

    def __enter__(self):
        self._fd = sys.stdin.fileno()
        self._old = termios.tcgetattr(self._fd)
        new = termios.tcgetattr(self._fd)
        new[3] &= ~(termios.ECHO | termios.ICANON)
        termios.tcsetattr(self._fd, termios.TCSADRAIN, new)
        return self

    def __exit__(self, exc_type, exc, tb):
        termios.tcsetattr(self._fd, termios.TCSADRAIN, self._old)
        return False


class OscExample:
    def __init__(self):
        self._running = True
        self._process = pdcom5.Process()
        self._enable_var: pdcom5.Variable = None

    async def run(self, url):
        # connect to process
        await self._process.connect(url)
        # find parameter which enables oscillator output
        self._enable_var = await self._process.find("/osc/enable")
        if self._enable_var is None:
            raise RuntimeError("Variable not found")

        # subscribe to oscillator output as a background task
        self._task = asyncio.get_event_loop().create_task(self._variable_task())

        try:
            # accept commands on console
            await self._read_console_input()
        finally:
            # clean up background task
            self._task.cancel()
            await asyncio.gather(self._task, return_exceptions=True)

    async def _read_console_input(self):
        # parse commands from console.
        reader = asyncio.StreamReader()
        loop = asyncio.get_event_loop()
        await loop.connect_read_pipe(
            lambda: asyncio.StreamReaderProtocol(reader), sys.stdin
        )

        print(
            "Connected. Press 'f' to freeze and 'u' to unfreeze the amplitude and q to quit."
        )

        while not reader.at_eof() and self._running:
            cmd = await reader.read(1)
            # '' means EOF, chr(4) means EOT (sent by CTRL+D on UNIX terminals)
            if not cmd or ord(cmd) <= 4 or cmd == b"q":
                self._running = False
                break
            elif cmd == b"f":
                # freeze. set enable to off
                await self._enable_var.setValue(False)
            elif cmd == b"u":
                # unfreeze. set enable to on
                await self._enable_var.setValue(True)

    async def _variable_task(self):
        # subscribe to variable with 2Hz and print spinning bar
        subscription = await self._process.subscribe(0.5, "/osc/cos")
        progress_char = ("-", "/", "|", "\\")
        progress = 0
        async for _ in subscription.newValues():
            # _ parameter is the timestamp which is unused.
            data = subscription.value
            print("\r" + progress_char[progress], "%.2f     " % data, end="")
            progress = (progress + 1) % 4
            if not self._running:
                break


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage:", sys.argv[0], "url")
        exit(1)
    with TerminalGuard():
        osc = OscExample()
        asyncio.run(osc.run(sys.argv[1]))
