# Copyright (C) 2022 Bjarne von Horn (vh at igh dot de).
#
# This file is part of the PdCom library.
#
# The PdCom library is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# The PdCom library is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
# License for more .
#
# You should have received a copy of the GNU Lesser General Public License
# along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.

from asyncio import run
from pdcom5 import Process, Transmission
from datetime import timedelta


async def main():
    p = Process()
    await p.connect("msr://localhost")
    print("Connected!")
    var_list = await p.list("")
    print("Vars:")
    for var in var_list.variables:
        print("  ", var.path)
    print("Dirs:")
    for dir in var_list.directories:
        print("  ", dir)
    var1 = await p.find("/SawTooth")
    if var1 is None:
        print("Variable not found")
        return
    sub = await p.subscribe(Transmission(timedelta(seconds=1)), var1)
    print("Subscribed!")
    async for _ in sub.newValues():
        print("Value:", sub.value)


if __name__ == "__main__":
    run(main())
