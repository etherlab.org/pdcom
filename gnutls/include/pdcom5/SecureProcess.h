/*****************************************************************************
 * vim:tw=78
 *
 * Copyright (C) 2021 Bjarne von Horn (vh at igh dot de).
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

/** @file */

#ifndef PDCOM5_SECUREPROCESS_H
#define PDCOM5_SECUREPROCESS_H

#include "pdcom5-gnutls_export.h"

#include <memory>
#include <pdcom5/Process.h>

namespace PdCom {

/** Process implementation for TLS encrypted traffic.
 *
 * Please note that gnutls has some internal buffering,
 * so please do not do some buffering on your own and also do not rely on
 * flush(). Just write directly to the socket, as in
 * PosixProcess::posixWriteDirect().
 *
 * \example gnutls_example.cpp
 *
 */
class PDCOM5_GNUTLS_EXPORT SecureProcess : public Process
{
  public:
    struct PDCOM5_GNUTLS_EXPORT EncryptionDetails
    {
        enum Flags {
            Default = 0,
        } flags_;
        std::string server_ca_, server_hostname_, client_cert_, client_key_;

        /** Struct which contains certificates and options */
        EncryptionDetails(
                Flags flags,
                std::string server_ca,
                std::string hostname,
                std::string client_cert = "",
                std::string client_key  = "") :
            flags_(flags),
            server_ca_(server_ca),
            server_hostname_(hostname),
            client_cert_(client_cert),
            client_key_(client_key)
        {}
        EncryptionDetails(
                std::string server_ca,
                std::string hostname,
                std::string client_cert = "",
                std::string client_key  = "") :
            flags_(Default),
            server_ca_(server_ca),
            server_hostname_(hostname),
            client_cert_(client_cert),
            client_key_(client_key)
        {}
    };

    /** GnuTls global initialization.
     *
     * Call this at startup of your application to prepare the underlying TLS
     * library.
     *
     * \throws PdCom::Exception Initialization failed.
     */
    static void InitLibrary();
    /** GnuTls global finalization.
     */
    static void FinalizeLibrary();

    explicit SecureProcess(EncryptionDetails const &);

    /**
     * calls Process::asyncData() until gnutls' buffers are empty.
     */
    void asyncData();

    /** TLS Handshake.
     * Start a TLS Session with the server. Call this until it returns true.
     * \return True if successful, false if another call is needed.
     * \throw TlsError Fatal Error occured.
     */
    bool handshake();
    /** Close a TLS session */
    void bye();

  private:
    struct PDCOM5_GNUTLS_NO_EXPORT Impl;

    /**
     * Gnutls does some buffering internally, so no need to flush.
     */
    void flush() override;
};

}  // namespace PdCom

#endif  // PDCOM5_SECUREPROCESS_H
