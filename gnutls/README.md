How to generate certificates
============================


```bash
$ certtool --generate-privkey --outfile ca-key.pem
$ certtool --generate-self-signed --load-privkey ca-key.pem \
   --outfile ca-cert.pem   --template ca.template
$ certtool --generate-privkey --outfile server-key.pem
$ certtool --generate-certificate --load-privkey server-key.pem \
   --outfile server-cert.pem --load-ca-certificate ca-cert.pem \
   --load-ca-privkey ca-key.pem  --template server.template
```

Run `/usr/sbin/stunnel stunnel.conf` to start the TLS wrapper.
