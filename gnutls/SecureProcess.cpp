/*****************************************************************************
 * vim:tw=78
 *
 * Copyright (C) 2021 Bjarne von Horn (vh at igh dot de).
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#include "../src/Process.h"

#include <algorithm>
#include <cstring>
#include <exception>
#include <gnutls/gnutls.h>
#include <pdcom5/Exception.h>
#include <pdcom5/SecureProcess.h>

using PdCom::SecureProcess;

namespace {
template <typename T, typename... Args>
auto wrap(const char *msg, T func, Args &&...args)
        -> decltype(func(std::forward<Args>(args)...))
{
    const auto ans = func(std::forward<Args>(args)...);
    if (ans < 0 && gnutls_error_is_fatal(static_cast<int>(ans)))
        throw PdCom::TlsError(msg, static_cast<int>(ans));
    return ans;
}

struct TlsDeleter
{
    void operator()(gnutls_session_t s)
    {
        if (s)
            gnutls_deinit(s);
    }
    void operator()(gnutls_certificate_credentials_t c)
    {
        if (c)
            gnutls_certificate_free_credentials(c);
    }
};
}  // namespace

struct PDCOM5_LOCAL SecureProcess::Impl final : public PdCom::impl::Process
{
    Impl(SecureProcess::EncryptionDetails const &ed, SecureProcess *This);
    std::string host_;
    std::unique_ptr<gnutls_certificate_credentials_st, TlsDeleter> cred_;

    struct TlsLayer : PdCom::impl::IOLayer
    {
        std::unique_ptr<gnutls_session_int, TlsDeleter> session_;
        std::exception_ptr ex_ptr_;

        TlsLayer(
                IOLayer *,
                gnutls_certificate_credentials_t cc,
                const std::string &host);

        // from IOLayer
        void write(const char *buf, size_t count) override;
        int read(char *buf, int count) override;
        void flush() override {}


        bool handshake();
        /** Close a TLS session */
        void bye();
    } tls_layer_;
};

SecureProcess::SecureProcess(SecureProcess::EncryptionDetails const &ed) :
    PdCom::Process(std::make_shared<Impl>(ed, this))
{}

int SecureProcess::Impl::TlsLayer::read(char *const buf, int const count)
{
    const ssize_t recv_chars = gnutls_record_recv(session_.get(), buf, count);
    if (ex_ptr_)
        std::rethrow_exception(ex_ptr_);
    if (recv_chars >= 0)
        return static_cast<int>(recv_chars);

    if (recv_chars == GNUTLS_E_REHANDSHAKE) {
        handshake();
        return -EAGAIN;
    }
    if (!gnutls_error_is_fatal(static_cast<int>(recv_chars))) {
        return -EAGAIN;
    }
    throw TlsError("gnutls_record_recv() failed", recv_chars);
}

void SecureProcess::Impl::TlsLayer::write(const char *buf, size_t count)
{
    if (count == 0)
        return;
    const auto ans = gnutls_record_send(session_.get(), buf, count);
    if (ex_ptr_)
        std::rethrow_exception(ex_ptr_);
    if (ans < 0)
        throw TlsError(
                "SecureProcess::secureWrite() failed", static_cast<int>(ans));
    if (static_cast<size_t>(ans) != count)
        throw TlsError("short write in SecureProcess::secureWrite", 0);
}


static std::unique_ptr<gnutls_certificate_credentials_st, TlsDeleter>
load_certificates(SecureProcess::EncryptionDetails const &ed)
{
    std::unique_ptr<gnutls_certificate_credentials_st, TlsDeleter> ans;

    gnutls_certificate_credentials_t cc = nullptr;
    wrap("gnutls_certificate_allocate_credentials() failed",
         gnutls_certificate_allocate_credentials, &cc);
    ans.reset(cc);

    const auto to_datum_t = [](const std::string &s) -> gnutls_datum_t {
        return {const_cast<unsigned char *>(
                        reinterpret_cast<const unsigned char *>(s.data())),
                static_cast<unsigned>(s.size())};
    };
    if (!ed.server_ca_.empty()) {
        const gnutls_datum_t f = to_datum_t(ed.server_ca_);
        const auto a =
                wrap("reading CA failed", gnutls_certificate_set_x509_trust_mem,
                     cc, &f, GNUTLS_X509_FMT_PEM);
        if (a < 1)
            throw PdCom::TlsError("Less than one CA loaded", 0);
    }
    if (!ed.client_key_.empty() and !ed.client_cert_.empty()) {
        const auto key  = to_datum_t(ed.client_key_);
        const auto cert = to_datum_t(ed.client_cert_);
        wrap("importing client certificate failed",
             gnutls_certificate_set_x509_key_mem, cc, &cert, &key,
             GNUTLS_X509_FMT_PEM);
    }

    return ans;
}


SecureProcess::Impl::Impl(
        SecureProcess::EncryptionDetails const &ed,
        SecureProcess *This) :
    PdCom::impl::Process(This),
    host_(ed.server_hostname_),
    cred_(load_certificates(ed)),
    tls_layer_(this, cred_.get(), host_)
{
    // register our tls layer
    Process::io = &tls_layer_;
}

SecureProcess::Impl::TlsLayer::TlsLayer(
        IOLayer *io,
        gnutls_certificate_credentials_t cc,
        const std::string &host) :
    IOLayer(io)
{
    {
        gnutls_session_t s = nullptr;
        wrap("gnutls_init() failed", gnutls_init, &s,
             GNUTLS_CLIENT | GNUTLS_NONBLOCK);
        session_.reset(s);
    }

    wrap("gnutls_priority_set_direct failed", gnutls_priority_set_direct,
         session_.get(), "NORMAL", nullptr);


    wrap("using Certificates failed", gnutls_credentials_set, session_.get(),
         GNUTLS_CRD_CERTIFICATE, cc);

    gnutls_transport_set_push_function(
            session_.get(),
            [](gnutls_transport_ptr_t uptr, const void *data,
               size_t count) -> ssize_t {
                auto &sp = *reinterpret_cast<TlsLayer *>(uptr);
                try {
                    sp.IOLayer::write(
                            reinterpret_cast<const char *>(data), count);
                }
                catch (std::exception const &e) {
                    sp.ex_ptr_ = std::current_exception();
                    // make sure that errno != EAGAIN;
                    errno = EIO;
                    return -1;
                }
                return count;
            });
    gnutls_transport_set_pull_function(
            session_.get(),
            [](gnutls_transport_ptr_t uptr, void *data,
               size_t count) -> ssize_t {
                auto &sp = *reinterpret_cast<TlsLayer *>(uptr);
                try {
                    const int ans = sp.IOLayer::read(
                            reinterpret_cast<char *>(data), count);
                    if (ans >= 0)
                        return ans;
                    if (ans == -EAGAIN)
                        errno = EAGAIN;
                    return -1;
                }
                catch (std::exception const &e) {
                    sp.ex_ptr_ = std::current_exception();
                    // make sure that errno != EAGAIN;
                    errno = EIO;
                    return -1;
                }
            });
    if (!host.empty())
        gnutls_session_set_verify_cert(session_.get(), host.c_str(), 0);

    gnutls_transport_set_ptr(session_.get(), this);
}

bool SecureProcess::Impl::TlsLayer::handshake()
{
    const auto a = gnutls_handshake(session_.get());
    if (a == GNUTLS_E_AGAIN or a == GNUTLS_E_INTERRUPTED)
        return false;
    else if (gnutls_error_is_fatal(a))
        throw TlsError("TLS Handshake failed", a);
    return true;
}

void SecureProcess::Impl::TlsLayer::bye()
{
    gnutls_bye(session_.get(), GNUTLS_SHUT_RDWR);
}


void SecureProcess::bye()
{
    return static_cast<Impl &>(*pimpl).tls_layer_.bye();
}

bool SecureProcess::handshake()
{
    return static_cast<Impl &>(*pimpl).tls_layer_.handshake();
}

void SecureProcess::InitLibrary()
{
    if (gnutls_global_init() != GNUTLS_E_SUCCESS)
        throw PdCom::Exception("gnults_global_init() failed");
}

void SecureProcess::FinalizeLibrary()
{
    gnutls_global_deinit();
}

void SecureProcess::flush()
{}

void SecureProcess::asyncData()
{
    do {
        Process::asyncData();
    } while (gnutls_record_check_pending(
            static_cast<Impl &>(*pimpl).tls_layer_.session_.get()));
}
