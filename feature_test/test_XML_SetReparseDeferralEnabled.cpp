/** Test for XML_SetReparseDeferralEnabled()
 *
 * This feature was implemented in expat 2.6.0 but is backported to
 * distributions using earlier versions of this library
 */

#include <expat.h>

int main()
{
    (void) XML_SetReparseDeferralEnabled;
}
