/*****************************************************************************
 *
 * Copyright (C) 2015-2016  Richard Hacker (lerichi at gmx dot net)
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/
#include "Variable.h"

#include "Debug.h"
#include "Future.h"
#include "Process.h"
#include "Selector.h"
#include "TemplateVodoo.inc"

#include <algorithm>
#include <array>
#include <cassert>
#include <numeric>
#include <pdcom5/Subscriber.h>
#include <pdcom5/Subscription.h>
#include <pdcom5/Variable.h>
#include <sstream>
#include <stdint.h>

namespace {

using copyFn = void (*)(void *, const void *, size_t, size_t);

template <
        PdCom::TypeInfo::DataType dest_type,
        PdCom::TypeInfo::DataType src_type>
struct DataConverter
{
    using Src  = typename PdCom::details::DataTypeTraits<src_type>::value_type;
    using Dest = typename PdCom::details::DataTypeTraits<dest_type>::value_type;

    static void
    copyData(void *_dest, const void *_src, size_t nelem, size_t offset)
    {
        auto dest = reinterpret_cast<Dest *>(_dest);
        auto src  = reinterpret_cast<const Src *>(_src) + offset;
        for (size_t i = 0; i < nelem; ++i)
            dest[i] = static_cast<Dest>(src[i]);
    }
};

template <
        PdCom::TypeInfo::DataType dest_type,
        PdCom::TypeInfo::DataType... src_types>
constexpr std::array<copyFn, sizeof...(src_types)>
getcopyFnRow(sequence<src_types...>)
{
    return {{DataConverter<dest_type, src_types>::copyData...}};
}

template <PdCom::TypeInfo::DataType... dest_types>
constexpr std::
        array<std::array<copyFn, sizeof...(dest_types)>, sizeof...(dest_types)>
        getcopyFnMatrix(sequence<dest_types...> seq)
{
    return {{getcopyFnRow<dest_types>(seq)...}};
}


/** Some comments on the template vodoo:
 * DataTypeSequence will expand to a struct sequence<boolean_T, char_T, ...,
 * double_T, single_T>. The template arguments of sequence (boolean_T, ...,
 * single_T) can then be used to initialize (list initialization) an array by
 * applying one function on each of these template arguments.
 * This is used to generate the matrix row by row
 */


}  // namespace

void PdCom::details::copyData(
        void *dst,
        TypeInfo::DataType dst_type,
        const void *src,
        TypeInfo::DataType src_type,
        size_t nelem,
        size_t offset)
{
    using PdCom::TypeInfo;
    if (dst_type < TypeInfo::DataTypeBegin or dst_type >= TypeInfo::DataTypeEnd
        or src_type < TypeInfo::DataTypeBegin
        or src_type >= TypeInfo::DataTypeEnd)
        return;

    static constexpr auto copyMatrix = getcopyFnMatrix(DataTypeSequence {});
    static_assert(
            copyMatrix.size() == DataTypeCount, "Copy Matrix size mismatch");
    // first index: dst, second index: sr

    copyMatrix[dst_type][src_type](dst, src, nelem, offset);
}

constexpr PdCom::TypeInfo PdCom::details::TypeInfoTraits<uint8_t>::type_info;
constexpr PdCom::TypeInfo PdCom::details::TypeInfoTraits<uint16_t>::type_info;
constexpr PdCom::TypeInfo PdCom::details::TypeInfoTraits<uint32_t>::type_info;
constexpr PdCom::TypeInfo PdCom::details::TypeInfoTraits<uint64_t>::type_info;
constexpr PdCom::TypeInfo PdCom::details::TypeInfoTraits<int8_t>::type_info;
constexpr PdCom::TypeInfo PdCom::details::TypeInfoTraits<int16_t>::type_info;
constexpr PdCom::TypeInfo PdCom::details::TypeInfoTraits<int32_t>::type_info;
constexpr PdCom::TypeInfo PdCom::details::TypeInfoTraits<int64_t>::type_info;
constexpr PdCom::TypeInfo PdCom::details::TypeInfoTraits<bool>::type_info;
constexpr PdCom::TypeInfo PdCom::details::TypeInfoTraits<float>::type_info;
constexpr PdCom::TypeInfo PdCom::details::TypeInfoTraits<double>::type_info;
constexpr PdCom::TypeInfo PdCom::details::TypeInfoTraits<char>::type_info;


using PdCom::Variable;

PdCom::SizeInfo Variable::getSizeInfo() const
{
    if (auto p = pimpl_.lock())
        return p->size_info;
    throw EmptyVariable();
}

PdCom::TypeInfo Variable::getTypeInfo() const
{
    if (auto p = pimpl_.lock())
        return *p->type_info;
    throw EmptyVariable();
}

std::string Variable::getName() const
{
    if (auto p = pimpl_.lock())
        return p->getName();
    throw EmptyVariable();
}

std::string Variable::getPath() const
{
    if (auto p = pimpl_.lock())
        return p->getPath();
    throw EmptyVariable();
}

Variable::SetValueFuture Variable::setValue(
        const void *data,
        PdCom::TypeInfo::DataType t,
        size_t count,
        const PdCom::Selector &selector) const
{
    if (auto p = pimpl_.lock()) {
        if (selector.impl_)
            return selector.impl_->applySetValue(*p, data, t, count);
        return PdCom::impl::Selector().applySetValue(*p, data, t, count);
    }
    throw EmptyVariable();
}

Variable::SetValueFuture Variable::setValue(
        const void *src,
        PdCom::TypeInfo::DataType src_type,
        size_t count,
        size_t offset) const
{
    if (auto p = pimpl_.lock()) {
        return p->setValue(src, src_type, offset, count);
    }
    throw EmptyVariable();
}

std::string Variable::getAlias() const
{
    if (auto p = pimpl_.lock())
        return p->getAlias();
    throw EmptyVariable();
}

int Variable::getTaskId() const
{
    if (auto p = pimpl_.lock())
        return p->getTaskId();
    throw EmptyVariable();
}

bool Variable::isWriteable() const
{
    if (auto p = pimpl_.lock())
        return p->isWriteable();
    throw EmptyVariable();
}

std::chrono::duration<double> Variable::getSampleTime() const
{
    if (auto p = pimpl_.lock())
        return p->getSampleTime();
    throw EmptyVariable();
}

PdCom::Process *Variable::getProcess() const
{
    if (auto p = pimpl_.lock()) {
        if (const auto process = p->process.lock()) {
            return process->This;
        }
        throw ProcessGoneAway();
    }
    throw EmptyVariable();
}

template class PdCom::Future<
        PdCom::Exception const &,
        PdCom::VariablePollResult,
        std::chrono::nanoseconds>;

template class PdCom::Future<PdCom::Exception const &>;

using PdCom::details::is_contiguous;
static_assert(
        is_contiguous<std::vector<int>>::value,
        "vector<int> is contiguous");
static_assert(
        !is_contiguous<std::vector<bool>>::value,
        "vector<bool> is not contiguous");
static_assert(
        is_contiguous<std::array<int, 5>>::value,
        "array<int> is contiguous");
static_assert(is_contiguous<std::string>::value, "string is contiguous");
