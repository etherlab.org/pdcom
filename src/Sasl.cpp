/*****************************************************************************
 * vim:tw=78
 *
 * Copyright (C) 2021 Bjarne von Horn (vh at igh dot de).
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#include "Process.h"
#include "ProtocolHandler.h"

#include <pdcom5/Exception.h>
#include <pdcom5/Sasl.h>


using PdCom::Sasl;

Sasl::Sasl() = default;
Sasl::Sasl(Sasl &&o) noexcept : process_(std::move(o.process_))
{
    if (const auto p = process_.lock()) {
        if (p->auth_manager_ == &o) {
            p->auth_manager_ = this;
        }
    }
}
Sasl &Sasl::operator=(Sasl &&o) noexcept
{
    if (&o == this)
        return *this;
    if (const auto my_process = process_.lock())
        my_process->auth_manager_ = nullptr;
    process_ = std::move(o.process_);
    if (const auto my_process = process_.lock())
        my_process->auth_manager_ = this;
    return *this;
}
Sasl::~Sasl()
{
    if (const auto p = process_.lock())
        p->setAuthManager(nullptr);
}

bool Sasl::loginStep(const char *mech, const char *clientData)
{
    if (const auto p = process_.lock())
        return p->protocolHandler().login(mech, clientData);
    else
        throw ProcessGoneAway();
}

void Sasl::logout()
{
    if (const auto p = process_.lock())
        p->protocolHandler().logout();
    else
        throw ProcessGoneAway();
}
