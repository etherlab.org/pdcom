/*****************************************************************************
 * vim:tw=78
 *
 * Copyright (C) 2021 Richard Hacker (lerichi at gmx dot net),
 *                    Florian Pose (fp at igh dot de),
 *                    Bjarne von Horn (vh at igh dot de).
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/


#ifndef PDCOM5_PROCESS_IMPL_H
#define PDCOM5_PROCESS_IMPL_H

#include "IOLayer.h"
#include "ProtocolHandler.h"
#include "Subscription.h"

#include <list>
#include <memory>
#include <pdcom5/Exception.h>
#include <pdcom5/MessageManagerBase.h>
#include <pdcom5/Process.h>
#include <pdcom5/Sasl.h>
#include <pdcom5/Subscription.h>
#include <set>

namespace PdCom { namespace impl {
class AuthManager;
class Variable;
class Subscription;
struct Selector;

/** Base class for PdCom protocol handler
 *
 * This is the base class to interact with real time process server. The
 * PdCom protocol ist implemented using this class.
 *
 * For socket input and output, the library completely relies on a
 * derived class where read(), write(), flush() and connected() methods
 * are reimplemented.
 *
 * When data is available for reading, call asyncData() which in turn
 * calls the reimplemented read() method.
 *
 * When the protocol is initialized, the reimplemented connected()
 * method is called. Other than startTLS(), login(), none of the command
 * methods listed below may be called prior to connected().
 *
 * After connected(), the following commands can be issued: * list():
 * list a directory; returns result in listReply() * find(): find a
 * variable; returns result in findReply() * login(): start a SASL login
 * interaction; returns result in loginReply() * ping(): ping the
 * server; returns result in pingReply() * getMessage(): request a
 * specific message result in multiple calls to processMessage() if
 * required * Variable interactions * Variable::Subscription
 * interactions
 *
 * startTLS() and login() may only be called prior to connected() when
 * the library has called startTLSReply() or loginReply() previously.
 *
 * All these commands are non-blocking asynchronous calls and either
 * return the result immediately with the corresponding reply methods or
 * issue a command to the server using excessive (!) calls to write().
 * Data should be written to a buffer to optimize network communication.
 * To flush the buffer to wire, flush() is issued by the library when
 * required.
 *
 * The server may query presence of the user by issuing an alive() call.
 * Using this call, certain actions could be undertaken by the server if
 * the user is not active any more.
 *
 * Certain actions in the real time process, such as alarms, triggers a
 * call to processMessage(). Messages are kept in a ring buffer in the
 * process.
 *
 * Calling activeMessages() will return a list of all active messages as
 * well as the latest message in activeMessagesReply().
 *
 * Any message can be recalled with getMessage(), the result returned in
 * getMessageReply(). If the message does not exist, the Message
 * returned will have the requested \a seqNo with \a path empty.
 *
 * The subscribe() method subscribes a variable using the path only.
 */


// we need to export this class to allow SecureProcess::Impl to derive from
// us
class PDCOM5_PUBLIC Process :
    public std::enable_shared_from_this<Process>,
    public IOLayer
{
    std::unique_ptr<impl::ProtocolHandler> protocolHandler_;

    struct RegisteredSubscription
    {
        std::weak_ptr<impl::Subscription> subscription_;
        std::string path_;
        std::shared_ptr<const PdCom::impl::Selector> selector_;
    };

    struct RSComparator
    {
        bool operator()(
                RegisteredSubscription const &lhs,
                RegisteredSubscription const &rhs) const noexcept
        {
            return lhs.subscription_.owner_before(rhs.subscription_);
        }
    };

    std::set<RegisteredSubscription, RSComparator> all_subscriptions_;

  public:
    Process(PdCom::Process *This);
    static Process &fromUApi(PdCom::Process &p) { return *p.pimpl; }

    /** Destructor
     *
     * The destructor cleans up all internally allocated structures
     */
    virtual ~Process() = default;


    /** Reset communications and clean up internal buffers */
    void reset();

    /** Name of application user application.
     *
     * The application name is transferred to the server to be able to
     * identify the clients more easily.
     *
     * \return a descriptive name of your application.
     */
    std::string applicationName() const { return This->applicationName(); }

    /** Host name of remote server.
     *
     * Reimplement this method to return the remote server host name
     * this library connects to. This is especially important in
     * multi-hosted TLS environments, where multiple hosts resolv to
     * the same IP address. TLS needs to know the original server host
     * name.
     *
     * \return server host name
     */
    std::string hostname() const { return This->hostname(); }

    /** Read data from server
     *
     * Reimplement this method to transfer data from the server to
     * the library. This method is called within the call to
     * asyncData().
     *
     * Essentially this method is a little wrapper around your
     * socket's `%read()` function:
     * \code
     *   int MyProcess::read(char *buf, size_t count)
     *   {
     *       return ::read(this->socket_fd, buf, count);
     *   }
     * \endcode
     *
     * The method must return the number of bytes read, which may of
     * coarse be less than \p count or even 0. Return values of
     * &lt;=&nbsp;0 are not interpreted by the protocol handler.
     *
     * @param buf data destination
     * @param count buffer size
     *
     * \return
     *    return value of `%read()` function, which in turn will be
     *    returned by asyncData()
     */
    int read(char *buf, int count) override { return This->read(buf, count); }


    void asyncData();

    /** Write data to server
     *
     * Reimplement this method to transfer data from the library to
     * the server. This method is called when any library
     * operation requires data to be sent to the server.
     *
     * Note: the library makes many calls to write(), so use
     * buffered output otherwise you're in for performance problems!
     *
     * Essentially this method is a little wrapper around your
     * socket's `%write()` function:
     * \code
     * void MyProcess::write(const char *buf, size_t count)
     * {
     *     if (count != ::fwrite(buf, 1, count, this->socket_file)) {
     *         // react to errors, set flags, etc
     *     }
     * }
     * \endcode
     *
     * Note: the library does not have an internal buffer and expects
     * that all data is sent. If your implementation might send less
     * than \p count, it is your responsibility to buffer the data and
     * send it later.
     *
     * @param buf data to be sent
     * @param count number of bytes to send
     */
    void write(const char *buf, size_t count) override
    {
        This->write(buf, count);
    }

    /** Flush unsent data in output buffer
     *
     * Reimplement this method to flush data in the output buffer.
     *
     * This method tells the user that it is time to flush the
     * output buffer to the wire. The library only expects that data
     * is sent to the server within this call.
     *
     * Essentially this method is a little wrapper around your
     * socket's `fflush()` function:
     * \code
     * void MyProcess::flush()
     * {
     *     if (::fflush(this->socket_file)) {
     *         // react to errors
     *     }
     * }
     * \endcode
     */
    void flush() override { This->flush(); }

    /** Protocol initialization completed
     *
     * This is a signal emitted by the library to indicate that
     * protocol initialization has been completed and that library
     * operations can be performed thereafter.
     *
     * Reimplement this method to get the signal.
     *
     * Absolutely NO process operations other than asyncData(),
     * startTLS() and login() (and then only due to a previous
     * loginReply() are permitted before this signal has been
     * emitted.
     */
    void connected();

    /** Reply to list() call
     *
     * You must reimplement this method to receive replies to list()
     * calls.
     *
     * Note that a variable can have the same path as a directory!
     * An example is a vector variable with atomized elements.
     *
     * Replies are in strict order of list() calls.
     *
     * @param variables list of variables
     * @param directories string list of directories
     */
    void listReply(
            std::vector<PdCom::Variable> variables,
            std::vector<std::string> directories)
    {
        This->listReply(std::move(variables), std::move(directories));
    }

    /** Reply to find()
     *
     * This virtual method is called within the context of asyncData()
     * when the server's reply to a variable discovery is processed.
     *
     * findReply()ies are called in strict order of find()
     *
     * @param variable pointer to Variable; NULL if not found
     */
    void findReply(const PdCom::Variable &variable)
    {
        This->findReply(variable);
    }

    /** Ping reply
     *
     * You must reimplement this method to receive the reply to a
     * ping() call.
     */
    void pingReply() { This->pingReply(); }

    /** Test from process whether client is alive.
     *
     * In some cases the server may want to know whether the client
     * is still alive. Default implementation is to return true.
     * Reimplement this if you wish to control presence
     *
     * \return true to indicate user presence
     */
    bool alive() { return This->alive(); }

    /** Client to server communication semaphore
     *
     * Reimplement this method to intercept the transmission
     * semaphore. The default implementation always returns success.
     *
     * Every interaction from client to server (change a parameter,
     * ping, etc) starts a message tag.  If the client is
     * multi-threaded, more than one message tag could be started
     * which would lead to corruption. This semaphore can be used to
     * serialize all client to server interactions.
     *
     * @param state true = lock; false = release
     */
    void transmitSemaphore(bool /* state */) {}
    void setAuthManager(Sasl *am)
    {
        if (auth_manager_) {
            auth_manager_->process_ = {};
        }
        auth_manager_ = am;
        if (auth_manager_)
            auth_manager_->process_ = shared_from_this();
    }

    void setMessageManager(MessageManagerBase *mm)
    {
        if (message_manager_) {
            message_manager_->process_ = {};
        }
        message_manager_ = mm;
        if (message_manager_)
            message_manager_->process_ = shared_from_this();
    }

    void loginReply(const char *mechlist, const char *serverData, int finished)
    {
        if (auth_manager_)
            auth_manager_->loginReply(mechlist, serverData, finished);
    }

    void processMessage(Message message)
    {
        if (message_manager_)
            message_manager_->processMessage(std::move(message));
    }

    void getMessageReply(Message msg)
    {
        if (message_manager_)
            message_manager_->getMessageReply(std::move(msg));
    }

    void activeMessagesReply(std::vector<Message> msgs)
    {
        if (message_manager_)
            message_manager_->activeMessagesReply(std::move(msgs));
    }

    void broadcastReply(
            const std::string &message,
            const std::string &attr,
            std::chrono::nanoseconds time_ns,
            const std::string &user)
    {
        This->broadcastReply(message, attr, time_ns, user);
    }

    void clientStatisticsReply(std::vector<PdCom::ClientStatistics> statistics)
    {
        This->clientStatisticsReply(std::move(statistics));
    }

    template <typename P = ProtocolHandler>
    P &protocolHandler() const
    {
        if (!protocolHandler_)
            throw NotConnected();
        return static_cast<P &>(*protocolHandler_);
    }

    template <typename P = ProtocolHandler>
    P *castProtocolHandler() const noexcept
    {
        return static_cast<P *>(protocolHandler_.get());
    }

    std::shared_ptr<Subscription> subscribe(
            PdCom::Subscription *subscription,
            const std::string &path,
            PdCom::Subscriber &subscriber,
            const PdCom::Selector &selector);
    std::shared_ptr<Subscription> subscribe(
            PdCom::Subscription *subscription,
            std::shared_ptr<const Variable> var,
            PdCom::Subscriber &subscriber,
            const PdCom::Selector &selector);

    // replace one subscriber pointer entry in all_subscriptions
    // this is needed when impl::Subscription is replaced
    void
    replace(std::shared_ptr<Subscription> const &old_s,
            std::shared_ptr<Subscription> const &new_s);

    PdCom::Process *const This;
    IOLayer *io;
    Sasl *auth_manager_                  = nullptr;
    MessageManagerBase *message_manager_ = nullptr;

    struct PendingCallbackQueue :
        std::list<PdCom::impl::Subscription::PendingStateChange>
    {
        void flush();
    } pending_callbacks_;


    class AsyncDataMutex
    {
        bool locked_ = false;

      public:
        void unlock() { locked_ = false; }
        void lock()
        {
            if (locked_)
                throw PdCom::InvalidArgument("This function must not be called "
                                             "from PdCom5 Callback");
            locked_ = true;
        }

        bool isLocked() const noexcept { return locked_; }
    } async_data_mutex_;
};
}}  // namespace PdCom::impl

#endif  // PDCOM5_PROCESS_IMPL_H
