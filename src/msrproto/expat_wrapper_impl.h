/*****************************************************************************
 *
 * Copyright (C) 2021 Bjarne von Horn (vh at igh dot de)
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#ifndef MSRPROTO_EXPAT_WRAPPER_IMPL_H
#define MSRPROTO_EXPAT_WRAPPER_IMPL_H

#include "expat_wrapper.h"

#include <expat.h>
#include <expat_external.h>
#include <pdcom5/Exception.h>
#include <stdexcept>

namespace PdCom { namespace impl { namespace MsrProto {
inline void XmlParserDeleter::operator()(XML_ParserStruct *s) noexcept
{
    if (s)
        XML_ParserFree(s);
}


template <typename Handler, bool useExpatBuffer>
struct ExpatWrapper<Handler, useExpatBuffer>::CallbackWrapper
{
    // TODO(ighvh): use template<auto> when C++17 is mandatory (so in about 10
    // years)
    template <typename T, T, int = 0>
    struct Impl;

    // We use a partial specialisation of the struct above
    // to extract the argument types (Args) from the Member function.
    template <
            typename... Args,
            void (Handler::*callback)(Args...),
            int level_change>
    struct Impl<void (Handler::*)(Args...), callback, level_change>
    {
        static void XMLCALL call(void *userdata, Args... args)
        {
            auto &This =
                    *reinterpret_cast<ExpatWrapper<Handler, useExpatBuffer> *>(
                            userdata);
            // increment (start handler only)
            This.level_ += level_change == 1;
            if (This.pending_exception_)
                return;
            try {
                (static_cast<Handler &>(This).*callback)(args...);
            }
            catch (...) {
                This.pending_exception_ = std::current_exception();
                XML_StopParser(This.parser_.get(), false);
            }
            // decrement (end handler only)
            This.level_ -= level_change == -1;
        }
    };
};

template <typename Handler, bool useExpatBuffer>
inline ExpatWrapper<Handler, useExpatBuffer>::ExpatWrapper(const char *encoding)
{
    reset(encoding);
}

template <typename Handler, bool useExpatBuffer>
inline void ExpatWrapper<Handler, useExpatBuffer>::reset(const char *encoding)
{
    pending_exception_ = nullptr;
    level_             = 0;
    parser_.reset(XML_ParserCreate(encoding));
    if (!parser_)
        throw PdCom::Exception("Could not create XML parser");

#if HAVE_XML_SetReparseDeferralEnabled
    if (XML_SetReparseDeferralEnabled(parser_.get(), XML_FALSE) != XML_TRUE)
        throw PdCom::Exception("Could not set ReparseDeferralEnabled attribute "
                               "of XML parser.");
#endif

    using W1 = typename CallbackWrapper::template Impl<
            decltype(&Handler::startElement), &Handler::startElement, 1>;
    using W2 = typename CallbackWrapper::template Impl<
            decltype(&Handler::endElement), &Handler::endElement, -1>;

    XML_SetUserData(parser_.get(), this);
    XML_SetStartElementHandler(parser_.get(), &W1::call);
    XML_SetEndElementHandler(parser_.get(), &W2::call);
}

template <typename Handler, bool useExpatBuffer>
template <typename T>
inline typename std::
        enable_if<!useExpatBuffer && std::is_same<T, char>::value, bool>::type
        ExpatWrapper<Handler, useExpatBuffer>::parse(
                const T *s,
                std::size_t n,
                bool final)
{
    if (XML_STATUS_OK == XML_Parse(parser_.get(), s, n, final))
        return true;

    const XML_Error err_code = XML_GetErrorCode(parser_.get());
    if (err_code == XML_ERROR_ABORTED && pending_exception_) {
        auto pe            = pending_exception_;
        pending_exception_ = nullptr;
        std::rethrow_exception(pe);
    }

    static_cast<Handler *>(this)->xmlError(XML_ErrorString(err_code));
    return false;
}


template <typename Handler, bool useExpatBuffer>
template <std::size_t bufsize>
ExpatWrapper<Handler, useExpatBuffer>::ExpatBuffer<bufsize>::ExpatBuffer(
        ExpatWrapper<Handler, useExpatBuffer> &wrapper)
{
    static_assert(bufsize > 0, "Buffer size of 0 makes no sense.");
    static_assert(useExpatBuffer, "Don't make a buffer when not using it");
    buffer_ = XML_GetBuffer(wrapper.parser_.get(), bufsize);
    if (!buffer_)
        throw std::bad_alloc();
}

template <typename Handler, bool useExpatBuffer>
template <std::size_t bufsize>
inline typename std::enable_if<(useExpatBuffer && bufsize > 0), bool>::type
ExpatWrapper<Handler, useExpatBuffer>::parse(
        ExpatWrapper<Handler, useExpatBuffer>::ExpatBuffer<
                bufsize> /* buffer */,
        std::size_t n,
        bool final)
{
    if (n > bufsize)
        throw PdCom::InvalidArgument(
                "buffer overflow, wrote more bytes than buffer size.");

    if (XML_STATUS_OK == XML_ParseBuffer(parser_.get(), n, final))
        return true;

    const XML_Error err_code = XML_GetErrorCode(parser_.get());
    if (err_code == XML_ERROR_ABORTED && pending_exception_) {
        auto pe            = pending_exception_;
        pending_exception_ = nullptr;
        std::rethrow_exception(pe);
    }

    static_cast<Handler *>(this)->xmlError(XML_ErrorString(err_code));
    return false;
}

}}}  // namespace PdCom::impl::MsrProto

#endif  // MSRPROTO_EXPAT_WRAPPER_H
