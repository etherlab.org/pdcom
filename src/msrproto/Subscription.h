/*****************************************************************************
 *
 * Copyright (C) 2021 Richard Hacker (lerichi at gmx dot net),
 *                    Florian Pose (fp at igh dot de),
 *                    Bjarne von Horn (vh at igh dot de).
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#ifndef PDCOM5_MSRPROTO_SUBSCRIPTION_H
#define PDCOM5_MSRPROTO_SUBSCRIPTION_H

#include "../Selector.h"
#include "../Subscription.h"

#include <vector>

namespace PdCom { namespace impl { namespace MsrProto {

class Variable;

class InvalidSubscription : public PdCom::impl::Subscription
{
  public:
    InvalidSubscription(
            PdCom::Subscription *This,
            PdCom::Subscriber &subscriber,
            std::weak_ptr<impl::Process> process,
            std::shared_ptr<const PdCom::impl::Variable> var = nullptr) :
        PdCom::impl::Subscription(var, This, subscriber, process)
    {}

    void poll() override;
    const void *getData() const override;

  protected:
    template <typename T>
    static void unsubscribe(T &This);
};

class PendingSubscription : public InvalidSubscription
{
    std::shared_ptr<const PdCom::impl::Selector> selector_;

  public:
    PendingSubscription(
            PdCom::Subscription *This,
            PdCom::Subscriber &subscriber,
            std::weak_ptr<impl::Process> process,
            std::shared_ptr<const PdCom::impl::Selector> selector) :
        InvalidSubscription(This, subscriber, process),
        selector_(std::move(selector))
    {}

    std::shared_ptr<const PdCom::impl::Selector> getSelector() const
    {
        return selector_;
    }
};

class PollSubscription :
    public InvalidSubscription,
    public std::enable_shared_from_this<PollSubscription>
{
  public:
    PollSubscription(
            std::shared_ptr<const Variable> variable,
            PdCom::Subscription *This,
            PdCom::Subscriber &subscriber,
            std::weak_ptr<impl::Process> process,
            const PdCom::Selector &selector);
    void poll() override;
    const void *getData() const override { return data_.data(); }

    void readData(const char *data) { copy_function_(data_.data(), data); }
    bool pollData(uint64_t mtime, const char *data);
    const bool is_parameter_;

  private:
    PdCom::impl::Selector::CopyFunctionType copy_function_;
    std::vector<char> data_;
};

class EventSubscription : public PollSubscription
{
  public:
    using PollSubscription::PollSubscription;

    ~EventSubscription();
};

class PeriodicSubscription : public PollSubscription
{
  public:
    using PollSubscription::PollSubscription;
    ~PeriodicSubscription();
};
}}}     // namespace PdCom::impl::MsrProto
#endif  // PDCOM5_MSRPROTO_SUBSCRIPTION_H
