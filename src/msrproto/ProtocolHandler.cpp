/*****************************************************************************
 *
 * Copyright (C) 2015-2016  Richard Hacker (lerichi at gmx dot net)
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#include "ProtocolHandler.h"

#include "../Debug.h"
#include "../SpyLayer.h"
#include "Channel.h"
#include "DirNode.h"
#include "Parameter.h"
#include "Subscription.h"
#include "expat_wrapper_impl.h"

#include <algorithm>
#include <cstring>
#include <errno.h>
#include <mutex>
#include <ostream>
#include <pdcom5/Exception.h>
#include <pdcom5/Subscriber.h>
#include <sstream>
#include <stdexcept>
#include <typeinfo>

#if defined _WIN32 || defined __ANDROID__
namespace {
struct PthreadCancelDisabler
{};
}  // namespace

#else

#include <pthread.h>
namespace {
class PthreadCancelDisabler
{
    int cancel_state_ = 0;

  public:
    PthreadCancelDisabler()
    {
        pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, &cancel_state_);
    }
    ~PthreadCancelDisabler() { pthread_setcancelstate(cancel_state_, nullptr); }
};
}  // namespace

#endif

#ifdef PDC_DEBUG
#include <stdlib.h>  // getenv(), atoi()
#endif

using PdCom::impl::MsrProto::Channel;
using PdCom::impl::MsrProto::ChannelSubscriptionsWithGroup;
using PdCom::impl::MsrProto::ChannelSubscriptionsWithoutGroup;
using PdCom::impl::MsrProto::EventSubscription;
using PdCom::impl::MsrProto::ExpatWrapper;
using PdCom::impl::MsrProto::Parameter;
using PdCom::impl::MsrProto::PeriodicSubscription;
using PdCom::impl::MsrProto::PollSubscription;
using PdCom::impl::MsrProto::ProtocolHandler;
using PdCom::impl::MsrProto::Variable;
using PdCom::impl::MsrProto::XmlStream;


namespace {
struct Attribute
{ /*{{{*/
    Attribute(const char **atts) : atts(atts) {}

    const char *find(const char *const arg, const char *const else_return = "")
    {
        for (size_t i = 0; arg and atts[i]; i += 2) {
            if (!strcmp(atts[i], arg))
                return atts[i + 1];
        }

        return else_return;
    }

    bool getString(const char *arg, const char *&val)
    {
        const char *attr = find(arg, nullptr);

        if (!attr)
            return false;

        val = attr;
        return true;
    };

    bool getUInt(const char *arg, unsigned int &val)
    {
        const char *attr = find(arg);

        // Only test for numbers, even leading '+' is ignored
        if (*attr < '0' or *attr > '9')
            return false;

        val = atol(attr);
        return true;
    }

    bool getInt(const char *arg, int &val)
    {
        const char *attr = find(arg);

        // Only test for numbers, even leading '+' is ignored
        if ((*attr < '0' or *attr > '9') and *attr != '-')
            return false;

        val = atoi(attr);
        return true;
    }

    bool isTrue(const char *arg)
    {
        unsigned int val;
        return getUInt(arg, val) and val;
    }

    bool getDouble(const char *arg, double &val)
    {
        const char *attr = find(arg);

        if (*attr)
            val = atof(attr);

        return *attr;
    }

    std::chrono::nanoseconds getTime_ns(const char *arg)
    {
        const char *attr = find(arg);

        int64_t time_ns = 0;
        size_t i        = 0;
        do {
            if (!*attr or (!i and *attr == '.')) {
                if (!i)
                    i = 10;
            }
            else if (*attr < '0' or *attr > '9')
                return std::chrono::nanoseconds {0};

            if (*attr != '.') {
                if (*attr or i != 10)
                    time_ns *= 10;

                if (*attr)
                    time_ns += *attr - '0';
            }

            if (*attr)
                ++attr;

        } while (*attr == '.' or !i or --i);

        return std::chrono::nanoseconds {time_ns};
    }

    bool getMessage(const char *tag_name, PdCom::Message &msg, bool hasSeqNo)
    {
        const bool is_reset = !strcmp(tag_name, "reset");
        const char *c       = nullptr;
        unsigned int u      = 0;
        if (!getString("name", c) && !getString("path", c)) {
            log_debug("Missing name in message");
            return false;
        }
        msg.path = c;
        if (hasSeqNo && !getUInt("seq", u)) {
            log_debug("Invalid or missing sequence number in message");
            return false;
        }
        msg.seqNo = u;
        // index is optional, set it to -1 if absent.
        msg.index = -1;
        getInt("index", msg.index);
        msg.time = getTime_ns("time");
        if (is_reset) {
            msg.level = PdCom::LogLevel::Reset;
            return true;
        }
        // text is also optional
        if (getString("text", c)) {
            msg.text = c;
        }
        else {
            msg.text = std::string();
        }
        if (getUInt("state", u)) {
            //
            if (!u) {
                msg.level = PdCom::LogLevel::Reset;
            }
            else if (!strcmp(tag_name, "crit_error"))
                msg.level = PdCom::LogLevel::Critical;
            else if (!strcmp(tag_name, "error"))
                msg.level = PdCom::LogLevel::Error;
            else if (!strcmp(tag_name, "warn"))
                msg.level = PdCom::LogLevel::Warn;
            else if (!strcmp(tag_name, "info"))
                msg.level = PdCom::LogLevel::Info;
            else {
                log_debug("Invalid event attribute name %s", tag_name);
                return false;
            }
        }
        else if (!getUInt("prio", u) || u > 7) {
            log_debug("Invalid priority in message");
            return false;
        }
        else
            msg.level = static_cast<PdCom::LogLevel>(u);
        return true;
    }

    bool getClientStatistics(PdCom::ClientStatistics &stats)
    {
        const char *c  = nullptr;
        unsigned count = 0;
        double d       = 0.0;
        stats          = {};

        if (getString("name", c)) {
            stats.name_ = c;
        }

        // yes, this is a typo. It is what it is.
        if (getString("apname", c)) {
            stats.application_name_ = c;
        }

        if (getUInt("countin", count))
            stats.received_bytes_ = count;

        if (getUInt("countout", count))
            stats.sent_bytes_ = count;

        if (getDouble("connectedtime", d))
            stats.connected_time_ =
                    std::chrono::duration_cast<std::chrono::nanoseconds>(
                            std::chrono::duration<double>(d));

        return true;
    }

    const char **atts;
}; /*}}}*/
}  // namespace


XmlStream::XmlStream(PdCom::impl::Process *process, std::streambuf *sb) :
    os(sb), process(process)
{
    os.exceptions(std::ios_base::badbit);
}
void XmlStream::lock()
{
    process->transmitSemaphore(true);
}
void XmlStream::unlock()
{
    process->transmitSemaphore(false);
}


namespace {
struct XmlCommand
{ /*{{{*/
    struct Attribute
    {
        // Create an attribute for a command.
        Attribute(XmlCommand &command, const char *name) : cout(command.cout)
        {
            // send the preamble
            cout << ' ' << name << "=\"";
        }
        ~Attribute() noexcept(false) { cout << '"'; }

        XmlStream &cout;
    };

    XmlCommand(XmlStream &cout, const char *command) :
        cout(cout), guard(cout), p_flush(true)
    {
        cout << '<' << command;
    }

    ~XmlCommand() noexcept(false)
    {
        cout << ">\r\n";
        if (p_flush)
            cout.flush();
    }

    XmlCommand &addEscapedAttribute(const char *name, const std::string &str)
    {
        cout << ' ' << name << '=' << '"';

        for (const char c : str) {
            switch (c) {
                case '"':
                    cout << "&quot;";
                    break;
                case '<':
                    cout << "&lt;";
                    break;
                case '&':
                    cout << "&amp;";
                    break;
                case '\0':
                    throw PdCom::InvalidArgument("Value must not contain NULL");
                default:
                    cout << c;
            }
        }

        cout << '"';

        return *this;
    }

    template <typename T>
    XmlCommand &addAttribute(const char *name, T const &val, bool active = true)
    {
        if (active)
            cout << ' ' << name << "=\"" << val << '"';
        return *this;
    }

    template <typename T>
    XmlCommand &addAttributeStream(const char *name, T &&callback)
    {
        cout << ' ' << name << "=\"";
        callback(cout);
        cout << '"';
        return *this;
    }

    XmlCommand &noFlush()
    {
        p_flush = false;
        return *this;
    }

    XmlCommand &setId(const char *id)
    {
        if (id and *id)
            cout << " id=\"" << id << '"';

        return *this;
    }

    XmlStream &cout;
    std::lock_guard<XmlStream> guard;
    bool p_flush;
}; /*}}}*/
}  // namespace

///////////////////////////////////////////////////////////////////////////
ProtocolHandler::ProtocolHandler(PdCom::impl::Process *process, IOLayer *io) :
    PdCom::impl::ProtocolHandler(process),
    StreambufLayer(io),
    ExpatWrapper<ProtocolHandler, true>(),
    cout(process, this),
    root(std::make_shared<DirNode>()),
    channel_subscriptions_(new ChannelSubscriptionsWithGroup())
{ /*{{{*/

#if PDC_DEBUG
    {
        char *spy = getenv("SPY");
        if (spy and atoi(spy))
            insert(new SpyLayer(io));
    }
#endif

    const char buf[] = "<xml>";
    ExpatBuffer<sizeof(buf)> inputBuf(*this);
    std::memcpy(inputBuf.fill(), buf, sizeof(buf));
    if (!parse(std::move(inputBuf), sizeof(buf) - 1, false))
        throw Exception("setting up xml parser failed");
} /*}}}*/

///////////////////////////////////////////////////////////////////////////
ProtocolHandler::~ProtocolHandler()
{ /*{{{*/
    log_debug("killed %p", static_cast<void *>(this));
} /*}}}*/

///////////////////////////////////////////////////////////////////////////
void ProtocolHandler::xmlError(const char *err_msg)
{ /*{{{*/
    if (state != StartUp and state != WaitForConnected)
        throw ProtocolError(std::string("Fatal XML parsing error: ") + err_msg);
} /*}}}*/

#if PDC_DEBUG
const char *statename[] = {
        "StartUp",
        "Idle",
        "GetListing",
        "WaitForConnected",
};
#endif

namespace {
constexpr const char *message_tags[] = {
        "crit_error", "error", "warn", "info", "reset",
};

bool is_message_tag(const char *name)
{
    for (const auto t : message_tags) {
        if (!strcmp(name, t))
            return true;
    }
    return false;
}
}  // namespace

///////////////////////////////////////////////////////////////////////////
void ProtocolHandler::startElement(const char *name, const char **atts)
{ /*{{{*/
    const char *id = nullptr;

    switch (state) {
        case StartUp:
            if (!strcmp(name, "xml") and level() == 1)
                state = WaitForConnected;
            break;

        case WaitForConnected:
            if (!strcmp(name, "connected") and level() == 2) {
                Attribute a(atts);
                feature   = a.find("features");
                m_name    = a.find("app");
                m_version = a.find("appversion");

                if (!feature.group)
                    channel_subscriptions_.reset(
                            new ChannelSubscriptionsWithoutGroup());

                // if login is mandatory, <saslauth> is sent automatically by
                // server
                if (feature.login and !strcmp(a.find("login"), "mandatory")) {
                    if (process->auth_manager_) {
                        state = WaitForLogin;
                        break;
                    }
                    else
                        throw LoginRequired();
                }
                XmlCommand(cout, "remote_host")
                        .addAttribute("access", "allow")
                        .addEscapedAttribute(
                                "applicationname", process->applicationName())
                        .addEscapedAttribute("hostname", process->hostname())
                        .setId("init");

                state = Idle;
            }
            break;

        case WaitForLogin:
            if (level() == 2 and !strcmp(name, "saslauth")
                and processLogin(atts)) {
                XmlCommand(cout, "remote_host")
                        .addAttribute("access", "allow")
                        .addEscapedAttribute(
                                "applicationname", process->applicationName())
                        .addEscapedAttribute("hostname", process->hostname())
                        .setId("init");

                state = Idle;
            }
            break;

        case Idle:
            if (level() != 2) {
                log_debug("Unknown XML tag in Idle: %s", name);
                break;
            }

            // find() returns "" if id is not found
            id = Attribute(atts).find("id");

            if (!strcmp(name, "data")) {
                Attribute a(atts);

                unsigned int dataGroupId = 0;
                a.getUInt("group", dataGroupId);

                dataTime = a.getTime_ns("time");

                if (!feature.group) {
                    state = ReadDataOrEvent;
                }
                else if (!dataGroupId) {
                    // Attribute "group" not found. Event transmission
                    state = ReadEvent;
                }
                else {
                    periodic_receive_handle_ =
                            channel_subscriptions_->startNewDataRecieving(
                                    dataGroupId, variable_cache_.lock(),
                                    dataTime);
                    state = ReadData;
                }
            }
            else if (!strcmp(name, "pu")) {
                unsigned int index;

                if (!feature.xsap and Attribute(atts).getUInt("index", index)) {
                    if (parameter_event_.find(index)
                        != parameter_event_.end()) {
                        pollParameter(index, "pollParameterEvent");
                    }
                }
            }
            else if (!strcmp(name, "parameter")) {
                Parameter *p = getParameter(atts);
                if (p) {
                    Attribute a(atts);
                    const auto time_ns = a.getTime_ns("mtime");
                    unsigned int pm;
                    const auto getValue = [&a, p, this]() {
                        auto cache                 = variable_cache_.lock();
                        const char *const hexvalue = a.find("hexvalue");
                        if (hexvalue && *hexvalue)
                            cache[0].readFromHexDec(hexvalue, *p);
                        else
                            cache[0].readFromString(a.find("value"), *p);
                        return cache;
                    };
                    if (!strcmp(id, "pollParameter")
                        and !subscription_pending_parameter_polls_.empty()) {
                        auto sub_collection = std::move(
                                subscription_pending_parameter_polls_.front());
                        subscription_pending_parameter_polls_.pop_front();
                        sub_collection.subscriptions_.readData(
                                getValue()[0].data(), time_ns);
                    }
                    else if (!strcmp(id, "VariablePollParameter")) {
                        variable_pending_polls_.processResult(
                                *p, getValue()[0].data(), time_ns);
                    }
                    else if (
                            !feature.xsap
                            and !strcmp(id, "pollParameterEvent")) {
                        parameter_event_[p->index_].readData(
                                getValue()[0].data(), time_ns);
                    }
                    else if (!strcmp(id, "pendingParameter"))
                        pendingAck(p);
                    else if (a.getUInt("pm", pm) && pm == 1) {
                        parameter_event_[p->index_].readData(
                                getValue()[0].data(), time_ns);
                    }
                }
            }
            else if (!strcmp(name, "channel")) {
                Channel *c = getChannel(atts);
                if (c) {
                    if (!strcmp(id, "pollChannel")
                        and !subscription_pending_channel_polls_.empty()) {
                        // can be nullptr if subscription was deleted in the
                        // meantime
                        auto channel_poll = std::move(
                                subscription_pending_channel_polls_.front());
                        subscription_pending_channel_polls_.pop_front();
                        Attribute a(atts);
                        auto cache = variable_cache_.lock();
                        if (const auto s = a.find("hexvalue", nullptr))
                            cache[0].readFromHexDec(s, *c);
                        else
                            cache[0].readFromString(a.find("value"), *c);
                        channel_poll.subscriptions_.readData(
                                cache[0].data(), a.getTime_ns("time"));
                    }
                    else if (!strcmp(id, "VariablePollChannel")) {
                        Attribute a(atts);
                        auto cache = variable_cache_.lock();
                        if (const auto s = a.find("hexvalue", nullptr))
                            cache[0].readFromHexDec(s, *c);
                        else
                            cache[0].readFromString(a.find("value"), *c);
                        variable_pending_polls_.processResult(
                                *c, cache[0].data(), a.getTime_ns("time"));
                    }
                    else if (!strcmp(id, "pendingChannel"))
                        pendingAck(c);
                }
            }
            else if (!strcmp(name, "ack")) {
                // Check which ack was called
                if (!strcmp(id, ChannelSubscriptions::xsadId))
                    channel_subscriptions_->xsadAck(process);
                else if (!strcmp(id, "xsapQ"))
                    xsapAck();
                else if (!strcmp(id, ChannelSubscriptions::xsodId))
                    channel_subscriptions_->xsodAck();
                else if (!strcmp(id, "pendingChannel"))
                    pendingAck(nullptr);
                else if (!strcmp(id, "findQ"))
                    processFindRequest();
                else if (!strcmp(id, "listQ")) {
                    fullListing = Cached;

                    // Process all pending list and find requests
                    while (processListRequest())
                        ;
                    while (processFindRequest())
                        ;
                }
                else if (!strcmp(id, "history") and !seqNoQ.empty()) {
                    if (messageSeqNo != seqNoQ.front()) {
                        PdCom::Message m = {};
                        m.seqNo          = seqNoQ.front();
                        process->getMessageReply(m);
                    }
                    seqNoQ.pop();
                }
                else if (!strcmp(id, "init"))
                    process->connected();
            }
            else if (
                    !strcmp(name, "listing") or !strcmp(name, "channels")
                    or !strcmp(name, "parameters")) {
                state = GetListing;
            }
            else if (!strcmp(name, "saslauth"))
                processLogin(atts);
            else if (!strcmp(name, "ping"))
                process->pingReply();
            else if (!strcmp(name, "broadcast"))
                processBroadcast(atts);
            else if (!strcmp(name, "message_history"))
                state = GetActiveMessages;
            else if (!strcmp(name, "clients")) {
                state = ReadClientStatistics;
                client_statistics.clear();
            }
            else if (is_message_tag(name) and process->message_manager_) {
                PdCom::Message msg = {};
                Attribute a(atts);
                if (a.getMessage(name, msg, feature.history)) {
                    if (!strcmp(id, "history")) {
                        messageSeqNo = msg.seqNo;
                        process->getMessageReply(std::move(msg));
                    }
                    else {
                        process->processMessage(std::move(msg));
                    }
                }
                else {
                    log_debug("Failed to parse message");
                }
            }
            break;

        case GetListing:
            if (level() != 3) {
                log_debug("Unknown XML tag in GetListing: %s", name);
                break;
            }

            if (!strcmp(name, "dir")) {
                const char *path;
                if (Attribute(atts).getString("path", path)
                    and !root->find(path)) {
                    DirNode *node = new DirNode;
                    root->insert(std::unique_ptr<DirNode>(node), path);
                    log_debug("Create dir %s", node->path().c_str());
                }
            }
            else if (!strcmp(name, "channel"))
                getChannel(atts);
            else if (!strcmp(name, "parameter"))
                getParameter(atts);
            break;

        case ReadData:
            if (level() != 3) {
                log_debug("Unknown XML tag in ReadData: %s", name);
                break;
            }

            readPeriodicData(name, atts);

            break;

        case ReadEvent:
            if (level() != 3) {
                log_debug("Unknown XML tag in ReadEvent: %s", name);
                break;
            }

            readEventData(name, atts);

            break;

        case ReadDataOrEvent:
            if (level() != 3) {
                log_debug("Unknown XML tag in ReadDataOrEvent: %s", name);
                break;
            }

            if (name[0] == 'F') {
                state = ReadData;
                periodic_receive_handle_ =
                        channel_subscriptions_->startNewDataRecieving(
                                0, variable_cache_.lock(), dataTime);
                readPeriodicData(name, atts);
            }
            else if (name[0] == 'E') {
                state = ReadEvent;
                readEventData(name, atts);
            }

            break;
        case GetActiveMessages:
            if (level() != 3) {
                log_debug("Unknown XML tag in GetActiveMessages: %s", name);
                break;
            }

            {
                Message msg;
                if (Attribute(atts).getMessage(name, msg, true)) {
                    messageList.push_back(std::move(msg));
                }
            }
            break;

        case ReadClientStatistics:
            if (level() != 3 or strcmp(name, "client")) {
                log_debug("Unknown XML tag in ReadClientStatistics: %s", name);
                break;
            }

            {
                PdCom::ClientStatistics stats;
                if (Attribute(atts).getClientStatistics(stats)) {
                    client_statistics.push_back(std::move(stats));
                }
                else {
                    log_debug("Drop incomplete <client> tag");
                }
            }
            break;

        default:
            break;
    };
} /*}}}*/

///////////////////////////////////////////////////////////////////////////
void ProtocolHandler::endElement(const char *name)
{ /*{{{*/
    switch (state) {
        case GetListing:
            if (level() == 2) {
                state = Idle;

                // name is either
                //      - channels
                //      - parameters
                //      - listing
                if (!strcmp(name, "listing"))
                    processListRequest();
            }

            break;

        case ReadData:
            if (level() != 2 or strcmp(name, "data"))
                break;
            if (periodic_receive_handle_) {
                periodic_receive_handle_.endNewDataRecieving();
                periodic_receive_handle_ = {};
            }
            state = Idle;
            break;

        case ReadEvent:
        case ReadDataOrEvent:
            if (level() == 2 and !strcmp(name, "data")) {
                state = Idle;
            }
            break;

        case GetActiveMessages:
            if (level() == 2 and !strcmp(name, "message_history")) {
                state = Idle;
                process->activeMessagesReply(std::move(messageList));
                messageList.clear();
            }
            break;

        case ReadClientStatistics:
            if (level() == 2 and !strcmp(name, "clients")) {
                state = Idle;
                process->clientStatisticsReply(std::move(client_statistics));
                client_statistics.clear();
            }
            break;

        default:
            break;
    }
} /*}}}*/

///////////////////////////////////////////////////////////////////////////
// Arguments:
//     var: if var == 0, this is a test to check whether path was found
//          if var != 0, found the variable --> subscribe it
void ProtocolHandler::pendingAck(Variable *var)
{ /*{{{*/
    if (pending_queue_.empty())
        throw ProtocolError("Empty pending request queue");

    for (const auto &weak_sub : pending_queue_.front().subscriptions_) {
        auto ps = weak_sub.lock();
        if (!ps)
            continue;

        std::shared_ptr<PdCom::impl::Subscription> new_sub;
        if (var) {
            new_sub = subscribe(
                    ps->This_,
                    std::static_pointer_cast<const MsrProto::Variable>(
                            var->shared_from_this()),
                    ps->subscriber_, ps->getSelector(), false);
        }
        if (!new_sub) {
            new_sub = std::make_shared<InvalidSubscription>(
                    ps->This_, ps->subscriber_, process->shared_from_this());
            process->pending_callbacks_.push_back(
                    {new_sub, PdCom::Subscription::State::Invalid});
        }
        process->replace(ps, new_sub);
        ps->replaceImpl(std::move(new_sub));
    }

    if (!var) {
        // End of processing; clean up structures
        pending_queue_.pop_front();
    }
} /*}}}*/

///////////////////////////////////////////////////////////////////////////
void ProtocolHandler::xsapAck()
{ /*{{{*/
    if (xsapQ.empty())
        throw ProtocolError("no pending xsad commands available");

    auto map_it = parameter_event_.find(xsapQ.front());
    if (map_it != parameter_event_.end()) {
        map_it->second.broadcastState(
                PdCom::Subscription::State::Active,
                process->pending_callbacks_);
    }

    xsapQ.pop();
} /*}}}*/

///////////////////////////////////////////////////////////////////////////
Parameter *ProtocolHandler::getParameter(const char **_atts)
{ /*{{{*/
    Attribute atts(_atts);
    unsigned index;

    if (!atts.getUInt("index", index))
        return 0;

    Parameter *&param = parameter[index];
    if (!param) {
        // index="0" name="/Constant/Value" datasize="8" typ="TDBL_LIST"
        // anz="10" cnum="10" rnum="1" orientation="VECTOR" flags="7"
        const char *name = atts.find("name");

        // Check index and name
        if (!*name) {
            log_debug("Missing index or name for variable. Ignoring");
            return 0;
        }

        // Check whether variable exists
        if (std::dynamic_pointer_cast<Variable>(root->find(name))) {
            log_debug("%s exists. Ignoring", name);
            return 0;
        }

        SizeInfo size_info;
        const auto type_info = getDataType(_atts, size_info);
        if (!type_info) {
            log_debug("Datatype for %s unspecified. Ignoring", name);
            return 0;
        }

        unsigned int flags;
        if (!atts.getUInt("flags", flags)) {
            log_debug("Flags for %s unspecified. Ignoring", name);
            return 0;
        }

        auto shared_param = std::make_shared<Parameter>(
                std::move(size_info), type_info, process->shared_from_this(),
                index, flags, atts.find("alias"), atts.isTrue("dir"));
        param = shared_param.get();
        root->insert(std::move(shared_param), name);
    }

    return param;
} /*}}}*/

///////////////////////////////////////////////////////////////////////////
Channel *ProtocolHandler::getChannel(const char **_atts)
{ /*{{{*/
    Attribute atts(_atts);
    unsigned index;

    if (!atts.getUInt("index", index))
        return 0;

    Channel *&chan = channel[index];
    if (!chan) {
        // index="0" name="/Constant/Value" datasize="8" typ="TDBL_LIST"
        // anz="10" cnum="10" rnum="1" orientation="VECTOR" flags="7"
        const char *name = atts.find("name");

        // Check index and name
        if (!*name) {
            log_debug("Missing index or name for variable. Ignoring");
            return 0;
        }

        // Check whether variable exists
        if (std::dynamic_pointer_cast<Variable>(root->find(name))) {
            log_debug("%s exists. Ignoring", name);
            return 0;
        }

        SizeInfo size_info;
        const auto type_info = getDataType(_atts, size_info);
        if (!type_info) {
            log_debug("Datatype for %s unspecified. Ignoring", name);
            return 0;
        }

        unsigned int bufsize;
        if (!atts.getUInt("bufsize", bufsize)) {
            log_debug("bufsize for %s unspecified. Ignoring", name);
            return 0;
        }

        double freq;
        if (!atts.getDouble("HZ", freq) or !freq) {
            log_debug("HZ for %s unspecified or zero. Ignoring", name);
            return 0;
        }

        unsigned int taskId = 0;
        atts.getUInt("task", taskId);

        auto shared_chan = std::make_shared<Channel>(
                std::move(size_info), type_info, process->shared_from_this(),
                index, taskId, 1.0 / freq, atts.find("alias"), bufsize,
                atts.isTrue("dir"));
        chan = shared_chan.get();
        root->insert(std::move(shared_chan), name);
    }

    return chan;
} /*}}}*/

namespace {
struct TypeMap
{
    const char *msr_name;
    const PdCom::TypeInfo *type_info;
    size_t len;
    template <size_t N>
    constexpr TypeMap(const char (&name)[N], PdCom::TypeInfo const &ti) :
        msr_name(&name[0]), type_info(&ti), len(N - 1)
    {}
};
}  // namespace

///////////////////////////////////////////////////////////////////////////
const PdCom::TypeInfo *
ProtocolHandler::getDataType(const char **atts, PdCom::SizeInfo &size_info)
{ /*{{{*/
    unsigned int rnum       = 0;
    unsigned int cnum       = 0;
    unsigned int count      = 0;
    const char *orientation = 0;
    using PdCom::SizeInfo;
    using PdCom::TypeInfo;
    const TypeInfo *ans = nullptr;

    static constexpr TypeMap type_map[] = {
            {"TDBL", details::TypeInfoTraits<double>::type_info},
            {"TINT", details::TypeInfoTraits<int32_t>::type_info},
            {"TUINT", details::TypeInfoTraits<uint32_t>::type_info},
            {"TCHAR", details::TypeInfoTraits<int8_t>::type_info},
            {"TUCHAR", details::TypeInfoTraits<uint8_t>::type_info},
            {"TSHORT", details::TypeInfoTraits<int16_t>::type_info},
            {"TUSHORT", details::TypeInfoTraits<uint16_t>::type_info},
            {"TLINT", details::TypeInfoTraits<int64_t>::type_info},
            {"TULINT", details::TypeInfoTraits<uint64_t>::type_info},
            {"TFLT", details::TypeInfoTraits<float>::type_info},
    };

    const char *const typeStr = Attribute(atts).find("typ");

    // Try and find the name in msr_dtypemap
    for (const auto &t : type_map) {
        if (!strncmp(typeStr, t.msr_name, t.len))
            ans = t.type_info;
    }
    if (!ans)
        return nullptr;

    // cache variable data
    for (int i = 0; atts[i]; i += 2) {
        if (!strcmp(atts[i], "rnum")) {
            rnum = atoi(atts[i + 1]);
        }
        else if (!strcmp(atts[i], "cnum")) {
            cnum = atoi(atts[i + 1]);
        }
        else if (!strcmp(atts[i], "anz")) {
            count = atoi(atts[i + 1]);
        }
        else if (!strcmp(atts[i], "orientation")) {
            orientation = atts[i + 1];
        }
    }

    if (orientation) {
        if (!strcmp(orientation, "VECTOR")) {
            if (!count)
                return nullptr;

            size_info = SizeInfo::RowVector(count);
        }
        else if (orientation and !strncmp(orientation, "MATRIX", 6)) {
            if (!(rnum and cnum))
                return nullptr;

            size_info = SizeInfo::Matrix(rnum, cnum);
        }
        else
            return nullptr;
    }
    else {
        if (rnum > 1 or cnum > 1 or count > 1)
            return nullptr;

        size_info = SizeInfo::Scalar();
    }

    return ans;
} /*}}}*/

///////////////////////////////////////////////////////////////////////////
bool ProtocolHandler::login(const char *mech, const char *clientData)
{ /*{{{*/
    if (!feature.login)
        return false;

    XmlCommand sasl(cout, "auth");
    if (mech and *mech)
        sasl.addAttribute("mech", mech);

    if (clientData and *clientData)
        sasl.addAttribute("clientdata", clientData);

    return true;
} /*}}}*/

///////////////////////////////////////////////////////////////////////////
void ProtocolHandler::logout()
{ /*{{{*/
    XmlCommand(cout, "auth").addAttribute("logout", 1);
} /*}}}*/

///////////////////////////////////////////////////////////////////////////
bool ProtocolHandler::processLogin(const char **_atts)
{ /*{{{*/
    // pdserv sends an empty saslauth tag after logout()
    if (!*_atts)
        return false;
    Attribute atts(_atts);
    int finished = 0;
    if (atts.getInt("success", finished) and !finished)
        finished = -1;

    const char *mechlist = 0;
    atts.getString("mechlist", mechlist);

    const char *serverdata = 0;
    atts.getString("serverdata", serverdata);

    process->loginReply(mechlist, serverdata, finished);
    return finished > 0;
} /*}}}*/


///////////////////////////////////////////////////////////////////////////
void ProtocolHandler::setPolite(bool state)
{ /*{{{*/
    if (state != polite and feature.polite) {
        polite = state;

        XmlCommand(cout, "remote_host").addAttribute("polite", state);
    }
} /*}}}*/

///////////////////////////////////////////////////////////////////////////
bool ProtocolHandler::asyncData()
{ /*{{{*/
    ExpatBuffer<ReadBufferSize> inputBuffer(*this);

    const auto n =
            StreambufLayer::read(inputBuffer.fill<char>(), inputBuffer.size());
    if (!protocolError and n > 0)
        parse(std::move(inputBuffer), n);
    return n != 0;
} /*}}}*/

///////////////////////////////////////////////////////////////////////////

void ProtocolHandler::cancelSubscriptions()
{
    for (auto &kv : cancelations_) {
        const auto tm = kv.first;
        if (tm == PdCom::event_mode) {
            for (const auto &var : kv.second) {
                if (var->is_parameter_) {
                    const bool unsubscribe =
                            parameter_event_.at(var->index_).remove_expired();
                    if (unsubscribe and feature.xsap) {
                        XmlCommand(cout, "xsop")
                                .addAttribute("parameters", var->index_);
                    }
                }
                else {
                    channel_subscriptions_->unsubscribeEvent(*var, cout);
                }
            }
            kv.second.clear();
        }

        else if (tm.getInterval() > 0) {
            for (const auto &var : kv.second) {
                channel_subscriptions_->unsubscribePeriodic(tm, *var, cout);
            }
            kv.second.clear();
        }
    }
}
///////////////////////////////////////////////////////////////////////////
bool ProtocolHandler::find(const std::string &path)
{ /*{{{*/
    auto var = std::dynamic_pointer_cast<Variable>(root->find(path));
    if (var or path.empty()) {
        process->findReply(PdCom::impl::Variable::toUApi(var));
        return true;
    }

    // we already have all variables cached, so we know the requested one does
    // not exist.
    if (fullListing == Cached) {
        process->findReply({});
        return true;
    }

    findQ.push(path);

    if (fullListing == Uncached) {
        // Do not flush next command
        XmlCommand(cout, "rp").addEscapedAttribute("name", path).noFlush();

        XmlCommand(cout, "rk").addEscapedAttribute("name", path).setId("findQ");
    }

    return false;
} /*}}}*/

///////////////////////////////////////////////////////////////////////////
bool ProtocolHandler::processFindRequest()
{                       /*{{{*/
    if (findQ.empty())  // required by listReply!
        return false;

    std::shared_ptr<DirNode> node = root->find(findQ.front());
    process->findReply(PdCom::impl::Variable::toUApi(
            std::dynamic_pointer_cast<PdCom::impl::Variable>(node)));
    findQ.pop();

    return !findQ.empty();
} /*}}}*/

///////////////////////////////////////////////////////////////////////////
bool ProtocolHandler::list(const std::string &dir)
{ /*{{{*/
    listQ.push(dir);

    if (fullListing != Uncached) {
        if (fullListing == Cached)
            processListRequest();
    }
    else if (dir.empty() or !feature.list) {
        // List everything. First send a <rp> and the state machine
        // will send a <rp> thereafter
        XmlCommand(cout, "rk").noFlush();
        XmlCommand(cout, "rp").setId("listQ");

        fullListing = InProgress;
    }
    else
        XmlCommand(cout, "list")
                .addEscapedAttribute("path", dir)
                .addAttribute("noderived", 1);

    return fullListing == Cached;
} /*}}}*/

void PdCom::impl::MsrProto::ProtocolHandler::getClientStatistics()
{
    XmlCommand(cout, "read_statistics");
}

///////////////////////////////////////////////////////////////////////////
bool ProtocolHandler::processListRequest()
{ /*{{{*/
    std::string path = listQ.front();
    listQ.pop();

    std::vector<std::string> dirList;

    DirNode::List list;
    const bool listall = path.empty();
    if (listall)
        root->getAllChildren(list);
    else {
        // Directory list
        std::shared_ptr<DirNode> node = root->find(path);
        if (node)
            node->getChildren(list);
    }

    std::vector<PdCom::Variable> varList;
    varList.reserve(list.size());

    for (const auto &node : list) {
        if (!listall and node->hasChildren())
            dirList.push_back(node->path());

        auto v = std::dynamic_pointer_cast<PdCom::impl::Variable>(node);
        if (v)
            varList.emplace_back(impl::Variable::toUApi(v));
    }

    process->listReply(varList, dirList);

    return !listQ.empty();
} /*}}}*/

///////////////////////////////////////////////////////////////////////////
void ProtocolHandler::pollChannel(PollSubscription &sub)
{ /*{{{*/
    const auto var_ptr = sub.variable_.lock();
    if (!var_ptr)
        throw EmptyVariable();
    const auto index = static_cast<const Variable &>(*var_ptr).index_;
    if (subscription_pending_channel_polls_.append(
                index, sub.shared_from_this()))
        pollChannel(index, "pollChannel");
}

void ProtocolHandler::pollChannel(size_t index, const char *id)
{
    XmlCommand(cout, "rk")
            .addAttribute("index", index)
            .addAttribute("hex", 1)
            .setId(id);
} /*}}}*/

void ProtocolHandler::pollParameter(PollSubscription &sub)
{
    const auto var_ptr = sub.variable_.lock();
    if (!var_ptr)
        throw EmptyVariable();
    const auto index = static_cast<const Variable &>(*var_ptr).index_;
    if (subscription_pending_parameter_polls_.append(
                index, sub.shared_from_this()))
        pollParameter(index, "pollParameter");
}

///////////////////////////////////////////////////////////////////////////
void ProtocolHandler::pollParameter(size_t index, const char *id)
{ /*{{{*/
    XmlCommand(cout, "rp")
            .addAttribute("index", index)
            .addAttribute("short", 1)
            .addAttribute("hex", 1)
            .setId(id);
} /*}}}*/

void ProtocolHandler::pollVariable(
        const PdCom::impl::Variable &_var,
        VariablePollPromise &&promise)
{
    const auto &var = static_cast<const MsrProto::Variable &>(_var);
    if (variable_pending_polls_.append(var, std::move(promise))) {
        if (var.is_parameter_)
            pollParameter(var.index_, "VariablePollParameter");
        else
            pollChannel(var.index_, "VariablePollChannel");
    }
}

///////////////////////////////////////////////////////////////////////////
std::shared_ptr<PdCom::impl::Subscription> ProtocolHandler::subscribe(
        PdCom::Subscription *subscription,
        std::shared_ptr<const PdCom::impl::Variable> _var,
        PdCom::Subscriber &subscriber,
        const PdCom::Selector &selector,
        bool notify_pending)
{
    auto var = std::dynamic_pointer_cast<const Variable>(_var);
    if (!var)
        return nullptr;
    if (subscriber.getTransmission() == PdCom::poll_mode) {
        auto ans = std::make_shared<PollSubscription>(
                var, subscription, subscriber, process->shared_from_this(),
                selector);
        process->pending_callbacks_.push_back(
                {ans, PdCom::Subscription::State::Active});
        return ans;
    }
    else if (subscriber.getTransmission() == PdCom::event_mode) {
        if (var->is_parameter_) {
            auto ans = std::make_shared<EventSubscription>(
                    var, subscription, subscriber, process->shared_from_this(),
                    selector);
            if (parameter_event_[var->index_].empty()) {
                // not yet subscribed
                if (feature.xsap) {
                    XmlCommand(cout, "xsap")
                            .addAttribute("parameters", var->index_)
                            .addAttribute("hex", 1)
                            .setId("xsapQ");
                    xsapQ.push(var->index_);
                    if (notify_pending)
                        process->pending_callbacks_.push_back(
                                {ans, PdCom::Subscription::State::Pending});
                }
                else {
                    parameter_event_[var->index_].currentState =
                            PdCom::Subscription::State::Active;
                    process->pending_callbacks_.push_back(
                            {ans, PdCom::Subscription::State::Active});
                }
            }
            else {
                if (notify_pending
                    or parameter_event_[var->index_].currentState
                            != PdCom::Subscription::State::Pending)
                    process->pending_callbacks_.push_back(
                            {ans, parameter_event_[var->index_].currentState});
            }
            parameter_event_[var->index_].insert(ans);
            return ans;
        }
        else {
            return channel_subscriptions_->subscribeEvent(
                    subscription, var, subscriber, selector, notify_pending,
                    cout);
        }
    }
    else {
        if (var->is_parameter_)
            return {};
        return channel_subscriptions_->subscribePeriodic(
                subscription, var, subscriber, selector, notify_pending, cout);
    }
}


///////////////////////////////////////////////////////////////////////////
std::shared_ptr<PdCom::impl::Subscription> ProtocolHandler::subscribe(
        PdCom::Subscription *subscription,
        const std::string &path,
        PdCom::Subscriber &subscriber,
        const PdCom::Selector &selector)
{ /*{{{*/
    setPolite(false);

    if (auto var = std::dynamic_pointer_cast<Variable>(root->find(path))) {
        return subscribe(subscription, var, subscriber, selector, false);
    }

    // Variable is unknown. Find it first. This requires interaction
    // with the server, which automatically postpones calls to
    // request->subscriber to a later point in time
    const auto it = std::find_if(
            pending_queue_.begin(), pending_queue_.end(),
            [&path](const PendingRequest &pr) { return pr.path_ == path; });

    auto ans = std::make_shared<PendingSubscription>(
            subscription, subscriber, process->shared_from_this(),
            selector.impl_);

    if (it == pending_queue_.end()) {
        // Discovery not yet active
        XmlCommand(cout, "rp")
                .addAttribute("name", path)
                .addAttribute("hex", 1)
                .setId("pendingParameter")
                .noFlush();
        XmlCommand(cout, "rk")
                .addAttribute("name", path)
                .setId("pendingChannel");
        pending_queue_.push_back({path, {{ans}}});
    }
    else {
        it->subscriptions_.insert(ans);
    }
    process->pending_callbacks_.push_back(
            {ans, PdCom::Subscription::State::Pending});
    return ans;
} /*}}}*/

void ProtocolHandler::unsubscribe(EventSubscription &s) noexcept
{
    const auto var_ptr = s.variable_.lock();
    if (!var_ptr)
        return;
    try {
        cancelations_[PdCom::event_mode].insert(
                std::static_pointer_cast<const Variable>(var_ptr));
    }
    catch (std::bad_alloc const &) {
        // never going to happen
    }
}

void ProtocolHandler::unsubscribe(PeriodicSubscription &s) noexcept
{
    const auto var_ptr = s.variable_.lock();
    if (!var_ptr)
        return;
    try {
        cancelations_[s.subscriber_.getTransmission()].insert(
                std::static_pointer_cast<const Variable>(var_ptr));
    }
    catch (std::bad_alloc const &) {
        // never going to happen
    }
    catch (...) {
    }
}

///////////////////////////////////////////////////////////////////////////
void ProtocolHandler::ping()
{ /*{{{*/
    XmlCommand(cout, "ping");
} /*}}}*/

///////////////////////////////////////////////////////////////////////////
std::string ProtocolHandler::name() const
{ /*{{{*/
    return m_name;
} /*}}}*/

///////////////////////////////////////////////////////////////////////////
std::string ProtocolHandler::version() const
{ /*{{{*/
    return m_version;
} /*}}}*/

///////////////////////////////////////////////////////////////////////////
PdCom::Variable::SetValueFuture ProtocolHandler::writeParameter(
        const Parameter &p,
        const void *src,
        PdCom::TypeInfo::DataType src_type,
        size_t idx,
        size_t n)
{ /*{{{*/
    XmlCommand(cout, "wp")
            .addAttribute("index", p.index_)
            .addAttribute("startindex", idx, idx)
            .addAttributeStream(
                    "hexvalue",
                    [&, this](XmlStream &os) {
                        encoder_.toHex(*p.type_info, src, src_type, n, os);
                    })
            .setId("wp");
    // TODO(igh-vh) implement parameter setValue notification in msr and
    // pdserv
    return {};
} /*}}}*/

///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
bool fcmp(const char *p, const char *target, size_t len)
{
    return len >= ::strlen(target) && !strncmp(p, target, len);
}

void ProtocolHandler::Feature::operator=(const char *str)
{ /*{{{*/
    if (!str)
        return;
    *this = Feature {};

    const char *p = str;
    size_t len;

    do {
        p   = str;
        str = strchr(str, ',');

        len = str ? str++ - p : strlen(p);

        if (len) {
            if (fcmp(p, "pushparameters", len))
                pushparameters = true;

            if (fcmp(p, "binparameters", len))
                binparameters = true;

            if (fcmp(p, "eventchannels", len))
                eventchannels = true;

            if (fcmp(p, "statistics", len))
                statistics = true;

            if (fcmp(p, "pmtime", len))
                pmtime = true;

            if (fcmp(p, "aic", len))
                aic = true;

            if (fcmp(p, "messages", len))
                messages = true;

            if (fcmp(p, "group", len))
                group = true;

            if (fcmp(p, "history", len))
                history = true;

            if (fcmp(p, "quiet", len))
                quiet = true;

            if (fcmp(p, "list", len)) {
                list  = true;
                group = true;
            }

            if (fcmp(p, "exclusive", len))
                exclusive = true;

            if (fcmp(p, "polite", len))
                polite = true;

            /* feature disabled, because xsap does not work on all
               parameters in pdserv (those who are generated with
               msr:splitvectors), Hm

                  if (fcmp(p, "xsap", len))
                      xsap = true;
            */

            if (fcmp(p, "login", 5))
                login = true;

            if (fcmp(p, "eventid=", 8))
                eventid = strtoul(p + 8, 0, 0);
        }

    } while (str);
} /*}}}*/


void ProtocolHandler::getMessage(uint32_t seqNo)
{
    if (!feature.history) {
        process->getMessageReply({});
    }
    else {
        XmlCommand(cout, "message_history")
                .addAttribute("seq", seqNo)
                .setId("history");
        seqNoQ.push(seqNo);
    }
}

void ProtocolHandler::getActiveMessages()
{
    if (!feature.history) {
        process->activeMessagesReply({});
    }
    else
        XmlCommand(cout, "message_history");
}

void ProtocolHandler::broadcast(
        const std::string &message,
        const std::string &attr)
{
    XmlCommand msg(cout, "broadcast");
    msg.addEscapedAttribute(attr.c_str(), message);
}


void ProtocolHandler::processBroadcast(const char **atts)
{
    Attribute a(atts);
    const char *try_keys[] = {"action", "text"};
    const char *value = nullptr, *user = nullptr;
    if (!a.getString("user", user))
        user = "anonymous";
    for (const char *key : try_keys) {
        if (a.getString(key, value)) {
            process->broadcastReply(value, key, a.getTime_ns("time"), user);
            return;
        }
    }
}

void PdCom::impl::MsrProto::ProtocolHandler::readEventData(
        const char *name,
        const char **atts)
{
    if (name[0] == 'E') {
        Attribute a(atts);
        unsigned int index;
        if (a.getUInt("c", index)) {
            const auto map_it =
                    channel_subscriptions_->getEventMap().find(index);
            if (map_it != channel_subscriptions_->getEventMap().end()
                and channel[index]) {
                auto cache = variable_cache_.lock();
                if (const char *d = a.find("d")) {
                    cache[0].readFromBase64(
                            a.find("d"), strlen(d), *channel[index]);
                    map_it->second.readData(cache[0].data(), dataTime);
                }
            }
        }
    }
}

void PdCom::impl::MsrProto::ProtocolHandler::readPeriodicData(
        const char *name,
        const char **atts)
{
    if (name[0] == 'F' and periodic_receive_handle_) {
        Attribute a(atts);
        unsigned int index = 0;
        if (a.getUInt("c", index) and channel[index]) {
            periodic_receive_handle_.readData(*channel[index], a.find("d"));
        }
    }
    else if (!strcmp(name, "time") and periodic_receive_handle_) {
        periodic_receive_handle_.readTimestamps(Attribute(atts).find("d"));
    }
}

template class PdCom::impl::MsrProto::ExpatWrapper<ProtocolHandler, true>;

bool ProtocolHandler::SubscriptionPendingPollList::append(
        unsigned int const id,
        const std::shared_ptr<PollSubscription> &sub)
{
    for (auto &pp : *this) {
        if (pp.id_ == id) {
            pp.subscriptions_.insert(sub);
            pp.subscriptions_.remove_expired();
            return false;
        }
    }
    push_back({id, {{sub}}});
    return true;
}

bool ProtocolHandler::VariablePendingPollList::append(
        const Variable &var,
        VariablePollPromise promise)
{
    for (auto &pp : polls_) {
        if (pp.id_ == var.index_ and pp.is_parameter_ == var.is_parameter_) {
            pp.promises_.push_back(std::move(promise));
            return false;
        }
    }
    polls_.emplace_back(std::move(promise), var.index_, var.is_parameter_);
    return true;
}


void ProtocolHandler::VariablePendingPollList::processResult(
        Variable const &var,
        const char *data,
        std::chrono::nanoseconds ts)
{
    if (polls_.empty())
        return;
    auto head = std::move(polls_.front());
    polls_.pop_front();
    if (var.is_parameter_ != head.is_parameter_)
        throw ProtocolError("Channel/Parameter poll reply mismatch");
    if (head.id_ != var.index_)
        throw ProtocolError("Got wrong parameter index reply");
    auto ans = PdCom::VariablePollResult {var.toUApi()};
    std::copy(
            data, data + var.totalSizeInBytes(),
            reinterpret_cast<char *>(ans.getData()));
    for (auto &promise : head.promises_)
        promise.resolve(ans, ts);
}

ProtocolHandler::VariablePendingPollList::~VariablePendingPollList()
{
    const PdCom::ProcessGoneAway ex {};
    for (auto &poll_list : polls_)
        for (auto &pending_poll : poll_list.promises_)
            try {
                pending_poll.reject(ex);
            }
            catch (...) {
            }
}

PdCom::Variable::SetValueFuture ProtocolHandler::PendingSetValues::push()
{
    auto ans = createFuture<PdCom::Exception const &>();
    queue_.push(std::move(ans.second));
    return std::move(ans.first);
}

void ProtocolHandler::PendingSetValues::pop()
{
    if (queue_.empty())
        return;

    auto promise = std::move(queue_.front());
    queue_.pop();
    promise.resolve();
}


ProtocolHandler::PendingSetValues::~PendingSetValues()
{
    auto q = std::move(queue_);
    const PdCom::ProcessGoneAway ex {};
    while (!q.empty()) {
        try {
            q.front().reject(ex);
        }
        catch (...) {
        }
        q.pop();
    }
}

std::vector<PdCom::Process::SubscriptionInfo>
ProtocolHandler::getActiveSubscriptions() const
{
    std::vector<PdCom::Process::SubscriptionInfo> ans;
    // parameter event
    for (const auto &map : parameter_event_) {
        PdCom::Variable v = impl::Variable::toUApi(
                std::static_pointer_cast<const MsrProto::Variable>(
                        parameter.at(map.first)->shared_from_this()));
        for (const auto &weak_sub : map.second) {
            if (const auto sub = weak_sub.lock()) {
                ans.emplace_back(sub->This_, &sub->subscriber_, v);
            }
        }
    }
    // channels
    channel_subscriptions_->dump(ans, channel);
    return ans;
}

std::shared_ptr<PdCom::impl::Subscription>
ChannelSubscriptionsWithGroup::subscribeEvent(
        PdCom::Subscription *subscription,
        std::shared_ptr<const Variable> var,
        PdCom::Subscriber &subscriber,
        const PdCom::Selector &selector,
        bool notify_pending,
        XmlStream &cout)
{
    auto process = var->process.lock();
    auto ans     = std::make_shared<EventSubscription>(
            var, subscription, subscriber, process, selector);

    if (event_[var->index_].empty()) {
        XmlCommand(cout, "xsad")
                .addAttribute("channels", var->index_)
                .addAttribute("group", 0)
                .addAttribute("coding", "Base64")
                .addAttribute("event", 1)
                .setId(xsadId);
        xsadQ.push({0, var->index_});
        if (notify_pending)
            process->pending_callbacks_.push_back(
                    {ans, PdCom::Subscription::State::Pending});
    }
    else {
        if (notify_pending
            or event_[var->index_].currentState
                    != PdCom::Subscription::State::Pending)
            process->pending_callbacks_.push_back(
                    {ans, event_[var->index_].currentState});
    }
    event_[var->index_].insert(ans);
    return ans;
}

std::shared_ptr<PdCom::impl::Subscription>
PdCom::impl::MsrProto::ChannelSubscriptionsWithGroup::subscribePeriodic(
        PdCom::Subscription *subscription,
        std::shared_ptr<const Variable> var,
        PdCom::Subscriber &subscriber,
        const PdCom::Selector &selector,
        bool notify_pending,
        XmlStream &cout)
{
    auto process = var->process.lock();
    auto ans     = std::make_shared<PeriodicSubscription>(
            var, subscription, subscriber, process, selector);
    const auto si = periodic_subscriptions_with_group_.add(
            process->pending_callbacks_, notify_pending, ans);
    if (si.needs_server_action) {
        XmlCommand(cout, "xsad")
                .addAttribute("channels", var->index_)
                .addAttribute("group", si.group_id)
                .addAttribute("coding", "Base64")
                .addAttribute("reduction", si.decimation_)
                .addAttribute("blocksize", si.blocksize_)
                .setId(xsadId);
        xsadQ.push({si.group_id, var->index_});
    }
    return ans;
}

void ChannelSubscriptionsWithGroup::unsubscribeEvent(
        const Variable &var,
        XmlStream &cout)
{
    const bool unsubscribe = event_.at(var.index_).remove_expired();
    if (unsubscribe) {
        XmlCommand(cout, "xsod")
                .addAttribute("channels", var.index_)
                .addAttribute("group", 0);
    }
}

void ChannelSubscriptionsWithGroup::unsubscribePeriodic(
        PdCom::Transmission tm,
        const Variable &var,
        XmlStream &cout)
{
    const auto si = periodic_subscriptions_with_group_.remove(var, tm);
    if (si.needs_server_action) {
        xsodQ.push(si.group_id);
        XmlCommand(cout, "xsod")
                .addAttribute("channels", var.index_)
                .addAttribute("group", si.group_id)
                .setId(xsodId);
    }
}

void PdCom::impl::MsrProto::ChannelSubscriptionsWithGroup::xsadAck(
        Process *process)
{
    if (xsadQ.empty())
        throw ProtocolError("no pending xsad commands available");
    const auto ids = xsadQ.front();
    xsadQ.pop();
    if (ids.group_id == 0) {
        const auto map_it = event_.find(ids.channel_id);
        if (map_it == event_.end())
            return;
        map_it->second.broadcastState(
                PdCom::Subscription::State::Active,
                process->pending_callbacks_);
    }
    else {
        periodic_subscriptions_with_group_.subscribeWasConfirmed(
                process->pending_callbacks_, ids.group_id, ids.channel_id);
    }
}

void PdCom::impl::MsrProto::ChannelSubscriptionsWithGroup::xsodAck()
{
    if (xsodQ.empty())
        throw ProtocolError("no pending xsod commands available");

    const auto id = xsodQ.front();
    xsodQ.pop();

    periodic_subscriptions_with_group_.unsubscribeDone(id);
}

void PdCom::impl::MsrProto::ChannelSubscriptionsWithGroup::dump(
        std::vector<PdCom::Process::SubscriptionInfo> &ans,
        const std::unordered_map<unsigned, Channel *> &channels) const
{
    ChannelSubscriptions::dump(ans, channels);
    // channel periodic
    periodic_subscriptions_with_group_.dump(ans, channels);
}

std::shared_ptr<PdCom::impl::Subscription>
PdCom::impl::MsrProto::ChannelSubscriptionsWithoutGroup::subscribeEvent(
        PdCom::Subscription *subscription,
        std::shared_ptr<const Variable> var,
        PdCom::Subscriber &subscriber,
        const PdCom::Selector &selector,
        bool notify_pending,
        XmlStream &cout)
{
    // can't have periodic and event at the same time
    if (periodic_subscriptions_without_group_.hasChannel(var->index_))
        return {};


    auto process = var->process.lock();
    auto ans     = std::make_shared<EventSubscription>(
            var, subscription, subscriber, process, selector);

    if (event_[var->index_].empty()) {
        event_[var->index_].currentState = PdCom::Subscription::State::Pending;
        XmlCommand(cout, "xsad")
                .addAttribute("channels", var->index_)
                .addAttribute("coding", "Base64")
                .addAttribute("event", 1)
                .setId(xsadId);
        xsadQ.push({true, var->index_});
        if (notify_pending)
            process->pending_callbacks_.push_back(
                    {ans, PdCom::Subscription::State::Pending});
    }
    else {
        if (notify_pending
            or event_[var->index_].currentState
                    != PdCom::Subscription::State::Pending)
            process->pending_callbacks_.push_back(
                    {ans, event_[var->index_].currentState});
    }
    event_[var->index_].insert(ans);
    return ans;
}

std::shared_ptr<PdCom::impl::Subscription>
ChannelSubscriptionsWithoutGroup::subscribePeriodic(
        PdCom::Subscription *subscription,
        std::shared_ptr<const Variable> var,
        PdCom::Subscriber &subscriber,
        const PdCom::Selector &selector,
        bool notify_pending,
        XmlStream &cout)
{
    {
        const auto it = event_.find(var->index_);
        if (it != event_.end() && !it->second.empty())
            // periodic and event not allowed at the same time.
            return {};
    }
    auto process = var->process.lock();
    auto ans     = std::make_shared<PeriodicSubscription>(
            var, subscription, subscriber, process, selector);
    const auto si = periodic_subscriptions_without_group_.add(
            process->pending_callbacks_, notify_pending, ans);
    if (si.needs_server_action) {
        XmlCommand(cout, "xsad")
                .addAttribute("channels", var->index_)
                .addAttribute("coding", "Base64")
                .addAttribute("reduction", si.decimation_)
                .addAttribute("blocksize", si.blocksize_)
                .setId(xsadId);
        xsadQ.push({false, var->index_});
    }
    return ans;
}

void PdCom::impl::MsrProto::ChannelSubscriptionsWithoutGroup::unsubscribeEvent(
        const Variable &var,
        XmlStream &cout)
{
    const bool unsubscribe = event_.at(var.index_).remove_expired();
    if (unsubscribe) {
        XmlCommand(cout, "xsod").addAttribute("channels", var.index_);
    }
}

void PdCom::impl::MsrProto::ChannelSubscriptionsWithoutGroup::
        unsubscribePeriodic(
                PdCom::Transmission /* tm */,
                const Variable &var,
                XmlStream &cout)
{
    const auto si = periodic_subscriptions_without_group_.remove(var);
    if (si.needs_server_action) {
        XmlCommand(cout, "xsod").addAttribute("channels", var.index_);
    }
}

void PdCom::impl::MsrProto::ChannelSubscriptionsWithoutGroup::xsadAck(
        Process *process)
{
    if (xsadQ.empty())
        throw ProtocolError("no pending xsad commands available");
    const auto ids = xsadQ.front();
    xsadQ.pop();
    if (ids.first) {
        const auto map_it = event_.find(ids.second);
        if (map_it == event_.end())
            return;
        map_it->second.broadcastState(
                PdCom::Subscription::State::Active,
                process->pending_callbacks_);
    }
    else {
        periodic_subscriptions_without_group_.subscribeWasConfirmed(
                process->pending_callbacks_, ids.second);
    }
}

void PdCom::impl::MsrProto::ChannelSubscriptionsWithoutGroup::dump(
        std::vector<PdCom::Process::SubscriptionInfo> &ans,
        const std::unordered_map<unsigned, Channel *> &channels) const
{
    ChannelSubscriptions::dump(ans, channels);
    periodic_subscriptions_without_group_.dump(ans, channels);
}

void PdCom::impl::MsrProto::ChannelSubscriptions::dump(
        std::vector<PdCom::Process::SubscriptionInfo> &ans,
        const std::unordered_map<unsigned, Channel *> &channels) const
{
    // channel event
    for (const auto &map : event_) {
        PdCom::Variable v = impl::Variable::toUApi(
                std::static_pointer_cast<const MsrProto::Variable>(
                        channels.at(map.first)->shared_from_this()));
        for (const auto &weak_sub : map.second) {
            if (const auto sub = weak_sub.lock()) {
                ans.emplace_back(sub->This_, &sub->subscriber_, v);
            }
        }
    }
}
