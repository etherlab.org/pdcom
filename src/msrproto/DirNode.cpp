/*****************************************************************************
 *
 * Copyright (C) 2015-2016  Richard Hacker (lerichi at gmx dot net)
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#include "DirNode.h"

#include "../Debug.h"

#include <algorithm>  // std::copy
#include <cstring>    // strchr()
#include <iterator>   // back_inserter

using namespace PdCom::impl::MsrProto;

namespace {
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
class DirectoryPathIterator
{
  public:
    DirectoryPathIterator(const char *path) { pathPtr = path; }

    std::string next()
    {
        const char *p = pathPtr;
        pathPtr       = strchr(pathPtr, dirSeparator);
        return pathPtr ? std::string(p, skip() - p) : p;
    }

    bool hasNext() const { return pathPtr and *pathPtr; }

  private:
    const char *pathPtr;

    static const char dirSeparator;

    const char *skip()
    {
        const char *p = pathPtr;

        while (*pathPtr and *pathPtr == dirSeparator)
            ++pathPtr;

        return p;
    }
};

}  // namespace
const char DirectoryPathIterator::dirSeparator = '/';

///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
DirNode::DirNode(bool isDir)
{
    m_parent  = this;
    directory = isDir;
}

///////////////////////////////////////////////////////////////////////////
std::string DirNode::path() const
{
    return m_parent == this ? std::string() : (m_parent->path() + '/' + m_name);
}

///////////////////////////////////////////////////////////////////////////
void DirNode::insert(std::shared_ptr<DirNode> child, const char *cpath)
{
    DirectoryPathIterator path(cpath);
    std::shared_ptr<DirNode> parent = shared_from_this();

    if (!path.hasNext()) {
        return;
    }

    while (true) {
        std::string name = path.next();

        if (name.empty()) {
            // Find root node
            parent = getRootNode();
            continue;
        }

        NodeVector::iterator it = std::lower_bound(
                parent->children.begin(), parent->children.end(), name,
                LessThan());

        // Insert a new node if it exists
        if (it == parent->children.end() or (*it)->m_name != name)
            it = parent->children.insert(it, 0);
        std::shared_ptr<DirNode> &node = *it;

        if (path.hasNext()) {
            if (!node) {
                node.reset(new DirNode);
                node->m_parent = parent.get();
                node->m_name   = name;
            }
            parent = node;
        }
        else {
            if (node) {
                // Node exists. This can happen, when a deep node has
                // been discovered before the current node. An example is a
                // signal that has parameters as children
                std::swap(node->children, child->children);
                child->directory = true;
                for (auto &cchild : child->children)
                    cchild->m_parent = child.get();

                node.reset();
            }

            child->m_parent = parent.get();
            child->m_name   = name;
            node            = std::move(child);

            return;
        }
    }
}

///////////////////////////////////////////////////////////////////////////
std::string DirNode::name() const
{
    return m_name;
}

///////////////////////////////////////////////////////////////////////////
std::shared_ptr<DirNode> DirNode::find(const std::string &pathStr)
{
    std::shared_ptr<DirNode> node = shared_from_this();
    DirectoryPathIterator path(pathStr.c_str());

    while (path.hasNext()) {
        std::string name = path.next();

        if (name.empty()) {
            // Find root node
            node = getRootNode();
            continue;
        }

        NodeVector::iterator it = std::lower_bound(
                node->children.begin(), node->children.end(), name, LessThan());

        if (it == node->children.end() or name != (*it)->m_name)
            return nullptr;

        node = *it;
    }

    return node;
}

///////////////////////////////////////////////////////////////////////////
void DirNode::getChildren(List &list) const
{
    for (const auto &child : children)
        list.push_back(child);
}

///////////////////////////////////////////////////////////////////////////
void DirNode::getAllChildren(List &list) const
{
    for (const auto &child : children) {
        list.push_back(child);
        child->getAllChildren(list);
    }
}

///////////////////////////////////////////////////////////////////////////
bool DirNode::hasChildren() const
{
    return directory or !children.empty();
}

/////////////////////////////////////////////////////////////////////////////
std::shared_ptr<DirNode> DirNode::getRootNode()
{
    DirNode *n = this;
    while (n->m_parent != n)
        n = n->m_parent;
    return n->shared_from_this();
}
