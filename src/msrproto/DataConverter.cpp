/*****************************************************************************
 *
 * Copyright (C) 2021 Richard Hacker (lerichi at gmx dot net),
 *                    Florian Pose (fp at igh dot de),
 *                    Bjarne von Horn (vh at igh dot de).
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#include "DataConverter.h"

#include "ProtocolHandler.h"
#include "Variable.h"

#include <cmath>
#include <cstdint>
#include <cstring>
#include <pdcom5/Exception.h>
#include <pdcom5/Process.h>
#include <pdcom5/SizeTypeInfo.h>
#include <pdcom5/details.h>
#include <sstream>

using PdCom::impl::MsrProto::DataDecoder;
using PdCom::impl::MsrProto::DataEncoder;
using PdCom::impl::MsrProto::Variable;

void DataDecoder::readFromHexDec(
        const char *s,
        const Variable &var,
        unsigned int blocksize)
{
    resize(0);
    resize(blocksize * var.totalSizeInBytes());
    uint8_t *dst = reinterpret_cast<uint8_t *>(data());
    size_t c1, c2;
    const char *c                      = s;
    static const uint8_t hexDecTable[] = {0, 1,  2,  3,  4,  5,  6, 7,
                                          8, 9,  0,  0,  0,  0,  0, 0,
                                          0, 10, 11, 12, 13, 14, 15};

    if (!s or strlen(s) < var.totalSizeInBytes() * 2)
        throw ProtocolError("invalid hexvalue attribute");

    size_t bytes = var.totalSizeInBytes();
    do {
        c1 = *c++ - '0';
        c2 = *c++ - '0';
        if (c1 >= sizeof(hexDecTable) or c2 >= sizeof(hexDecTable))
            throw ProtocolError("invalid hexvalue attribute");
        *dst++ = (hexDecTable[c1] << 4) + hexDecTable[c2];
    } while (--bytes);
}

//////////////////////////////////////////////////////////////////////
void DataDecoder::readFromString(
        const char *s,
        const Variable &var,
        unsigned int blocksize)
{
    if (!s)
        throw ProtocolError("invalid value attribute");

    const auto nelem = blocksize * var.numberOfElements();
    resize(0);
    resize(nelem * var.type_info->element_size);

    std::istringstream is(s);
    double src;


    is.imbue(std::locale::classic());

    for (size_t i = 0; i < nelem && is; ++i) {
        if (i)
            is.ignore(1);

        // istream does not like nan
        if (!strncmp("-nan", s + is.tellg(), 4)) {
            is.ignore(4);
            src = -NAN;
        }
        else {
            is >> src;
        }

        PdCom::details::copyData(
                data() + i * var.type_info->element_size, var.type_info->type,
                &src, PdCom::TypeInfo::double_T, 1);
    }

    if (!is)
        throw ProtocolError("invalid value attribute");
}


void DataDecoder::readFromBase64(
        const char *s,
        const size_t input_len,
        const Variable &var,
        unsigned int blocksize)
{
    resize(0);
    resize(blocksize * var.totalSizeInBytes()
           + 2 /* buffer must be longer for padding */);
    const size_t expected_size = blocksize * var.totalSizeInBytes();
    if (!PdCom::impl::MsrProto::readFromBase64(
                data(), s, input_len, expected_size))
        throw ProtocolError("Invalid base64 value");
}

bool PdCom::impl::MsrProto::readFromBase64(
        char *dst,
        const char *base64,
        const size_t len,
        size_t buflen)
{
    uint8_t c1, c2;
    static const uint8_t base64Value[256] = {
            0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
            0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
            0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  62, 0,  62, 0,  63,
            52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 0,  0,  0,  0,  0,  0,
            0,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9,  10, 11, 12, 13, 14,
            15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 0,  0,  0,  0,  63,
            0,  26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
            41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 0,  0,  0,  0,  0,
    };
    if (!base64 or !*base64)
        return false;

    if (len % 4 or len < 4)
        return false;

    if (len / 4 != (buflen + 2) / 3)
        return false;

    for (const char *const end = base64 + len; base64 != end;) {
        c1     = base64Value[uint8_t(*base64++)];
        c2     = base64Value[uint8_t(*base64++)];
        *dst++ = (c1 << 2) | (c2 >> 4);

        c1     = base64Value[uint8_t(*base64++)];
        *dst++ = (c2 << 4) | (c1 >> 2);

        c2     = base64Value[uint8_t(*base64++)];
        *dst++ = (c1 << 6) | c2;
    }

    return true;
}

void DataEncoder::toHex(
        PdCom::TypeInfo const &dst_type,
        const void *src,
        PdCom::TypeInfo::DataType src_type,
        size_t n,
        PdCom::impl::MsrProto::XmlStream &os)
{
    converted_value_.resize(0);
    converted_value_.resize(n * dst_type.element_size);
    details::copyData(converted_value_.data(), dst_type.type, src, src_type, n);

    static constexpr char hexTable[] = {'0', '1', '2', '3', '4', '5', '6', '7',
                                        '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

    for (const auto c : converted_value_)
        os << hexTable[c >> 4] << hexTable[c & 0x0F];

    converted_value_.resize(0);
}
