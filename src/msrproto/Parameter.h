/*****************************************************************************
 *
 * Copyright (C) 2015-2016  Richard Hacker (lerichi at gmx dot net)
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#ifndef MSR_PARAMETER_H
#define MSR_PARAMETER_H

#include "Variable.h"

#include <unordered_set>

namespace PdCom { namespace impl { namespace MsrProto {
class ProtocolHandler;
struct Request;

class Parameter : public Variable
{
  public:
    Parameter(
            SizeInfo size_info,
            const TypeInfo *type_info,
            std::weak_ptr<impl::Process> process,
            unsigned int index,
            unsigned int flags,
            const char *alias,
            bool isDir) :
        Variable(
                std::move(size_info),
                type_info,
                process,
                index,
                0,
                true,
                0.0,
                alias,
                isDir,
                true),
        flags(flags)
    {}

    const unsigned int flags;


    // Reimplemented from PdCom::impl::Variable
    PdCom::Variable::SetValueFuture
    setValue(const void *, TypeInfo::DataType t, size_t idx, size_t n)
            const override;
};

}}}  // namespace PdCom::impl::MsrProto

#endif  // MSR_PARAMETER_H
