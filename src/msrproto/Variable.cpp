/*****************************************************************************
 *
 * Copyright (C) 2021 Richard Hacker (lerichi at gmx dot net),
 *                    Florian Pose (fp at igh dot de),
 *                    Bjarne von Horn (vh at igh dot de).
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#include "Variable.h"

#include "Channel.h"
#include "Parameter.h"
#include "ProtocolHandler.h"

#include <cstring>
#include <pdcom5/Exception.h>
#include <sstream>

using PdCom::impl::MsrProto::Channel;
using PdCom::impl::MsrProto::Parameter;
using PdCom::impl::MsrProto::Variable;


Variable::Variable(
        PdCom::SizeInfo size_info,
        const PdCom::TypeInfo *type_info,
        std::weak_ptr<PdCom::impl::Process> process,
        unsigned int index,
        unsigned taskId,
        bool writeable,
        double sampleTime,
        const char *alias,
        bool isDir,
        bool is_parameter) :
    PdCom::impl::Variable(size_info, type_info, process),
    DirNode(isDir),
    is_parameter_(is_parameter),
    index_(index),
    sample_time_(sampleTime),
    task_id_(taskId),
    writeable_(writeable),
    alias_(alias)
{}

PdCom::Variable::SetValueFuture Parameter::setValue(
        const void *src,
        PdCom::TypeInfo::DataType t,
        size_t idx,
        size_t n) const
{
    if (const auto p = process.lock()) {
        auto &h = p->protocolHandler<ProtocolHandler>();
        return h.writeParameter(*this, src, t, idx, n);
    }
    throw PdCom::ProcessGoneAway();
}


PdCom::Variable::SetValueFuture Channel::setValue(
        const void * /* data */,
        PdCom::TypeInfo::DataType /* t */,
        size_t /* idx */,
        size_t /* n */) const
{
    throw PdCom::InvalidArgument("Signals are read-only");
}
