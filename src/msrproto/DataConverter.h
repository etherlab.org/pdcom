/*****************************************************************************
 *
 * Copyright (C) 2021 Richard Hacker (lerichi at gmx dot net),
 *                    Florian Pose (fp at igh dot de),
 *                    Bjarne von Horn (vh at igh dot de).
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#ifndef PDCOM5_MSRPROTO_DATADECODER_H
#define PDCOM5_MSRPROTO_DATADECODER_H

#include <cstddef>
#include <cstring>
#include <memory>
#include <ostream>
#include <pdcom5/Exception.h>
#include <pdcom5/SizeTypeInfo.h>
#include <unordered_map>
#include <vector>

namespace PdCom { namespace impl { namespace MsrProto {

class Variable;
class XmlStream;

struct DataDecoder : std::vector<char>
{
    void readFromHexDec(
            const char *s,
            const Variable &var,
            unsigned int blocksize = 1);
    void readFromString(
            const char *s,
            const Variable &var,
            unsigned int blocksize = 1);
    void readFromBase64(
            const char *s,
            const size_t base64length_,
            const Variable &var,
            unsigned int blocksize = 1);
};

class DataEncoder
{
    std::vector<unsigned char> converted_value_;

  public:
    void
    toHex(TypeInfo const &dst_type,
          const void *src,
          TypeInfo::DataType src_type,
          size_t n,
          XmlStream &os);
};

struct Base64Info
{
    explicit Base64Info(const char *data)
    {
        if (!data || !*data)
            throw PdCom::ProtocolError("Tag does not contain data");
        base64Length_ = ::strlen(data);

        if (base64Length_ < 4 || (base64Length_ % 4) != 0)
            throw PdCom::ProtocolError("Invalid base64 data tag");
        encodedDataLength_ = base64Length_ * 3 / 4;

        if (data[base64Length_ - 1] == '=')
            --encodedDataLength_;
        if (data[base64Length_ - 2] == '=')
            --encodedDataLength_;
    }

    size_t base64Length_, encodedDataLength_;
};

// len(dst) must be
bool readFromBase64(
        char *dst,
        const char *src,
        const size_t base64length_,
        size_t buflen);

class VariableCache
{
    using InternalType = std::unordered_map<unsigned int, DataDecoder>;
    InternalType cache_;
    bool locked_ = false;

    struct Unlocker
    {
        void operator()(VariableCache *vc) const
        {
            if (vc) {
                for (auto &kv : vc->cache_)
                    kv.second.resize(0);
                vc->locked_ = false;
            }
        }
    };

  public:
    struct Lock
    {
        std::unique_ptr<VariableCache, Unlocker> cache_ptr_;

        Lock(VariableCache *vc = nullptr) : cache_ptr_(vc) {}

        operator bool() const { return static_cast<bool>(cache_ptr_); }

        DataDecoder &operator[](unsigned int idx)
        {
            return cache_ptr_->cache_[idx];
        }
        const DataDecoder *filled_cache(unsigned int idx) const
        {
            if (cache_ptr_) {
                const auto it = cache_ptr_->cache_.find(idx);
                if (it != cache_ptr_->cache_.end() and it->second.size() > 0)
                    return &it->second;
            }
            return nullptr;
        }
    };

    Lock lock()
    {
        if (locked_)
            throw InternalError("Variable Cache is in use!");
        locked_ = true;
        return Lock {this};
    }
};

}}}  // namespace PdCom::impl::MsrProto


#endif  // PDCOM5_MSRPROTO_DATADECODER_H
