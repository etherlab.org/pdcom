/*****************************************************************************
 *
 * Copyright (C) 2015-2016  Richard Hacker (lerichi at gmx dot net)
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#ifndef PD_MSRPROTOCOLHANDLER_H
#define PD_MSRPROTOCOLHANDLER_H

#include "../Future.h"
#include "../Process.h"
#include "../ProtocolHandler.h"
#include "../StreambufLayer.h"
#include "../Subscription.h"
#include "DataConverter.h"
#include "PeriodicSubscriptions.h"
#include "Variable.h"
#include "expat_wrapper.h"

#include <list>
#include <memory>
#include <ostream>
#include <pdcom5/MessageManagerBase.h>
#include <queue>
#include <stddef.h>
#include <string>
#include <unordered_map>

namespace PdCom { namespace impl { namespace MsrProto {
class Channel;
class Parameter;
class PollSubscription;
class PeriodicSubscription;
class PendingSubscription;
class EventSubscription;
class Variable;

class XmlStream
{
    std::ostream os;

  public:
    XmlStream(PdCom::impl::Process *process, std::streambuf *buf);
    void lock();
    void unlock();
    PdCom::impl::Process *process;

    template <typename T>
    XmlStream &operator<<(T &&t)
    {
        try {
            os << std::forward<T>(t);
        }
        catch (std::ios_base::failure const &ex) {
            throw PdCom::WriteFailure(ex.what());
        }
        return *this;
    }

    XmlStream &flush()
    {
        os.flush();
        return *this;
    }
};

class ChannelSubscriptions
{
  public:
    using GroupId         = unsigned int;
    using ChannelId       = unsigned int;
    using SubscriptionMap = std::unordered_map<ChannelId, SubscriptionSet<>>;

    static constexpr const char *xsadId = "xsadQ";
    static constexpr const char *xsodId = "xsodQ";

    virtual ~ChannelSubscriptions() = default;


    virtual std::shared_ptr<PdCom::impl::Subscription> subscribeEvent(
            PdCom::Subscription *subscription,
            std::shared_ptr<const Variable> _var,
            PdCom::Subscriber &subscriber,
            const PdCom::Selector &selector,
            bool notify_pending,
            XmlStream &cout) = 0;
    virtual std::shared_ptr<PdCom::impl::Subscription> subscribePeriodic(
            PdCom::Subscription *subscription,
            std::shared_ptr<const Variable> _var,
            PdCom::Subscriber &subscriber,
            const PdCom::Selector &selector,
            bool notify_pending,
            XmlStream &cout) = 0;

    virtual void unsubscribeEvent(const Variable &var, XmlStream &cout) = 0;
    virtual void unsubscribePeriodic(
            PdCom::Transmission tm,
            const Variable &var,
            XmlStream &cout) = 0;

    virtual void xsadAck(Process *) {}
    virtual void xsodAck() {}


    virtual PeriodicSubscriptionsBase::DataReceiveHandle startNewDataRecieving(
            GroupId index,
            VariableCache::Lock lock,
            std::chrono::nanoseconds ts) = 0;

    virtual void
    dump(std::vector<PdCom::Process::SubscriptionInfo> &ans,
         const std::unordered_map<unsigned, Channel *> &channels) const;


    const SubscriptionMap &getEventMap() const { return event_; }

  protected:
    SubscriptionMap event_;
};

class ChannelSubscriptionsWithGroup final : public ChannelSubscriptions
{
  public:
    std::shared_ptr<PdCom::impl::Subscription> subscribeEvent(
            PdCom::Subscription *subscription,
            std::shared_ptr<const Variable> _var,
            PdCom::Subscriber &subscriber,
            const PdCom::Selector &selector,
            bool notify_pending,
            XmlStream &cout) override;
    std::shared_ptr<PdCom::impl::Subscription> subscribePeriodic(
            PdCom::Subscription *subscription,
            std::shared_ptr<const Variable> _var,
            PdCom::Subscriber &subscriber,
            const PdCom::Selector &selector,
            bool notify_pending,
            XmlStream &cout) override;

    void unsubscribeEvent(const Variable &var, XmlStream &cout) override;
    void unsubscribePeriodic(
            PdCom::Transmission tm,
            const Variable &var,
            XmlStream &cout) override;

    void xsadAck(Process *process) override;
    void xsodAck() override;


    PeriodicSubscriptionsBase::DataReceiveHandle startNewDataRecieving(
            GroupId index,
            VariableCache::Lock lock,
            std::chrono::nanoseconds ts) override
    {
        return periodic_subscriptions_with_group_.startNewDataRecieving(
                index, std::move(lock), ts);
    }

    void
    dump(std::vector<PdCom::Process::SubscriptionInfo> &ans,
         const std::unordered_map<unsigned, Channel *> &channels)
            const override;

  private:
    struct xsad_details
    {
        unsigned int group_id, channel_id;
    };
    std::queue<xsad_details> xsadQ;
    std::queue<GroupId> xsodQ;

    PeriodicSubscriptionsWithGroup periodic_subscriptions_with_group_;
};


class ChannelSubscriptionsWithoutGroup final : public ChannelSubscriptions
{
  public:
    std::shared_ptr<PdCom::impl::Subscription> subscribeEvent(
            PdCom::Subscription *subscription,
            std::shared_ptr<const Variable> _var,
            PdCom::Subscriber &subscriber,
            const PdCom::Selector &selector,
            bool notify_pending,
            XmlStream &cout) override;
    std::shared_ptr<PdCom::impl::Subscription> subscribePeriodic(
            PdCom::Subscription *subscription,
            std::shared_ptr<const Variable> _var,
            PdCom::Subscriber &subscriber,
            const PdCom::Selector &selector,
            bool notify_pending,
            XmlStream &cout) override;

    void unsubscribeEvent(const Variable &var, XmlStream &cout) override;
    void unsubscribePeriodic(
            PdCom::Transmission tm,
            const Variable &var,
            XmlStream &cout) override;

    void xsadAck(Process *process) override;

    PeriodicSubscriptionsBase::DataReceiveHandle startNewDataRecieving(
            GroupId /* index */,
            VariableCache::Lock lock,
            std::chrono::nanoseconds ts) override
    {
        return periodic_subscriptions_without_group_.startNewDataRecieving(
                std::move(lock), ts);
    }

    void
    dump(std::vector<PdCom::Process::SubscriptionInfo> &ans,
         const std::unordered_map<unsigned, Channel *> &channels)
            const override;

  private:
    std::queue<std::pair<bool /*event*/, GroupId>> xsadQ;
    PeriodicSubscriptionWithoutGroup periodic_subscriptions_without_group_;
};


class ProtocolHandler :
    public PdCom::impl::ProtocolHandler,
    public PdCom::impl::StreambufLayer,
    protected ExpatWrapper<ProtocolHandler, true>
{
  public:
    ProtocolHandler(PdCom::impl::Process *process, IOLayer *io);
    ~ProtocolHandler();

    using ExpatWrapper<ProtocolHandler, true>::parse;

    static constexpr int ReadBufferSize = 8192;


    void getMessage(uint32_t seqNo) override;
    void getActiveMessages() override;

    std::shared_ptr<impl::Subscription> subscribe(
            PdCom::Subscription *subscription,
            const std::string &path,
            PdCom::Subscriber &subscriber,
            const PdCom::Selector &selector) override;
    std::shared_ptr<impl::Subscription> subscribe(
            PdCom::Subscription *subscription,
            std::shared_ptr<const impl::Variable> var,
            PdCom::Subscriber &subscriber,
            const PdCom::Selector &selector,
            bool notify_pending);
    std::shared_ptr<impl::Subscription> subscribe(
            PdCom::Subscription *subscription,
            std::shared_ptr<const impl::Variable> var,
            PdCom::Subscriber &subscriber,
            const PdCom::Selector &selector) override
    {
        return subscribe(subscription, var, subscriber, selector, true);
    }
    void unsubscribe(EventSubscription &) noexcept;
    void unsubscribe(PeriodicSubscription &) noexcept;
    void cancelSubscriptions() override;

    void pollChannel(PollSubscription &sub);
    void pollParameter(PollSubscription &sub);
    void pollVariable(const impl::Variable &var, VariablePollPromise &&promise)
            override;
    PdCom::Variable::SetValueFuture writeParameter(
            const Parameter &p,
            const void *src,
            TypeInfo::DataType src_type,
            size_t idx,
            size_t n);

    std::vector<PdCom::Process::SubscriptionInfo>
    getActiveSubscriptions() const override;

  private:
    bool polite                       = false;
    bool protocolError                = false;
    std::chrono::nanoseconds dataTime = {};

    std::string m_name;
    std::string m_version;

    XmlStream cout;

    const std::shared_ptr<DirNode> root;

    // Structures required for list command
    std::queue<std::string> listQ;
    bool processListRequest();
    enum { Uncached, InProgress, Cached } fullListing = Uncached;

    std::queue<std::string> findQ;
    bool processFindRequest();

    // Structures required for login (return true on successful login)
    bool processLogin(const char **atts);

    struct Feature
    {
        bool pushparameters, binparameters, eventchannels, statistics, pmtime,
                group, history, aic, messages, quiet, list, exclusive, polite,
                xsap, login;
        unsigned eventid;
        void operator=(const char *list);
    };
    Feature feature = {};


    using SubscriptionMap =
            std::unordered_map<unsigned int /* id */, SubscriptionSet<>>;

    struct SubscriptionPendingPoll
    {
        const unsigned int id_;
        SubscriptionSet<> subscriptions_;
    };
    struct SubscriptionPendingPollList : std::list<SubscriptionPendingPoll>
    {
        bool
        append(unsigned int id_, const std::shared_ptr<PollSubscription> &sub);
    } subscription_pending_channel_polls_,
            subscription_pending_parameter_polls_;

    struct VariablePendingPoll
    {
        std::vector<VariablePollPromise> promises_;
        const unsigned int id_;
        const bool is_parameter_;

        VariablePendingPoll(
                VariablePollPromise p,
                unsigned int id,
                bool is_parameter) :
            promises_(), id_(id), is_parameter_(is_parameter)
        {
            promises_.push_back(std::move(p));
        }
    };
    class VariablePendingPollList
    {
        std::list<VariablePendingPoll> polls_;

      public:
        VariablePendingPollList() = default;
        bool append(const Variable &var, VariablePollPromise promise);

        void processResult(
                Variable const &var,
                const char *data,
                std::chrono::nanoseconds ts);

        ~VariablePendingPollList();
    } variable_pending_polls_;

    std::unique_ptr<ChannelSubscriptions> channel_subscriptions_;
    SubscriptionMap parameter_event_;


    VariableCache variable_cache_;
    impl::Subscription::SubscriberNotifier subscriber_notifier_;

    DataEncoder encoder_;

    PeriodicSubscriptionsBase::DataReceiveHandle periodic_receive_handle_;

    std::unordered_map<
            PdCom::Transmission,
            std::set<
                    std::shared_ptr<const Variable>,
                    std::owner_less<std::shared_ptr<const Variable>>>>
            cancelations_;

    struct PendingRequest
    {
        const std::string path_;
        SubscriptionSet<PendingSubscription> subscriptions_;
    };
    std::list<PendingRequest> pending_queue_;
    std::queue<unsigned int /* parameter id */> xsapQ;

    using ChannelMap = std::unordered_map<unsigned, Channel *>;
    ChannelMap channel;
    using ParameterMap = std::unordered_map<unsigned, Parameter *>;
    ParameterMap parameter;

    class PendingSetValues
    {
        std::queue<PdCom::Promise<PdCom::Exception const &>> queue_;

      public:
        PendingSetValues() = default;

        PdCom::Variable::SetValueFuture push();
        void pop();

        ~PendingSetValues();

    } pending_set_values_;


    enum {
        StartUp,
        Idle,
        GetListing,
        WaitForConnected,
        WaitForLogin,
        GetVariableFields,
        ReadData,
        ReadEvent,
        ReadDataOrEvent,
        GetActiveMessages,
        ReadClientStatistics,
    } state = StartUp;


    std::queue<uint32_t> seqNoQ;
    uint32_t messageSeqNo;
    std::vector<PdCom::Message> messageList;
    std::vector<PdCom::ClientStatistics> client_statistics;

    void setPolite(bool state);

    /** Read a variable tag */
    static const TypeInfo *getDataType(const char **atts, SizeInfo &size_info);
    Parameter *getParameter(const char **atts);
    Channel *getChannel(const char **atts);

    // callbacks for xml parser
    friend class ExpatWrapper<ProtocolHandler, true>;
    void startElement(const char *name, const char **atts);
    void endElement(const char *name);
    void xmlError(const char *);

    void xsapAck();
    void pendingAck(Variable *);
    void pollParameter(size_t index, const char *id);
    void pollChannel(size_t index, const char *id);

    // Reimplemented from PdCom::ProtocolHandler
    bool asyncData() override;
    bool find(const std::string &path) override;
    bool list(const std::string &directory) override;
    void getClientStatistics() override;
    void logout() override;
    bool login(const char *mech, const char *clientData) override;

    void ping() override;
    std::string name() const override;
    std::string version() const override;

    void
    broadcast(const std::string &message, const std::string &attr) override;
    void processBroadcast(const char **atts);

    void readEventData(const char *name, const char **atts);
    void readPeriodicData(const char *name, const char **atts);
};


}}}  // namespace PdCom::impl::MsrProto

#endif  // PD_MSRPROTOCOLHANDLER_H
