/*****************************************************************************
 *
 * Copyright (C) 2021 Richard Hacker (lerichi at gmx dot net),
 *                    Florian Pose (fp at igh dot de),
 *                    Bjarne von Horn (vh at igh dot de).
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#include "Subscription.h"

#include "../Process.h"
#include "ProtocolHandler.h"
#include "Variable.h"

#include <cstring>
#include <pdcom5/Exception.h>
#include <pdcom5/Process.h>
#include <pdcom5/details.h>

using PdCom::impl::MsrProto::EventSubscription;
using PdCom::impl::MsrProto::InvalidSubscription;
using PdCom::impl::MsrProto::PendingSubscription;
using PdCom::impl::MsrProto::PeriodicSubscription;
using PdCom::impl::MsrProto::PollSubscription;
using PdCom::impl::MsrProto::ProtocolHandler;

template <typename T>
void InvalidSubscription::unsubscribe(T &This)
{
    if (const auto p = This.process_.lock()) {
        const auto ph = p->template castProtocolHandler<ProtocolHandler>();
        if (ph)
            ph->unsubscribe(This);
    }
}

void InvalidSubscription::poll()
{
    throw PdCom::InvalidSubscription();
}

const void *InvalidSubscription::getData() const
{
    throw PdCom::InvalidSubscription();
}

EventSubscription::~EventSubscription()
{
    unsubscribe(*this);
}

PeriodicSubscription::~PeriodicSubscription()
{
    unsubscribe(*this);
}

PollSubscription::PollSubscription(
        std::shared_ptr<const PdCom::impl::MsrProto::Variable> variable,
        PdCom::Subscription *This,
        PdCom::Subscriber &subscriber,
        std::weak_ptr<PdCom::impl::Process> process,
        const PdCom::Selector &selector) :
    InvalidSubscription(
            This,
            subscriber,
            process,
            std::static_pointer_cast<const PdCom::impl::Variable>(variable)),
    is_parameter_(variable->is_parameter_),
    copy_function_(selector.impl_->getCopyFunction(*variable)),
    data_(selector.impl_->getRequiredSize(*variable) + 2)
{}

void PollSubscription::poll()
{
    if (const auto p = process_.lock()) {
        auto &h = p->template protocolHandler<ProtocolHandler>();
        if (is_parameter_)
            h.pollParameter(*this);
        else
            h.pollChannel(*this);
    }
    else
        throw ProcessGoneAway();
}
