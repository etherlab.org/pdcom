/*****************************************************************************
 *
 * Copyright (C) 2021 Bjarne von Horn (vh at igh dot de)
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#include "PeriodicSubscriptions.h"

#include "Channel.h"
#include "Subscription.h"

#include <cassert>
#include <cstring>
#include <pdcom5/Exception.h>
#include <sstream>

using PdCom::impl::MsrProto::DataDecoder;
using PdCom::impl::MsrProto::PeriodicSubscription;
using PdCom::impl::MsrProto::PeriodicSubscriptionsBase;
using PdCom::impl::MsrProto::PeriodicSubscriptionsWithGroup;
using PdCom::impl::MsrProto::PeriodicSubscriptionWithoutGroup;

using PdCom::impl::MsrProto::Variable;


PeriodicSubscriptionsWithGroup::transaction PeriodicSubscriptionsWithGroup::add(
        PdCom::impl::Process::PendingCallbackQueue &queue,
        bool notify_pending,
        const std::shared_ptr<PeriodicSubscription> &s)
{
    const auto var_ptr = s->variable_.lock();
    if (!var_ptr)
        throw EmptyVariable();
    const auto &var     = static_cast<Channel const &>(*var_ptr);
    const auto group_id = map_.find(s->subscriber_.getTransmission());
    auto map_it         = map_.at(group_id).find(var.index_);
    if (map_it == map_.at(group_id).end()) {
        // not yet subscribed
        unsigned decimation = s->subscriber_.getTransmission().getInterval()
                        / var.sample_time_.count()
                + 0.5;
        decimation += s->subscriber_.getTransmission().getInterval() > 0.0
                and !decimation;
        unsigned blocksize = 0;

        if (decimation > 0) {
            blocksize =
                    1.0 / var.sample_time_.count() / decimation / 25.0 + 0.5;
            blocksize = std::min(var.bufsize / decimation, blocksize);
            blocksize = std::min(blocksize, 1000U);
        }

        if (!blocksize)
            blocksize = 1;
        time_vector_.resize(
                std::max<size_t>(time_vector_.size(), blocksize + 2));

        map_.at(group_id).insert({var.index_, {blocksize, decimation, {{s}}}});

        if (notify_pending)
            queue.push_back({s, PdCom::Subscription::State::Pending});
        return {true, group_id, decimation, blocksize};
    }
    else {
        if (notify_pending
            or map_it->second.subscriptions_.currentState
                    != PdCom::Subscription::State::Pending)
            queue.push_back({s, map_it->second.subscriptions_.currentState});
        map_it->second.subscriptions_.insert(s);
        return {false, group_id, map_it->second.decimation_,
                map_it->second.blocksize_};
    }
}

PeriodicSubscriptionsWithGroup::transaction
PeriodicSubscriptionsWithGroup::remove(
        const Variable &var,
        PdCom::Transmission tm)
{
    // remove'ing is okay with locked ps,
    // because the subscription map is _not_ deleted
    // and the forward map _not_ altered. So pointers stay valid.
    const auto group_id = map_.erase(tm, var.index_);
    return {group_id != 0, group_id, 0, 0};
}

PdCom::impl::MsrProto::PeriodicSubscriptionsBase::DataReceiveHandle::
        DataReceiveHandle(
                PeriodicSubscriptionsBase &ps,
                ChannelSubscriptionMap &subscription_map,
                VariableCache::Lock variable_cache_lock,
                std::chrono::nanoseconds ts,
                bool consistent_blocksize) :
    ps_(&ps),
    variable_cache_lock_(std::move(variable_cache_lock)),
    subscription_map_(&subscription_map),
    ts_(ts),
    consistent_blocksize_(consistent_blocksize)
{
    if (ps.locked_)
        throw PdCom::InternalError(
                "Receiving periodic data already in progress");
    ps.locked_ = true;
}

void PeriodicSubscriptionsBase::DataReceiveHandle::readData(
        const PdCom::impl::MsrProto::Channel &c,
        const char *data)
{
    if (!ps_) {
        throw PdCom::InternalError("Operating on invalid DataReceiveHandle");
    }
    const Base64Info bi(data);

    const auto sub_info_it = subscription_map_->find(c.index_);
    if (sub_info_it == subscription_map_->end())
        return;
    if (sub_info_it->second.subscriptions_.currentState
        != PdCom::Subscription::State::Active)
        return;
    if (!consistent_blocksize_) {
        current_blocksize_ = std::min<int>(
                sub_info_it->second.blocksize_,
                bi.encodedDataLength_ / c.type_info->element_size);
        max_read_blocks_ = std::max(max_read_blocks_, current_blocksize_);
    }
    else if (current_blocksize_ == -1)
        current_blocksize_ = sub_info_it->second.blocksize_;
    else if (
            current_blocksize_
            != static_cast<int>(sub_info_it->second.blocksize_))
        throw ProtocolError("Blocksize mismatch");
    variable_cache_lock_[c.index_].readFromBase64(
            data, bi.base64Length_, c, current_blocksize_);
}


void PeriodicSubscriptionsBase::DataReceiveHandle::readTimestamps(
        const char *data)
{
    static_assert(
            sizeof(decltype(time_vector_)::value_type) == sizeof(int64_t),
            "time_vector_ is not size compatible to time tag");
    if (!ps_) {
        throw PdCom::InternalError("Operating on invalid DataReceiveHandle");
    }
    if (!data || !*data)
        throw PdCom::ProtocolError("Tag does not contain data");
    const auto b64len = ::strlen(data);
    if (current_blocksize_ == -1) {
        current_blocksize_ = b64len * 3 / 4 / sizeof(int64_t);
        max_read_blocks_   = current_blocksize_;
    }
    consistent_blocksize_ = true;

    ps_->time_vector_.resize(
            std::max<size_t>(current_blocksize_ + 1, ps_->time_vector_.size()));
    if (!readFromBase64(
                reinterpret_cast<char *>(ps_->time_vector_.data()), data,
                b64len, sizeof(int64_t) * current_blocksize_)) {
        throw ProtocolError("Invalid <time> child of <data>");
    }
    timestamps_read_ = true;
}

void PeriodicSubscriptionsWithGroup::dump(
        std::vector<PdCom::Process::SubscriptionInfo> &ans,
        const std::unordered_map<unsigned, Channel *> &channels) const
{
    map_.dump(ans, channels);
}

void PeriodicSubscriptionsBase::DataReceiveHandle::endNewDataRecieving()
{
    if (!ps_) {
        throw PdCom::InternalError("Operating on invalid DataReceiveHandle");
    }
    // make sure to release the variable cache lock
    auto cache = std::move(variable_cache_lock_);
    // as this is the final call on receiving a new dataset,
    // make sure to erase the ps pointer to mark us invalid afterwards
    const struct PsDeleter
    {
        std::unique_ptr<PeriodicSubscriptionsBase, Unlocker> &ps_;
        ~PsDeleter() { ps_.reset(); }
        PsDeleter(std::unique_ptr<PeriodicSubscriptionsBase, Unlocker> &ps) :
            ps_(ps)
        {}
    } psdeleter_ {ps_};
    // subscriptions can dissappear during <data> tag parsing, so don't
    // assume that subscriptions_ is not empty
    if (!cache or current_blocksize_ == -1)
        return;

    for (int block = 0; block < max_read_blocks_; ++block) {
        for (const auto &sub_map : *subscription_map_) {
            const DataDecoder *var = cache.filled_cache(sub_map.first);
            // no data recieved for given variable
            if (!var
                || sub_map.second.subscriptions_.currentState
                        != PdCom::Subscription::State::Active)
                continue;
            sendDataToSubscribers(*var, sub_map.second, block);
        }
        ps_->subscriber_notifier_.notify();
        ps_->subscriber_notifier_.clear();
    }
    current_blocksize_ = -1;
}

void PeriodicSubscriptionsBase::DataReceiveHandle::sendDataToSubscribers(
        const DataDecoder &var,
        const ChannelSubscripion &sub_map,
        const int block)
{
    bool size_check_done = false;
    for (const auto &weak_sub : sub_map.subscriptions_) {
        if (const auto sub = weak_sub.lock()) {
            const auto var_ptr = sub->variable_.lock();
            if (!var_ptr)
                throw EmptyVariable();
            if (!size_check_done) {
                if (consistent_blocksize_
                    && var.size() < sub_map.blocksize_
                                    * var_ptr->totalSizeInBytes()) {
                    std::ostringstream os;
                    os << "size mismatch in variable cache, "
                          "expected "
                       << sub_map.blocksize_ * var_ptr->totalSizeInBytes()
                       << ", got " << var.size();
                    throw ProtocolError(os.str());
                }
                else if (var.size() < (block + 1) * var_ptr->totalSizeInBytes())
                    return;
                size_check_done = true;
            }
            sub->readData(var.data() + block * var_ptr->totalSizeInBytes());
            std::chrono::nanoseconds ts;
            if (timestamps_read_)
                ts = ps_->time_vector_.at(block);
            else
                ts = ts_
                        + (block - max_read_blocks_ + 1)
                                * std::chrono::duration_cast<
                                        std::chrono::nanoseconds>(
                                        var_ptr->getSampleTime())
                                * sub_map.decimation_;
            ps_->subscriber_notifier_.insert(sub->subscriber_, ts);
        }
    }
}

PeriodicSubscriptionsBase::GroupId
PeriodicSubscriptionsWithGroup::GroupMap::find(PdCom::Transmission t)
{
    // does a group already exists for given transmission?
    const auto it = backward_.find(t);
    if (it != backward_.end()) {
        if (forward_.at(it->second - 1).state
            == ChannelSubscriptionMapImpl::Taken)
            return it->second;
    }
    // find a free one;
    for (unsigned int i = 0; i < forward_.size(); ++i) {
        if (forward_[i].state == ChannelSubscriptionMapImpl::Empty) {
            const auto group_id = i + 1;
            backward_[t]        = group_id;
            forward_[i].state   = ChannelSubscriptionMapImpl::Taken;
            return group_id;
        }
    }
    // allocate one at the back;
    forward_.emplace_back();
    forward_.back().state = ChannelSubscriptionMapImpl::Taken;
    const auto group_id = backward_[t] = forward_.size();
    return group_id;
}

PeriodicSubscriptionsBase::GroupId
PeriodicSubscriptionsWithGroup::GroupMap::erase(
        PdCom::Transmission t,
        PeriodicSubscriptionsBase::ChannelId channel_idx)
{
    const auto backward_it = backward_.find(t);
    if (backward_it == backward_.end()) {
        assert(backward_it != backward_.end()
               and "Inconsistent state, backward_ and forward_ out of sync");
        return 0;
    }
    auto &channel_map = at(backward_it->second);
    const auto map_it = channel_map.find(channel_idx);
    // already cleaned? if so, nothing to do.
    if (map_it == channel_map.end())
        return 0;
    // clean subscription map
    const bool no_channel_subs_left =
            map_it->second.subscriptions_.remove_expired();
    // if channels left, also nothing to do.
    if (!no_channel_subs_left)
        return 0;
    // no subscriptions for this channel is left, we have to remember the
    // group id
    const auto ans = backward_it->second;
    // now check whether any subscriptions are left for this transmission
    if (channel_map.guaranteed_empty()) {
        // group id no longer in use, free up channel map
        channel_map.state = ChannelSubscriptionMapImpl::AboutToBeDeleted;
        backward_.erase(backward_it);
    }
    return ans;
}

void PeriodicSubscriptionsWithGroup::subscribeWasConfirmed(
        PdCom::impl::Process::PendingCallbackQueue &q,
        PeriodicSubscriptionsWithGroup::GroupId group_id,
        PeriodicSubscriptionsWithGroup::ChannelId channel_id)
{
    auto &sub_map = map_.at(group_id);
    const auto it = sub_map.find(channel_id);
    if (it == sub_map.end())
        return;

    it->second.subscriptions_.broadcastState(
            PdCom::Subscription::State::Active, q);
}

void PeriodicSubscriptionsWithGroup::GroupMap::dump(
        std::vector<PdCom::Process::SubscriptionInfo> &ans,
        const std::unordered_map<unsigned, Channel *> &channels) const
{
    for (const auto &all_groups : forward_) {
        for (const auto &sub_map : all_groups) {
            const auto var = impl::Variable::toUApi(
                    std::static_pointer_cast<const MsrProto::Variable>(
                            channels.at(sub_map.first)->shared_from_this()));
            for (const auto &weak_sub : sub_map.second.subscriptions_) {
                if (const auto sub = weak_sub.lock()) {
                    ans.emplace_back(sub->This_, &sub->subscriber_, var);
                }
            }
        }
    }
}

PeriodicSubscriptionWithoutGroup::transaction
PeriodicSubscriptionWithoutGroup::add(
        Process::PendingCallbackQueue &q,
        bool notify_pending,
        const std::shared_ptr<PeriodicSubscription> &s)
{
    if (locked_)
        throw PdCom::InternalError("PeriodicSubscription is locked!");
    const auto var_ptr = s->variable_.lock();
    if (!var_ptr)
        throw EmptyVariable();
    const auto &var = static_cast<Channel const &>(*var_ptr);
    auto map_it     = map_.find(var.index_);


    unsigned decimation = s->subscriber_.getTransmission().getInterval()
                    / var.sample_time_.count()
            + 0.5;
    decimation += s->subscriber_.getTransmission().getInterval() > 0.0
            and !decimation;
    unsigned blocksize = 0;

    if (decimation > 0) {
        blocksize = 1.0 / var.sample_time_.count() / decimation / 25.0 + 0.5;
        blocksize = std::min(var.bufsize / decimation, blocksize);
    }

    if (!blocksize)
        blocksize = 1;
    time_vector_.resize(std::max<size_t>(time_vector_.size(), blocksize + 2));

    if (map_it == map_.end()) {
        // not yet subscribed
        map_.insert({var.index_, {blocksize, decimation, {{s}}}});

        if (notify_pending)
            q.push_back({s, PdCom::Subscription::State::Pending});
        return {true, decimation, blocksize};
    }
    else if (decimation < map_it->second.decimation_) {
        // we have to resubscribe
        map_it->second.subscriptions_.currentState =
                PdCom::Subscription::State::Pending;

        if (notify_pending)
            q.push_back({s, PdCom::Subscription::State::Pending});
        auto subs = std::move(map_it->second.subscriptions_);
        subs.insert(s);
        map_.erase(map_it);
        map_.insert({var.index_, {blocksize, decimation, std::move(subs)}});
        return {true, decimation, blocksize};
    }
    else {
        if (notify_pending
            or map_it->second.subscriptions_.currentState
                    != PdCom::Subscription::State::Pending)
            q.push_back({s, map_it->second.subscriptions_.currentState});
        map_it->second.subscriptions_.insert(s);
        return {false, map_it->second.decimation_, map_it->second.blocksize_};
    }
}

PeriodicSubscriptionWithoutGroup::transaction
PeriodicSubscriptionWithoutGroup::remove(const Variable &_var)
{
    const auto it = map_.find(_var.index_);
    if (it == map_.end() || !it->second.subscriptions_.remove_expired())
        // not found or still subscribed by others
        return {false, 0, 0};
    else {
        // now empty
        map_.erase(it);
        return {true, 0, 0};
    }
}

void PeriodicSubscriptionWithoutGroup::subscribeWasConfirmed(
        Process::PendingCallbackQueue &q,
        ChannelId channel_id)
{
    const auto it = map_.find(channel_id);
    if (it == map_.end())
        return;

    it->second.subscriptions_.broadcastState(
            PdCom::Subscription::State::Active, q);
}

void PeriodicSubscriptionWithoutGroup::dump(
        std::vector<PdCom::Process::SubscriptionInfo> &ans,
        const std::unordered_map<unsigned, Channel *> &channels) const
{
    for (const auto &sub_map : map_) {
        const auto var = impl::Variable::toUApi(
                std::static_pointer_cast<const MsrProto::Variable>(
                        channels.at(sub_map.first)->shared_from_this()));
        for (const auto &weak_sub : sub_map.second.subscriptions_) {
            if (const auto sub = weak_sub.lock()) {
                ans.emplace_back(sub->This_, &sub->subscriber_, var);
            }
        }
    }
}
