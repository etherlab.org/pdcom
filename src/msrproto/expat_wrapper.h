/*****************************************************************************
 *
 * Copyright (C) 2021 Bjarne von Horn (vh at igh dot de)
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#ifndef MSRPROTO_EXPAT_WRAPPER_H
#define MSRPROTO_EXPAT_WRAPPER_H

#include <exception>
#include <functional>
#include <memory>

extern "C" {
struct XML_ParserStruct;
}

namespace PdCom { namespace impl { namespace MsrProto {
struct XmlParserDeleter
{
    void operator()(XML_ParserStruct *) noexcept;
};

template <typename Handler, bool useExpatBuffer>
class ExpatWrapper
{
  public:
    ExpatWrapper(const char *encoding = "UTF-8");

  protected:
    template <std::size_t Size>
    class ExpatBuffer
    {
        void *buffer_;

      public:
        explicit ExpatBuffer(ExpatWrapper &);
        ExpatBuffer(const ExpatBuffer &) = delete;
        ExpatBuffer(ExpatBuffer &&other) noexcept : buffer_(other.buffer_)
        {
            other.buffer_ = nullptr;
        }

        ExpatBuffer &operator=(const ExpatBuffer &) = delete;
        ExpatBuffer &operator=(ExpatBuffer &&other) noexcept
        {
            ExpatBuffer copy(std::move(other));
            std::swap(other.buffer_, buffer_);
            return *this;
        }


        template <typename T = void>
        T *fill() &
        {
            return static_cast<T *>(buffer_);
        }

        std::size_t size() const { return Size; }
    };

    template <typename T>
    typename std::enable_if<
            !useExpatBuffer && std::is_same<T, char>::value,
            bool>::type
    parse(const T *s, std::size_t n, bool final = false);
    template <size_t buffersize>
    typename std::enable_if<(useExpatBuffer && buffersize > 0), bool>::type
    parse(ExpatBuffer<buffersize> buffer, std::size_t n, bool final = false);

    void reset(const char *encoding = "UTF-8");
    int level() const noexcept { return level_; }

  private:
    struct CallbackWrapper;

    std::unique_ptr<XML_ParserStruct, XmlParserDeleter> parser_;
    std::exception_ptr pending_exception_ = nullptr;
    int level_                            = 0;
};


}}}  // namespace PdCom::impl::MsrProto


#endif  // MSRPROTO_EXPAT_WRAPPER_H
