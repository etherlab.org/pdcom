/*****************************************************************************
 *
 * Copyright (C) 2015-2016  Richard Hacker (lerichi at gmx dot net)
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#ifndef PD_DIRNODE_H
#define PD_DIRNODE_H

#include <list>
#include <memory>
#include <string>
#include <vector>

namespace PdCom { namespace impl { namespace MsrProto {
class DirNode : public std::enable_shared_from_this<DirNode>
{
  public:
    DirNode(bool isDir = 1);
    virtual ~DirNode() = default;

    std::string path() const;
    std::string name() const;

    typedef std::list<std::shared_ptr<DirNode>> List;
    void getChildren(List &list) const;
    void getAllChildren(List &list) const;

    bool hasChildren() const;

    void insert(std::shared_ptr<DirNode> n, const char *path);
    std::shared_ptr<DirNode> find(const std::string &path);

  private:
    DirNode *m_parent;
    std::string m_name;

    struct LessThan
    {
        bool operator()(const DirNode *n, const std::string &name) const
        {
            return n->m_name < name;
        }
        bool operator()(
                std::shared_ptr<DirNode> const &n,
                const std::string &name) const
        {
            return n->m_name < name;
        }
    };
    typedef std::vector<std::shared_ptr<DirNode>> NodeVector;
    NodeVector children;

    // Value is set if a <listing> command returned a <dir> tag
    bool directory;

    std::shared_ptr<DirNode> getRootNode();
};

}}}  // namespace PdCom::impl::MsrProto

#endif  // PD_DIRNODE_H
