/*****************************************************************************
 *
 * Copyright (C) 2021 Bjarne von Horn (vh at igh dot de)
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#ifndef MSRPROTO_PERIODICSUBSCRIPTIONS_H
#define MSRPROTO_PERIODICSUBSCRIPTIONS_H

#include "../Process.h"
#include "../Subscription.h"
#include "DataConverter.h"

#include <chrono>
#include <memory>
#include <pdcom5/Subscriber.h>
#include <set>
#include <unordered_map>
#include <vector>

namespace PdCom { namespace impl { namespace MsrProto {
class Channel;
class PollSubscription;
class PeriodicSubscription;
class Variable;

template <class S = PollSubscription>
struct SubscriptionSet :
    std::set<std::weak_ptr<S>, std::owner_less<std::weak_ptr<S>>>
{
    using std::set<std::weak_ptr<S>, std::owner_less<std::weak_ptr<S>>>::set;
    void readData(const char *data, std::chrono::nanoseconds ts) const
    {
        for (const auto &weak_sub : *this) {
            if (const auto sub = weak_sub.lock()) {
                sub->readData(data);
                sub->newValues(ts);
            }
        }
    }
    bool remove_expired() noexcept
    {
        for (auto it = this->begin(); it != this->end();) {
            if (it->expired())
                it = this->erase(it);
            else
                ++it;
        }
        return this->empty();
    }

    PdCom::Subscription::State currentState =
            PdCom::Subscription::State::Pending;

    void broadcastState(
            PdCom::Subscription::State state,
            Process::PendingCallbackQueue &q)
    {
        currentState = state;
        for (const auto &weak_sub : *this)
            if (const auto sub = weak_sub.lock())
                q.push_back({sub, state});
    }
};

class PeriodicSubscriptionsBase
{
  public:
    using GroupId   = unsigned int;
    using ChannelId = unsigned int;

    struct ChannelSubscripion
    {
        unsigned int const blocksize_, decimation_;
        SubscriptionSet<PeriodicSubscription> subscriptions_;
    };

    struct ChannelSubscriptionMap :
        std::unordered_map<ChannelId, ChannelSubscripion>
    {
        using std::unordered_map<ChannelId, ChannelSubscripion>::unordered_map;

        bool guaranteed_empty() noexcept
        {
            for (auto it = begin(); it != end();) {
                if (it->second.subscriptions_.empty())
                    it = erase(it);
                else
                    ++it;
            }
            return empty();
        }
    };


    class DataReceiveHandle
    {
        struct Unlocker
        {
            void operator()(PeriodicSubscriptionsBase *ps) const
            {
                if (ps)
                    ps->locked_ = false;
            }
        };

        std::unique_ptr<PeriodicSubscriptionsBase, Unlocker> ps_ = nullptr;
        VariableCache::Lock variable_cache_lock_;
        ChannelSubscriptionMap *subscription_map_ = nullptr;

        int current_blocksize_       = -1;
        int max_read_blocks_         = -1;
        std::chrono::nanoseconds ts_ = {};
        bool timestamps_read_        = false;
        // true if all arriving F tags have the same blocksize
        bool consistent_blocksize_ = false;

      public:
        DataReceiveHandle() {}
        DataReceiveHandle(
                PeriodicSubscriptionsBase &ps,
                ChannelSubscriptionMap &subscription_map,
                VariableCache::Lock variable_cache_lock,
                std::chrono::nanoseconds ts,
                bool consistent_blocksize);

        operator bool() const { return static_cast<bool>(ps_); }


        void readData(const Channel &c, const char *data);
        void readTimestamps(const char *data);
        void endNewDataRecieving();

      private:
        void sendDataToSubscribers(
                const DataDecoder &var,
                const ChannelSubscripion &subs,
                int block);
    };

  protected:
    std::vector<std::chrono::nanoseconds> time_vector_;
    impl::Subscription::SubscriberNotifier subscriber_notifier_;
    bool locked_ = false;
};

class PeriodicSubscriptionsWithGroup : public PeriodicSubscriptionsBase
{
  public:
    struct transaction
    {
        bool needs_server_action;
        // these are valid iff needs_server_action is true.
        // std::optional would be a way better alternative tbh
        unsigned int group_id, decimation_, blocksize_;
    };


    transaction
    add(Process::PendingCallbackQueue &q,
        bool notify_pending,
        const std::shared_ptr<PeriodicSubscription> &s);
    transaction remove(const Variable &_var, PdCom::Transmission tm);

    DataReceiveHandle startNewDataRecieving(
            GroupId index,
            VariableCache::Lock lock,
            std::chrono::nanoseconds ts)
    {
        return DataReceiveHandle {
                *this, map_.at(index), std::move(lock), ts, true};
    }


    const ChannelSubscriptionMap &at(GroupId group_id) const
    {
        return map_.at(group_id);
    }

    void unsubscribeDone(GroupId group_id)
    {
        auto &sub_map = map_.at(group_id);
        if (sub_map.state == ChannelSubscriptionMapImpl::AboutToBeDeleted)
            sub_map.state = ChannelSubscriptionMapImpl::Empty;
    }

    void subscribeWasConfirmed(
            Process::PendingCallbackQueue &q,
            GroupId group_id,
            ChannelId channel_id);

    void
    dump(std::vector<PdCom::Process::SubscriptionInfo> &ans,
         const std::unordered_map<unsigned, Channel *> &channels) const;

  private:
    struct ChannelSubscriptionMapImpl : ChannelSubscriptionMap
    {
        using ChannelSubscriptionMap::ChannelSubscriptionMap;

        enum State {
            Empty,
            Taken,
            AboutToBeDeleted, /* last <xsod> sent but no <ack> received yet */
        } state = Empty;
    };

    class GroupMap
    {
        std::vector<ChannelSubscriptionMapImpl> forward_;
        std::unordered_map<Transmission, GroupId> backward_;

      public:
        bool valid(GroupId idx) const noexcept
        {
            return idx > 0 and idx <= forward_.size();
        }
        ChannelSubscriptionMapImpl &at(GroupId group_id)
        {
            return forward_.at(group_id - 1);
        }
        const ChannelSubscriptionMapImpl &at(GroupId group_id) const
        {
            return forward_.at(group_id - 1);
        }
        GroupId find(Transmission t);
        // returns group id > 0 in case the last subscription has gone, else 0
        GroupId erase(Transmission t, ChannelId channel_idx);
        void
        dump(std::vector<PdCom::Process::SubscriptionInfo> &ans,
             const std::unordered_map<unsigned, Channel *> &channels) const;
    } map_;
};


class PeriodicSubscriptionWithoutGroup : public PeriodicSubscriptionsBase
{
  public:
    struct transaction
    {
        bool needs_server_action;
        // these are valid iff needs_server_action is true.
        // std::optional would be a way better alternative tbh
        unsigned int decimation_, blocksize_;
    };


    transaction
    add(Process::PendingCallbackQueue &q,
        bool notify_pending,
        const std::shared_ptr<PeriodicSubscription> &s);
    transaction remove(const Variable &_var);

    DataReceiveHandle
    startNewDataRecieving(VariableCache::Lock lock, std::chrono::nanoseconds ts)
    {
        return DataReceiveHandle {*this, map_, std::move(lock), ts, false};
    }

    bool hasChannel(ChannelId id) const
    {
        const auto it = map_.find(id);
        return it != map_.end() && !it->second.subscriptions_.empty();
    }


    void subscribeWasConfirmed(
            Process::PendingCallbackQueue &q,
            ChannelId channel_id);

    void
    dump(std::vector<PdCom::Process::SubscriptionInfo> &ans,
         const std::unordered_map<unsigned, Channel *> &channels) const;

  private:
    ChannelSubscriptionMap map_;
};

}}}  // namespace PdCom::impl::MsrProto

#endif  // MSRPROTO_PERIODICSUBSCRIPTIONS_H
