/*****************************************************************************
 *
 * Copyright (C) 2021 Richard Hacker (lerichi at gmx dot net),
 *                    Florian Pose (fp at igh dot de),
 *                    Bjarne von Horn (vh at igh dot de).
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#ifndef PDCOM5_MSRPROTO_VARIABLE_H
#define PDCOM5_MSRPROTO_VARIABLE_H

#include "../Variable.h"
#include "DirNode.h"

#include <pdcom5/SizeTypeInfo.h>
#include <string>
#include <vector>

namespace PdCom { namespace impl { namespace MsrProto {

class ProtocolHandler;

class Variable : public PdCom::impl::Variable, public DirNode
{
  public:
    Variable(
            PdCom::SizeInfo size_info,
            const PdCom::TypeInfo *type_info,
            std::weak_ptr<impl::Process> process,
            unsigned int index,
            unsigned taskId,
            bool writeable,
            double sampleTime,
            const char *alias,
            bool isDir,
            bool is_parameter);

    std::string getPath() const override { return DirNode::path(); }
    std::string getName() const override { return DirNode::name(); }
    std::string getAlias() const override { return alias_; }
    std::chrono::duration<double> getSampleTime() const override
    {
        return sample_time_;
    }
    int getTaskId() const override { return task_id_; }
    bool isWriteable() const override { return writeable_; }
    PdCom::Variable toUApi() const
    {
        return impl::Variable::toUApi(
                std::static_pointer_cast<const Variable>(shared_from_this()));
    }

    const bool is_parameter_;
    const unsigned int index_;
    const std::chrono::duration<double> sample_time_;
    const int task_id_;
    const bool writeable_;
    const std::string alias_;
};
}}}  // namespace PdCom::impl::MsrProto

#endif  // PDCOM5_MSRPROTO_VARIABLE_H
