/*****************************************************************************
 *
 * Copyright (C) 2021 Bjarne von Horn (vh at igh dot de).
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#ifndef PDCOM_IMPL_SELECTOR_H
#define PDCOM_IMPL_SELECTOR_H

#include <functional>
#include <pdcom5/SizeTypeInfo.h>
#include <pdcom5/Variable.h>
#include <vector>

namespace PdCom { namespace impl {
class Variable;

/** Selector base class for creating views on multidimensional data.
 *
 * Developers have to override clone(), getRequiredSize() and getCopyFunction()
 * in their derived class.
 *
 */
struct Selector
{
    using CopyFunctionType = std::function<
            void(void * /* destination */, const void * /* src */)>;

    virtual ~Selector() = default;
    /** Required size of the view, in bytes.
     *
     * \return The required memory space for the view, in bytes.
     */
    virtual std::size_t getRequiredSize(const Variable &variable) const;
    /** Smart memcpy().
     *
     * \returns A functor which will copy arriving variable data to the provided
     * view memory.
     */
    virtual CopyFunctionType getCopyFunction(const Variable &variable) const;

    virtual PdCom::Variable::SetValueFuture applySetValue(
            const Variable &variable,
            const void *src_data,
            TypeInfo::DataType src_type,
            size_t src_count) const;

    virtual SizeInfo getViewSizeInfo(const Variable &variable) const;
};

/** Selects one scalar out of a vector or matrix.
 *
 * The index is in Row major format, ((layer, )row, column).
 */
class ScalarSelector : public Selector
{
    std::vector<int> indices_;

  public:
    std::size_t getRequiredSize(const Variable &) const override;
    CopyFunctionType getCopyFunction(const Variable &) const override;

    ScalarSelector(std::vector<int> indices);

    size_t getOffset(const Variable &variable) const;

    SizeInfo getViewSizeInfo(const Variable &variable) const override;

    PdCom::Variable::SetValueFuture applySetValue(
            const Variable &variable,
            const void *src_data,
            TypeInfo::DataType src_type,
            size_t src_count) const override;
};
}}  // namespace PdCom::impl


#endif  // PDCOM_IMPL_SELECTOR_H
