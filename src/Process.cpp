/*****************************************************************************
 *
 * Copyright (C) 2007-2016  Richard Hacker (lerichi at gmx dot net)
 *                          Florian Pose <fp@igh.de>
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#include "Process.h"

#include "Debug.h"
#include "Future.h"
#include "IOLayer.h"
#include "ProtocolHandler.h"
#include "Subscription.h"
#include "Variable.h"
#include "msrproto/ProtocolHandler.h"
#include "pdcom5.h"

#include <algorithm>
#include <git_revision_hash.h>
#include <mutex>

#define STR(x) #x
#define QUOTE(x) STR(x)

const char *const PdCom::pdcom_version_code =
        QUOTE(PDCOM_MAJOR) "." QUOTE(PDCOM_MINOR) "." QUOTE(PDCOM_RELEASE);

const char *const PdCom::pdcom_full_version = GIT_REV;

using iProcess = PdCom::impl::Process;

namespace PdCom { namespace impl {
class PendingSubscription : public PdCom::impl::Subscription
{
  public:
    PendingSubscription(
            std::shared_ptr<const PdCom::impl::Variable> variable,
            PdCom::Subscription *This,
            PdCom::Subscriber &subscriber,
            std::shared_ptr<iProcess> process) :
        Subscription(std::move(variable), This, subscriber, process)
    {}

    static std::shared_ptr<PendingSubscription> make_and_notify(
            std::shared_ptr<const PdCom::impl::Variable> variable,
            PdCom::Subscription *This,
            PdCom::Subscriber &subscriber,
            std::shared_ptr<iProcess> process)
    {
        auto ans = std::make_shared<PendingSubscription>(
                std::move(variable), This, subscriber, process);

        process->pending_callbacks_.push_back(
                {ans, PdCom::Subscription::State::Pending});
        return ans;
    }


    void poll() override { throw PdCom::InvalidSubscription(); }
    const void *getData() const override { throw PdCom::InvalidSubscription(); }
};
}}  // namespace PdCom::impl

using PdCom::impl::PendingSubscription;

///////////////////////////////////////////////////////////////////////////
iProcess::Process(PdCom::Process *This) :
    std::enable_shared_from_this<iProcess>(),
    PdCom::impl::IOLayer(nullptr),
    This(This),
    io(this)
{}

///////////////////////////////////////////////////////////////////////////
void iProcess::reset()
{
    std::unique_lock<AsyncDataMutex> lck(async_data_mutex_);

    io->rxbytes = 0;
    io->txbytes = 0;

    protocolHandler_.reset();
}

///////////////////////////////////////////////////////////////////////////
void iProcess::asyncData()
{
    std::unique_lock<AsyncDataMutex> lck(async_data_mutex_);
    pending_callbacks_.flush();
    if (!protocolHandler_)
        protocolHandler_.reset(new MsrProto::ProtocolHandler(this, io));

    protocolHandler_->cancelSubscriptions();
    if (!protocolHandler_->asyncData()) {
        protocolHandler_.reset();
        // first, make all subs invalid
        auto old_subs = std::move(all_subscriptions_);
        for (const auto &registered_s : old_subs) {
            if (const auto sub = registered_s.subscription_.lock()) {
                pending_callbacks_.push_back(
                        {sub, PdCom::Subscription::State::Invalid});
            }
        }
        // give control flow back
        pending_callbacks_.flush();
        // replace all sub implementations as they are invalid now
        for (const auto &registered_s : old_subs) {
            if (const auto sub = registered_s.subscription_.lock()) {
                if (!sub->This_)
                    continue;
                auto new_sub = std::make_shared<PendingSubscription>(
                        nullptr, sub->This_, sub->subscriber_,
                        shared_from_this());
                sub->replaceImpl(new_sub);

                all_subscriptions_.insert(
                        {new_sub, registered_s.path_, registered_s.selector_});
            }
        }
    }
    else {
        protocolHandler_->cancelSubscriptions();
    }
    pending_callbacks_.flush();
}


std::shared_ptr<PdCom::impl::Subscription> iProcess::subscribe(
        PdCom::Subscription *subscription,
        const std::string &path,
        PdCom::Subscriber &subscriber,
        const PdCom::Selector &selector)
{
    std::shared_ptr<PdCom::impl::Subscription> ans;
    if (protocolHandler_)
        ans = protocolHandler_->subscribe(
                subscription, path, subscriber, selector);
    else
        ans = PendingSubscription::make_and_notify(
                nullptr, subscription, subscriber, shared_from_this());
    all_subscriptions_.insert({ans, path, selector.impl_});
    return ans;
}
std::shared_ptr<PdCom::impl::Subscription> iProcess::subscribe(
        PdCom::Subscription *subscription,
        std::shared_ptr<const PdCom::impl::Variable> var,
        PdCom::Subscriber &subscriber,
        const PdCom::Selector &selector)
{
    std::shared_ptr<PdCom::impl::Subscription> ans;
    if (protocolHandler_)
        ans = protocolHandler_->subscribe(
                subscription, var, subscriber, selector);
    else
        ans = PendingSubscription::make_and_notify(
                nullptr, subscription, subscriber, shared_from_this());
    all_subscriptions_.insert({ans, var->getPath(), selector.impl_});
    return ans;
}

void iProcess::connected()
{
    auto old_subs = std::move(all_subscriptions_);
    for (const auto &registered_sub : old_subs)
        if (const auto sub = registered_sub.subscription_.lock()) {
            auto new_sub = protocolHandler_->subscribe(
                    sub->This_, registered_sub.path_, sub->subscriber_,
                    {registered_sub.selector_});
            sub->replaceImpl(new_sub);
            all_subscriptions_.insert(
                    {new_sub, registered_sub.path_, registered_sub.selector_});
        }
    This->connected();
}

using PdCom::impl::Subscription;

void iProcess::replace(
        std::shared_ptr<Subscription> const &old_s,
        std::shared_ptr<Subscription> const &new_s)
{
    const auto it = std::find_if(
            all_subscriptions_.begin(), all_subscriptions_.end(),
            [&old_s](const RegisteredSubscription &rs) {
                return !rs.subscription_.owner_before(old_s)
                        and !old_s.owner_before(rs.subscription_);
            });
    if (it != all_subscriptions_.end()) {
        all_subscriptions_.insert({new_s, it->path_, it->selector_});
        all_subscriptions_.erase(it);
    }
}


using PdCom::Process;

Process::~Process() = default;

Process::Process() : pimpl(std::make_shared<PdCom::impl::Process>(this))
{}

Process::Process(std::shared_ptr<iProcess> impl) : pimpl(impl)
{}

std::string Process::name() const
{
    return pimpl->protocolHandler().name();
}

std::string Process::version() const
{
    return pimpl->protocolHandler().version();
}

void Process::setAuthManager(PdCom::Sasl *am)
{
    pimpl->setAuthManager(am);
}

PdCom::Sasl *Process::getAuthManager() const
{
    return pimpl->auth_manager_;
}

void Process::setMessageManager(PdCom::MessageManagerBase *mm)
{
    pimpl->setMessageManager(mm);
}

void Process::reset()
{
    pimpl->reset();
}

void Process::asyncData()
{
    pimpl->asyncData();
}

bool Process::list(const std::string &path)
{
    return pimpl->protocolHandler().list(path);
}

bool Process::find(const std::string &path)
{
    return pimpl->protocolHandler().find(path);
}

void PdCom::Process::getClientStatistics()
{
    pimpl->protocolHandler().getClientStatistics();
}

void PdCom::Process::clientStatisticsReply(std::vector<PdCom::ClientStatistics>)
{}

void Process::ping()
{
    pimpl->protocolHandler().ping();
}

void Process::broadcast(const std::string &message, const std::string &attr)
{
    pimpl->protocolHandler().broadcast(message, attr);
}

void Process::broadcastReply(
        const std::string &,
        const std::string &,
        std::chrono::nanoseconds,
        const std::string &)
{}
void Process::listReply(std::vector<PdCom::Variable>, std::vector<std::string>)
{}

void Process::findReply(const PdCom::Variable &)
{}

void Process::callPendingCallbacks()
{
    pimpl->pending_callbacks_.flush();
    if (const auto p = pimpl->castProtocolHandler())
        if (!pimpl->async_data_mutex_.isLocked())
            p->cancelSubscriptions();
}

std::vector<Process::SubscriptionInfo> Process::getActiveSubscriptions() const
{
    return pimpl->protocolHandler().getActiveSubscriptions();
}

void iProcess::PendingCallbackQueue::flush()
{
    while (!empty()) {
        const auto p = front();
        pop_front();
        p.execute();
    }
}

PdCom::Variable::PollFuture PdCom::Variable::poll() const
{
    if (auto impl = pimpl_.lock()) {
        if (const auto process = impl->process.lock()) {
            auto ans = createFuture<
                    PdCom::Exception const &, VariablePollResult,
                    std::chrono::nanoseconds>();
            process->protocolHandler().pollVariable(
                    *impl, std::move(ans.second));
            return std::move(ans.first);
        }
        throw ProcessGoneAway();
    }
    throw EmptyVariable();
}
