/*****************************************************************************
 *
 * Copyright (C) 2021 Bjarne von Horn (vh at igh dot de).
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/


#include "Selector.h"

#include "Variable.h"

#include <cstring>
#include <pdcom5/Exception.h>
#include <pdcom5/Subscription.h>
#include <sstream>


PdCom::Selector::Selector() : impl_(std::make_shared<impl::Selector>())
{}
PdCom::ScalarSelector::ScalarSelector(std::vector<int> indices) :
    PdCom::Selector(std::make_shared<impl::ScalarSelector>(std::move(indices)))
{}

PdCom::SizeInfo
PdCom::Selector::getViewSizeInfo(const PdCom::Variable &variable) const
{
    if (!impl_)
        throw PdCom::InvalidArgument("impl must not be NULL!");
    return impl_->getViewSizeInfo(*impl::Variable::fromUApi(variable));
}

using PdCom::impl::Selector;
using PdCom::impl::Variable;


Selector::CopyFunctionType
Selector::getCopyFunction(const Variable &variable) const
{
    const auto count = getRequiredSize(variable);
    return [count](void *dest, const void *src) {
        std::memcpy(dest, src, count);
    };
}

std::size_t Selector::getRequiredSize(const Variable &variable) const
{
    return variable.totalSizeInBytes();
}

PdCom::Variable::SetValueFuture Selector::applySetValue(
        const Variable &variable,
        const void *const src_data,
        PdCom::TypeInfo::DataType const src_type,
        size_t const src_count) const
{
    if (!src_count or src_count > variable.size_info.totalElements()) {
        throw PdCom::InvalidArgument(
                "Range Error, got " + std::to_string(src_count)
                + " Parameter values, expected max. "
                + std::to_string(variable.size_info.totalElements()));
    }
    return variable.setValue(src_data, src_type, 0, src_count);
}

PdCom::SizeInfo
Selector::getViewSizeInfo(const PdCom::impl::Variable &variable) const
{
    return variable.size_info;
}

using PdCom::impl::ScalarSelector;

ScalarSelector::ScalarSelector(std::vector<int> indices) :
    indices_(std::move(indices))
{
    if (indices_.empty())
        throw InvalidArgument("indices must not be empty");
}


std::size_t ScalarSelector::getRequiredSize(const Variable &variable) const
{
    return variable.type_info->element_size;
}

PdCom::SizeInfo
ScalarSelector::getViewSizeInfo(const PdCom::impl::Variable &) const
{
    return PdCom::SizeInfo::Scalar();
}

namespace {
template <typename It>
size_t product(It begin, It end)
{
    size_t ans = 1;
    while (begin != end) {
        ans *= *begin;
        ++begin;
    }
    return ans;
}
}  // namespace

size_t ScalarSelector::getOffset(const Variable &variable) const
{
    size_t offset_indices = 0;
    for (unsigned int dim = 0; dim < indices_.size(); ++dim) {
        offset_indices += indices_[dim]
                * product(variable.size_info.begin() + 1 + dim,
                          variable.size_info.end());
    }
    return offset_indices;
}


Selector::CopyFunctionType
ScalarSelector::getCopyFunction(const Variable &variable) const
{
    if (variable.size_info.isScalar() and indices_.size() == 1
        and indices_[0] == 0) {
        return Selector::getCopyFunction(variable);
    }
    else if (variable.size_info.dimension() != indices_.size()) {
        std::ostringstream os;
        os << "Dimension error, expected " << variable.size_info.dimension()
           << " dimensions, got " << indices_.size() << " for variable "
           << variable.getPath();
        throw InvalidArgument(os.str());
    }
    auto max_idx = variable.size_info.begin();
    for (auto idx = indices_.begin(); idx != indices_.end(); ++idx, ++max_idx) {
        if (*idx < 0 or *idx >= static_cast<int>(*max_idx)) {
            std::ostringstream os;
            os << "Index " << *idx << " at dimension "
               << std::to_string(indices_.end() - 1 - idx)
               << " out of range [0, " << *max_idx << "] for variable"
               << variable.getPath();
            throw InvalidArgument(os.str());
        }
    }
    const size_t el_size = variable.type_info->element_size;

    const auto offset_bytes = getOffset(variable) * el_size;

    return [offset_bytes, el_size](void *dest, const void *src) {
        std::memcpy(
                dest, reinterpret_cast<const char *>(src) + offset_bytes,
                el_size);
    };
}


PdCom::Variable::SetValueFuture ScalarSelector::applySetValue(
        const Variable &variable,
        const void *src_data,
        PdCom::TypeInfo::DataType src_type,
        size_t src_count) const
{
    if (src_count != 1) {
        throw PdCom::InvalidArgument(
                "Expected exactly one value, got " + std::to_string(src_count));
    }
    return variable.setValue(
            src_data, src_type, getOffset(variable), src_count);
}
