/*****************************************************************************
 * vim:tw=78
 *
 * Copyright (C) 2022 Bjarne von Horn (vh at igh dot de).
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#ifndef PDCOM5_FUTURE_IMPL_H
#define PDCOM5_FUTURE_IMPL_H

#include <functional>
#include <memory>
#include <pdcom5/Exception.h>
#include <pdcom5/Future.h>
#include <tuple>
#include <type_traits>

namespace PdCom {
namespace impl {
template <class Exception>
struct PromiseBase
{
    std::function<void(Exception)> reject_;
};

template <class Exception, class... Result>
struct Promise : public PromiseBase<Exception>
{
    std::function<void(Result...)> resolve_;
};
}  // namespace impl
template <class Exception>
class PromiseBase
{
  protected:
    std::weak_ptr<impl::PromiseBase<Exception>> impl_;

  public:
    using ExceptionType = Exception;

    PromiseBase(std::weak_ptr<impl::PromiseBase<Exception>> impl) noexcept :
        impl_(std::move(impl))
    {}
    PromiseBase(PromiseBase &&)      = default;
    PromiseBase(PromiseBase const &) = delete;
    PromiseBase &operator=(PromiseBase &&) = default;
    PromiseBase &operator=(PromiseBase const &) = delete;

    template <class... Args>
    bool reject(Args &&...args);
};

template <class Exception, class... Result>
class Promise : public PromiseBase<Exception>
{
    using Impl = impl::Promise<Exception, Result...>;

  public:
    using ResolveFn = std::function<void(Result...)>;


    Promise() noexcept : PromiseBase<Exception>(nullptr) {}
    Promise(std::weak_ptr<Impl> impl) noexcept :
        PromiseBase<Exception>(std::move(impl))
    {}

    template <class... Args>
    bool resolve(Args &&...result);

    PromiseBase<Exception> toBase()
    {
        return {std::move(PromiseBase<Exception>::impl_)};
    }
};

template <class Exception, class... Result>
const Future<Exception, Result...> &Future<Exception, Result...>::then(
        Future<Exception, Result...>::ResolveFn resolve) const &
{
    if (!impl_)
        throw InvalidArgument("Future is empty");
    impl_->resolve_ = resolve;
    return *this;
}

template <class Exception, class... Result>
const Future<Exception, Result...> &
Future<Exception, Result...>::handle_exception(
        Future<Exception, Result...>::RejectFn reject) const &
{
    if (!impl_)
        throw InvalidArgument("Future is empty");
    impl_->reject_ = reject;
    return *this;
}

template <class Exception, class... Result>
Future<Exception, Result...> Future<Exception, Result...>::then(
        Future<Exception, Result...>::ResolveFn resolve) &&
{
    if (!impl_)
        throw InvalidArgument("Future is empty");
    impl_->resolve_ = resolve;
    return std::move(*this);
}

template <class Exception, class... Result>
Future<Exception, Result...> Future<Exception, Result...>::handle_exception(
        Future<Exception, Result...>::RejectFn reject) &&
{
    if (!impl_)
        throw InvalidArgument("Future is empty");
    impl_->reject_ = reject;
    return std::move(*this);
}

template <class Exception, class... Result>
template <class... Args>
inline bool Promise<Exception, Result...>::resolve(Args &&...args)
{
    bool ans = false;
    if (const auto impl = std::static_pointer_cast<Impl>(
                PromiseBase<Exception>::impl_.lock())) {
        if (impl->resolve_) {
            impl->resolve_(std::forward<Args>(args)...);
            ans = true;
        }
    }
    PromiseBase<Exception>::impl_.reset();
    return ans;
}

template <class Exception>
template <class... Args>
inline bool PromiseBase<Exception>::reject(Args &&...args)
{
    if (const auto impl = impl_.lock()) {
        if (impl->reject_) {
            impl->reject_(std::forward<Args>(args)...);
            return true;
        }
    }
    impl_.reset();
    return false;
}

template <class Exception, class... Result>
inline std::pair<Future<Exception, Result...>, Promise<Exception, Result...>>
createFuture()
{
    auto ans = std::make_shared<impl::Promise<Exception, Result...>>();
    return {Future<Exception, Result...> {ans},
            Promise<Exception, Result...> {ans}};
}

}  // namespace PdCom


#endif  // PDCOM5_FUTURE_IMPL_H
