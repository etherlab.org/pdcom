/*****************************************************************************
 *
 * Copyright (C) 2007-2016  Richard Hacker (lerichi at gmx dot net)
 *                          Florian Pose <fp@igh.de>
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#ifndef PDCOM_PROTOCOLHANDLER_H
#define PDCOM_PROTOCOLHANDLER_H

#include <memory>
#include <pdcom5/Process.h>
#include <pdcom5/Subscription.h>
#include <stddef.h>
#include <stdint.h>
#include <string>

namespace PdCom {
class Subscriber;
template <class Exception, class... Result>
class Promise;
class Variable;

namespace impl {
class Process;
class Variable;

class ProtocolHandler
{
  public:
    using VariablePollPromise = PdCom::Promise<
            PdCom::Exception const &,
            PdCom::VariablePollResult,
            std::chrono::nanoseconds>;


    ProtocolHandler(PdCom::impl::Process *process) : process(process) {}
    virtual ~ProtocolHandler() = default;

    virtual bool asyncData()                                     = 0;
    virtual bool find(const std::string &path)                   = 0;
    virtual bool list(const std::string &path)                   = 0;
    virtual void getClientStatistics()                           = 0;
    virtual bool login(const char *mech, const char *clientData) = 0;
    virtual void logout()                                        = 0;

    virtual void
    pollVariable(const Variable &var, VariablePollPromise &&promise) = 0;

    virtual std::shared_ptr<Subscription> subscribe(
            PdCom::Subscription *subscription,
            const std::string &path,
            PdCom::Subscriber &subscriber,
            const PdCom::Selector &selector) = 0;
    virtual std::shared_ptr<Subscription> subscribe(
            PdCom::Subscription *subscription,
            std::shared_ptr<const Variable> var,
            PdCom::Subscriber &subscriber,
            const PdCom::Selector &selector) = 0;
    virtual void cancelSubscriptions()       = 0;
    virtual void ping()                      = 0;
    virtual std::string name() const         = 0;
    virtual std::string version() const      = 0;

    virtual std::vector<PdCom::Process::SubscriptionInfo>
    getActiveSubscriptions() const = 0;

    virtual void
    broadcast(const std::string &message, const std::string &attr) = 0;

    virtual void getMessage(uint32_t seqNo) = 0;
    virtual void getActiveMessages()        = 0;

    PdCom::impl::Process *const process;
};
}  // namespace impl
}  // namespace PdCom
#endif  // PDCOM_PROTOCOLHANDLER_H
