/*****************************************************************************
 * vim:tw=78
 *
 * Copyright (C) 2021 Richard Hacker (lerichi at gmx dot net),
 *                    Florian Pose (fp at igh dot de),
 *                    Bjarne von Horn (vh at igh dot de).
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/


#ifndef PDCOM5_VARIABLE_IMPL_H
#define PDCOM5_VARIABLE_IMPL_H

#include <memory>
#include <pdcom5/Exception.h>
#include <pdcom5/Variable.h>

namespace PdCom {
class Subscription;
class Subscriber;
struct Selector;

namespace impl {
class Process;

class Variable
{
  public:
    static std::shared_ptr<const Variable> fromUApi(const PdCom::Variable &p)
    {
        if (auto ans = p.pimpl_.lock())
            return ans;
        throw EmptyVariable();
    }
    static PdCom::Variable toUApi(std::weak_ptr<const Variable> var)
    {
        return PdCom::Variable {var};
    }

    static std::shared_ptr<Subscription> subscribe(
            PdCom::Subscription *subscription,
            const PdCom::Variable &p,
            PdCom::Subscriber &subscriber,
            const PdCom::Selector &selector);

    virtual ~Variable() = default;
    Variable(
            SizeInfo size_info,
            const TypeInfo *type_info,
            std::weak_ptr<PdCom::impl::Process> p) :
        size_info(std::move(size_info)), type_info(type_info), process(p)
    {}

    Variable(Variable &&) noexcept;
    Variable &operator=(Variable &&) noexcept;

    virtual PdCom::Variable::SetValueFuture
    setValue(const void *, TypeInfo::DataType t, size_t idx, size_t n)
            const                        = 0;
    virtual std::string getPath() const  = 0;
    virtual std::string getName() const  = 0;
    virtual std::string getAlias() const = 0;
    virtual int getTaskId() const        = 0;
    virtual bool isWriteable() const     = 0;

    virtual std::chrono::duration<double> getSampleTime() const = 0;

    std::size_t totalSizeInBytes() const
    {
        return numberOfElements() * type_info->element_size;
    }
    std::size_t numberOfElements() const { return size_info.totalElements(); }

    SizeInfo size_info;
    const TypeInfo *type_info;
    std::weak_ptr<PdCom::impl::Process> process;

  protected:
    void getValue(
            char *dst,
            const void *src,
            TypeInfo::DataType UserTypeIdx,
            size_t n) const
    {
        details::copyData(dst, type_info->type, src, UserTypeIdx, n);
    }
};


}  // namespace impl

}  // namespace PdCom

#endif  // PDCOM5_VARIABLE_H
