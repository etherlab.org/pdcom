/*****************************************************************************
 * vim:tw=78
 *
 * Copyright (C) 2021 Bjarne von Horn (vh at igh dot de).
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#include "Process.h"

#include <pdcom5/Exception.h>
#include <pdcom5/MessageManagerBase.h>

using PdCom::MessageManagerBase;

MessageManagerBase::MessageManagerBase() = default;

MessageManagerBase::MessageManagerBase(MessageManagerBase &&o) noexcept :
    process_(std::move(o.process_))
{
    if (const auto p = process_.lock()) {
        if (p->message_manager_ == &o) {
            p->message_manager_ = this;
        }
    }
}

MessageManagerBase &
MessageManagerBase::operator=(MessageManagerBase &&o) noexcept
{
    if (&o == this)
        return *this;
    if (const auto my_process = process_.lock())
        my_process->message_manager_ = nullptr;
    process_ = std::move(o.process_);
    if (const auto my_process = process_.lock())
        my_process->message_manager_ = this;
    return *this;
}

MessageManagerBase::~MessageManagerBase()
{
    if (const auto p = process_.lock())
        p->setMessageManager(nullptr);
}

void MessageManagerBase::getMessage(uint32_t seqNo) const
{
    if (const auto p = process_.lock()) {
        p->protocolHandler().getMessage(seqNo);
    }
    else
        throw ProcessGoneAway();
}

void MessageManagerBase::activeMessages() const
{
    if (const auto p = process_.lock()) {
        p->protocolHandler().getActiveMessages();
    }
    else
        throw ProcessGoneAway();
}

void MessageManagerBase::getMessageReply(PdCom::Message)
{}

void MessageManagerBase::processMessage(PdCom::Message)
{}

void MessageManagerBase::activeMessagesReply(std::vector<PdCom::Message>)
{}
