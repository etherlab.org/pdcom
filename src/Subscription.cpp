/*****************************************************************************
 *
 * Copyright (C) 2021 Richard Hacker (lerichi at gmx dot net),
 *                    Florian Pose (fp at igh dot de),
 *                    Bjarne von Horn (vh at igh dot de).
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#include "Subscription.h"

#include "Process.h"
#include "TemplateVodoo.inc"
#include "Variable.h"

#include <algorithm>
#include <array>
#include <cstring>
#include <pdcom5/Exception.h>
#include <pdcom5/Subscriber.h>
#include <pdcom5/Subscription.h>
#include <sstream>


using PdCom::Subscription;

Subscription::Subscription(Subscription &&s) noexcept :
    pimpl(std::move(s.pimpl)), state_(s.state_)
{
    if (pimpl)
        pimpl->This_ = this;
    s.state_ = State::Invalid;
}


Subscription::Subscription(
        PdCom::Subscriber &subscriber,
        const PdCom::Variable &variable,
        const PdCom::Selector &selector)
{
    if (!selector.impl_)
        throw InvalidArgument("Selector must not be null");
    pimpl = PdCom::impl::Variable::subscribe(
            this, variable, subscriber, selector);
    if (!pimpl)
        throw InvalidSubscription();
    pimpl->path_ = variable.getPath();
}

Subscription::Subscription(
        PdCom::Subscriber &subscriber,
        PdCom::Process &process,
        const std::string &path,
        const PdCom::Selector &selector)
{
    if (!selector.impl_)
        throw InvalidArgument("Selector must not be null");
    pimpl = PdCom::impl::Process::fromUApi(process).subscribe(
            this, path, subscriber, selector);
    if (!pimpl)
        throw InvalidSubscription();
    pimpl->path_ = path;
}

Subscription &Subscription::operator=(Subscription &&p) noexcept
{
    if (&p == this)
        return *this;
    std::swap(p.pimpl, pimpl);
    std::swap(p.state_, state_);
    if (pimpl)
        pimpl->This_ = this;
    if (p.pimpl)
        p.pimpl->This_ = &p;
    return *this;
}

void Subscription::poll()
{
    if (pimpl)
        pimpl->poll();
    else
        throw InvalidSubscription();
}

const void *Subscription::getData() const
{
    if (!pimpl)
        throw InvalidSubscription();
    return pimpl->getData();
}

PdCom::Variable Subscription::getVariable() const
{
    if (!pimpl)
        throw InvalidSubscription();
    return PdCom::impl::Variable::toUApi(pimpl->variable_);
}

PdCom::Process *Subscription::getProcess() const
{
    if (!pimpl)
        throw InvalidSubscription();
    if (auto p = pimpl->process_.lock()) {
        return p->This;
    }
    throw ProcessGoneAway();
}

std::string Subscription::getPath() const
{
    if (!pimpl)
        throw InvalidSubscription();
    return pimpl->path_;
}

std::shared_ptr<PdCom::impl::Subscription> PdCom::impl::Variable::subscribe(
        PdCom::Subscription *subscription,
        const PdCom::Variable &var,
        PdCom::Subscriber &subscriber,
        const PdCom::Selector &selector)
{
    auto impl_var = fromUApi(var);
    if (const auto p = impl_var->process.lock())
        return p->subscribe(
                subscription, std::move(impl_var), subscriber, selector);
    else
        throw ProcessGoneAway();
}


namespace {
template <class T>
inline T convert(T val)
{
    return val;
}
inline int convert(int8_t val)
{
    return val;
}
inline unsigned convert(uint8_t val)
{
    return val;
}

template <class T>
struct Printer
{
    static void
    print(std::ostream &os, const void *buf, char delim, size_t nelem)
    {
        const T *value = reinterpret_cast<const T *>(buf);

        if (nelem > 1)
            os << '[';

        for (size_t i = 0; i < nelem; ++i) {
            if (i)
                os << delim;
            os << convert(*value++);
        }

        if (nelem > 1)
            os << ']';
    }
};

using printFn = void (*)(std::ostream &, const void *, char, size_t);

template <PdCom::TypeInfo::DataType... src_types>
constexpr std::array<printFn, sizeof...(src_types)>
getPrintFns(sequence<src_types...>)
{
    return {{Printer<typename PdCom::details::DataTypeTraits<
            src_types>::value_type>::print...}};
}
}  // namespace

void PdCom::Subscription::print(std::ostream &os, char delimiter) const
{
    static constexpr auto printers = getPrintFns(DataTypeSequence {});
    static_assert(
            printers.size() == DataTypeCount,
            "Print function vector size mismatch");

    if (getVariable().empty())
        throw InvalidSubscription();

    printers[getVariable().getTypeInfo().type](
            os, getData(), delimiter,
            getVariable().getSizeInfo().totalElements());
}
