/*****************************************************************************
 * vim:tw=78
 *
 * Copyright (C) 2021 Bjarne von Horn (vh at igh dot de).
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#include <algorithm>
#include <arpa/inet.h>
#include <array>
#include <cstring>
#include <errno.h>
#include <memory>
#include <netdb.h>
#include <netinet/in.h>
#include <pdcom5/Exception.h>
#include <pdcom5/PosixProcess.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

using PdCom::PosixProcess;

namespace {
template <typename T>
bool do_connect(int &fd, const T &addr, int af, socklen_t len = sizeof(T))
{
    static_assert(!std::is_pointer<T>::value, "addr must be by reference");
    fd = socket(af, SOCK_STREAM, 0);
    if (fd == -1)
        return false;
    if (::connect(fd, reinterpret_cast<const sockaddr *>(&addr), len) >= 0)
        return true;
    ::close(fd);
    fd = -1;
    return false;
}

void writeToSocket(
        const char *begin,
        const char *const end,
        const int fd,
        const timeval &ts)
{
    using PdCom::WriteFailure;
    while (begin != end) {
        const auto res = ::write(fd, begin, end - begin);
        if (res > 0) {
            begin += res;
        }
        else if (res <= 0) {
            if (errno != EAGAIN)
                throw WriteFailure(errno);

            if (fd < 0 || fd >= FD_SETSIZE)
                throw WriteFailure("fd >= FD_SETSIZE");
            timeval timeout = ts;
            fd_set fds;
            FD_ZERO(&fds);
            FD_SET(fd, &fds);
            switch (::select(fd + 1, nullptr, &fds, nullptr, &timeout)) {
                case 1:
                    break;
                case 0:
                    throw WriteFailure(ETIMEDOUT);
                default:
                    throw WriteFailure(errno);
            }
        }
    }
}
}  // namespace

class PDCOM5_LOCAL PdCom::PosixProcess::Impl
{
  public:
    class PDCOM5_LOCAL Buffer
    {
        std::array<char, 1024> buffer_ = {};

        size_t size_ = 0;

      public:
        ::timeval timeout = {1, 0};

        size_t size() const { return size_; }
        size_t capacity() const { return buffer_.size(); }

        bool append(const char *buf, size_t count)
        {
            if (count > (capacity() - size()))
                return false;
            std::copy(buf, buf + count, buffer_.data() + size_);
            size_ += count;
            return true;
        }

        void flush(const int fd)
        {
            writeToSocket(buffer_.data(), buffer_.data() + size_, fd, timeout);
            size_ = 0;
        }

        void clear() { size_ = 0; }
    } buffer_;
};

PosixProcess::PosixProcess(const char *host, unsigned short port) :
    fd_(-1), impl_(std::make_shared<Impl>())
{
    reconnect(host, port);
}

void PosixProcess::reconnect(const char *host, unsigned short port)
{
    const auto connect_v4 = [&]() {
        sockaddr_in server_addr;
        ::memset(&server_addr, 0, sizeof(server_addr));
        server_addr.sin_family = AF_INET;
        server_addr.sin_port   = htons(port);
        if (inet_pton(AF_INET, host, &server_addr.sin_addr) <= 0)
            return false;
        return do_connect(fd_, server_addr, AF_INET);
    };

    const auto connect_v6 = [&]() {
        sockaddr_in6 server_addr;
        ::memset(&server_addr, 0, sizeof(server_addr));
        server_addr.sin6_family = AF_INET6;
        server_addr.sin6_port   = htons(port);
        if (inet_pton(AF_INET6, host, &server_addr.sin6_addr) <= 0)
            return false;
        return do_connect(fd_, server_addr, AF_INET6);
    };

    const auto connect_dns = [&]() {
        const auto port_s = std::to_string(port);
        const ::addrinfo hints {
                AI_NUMERICSERV | AI_V4MAPPED | AI_ADDRCONFIG,
                AF_UNSPEC,
                SOCK_STREAM,
                0,
                0,
                nullptr,
                nullptr,
                nullptr};
        struct MyAddrInfo
        {
            ::addrinfo *res = nullptr;
            ~MyAddrInfo()
            {
                if (res)
                    freeaddrinfo(res);
            }
        } res;
        if (::getaddrinfo(host, port_s.c_str(), &hints, &res.res) != 0)
            return false;
        ::addrinfo *i = res.res;
        while (i) {
            if (do_connect(fd_, *(i->ai_addr), i->ai_family, i->ai_addrlen))
                return true;
            i = i->ai_next;
        }
        return false;
    };

    if (fd_ != -1)
        close(fd_);
    impl_->buffer_.clear();
    if (!connect_v4() and !connect_v6() and !connect_dns()) {
        throw ConnectionFailed();
    }
}

PosixProcess::PosixProcess(int fd) : fd_(fd), impl_(std::make_shared<Impl>())
{}

PosixProcess::~PosixProcess()
{
    if (fd_ >= 0)
        close(fd_);
}


void PosixProcess::posixFlush()
{
    impl_->buffer_.flush(fd_);
}

void PosixProcess::posixWriteBuffered(const char *buf, size_t count)
{
    if (!impl_->buffer_.append(buf, count)) {
        impl_->buffer_.flush(fd_);
        posixWriteDirect(buf, count);
    }
}

void PosixProcess::posixWriteDirect(const char *buf, size_t count)
{
    writeToSocket(buf, buf + count, fd_, impl_->buffer_.timeout);
}

int PosixProcess::posixRead(char *buf, int count)
{
    const auto res = ::read(fd_, buf, count);
    if (res < 0) {
        if (errno != EAGAIN && errno != EINTR)
            throw ReadFailure(errno);
        int e = errno;
        errno = 0;
        return -e;
    }
    return res;
}

void PosixProcess::setWriteTimeout(std::chrono::milliseconds ms)
{
    const auto seconds = std::chrono::duration_cast<std::chrono::seconds>(ms);
    const auto us =
            std::chrono::duration_cast<std::chrono::microseconds>(ms - seconds);

    impl_->buffer_.timeout.tv_sec  = seconds.count();
    impl_->buffer_.timeout.tv_usec = us.count();
}
