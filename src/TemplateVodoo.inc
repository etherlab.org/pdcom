/*****************************************************************************
 *
 * Copyright (C) 2022 Bjarne von Horn (vh at igh dot de).
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#include <pdcom5/SizeTypeInfo.h>
#include <pdcom5/details.h>

namespace {

constexpr PdCom::TypeInfo::DataType increment(PdCom::TypeInfo::DataType t)
{
    return static_cast<PdCom::TypeInfo::DataType>(static_cast<int>(t) + 1);
}

constexpr auto DataTypeCount =
        static_cast<unsigned int>(PdCom::TypeInfo::DataType::DataTypeEnd);

template <PdCom::TypeInfo::DataType...>
struct sequence
{};
template <
        PdCom::TypeInfo::DataType DT,
        PdCom::TypeInfo::DataType... Previous_DTs>
struct generate_sequence : generate_sequence<increment(DT), Previous_DTs..., DT>
{};
template <PdCom::TypeInfo::DataType... DTs>
struct generate_sequence<PdCom::TypeInfo::DataType::DataTypeEnd, DTs...> :
    sequence<DTs...>
{};

using DataTypeSequence =
        generate_sequence<PdCom::TypeInfo::DataType::DataTypeBegin>;

}  // namespace
