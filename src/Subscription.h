/*****************************************************************************
 * vim:tw=78
 *
 * Copyright (C) 2021 Richard Hacker (lerichi at gmx dot net),
 *                    Florian Pose (fp at igh dot de),
 *                    Bjarne von Horn (vh at igh dot de).
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#ifndef PDCOM5_SUBSCRIPTION_IMPL_H
#define PDCOM5_SUBSCRIPTION_IMPL_H

#include <memory>
#include <pdcom5/Subscriber.h>
#include <pdcom5/Subscription.h>
#include <unordered_map>

namespace PdCom { namespace impl {
class Process;
class Variable;

class Subscription
{
  public:
    Subscription(
            std::shared_ptr<const Variable> variable,
            PdCom::Subscription *This,
            PdCom::Subscriber &subscriber,
            std::weak_ptr<Process> process) :
        variable_(std::move(variable)),
        This_(This),
        subscriber_(subscriber),
        process_(process)
    {}


    virtual void poll()                 = 0;
    virtual const void *getData() const = 0;

    std::weak_ptr<const Variable> const variable_;
    PdCom::Subscription *This_ = nullptr;
    PdCom::Subscriber &subscriber_;
    std::weak_ptr<Process> process_;
    std::string path_;


    virtual ~Subscription() = default;

    // this member method commits suicide!
    void replaceImpl(std::shared_ptr<Subscription> impl)
    {
        // assume that we don't need to unregister ourselves
        process_.reset();
        if (impl)
            impl->path_ = path_;
        std::swap(impl, This_->pimpl);
    }

    void newValues(std::chrono::nanoseconds time_ns)
    {
        subscriber_.newValues(time_ns);
    }

    class SubscriberNotifier
    {
        std::unordered_map<Subscriber *, std::chrono::nanoseconds> subscribers_;

      public:
        void clear() { subscribers_.clear(); }
        void insert(Subscriber &s, std::chrono::nanoseconds ts)
        {
            subscribers_[&s] = ts;
        }

        void notify()
        {
            for (const auto s : subscribers_)
                s.first->newValues(s.second);
        }
    };

    struct PendingStateChange
    {
        PendingStateChange(
                const std::shared_ptr<Subscription> &sub,
                PdCom::Subscription::State st) :
            subscription_(sub), state_(st)
        {
            sub->This_->state_ = st;
        }
        std::weak_ptr<Subscription> subscription_;
        PdCom::Subscription::State state_;
        void execute() const
        {
            if (const auto sub = subscription_.lock())
                if (sub->This_) {
                    sub->This_->state_ = state_;
                    sub->subscriber_.stateChanged(*sub->This_);
                }
        }
    };
};


}}  // namespace PdCom::impl


#endif  // PDCOM5_SUBSCRIPTION_IMPL_H
