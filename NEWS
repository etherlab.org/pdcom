[2025-01-07]

  * Use compile test to detect expat Reparse Deferral function.
    This also detects backports, like on Debian 11.

[2024-11-05]

  * Always disable expat Reparse Deferral to avoid getting stuck

[2024-06-06]

Version 5.3.0

  * Introduce async for Python API to avoid data loss on high subscription rate.

Version 5.2.6

  * Fix a bug that caused an exception when trying to update the subscription
    while the <data> tag was open

Version 5.2.5

  * Improved performance when copying from XML buffers

Version 5.2.4

  * Enable group feature if list feature is available

Version 5.2.3

  * Fix timestamp calculation on decimation > 1

Version 5.2.2

  * Writable property for Python variable

[2023-11-08]

Version 5.2.1

  * Improve compatibility to PdServ 1.0.0

[2023-09-12]

Version 5.2.0

  * Rework asyncio interface, Subscriber::newValues() now returns an
    AsyncContextManager.
  * SimpleLoginManager has a new NoSaslMechanism error code
  * Timestamp of a Parameter is not 0 anymore

Version 5.1.1

  * Fix compatibility to PdServ 1.1.0
  * Embed git commit hash and version information into library

[2023-05-23]

Version 5.1.0

  * Major API redesign based on versions 3 and 4

[2021-04-21]

Version 4.2.0

- small fixes

- changes to be aware of:
  * python interface reworked; removed boost depenency
  * Subscriber::invalid() got a new overloaded virtual method; old method is
    deprecated
  * The mechanics of requesting message history is completely reworked;
    - Process::messageHistory() and messageHistoryEnd() removed
    - Process::processMessage() got a real knock on the head

[2018-06-07]

Version 4.1.0

- Header files moved: The header files used to be pdcom.h and pdcom/*.h.
  These have now been moved to pdcom4.h and pdcom4/*.h to enable parallel
  installation with stable-3.0

- new features:

  * broadcast(), broadcastReply(): used to transmit and receive messages
    between all clients

  * transmitSemaphore(): used in multithreaded applications to serialize
    transmitted commands on the network.

  * messageHistoryEnd(): used to signal the end of messageHistory()

- changes to be aware of:

  * messageHistory(): due to improvements in the protocol, the messages come in
    reverse order when calling messageHistory(). If you used messageHistory(),
    this is an incompatable change and you must rework processMessage()

  * processMessage(): A parameter "text" is new and bears a string with
    the message text.

- python interface
  * changed to reflect the C++ changes
  * version now a string instead of number list

----------------------------------------------
Version 4.0.0
