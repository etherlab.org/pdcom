# Python wrapper around PdCom5

## The Base Wrapper

Most of the PdCom5 C++ classes are also available as Python classes.
The translation is done with the help of pybind11.

### Module methods

Methods directly available on module level

#### has_sasl

Returns true when PdCom5 was built with SASL support.

#### full_version

Returns the full version code of the C++ library,
including the hash of the commit and a trailing plus sign in case of
uncommitted changes during the build.
Example: "5.1.0.8.gea62937+".

### Class overview

The following classes are available in the `pdcom5.PdComWrapper` module:
 - `Process`
 - `Variable`
 - `Subscription`
 - `Subscriber`
 - `Transmission`

The following exceptions (from the `pdcom5.PdComWrapper` module) can be raised:
 - `Exception` (aka `PdCom::Exception` from C++), base class for all derived exceptions
 - `InternalError`
 - `InvalidArgument`
 - `InvalidSubscription`
 - `NotConnected`
 - `ProcessGoneAway`
 - `ProtocolError`

To catch them, you'll have to import them
(`from pdcom5.PdComWrapper import Exception as PdComException`).

### Virtual Functions

Many callbacks from the PdCom5 C++ library are implemented as virtual functions.
So, some classes need to be derived from.
This also applies to the python classes from the `pdcom5.PdComWrapper` module.
Please note that the signature of the overridden virtual functions must be an
exact match, so please don't do things like `**kwargs`.
Raising an Exception is allowed and encouraged.

#### Process class

```python
class MyProcess(Process):
    def write(self, b: memoryview) -> None:
        pass
    def read(self, b: memoryview) -> int:
        pass
    def applicationName(self) -> str:
        pass
    def connected(self) -> None:
        pass
    def listReply(self, vars: list, directiores: list) -> None:
        pass
    def findReply(self, var: Optional[Variable]) -> None:
        # var can be Variable if found or None if not found.
        pass
    def pingReply(self) -> None:
        pass
    def alive(self) -> None:
        pass
    def broadcastReply(self, message: str, attr: str, time: datetime.timedelta) -> None:
        pass

    def clientStatisticsReply(self, statistics: list[ClientStatistics]) -> None:
        pass
```

#### Subscriber class

```python
class MySubscriber(Subscriber):
    def __init__(self, transmission: Transmission):
        super().__init__(transmission)

    def stateChanged(self, s: Subscription) -> None:
        pass

    def newValues(self, time: datetime.timedelta) -> None:
        pass
```

### Member functions

The following member functions are available to the library user:

#### Process class

```python
class Process:
    def asyncData(self):
        pass
    def callPendingCallbacks(self) -> None:
        pass
    def list(self, path: str = "") -> bool:
        pass
    def find(self, path: str) -> bool:
        pass
    def broadcast(self, message: str, attr: str = "text") -> None:
        pass
```
In addition, the properties `name` and `version` are available.

#### Variable class

```python
class Variable:
    def setValue(self, value: numpy.ndarray, offset: int = 0) -> SetValueFuture:
        pass

    def poll(self) -> PollFuture:
        pass

class SetValueFuture:
    def then(self, callback: Callable[[], None]) -> None:
        pass

    def handle_exception(self, callback: Callable[[Exception], None]) -> None:
        pass

class PollFuture:
    def then(self, callback: Callable[[VariablePollResult, timedelta], None]) -> None:
        pass

    def handle_exception(self, callback: Callable[[Exception], None]) -> None:
        pass
```

Both member functions of `Variable` return a Future.
The Future can take two callbacks, which receive the result resp. an exception.
The first parameter of the PollFuture callback, of type `VariablePollResult`,
has a `value` attribute.

Properties (readonly):
 - `empty`
 - `path`
 - `name`
 - `shape`

#### Transmission class

A `datetime.timedelta` instance can be converted to a Transmission.
For event and poll mode, please use the static properties `event_mode` and `poll_mode`.

#### Subscriber class

The Subscriber class needs to be derived from, as shown above.
Its constructor takes a Transmission instance as parameter.
This transmission instance is later available as property.

#### Subscription class

The Subscription class has three overloaded constructors:
 - a default ctor (no arguments)
 - a Subscriber instance and a Variable instance
 - a Subscriber instance, a Process instance and a path (a `str`)

Furthermore, it has a `cancel()` member and the following properties:
 - `variable` (can be `None`)
 - `value` (NumPy Array)
 - `state` (See the `State` enum)

The `State` static class member is an enum with the following values:
   - `Pending`
   - `Active`
   - `Invalid`

#### ClientStatistics class

Properties:
 - `name`
 - `application_name`
 - `received_bytes`
 - `sent_bytes`
 - `connected_time` (datetime.timedelta instance)


## FAQ

### Error: Unable to cast Python instance to C++ type (compile in debug mode for details)

Make sure that the function signature of the python class is right
(especially the return type).
