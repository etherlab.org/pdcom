/*****************************************************************************
 *
 * Copyright (C) 2021 Bjarne von Horn (vh at igh dot de).
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#ifndef PY_SSIZE_T_CLEAN
#define PY_SSIZE_T_CLEAN
#endif
#include <Python.h>
#include <memory>
#include <pdcom5.h>
#include <pdcom5/ClientStatistics.h>
#include <pdcom5/Exception.h>
#include <pdcom5/Future.h>
#include <pdcom5/MessageManagerBase.h>
#include <pdcom5/Process.h>
#include <pdcom5/Subscriber.h>
#include <pdcom5/Subscription.h>
#include <pdcom5/Variable.h>
#include <pybind11/chrono.h>
#include <pybind11/numpy.h>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#if USE_SASL
#include <pdcom5/SimpleLoginManager.h>
#endif

namespace py = pybind11;

template <class Future>
struct FutureRegisterer;

template <class Exception, class... Result>
struct FutureRegisterer<PdCom::Future<Exception, Result...>>
{
    static void do_register(py::object &m, const char *name)
    {
        using Future = PdCom::Future<Exception, Result...>;

        py::class_<Future>(m, name)
                .def("then",
                     [](Future const &fut, py::object callable) {
                         fut.then([callable](Result... res) {
                             py::gil_scoped_acquire gil;
                             callable(std::forward<Result>(res)...);
                         });
                     })
                .def("handle_exception",
                     [](Future const &fut, py::object callable) {
                         fut.handle_exception([callable](Exception const &ex) {
                             py::gil_scoped_acquire gil;
                             callable(ex);
                         });
                     })
                .def_property_readonly("empty", &Future::empty)
                //
                ;
    }
};

namespace {
PdCom::TypeInfo::DataType convert_dt(py::dtype const np_dt)
{
    using TI = PdCom::TypeInfo::DataType;
    switch (np_dt.kind()) {
        case 'b':
        case 'i':
            switch (np_dt.itemsize()) {
                case 1:
                    return TI::int8_T;
                case 2:
                    return TI::int16_T;
                case 4:
                    return TI::int32_T;
                case 8:
                    return TI::int64_T;
            }
            break;
        case 'B':
        case 'u':
            switch (np_dt.itemsize()) {
                case 1:
                    return TI::uint8_T;
                case 2:
                    return TI::uint16_T;
                case 4:
                    return TI::uint32_T;
                case 8:
                    return TI::uint64_T;
            }
            break;
        case 'f':
            switch (np_dt.itemsize()) {
                case 4:
                    return TI::single_T;
                case 8:
                    return TI::double_T;
            }
    }
    throw std::runtime_error("unknown datatype");
}
py::array create_buf(const PdCom::Variable &v, const PdCom::SizeInfo &si)
{
    using dt = PdCom::TypeInfo::DataType;

    const py::array::ShapeContainer sc {si.begin(), si.end()};
    switch (v.getTypeInfo().type) {
        // case dt::boolean_T:
        case dt::uint8_T:
            return {py::dtype("B"), sc};
        case dt::int8_T:
            return {py::dtype("b"), sc};
        case dt::uint16_T:
            return {py::dtype("u2"), sc};
        case dt::int16_T:
            return {py::dtype("i2"), sc};
        case dt::uint32_T:
            return {py::dtype("u4"), sc};
        case dt::int32_T:
            return {py::dtype("i4"), sc};
        case dt::uint64_T:
            return {py::dtype("u8"), sc};
        case dt::int64_T:
            return {py::dtype("i8"), sc};
        case dt::double_T:
            return {py::dtype("f8"), sc};
        case dt::single_T:
            return {py::dtype("f4"), sc};
        default:
            throw std::runtime_error("unknown type id");
    }
}

PdCom::Selector cast_selector_nullptr(py::object selector)
{
    if (selector.is_none())
        return {nullptr};
    return py::cast<PdCom::Selector>(selector);
}

PdCom::Selector cast_selector_default(py::object selector)
{
    if (selector.is_none())
        return PdCom::Selector {};
    return py::cast<PdCom::Selector>(selector);
}
}  // namespace

class WrappedProcess;

class WrappedVariable : public PdCom::Variable
{
    std::shared_ptr<WrappedProcess> process_;

  public:
    std::shared_ptr<WrappedProcess> getProcess() const { return process_; }

    WrappedVariable(
            PdCom::Variable v,
            std::shared_ptr<WrappedProcess> process) :
        PdCom::Variable(std::move(v)), process_(std::move(process))
    {}

    PdCom::Variable::SetValueFuture
    setValue(py::array values, py::object selector)
    {
        if (!(values.flags() & py::array::c_style))
            throw PdCom::InvalidArgument("Array must be c-style contiguous");
        const auto np_dt = values.dtype();
        if (np_dt.has_fields())
            throw PdCom::InvalidArgument("array must not contain fields");
        const auto pd_type = convert_dt(np_dt);
        const auto s       = cast_selector_nullptr(selector);
        const auto size    = values.size();
        const auto data    = values.data();

        py::gil_scoped_release gil;
        return PdCom::Variable::setValue(data, pd_type, size, s);
    }
    py::object getSizeInfoPy()
    {
        return py::cast(
                static_cast<const std::vector<PdCom::SizeInfo::value_type> &>(
                        getSizeInfo()));
    }
};

class PythonSubscriber :

    public std::enable_shared_from_this<PythonSubscriber>,
    public PdCom::Subscriber
{
  public:
    PythonSubscriber(const PdCom::Transmission &tr) :
        std::enable_shared_from_this<PythonSubscriber>(), PdCom::Subscriber(tr)
    {}
    virtual ~PythonSubscriber() = default;
};

class SubscriberTrampoline : public PythonSubscriber
{
  public:
    static std::shared_ptr<SubscriberTrampoline>
    create(const PdCom::Transmission &tr)
    {
        return std::make_shared<SubscriberTrampoline>(tr);
    }

    using PythonSubscriber::PythonSubscriber;

    void newValues(std::chrono::nanoseconds time_ns) override
    {
        PYBIND11_OVERRIDE_PURE(void, PythonSubscriber, newValues, time_ns);
    }

    void stateChanged(PdCom::Subscription const &) override;
};


class WrappedProcess :
    public std::enable_shared_from_this<WrappedProcess>,
    public PdCom::Process
{
  protected:
    WrappedProcess() :
        std::enable_shared_from_this<WrappedProcess>(), PdCom::Process()
    {}

  public:
    virtual ~WrappedProcess() = default;

    using Process::find;
    using Process::getClientStatistics;
    using Process::list;
    using Process::ping;
    using Process::reset;
    using Process::setAuthManager;
    using Process::setMessageManager;
};

class ProcessTrampoline final : public WrappedProcess
{
  public:
    ProcessTrampoline() : WrappedProcess() {}
    virtual ~ProcessTrampoline() = default;

    static std::shared_ptr<WrappedProcess> create()
    {
        return std::make_shared<ProcessTrampoline>();
    }

  private:
    std::string applicationName() const override
    {
        PYBIND11_OVERRIDE(std::string, WrappedProcess, applicationName, );
    }
    int read(char *buf, int count) override
    {
        PYBIND11_OVERRIDE_PURE(
                int, WrappedProcess, read,
                py::memoryview::from_memory(buf, count, false));
    }
    void write(const char *buf, size_t count) override
    {
        PYBIND11_OVERRIDE_PURE(
                void, WrappedProcess, write,
                py::memoryview::from_memory(buf, count));
    }
    void flush() override
    {
        py::gil_scoped_acquire gil;
        py::function override = py::get_override(
                static_cast<const WrappedProcess *>(this), "flush");
        if (override) {
            override();
        }
    }
    void connected() override
    {
        PYBIND11_OVERRIDE_PURE(void, WrappedProcess, connected, );
    }
    void listReply(
            std::vector<PdCom::Variable> variables,
            std::vector<std::string> directories) override
    {
        py::gil_scoped_acquire gil;
        py::function override = py::get_override(
                static_cast<const WrappedProcess *>(this), "listReply");
        if (override) {
            py::list vars;
            for (auto &var : variables) {
                vars.append(
                        WrappedVariable(std::move(var), shared_from_this()));
            }
            override(vars, directories);
        }
    }
    void findReply(const PdCom::Variable &variable) override
    {
        py::gil_scoped_acquire gil;
        py::function override = py::get_override(
                static_cast<const WrappedProcess *>(this), "findReply");
        if (override) {
            if (variable.empty())
                override(py::none());
            else
                override(std::unique_ptr<WrappedVariable>(
                        new WrappedVariable(variable, shared_from_this())));
        }
    }

    void clientStatisticsReply(
            std::vector<PdCom::ClientStatistics> statistics) override
    {
        PYBIND11_OVERRIDE(
                void, WrappedProcess, clientStatisticsReply, statistics);
    }

    void pingReply() override
    {
        py::gil_scoped_acquire gil;
        py::function override = py::get_override(
                static_cast<const WrappedProcess *>(this), "pingReply");
        if (override) {
            override();
        }
    }
    bool alive() override
    {
        py::gil_scoped_acquire gil;
        py::function override = py::get_override(
                static_cast<const WrappedProcess *>(this), "alive");
        if (override) {
            return py::cast<bool>(override());
        }
        return true;
    }

    void broadcastReply(
            const std::string &message,
            const std::string &attr,
            std::chrono::nanoseconds time_ns,
            const std::string &user) override
    {
        py::gil_scoped_acquire gil;
        py::function override = py::get_override(
                static_cast<const WrappedProcess *>(this), "broadcastReply");
        if (override) {
            override(message, attr, time_ns, user);
        }
    }
};

template <class T>
py::object getValuePy(const T &t, const PdCom::SizeInfo &si)
{
    auto var = t.getVariable();
    auto ans = create_buf(var, si);
    if (!(ans.flags() & py::array::c_style))
        throw PdCom::InternalError("Numpy buffer is not c-style contiguous");
    std::memcpy(
            ans.mutable_data(), t.getData(),
            si.totalElements() * var.getTypeInfo().element_size);
    return ans;
}

template <class T>
py::object getValuePy(const T &t)
{
    return getValuePy(t, t.getVariable().getSizeInfo());
}


class WrappedSubscription :
    public std::enable_shared_from_this<WrappedSubscription>,
    public PdCom::Subscription
{
    std::shared_ptr<WrappedProcess> process_;
    std::shared_ptr<PythonSubscriber> mailbox_;

  public:
    PdCom::Selector selector_;

    WrappedSubscription() = default;
    WrappedSubscription(
            std::shared_ptr<PythonSubscriber> sub,
            const WrappedVariable &var,
            const PdCom::Selector &selector) :
        std::enable_shared_from_this<WrappedSubscription>(),
        PdCom::Subscription(*sub, var, selector),
        process_(var.getProcess()),
        mailbox_(sub),
        selector_(selector)
    {}
    WrappedSubscription(
            std::shared_ptr<PythonSubscriber> sub,
            std::shared_ptr<WrappedProcess> process,
            const std::string &path,
            const PdCom::Selector &selector) :
        std::enable_shared_from_this<WrappedSubscription>(),
        PdCom::Subscription(*sub, *process, path, selector),
        process_(process),
        mailbox_(sub),
        selector_(selector)
    {}

    static std::shared_ptr<WrappedSubscription> create1()
    {
        return std::make_shared<WrappedSubscription>();
    }
    static std::shared_ptr<WrappedSubscription>
    create2(std::shared_ptr<PythonSubscriber> sub,
            const WrappedVariable &var,
            py::object selector)
    {
        return std::make_shared<WrappedSubscription>(
                sub, var, cast_selector_default(selector));
    }
    static std::shared_ptr<WrappedSubscription>
    create3(std::shared_ptr<PythonSubscriber> sub,
            std::shared_ptr<WrappedProcess> process,
            const std::string &path,
            py::object selector)
    {
        return std::make_shared<WrappedSubscription>(
                sub, process, path, cast_selector_default(selector));
    }


    void cancel()
    {
        static_cast<PdCom::Subscription &>(*this) = PdCom::Subscription();
    }
    WrappedSubscription(WrappedSubscription const &) = delete;
    WrappedSubscription(WrappedSubscription &&)      = default;
    WrappedSubscription &operator=(WrappedSubscription const &) = delete;
    WrappedSubscription &operator=(WrappedSubscription &&) = default;

    virtual ~WrappedSubscription()
    {
        // unsubscribe before Process goes away
        if (getState() != State::Invalid)
            cancel();
    }

    WrappedVariable getVariablePy() { return {getVariable(), process_}; }
};

void SubscriberTrampoline::stateChanged(PdCom::Subscription const &s)
{
    PYBIND11_OVERRIDE_PURE(
            void, PythonSubscriber, stateChanged,
            const_cast<WrappedSubscription &>(
                    static_cast<WrappedSubscription const &>(s))
                    .shared_from_this());
}

class MessageManagerWrapper : public PdCom::MessageManagerBase
{
  public:
    using MessageManagerBase::MessageManagerBase;

    using MessageManagerBase::activeMessages;
    using MessageManagerBase::getMessage;

    virtual ~MessageManagerWrapper() = default;
};

class MessageManagerTrampoline final : MessageManagerWrapper
{
  public:
    using MessageManagerWrapper::MessageManagerWrapper;

    void processMessage(PdCom::Message message) override
    {
        PYBIND11_OVERRIDE_PURE(
                void, MessageManagerWrapper, processMessage, message);
    }
    void getMessageReply(PdCom::Message message) override
    {
        PYBIND11_OVERRIDE_PURE(
                void, MessageManagerWrapper, getMessageReply, message);
    }

    void activeMessagesReply(std::vector<PdCom::Message> messageList) override
    {
        PYBIND11_OVERRIDE_PURE(
                void, MessageManagerWrapper, activeMessagesReply, messageList);
    }
};

#if USE_SASL

class SLMWrapper : public PdCom::SimpleLoginManager
{
  public:
    using SimpleLoginManager::SimpleLoginManager;

    using SimpleLoginManager::login;
    using SimpleLoginManager::logout;
};

class SLMTrampoline final : public SLMWrapper
{
  public:
    using SLMWrapper::SLMWrapper;

    void completed(LoginResult success) override
    {
        PYBIND11_OVERRIDE_PURE(void, SLMWrapper, completed, success);
    }

    std::string getAuthname() override
    {
        PYBIND11_OVERRIDE(std::string, SLMWrapper, getAuthname, );
    }

    std::string getPassword() override
    {
        PYBIND11_OVERRIDE(std::string, SLMWrapper, getPassword, );
    }

    std::string
    getRealm(const std::vector<const char *> &available_realms) override
    {
        PYBIND11_OVERRIDE(std::string, SLMWrapper, getRealm, available_realms);
    }

    std::string getOption(const char *plugin_name, const char *option) override
    {
        PYBIND11_OVERRIDE(
                std::string, SLMWrapper, getOption, plugin_name, option);
    }
    void log(int level, const char *message) override
    {
        PYBIND11_OVERRIDE(void, SLMWrapper, log, level, message);
    }
};


#endif  // USE_SASL

PYBIND11_MODULE(_PdComWrapper, m)
{
    using PdCom::ClientStatistics;
    using PdCom::Message;
    using PdCom::Subscription;
    using PdCom::Transmission;
    using PdCom::Variable;

    FutureRegisterer<Variable::SetValueFuture>::do_register(
            m, "SetValueFuture");
    FutureRegisterer<Variable::PollFuture>::do_register(m, "PollFuture");
    py::class_<PdCom::VariablePollResult>(m, "VariablePollResult")
            .def_property_readonly(
                    "value", [](const PdCom::VariablePollResult &r) {
                        return getValuePy(r);
                    });

    m.def("has_sasl", []() -> bool { return USE_SASL; });
    m.def("full_version",
          []() -> std::string { return PdCom::pdcom_full_version; });

    py::class_<PdCom::Selector>(m, "Selector", "Default Selector")
            .def(py::init());
    py::class_<PdCom::ScalarSelector, PdCom::Selector>(
            m, "ScalarSelector",
            "Select a Scalar from a multidimensional variable.\n\n"
            ":param indices: Vector of indices, row major")
            .def(py::init<std::vector<int>>(), py::arg("indices"));

#if USE_SASL
    py::class_<SLMWrapper, SLMTrampoline> slm(m, "SimpleLoginManager");
    slm.def(py::init<const char *>(), py::arg("remote host"))
            .def("login",
                 [](SLMWrapper &slm) {
                     py::gil_scoped_release gil;
                     return slm.login();
                 })
            .def("logout", [](SLMWrapper &slm) {
                py::gil_scoped_release gil;
                slm.logout();
            });
    py::register_exception<PdCom::SimpleLoginManager::Cancel>(slm, "Cancel");
    py::enum_<SLMWrapper::LoginResult>(slm, "LoginResult")
            .value("Success", SLMWrapper::LoginResult::Success)
            .value("Error", SLMWrapper::LoginResult::Error)
            .value("Canceled", SLMWrapper::LoginResult::Canceled)
            //
            ;

    PdCom::SimpleLoginManager::InitLibrary();
#endif

    py::class_<ClientStatistics>(
            m, "ClientStatistics", "Info about connected clients.")
            .def(py::init())
            .def_readwrite(
                    "name", &ClientStatistics::name_,
                    "Name of connected client host.")
            .def_readwrite(
                    "application_name", &ClientStatistics::application_name_,
                    "Name of connected client application.")
            .def_readwrite(
                    "received_bytes", &ClientStatistics::received_bytes_,
                    "Number of received bytes")
            .def_readwrite(
                    "sent_bytes", &ClientStatistics::sent_bytes_,
                    "Number of transmitted bytes")
            .def_readwrite(
                    "connected_time", &ClientStatistics::connected_time_,
                    "Timepoint when client has connected.")
            //
            ;

    py::enum_<PdCom::LogLevel>(m, "LogLevel", "Message Severity")
            .value("Reset", PdCom::LogLevel::Reset)
            .value("Emergency", PdCom::LogLevel::Emergency)
            .value("Alert", PdCom::LogLevel::Alert)
            .value("Critical", PdCom::LogLevel::Critical)
            .value("Error", PdCom::LogLevel::Error)
            .value("Warn", PdCom::LogLevel::Warn)
            .value("Info", PdCom::LogLevel::Info)
            .value("Debug", PdCom::LogLevel::Debug)
            .value("Trace", PdCom::LogLevel::Trace)
            //
            ;

    py::class_<Message>(m, "Message", "Messages from PdServ Events")
            .def(py::init())
            .def_readwrite(
                    "seqNo", &Message::seqNo,
                    "Sequence Number, wraps naturally.")
            .def_readwrite(
                    "level", &Message::level,
                    "Severity, see :py:class:`LogLevel`.")
            .def_readwrite("path", &Message::path, "Path of Event")
            .def_readwrite("time", &Message::time, "Event time")
            .def_readwrite("text", &Message::text, "Description of Event.")
            .def_readwrite("index", &Message::index, "Index, -1 for scalar.")
            //
            ;


    py::class_<MessageManagerWrapper, MessageManagerTrampoline>(
            m, "MessageManagerBase")
            .def(py::init())
            .def("activeMessages",
                 [](MessageManagerWrapper &mm) {
                     py::gil_scoped_release gil;
                     mm.activeMessages();
                 })
            .def(
                    "getMessage",
                    [](MessageManagerWrapper &mm, uint32_t no) {
                        py::gil_scoped_release gil;
                        mm.getMessage(no);
                    },
                    py::arg("sequence_number"))
            //
            ;

    py::class_<
            WrappedProcess, ProcessTrampoline, std::shared_ptr<WrappedProcess>>(
            m, "Process")
            .def(py::init<>(&ProcessTrampoline::create))
            .def_property_readonly("name", &WrappedProcess::name)
            .def_property_readonly("version", &WrappedProcess::version)
            .def("asyncData",
                 [](WrappedProcess &p) {
                     py::gil_scoped_release r;
                     p.asyncData();
                 })
            .def("callPendingCallbacks", &WrappedProcess::callPendingCallbacks)
            .def("list", &WrappedProcess::list, py::arg("path") = "")
            .def("find", &WrappedProcess::find, py::arg("path"))
            .def("getClientStatistics", &WrappedProcess::getClientStatistics)
            .def("ping", &WrappedProcess::ping)
            .def("broadcast", &WrappedProcess::broadcast, py::arg("message"),
                 py::arg("attr") = "text")
            .def("reset", &WrappedProcess::reset)
            .def("setMessageManager",
                 [](WrappedProcess &p, MessageManagerWrapper &w) {
                     p.setMessageManager(&w);
                 })
#if USE_SASL
            .def("setAuthManager",
                 [](WrappedProcess &p, SLMWrapper &a) { p.setAuthManager(&a); })
#endif
            //
            ;
    py::class_<PdCom::Variable>(m, "_Variable");
    py::class_<WrappedVariable, PdCom::Variable>(m, "Variable")
            .def_property_readonly("empty", &Variable::empty)
            .def_property_readonly("path", &Variable::getPath)
            .def_property_readonly("name", &Variable::getName)
            .def_property_readonly("shape", &WrappedVariable::getSizeInfoPy)
            .def_property_readonly("task_id", &WrappedVariable::getTaskId)
            .def_property_readonly("writeable", &Variable::isWriteable)
            .def("poll", &WrappedVariable::poll)
            .def("setValue", &WrappedVariable::setValue, py::arg("value"),
                 py::arg("selector") = py::none());
    py::class_<WrappedSubscription, std::shared_ptr<WrappedSubscription>> sub(
            m, "Subscription");
    sub.def(py::init<>(&WrappedSubscription::create1))
            .def(py::init<>(&WrappedSubscription::create2),
                 py::arg("subscriber"), py::arg("variable"),
                 py::arg("selector") = py::none())
            .def(py::init<>(&WrappedSubscription::create3),
                 py::arg("subscriber"), py::arg("process"), py::arg("path"),
                 py::arg("selector") = py::none())
            .def("cancel", &WrappedSubscription::cancel)
            .def_property_readonly(
                    "variable", &WrappedSubscription::getVariablePy)
            .def_property_readonly(
                    "value",
                    [](const WrappedSubscription &s) {
                        return getValuePy(
                                s,
                                s.selector_.getViewSizeInfo(s.getVariable()));
                    })
            .def_property_readonly(
                    "state",
                    [](const WrappedSubscription &s) { return s.getState(); })
            .def("poll",
                 [](WrappedSubscription &s) {
                     py::gil_scoped_release gil;
                     s.poll();
                 })
            //
            ;
    py::class_<
            PythonSubscriber, std::shared_ptr<PythonSubscriber>,
            SubscriberTrampoline>(m, "Subscriber")
            .def(py::init<>(&SubscriberTrampoline::create),
                 py::arg("transmission"))
            .def_property_readonly(
                    "transmission", &PdCom::Subscriber::getTransmission);

    py::enum_<Subscription::State>(sub, "State")
            .value("Pending", Subscription::State::Pending)
            .value("Active", Subscription::State::Active)
            .value("Invalid", Subscription::State::Invalid);
    py::class_<Transmission>(m, "Transmission")
            .def(py::init<std::chrono::duration<double>>(), py::arg("duration"))
            .def_property_readonly_static(
                    "poll_mode",
                    [](py::object) { return Transmission {PdCom::poll_mode}; })
            .def_property_readonly_static("event_mode", [](py::object) {
                return Transmission {PdCom::event_mode};
            });

    const py::handle ex_base =
            py::register_exception<PdCom::Exception>(m, "Exception");
    py::register_exception<PdCom::InternalError>(m, "InternalError", ex_base);
    py::register_exception<PdCom::InvalidArgument>(
            m, "InvalidArgument", ex_base);
    py::register_exception<PdCom::EmptyVariable>(m, "EmptyVariable", ex_base);
    py::register_exception<PdCom::InvalidSubscription>(
            m, "InvalidSubscription", ex_base);
    py::register_exception<PdCom::NotConnected>(m, "NotConnected", ex_base);
    py::register_exception<PdCom::ProcessGoneAway>(
            m, "ProcessGoneAway", ex_base);
    py::register_exception<PdCom::ProtocolError>(m, "ProtocolError", ex_base);
    py::register_exception<PdCom::LoginRequired>(m, "LoginRequired", ex_base);
    // workaround: pybind11 does not support exporting exceptions as normal
    // classes
    py::class_<PdCom::ProcessGoneAway>(m, "ProcessGoneAwayClass");
}
