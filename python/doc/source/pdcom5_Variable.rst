PdCom5 Variable
===============

.. py::currentmodule:: pdcom5

.. autoclass:: pdcom5.Variable
   :members:
