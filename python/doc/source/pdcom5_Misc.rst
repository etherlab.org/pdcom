PdCom5 Miscellaneous
====================
.. py:currentmodule:: pdcom5

Broadcasts
----------

.. autoclass:: pdcom5.BroadcastMessage
   :members:

Client Statistics
-----------------

.. autoclass:: pdcom5.ClientStatistics
   :members:

Events / Messages
-----------------

.. autoclass:: pdcom5.LogLevel
   :members:

.. autoclass:: pdcom5.Message
   :members:

List of Variables
-----------------

.. autoclass:: pdcom5.ListReply
   :members:
