PdCom5 Subscription
===================

.. py:currentmodule:: pdcom5

Selectors
---------


.. autoclass:: pdcom5.Selector
   :members:

.. autoclass:: pdcom5.ScalarSelector
   :members:

Subscriptions
-------------

.. autoclass:: pdcom5.Subscription
   :members:

.. autoclass:: pdcom5.SubscriptionWithSubscriber
   :members:
