PdCom5 Exceptions
=================
.. py:currentmodule:: pdcom5

.. autoclass:: pdcom5.InternalError
    :members:

.. autoclass:: pdcom5.InvalidArgument
    :members:

.. autoclass:: pdcom5.InvalidSubscription
    :members:

.. autoclass:: pdcom5.LoginFailed
    :members:

.. autoclass:: pdcom5.LoginRequired
    :members:

.. autoclass:: pdcom5.NotConnected
    :members:

.. autoclass:: pdcom5.ProcessGoneAway
    :members:
