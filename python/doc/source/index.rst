.. PdCom5 documentation master file, created by
   sphinx-quickstart on Fri Jul  8 09:20:30 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to PdCom5's documentation!
==================================

.. image:: https://gitlab.com/etherlab.org/pdcom/badges/master/pipeline.svg
    :target: https://gitlab.com/etherlab.org/pdcom/commits/master
    :alt: pipeline status

.. image:: https://gitlab.com/etherlab.org/pdcom/badges/master/coverage.svg
    :target: https://gitlab.com/etherlab.org/pdcom/commits/master
    :alt: coverage report

.. py:module:: pdcom5

How to install
--------------

You can install PdCom5 using `pip`: `pip3 install pdcom5`.
The sources can be found `here <https://gitlab.com/etherlab.org/pdcom>`_ on GitLab.
They can be used to build PdCom5 on your own if the packages provided on PyPI
or Linux package repositories don't suit you.

Quickstart
----------

The counterpart of PdCom is `PdServ <https://gitlab.com/etherlab.org/pdserv>`_ which ships an example application.
This can be used for exploring the Python interface of PdCom5 if you don't have your own PdServ application.

Please note that this Python API is based on asyncio,
so uses a lot of coroutines (these things with `async` and `await`).
But if you have Python >= 3.8, you can still use this library interactively on the CLI
by starting the asyncio REPL: `python3 -m asyncio`.

The starting point for almost everything is the :py:class:`Process` class
which is in charge of handling the communication.
You can use it to find variables, to set parameters and to subscribe to signals.

In this snippet, a connection is made to the oscillator example in PdServ.
Its cosine output is polled twice,
then the `enable` Parameter is set to False.
After that, the cosine output is again polled twice.
Then, the cosine is enabled again.
Finally, a periodic subscription is established,
with a period of two Hz.

>>> import pdcom5
>>> process = pdcom5.Process()
>>> await process.connect("msr://localhost:2345")
>>> variable = await process.find("/osc/cos")
>>> variable.shape
(1,)
>>> await variable.poll()
(-6.698516364546891, datetime.timedelta(days=19831, seconds=52590, microseconds=757770))
>>> await variable.poll()
(-0.6620524859141752, datetime.timedelta(days=19831, seconds=52592, microseconds=717771))
>>> await process.setVariableValue("/osc/enable", False)
>>> await variable.poll()
(-1.336504969688323, datetime.timedelta(days=19831, seconds=52606, microseconds=527782))
>>> await variable.poll()
(-1.336504969688323, datetime.timedelta(days=19831, seconds=52607, microseconds=547806))
>>> await process.setVariableValue("/osc/enable", True)
>>> subscription = await process.subscribe(0.5, "/osc/cos")
>>> async for timestamp in subscription.newValues():
...    print((subscription.value, timestamp))
...
(7.271824824350419, datetime.timedelta(days=19831, seconds=52679, microseconds=597804))
(2.033658184944072, datetime.timedelta(days=19831, seconds=52680, microseconds=97877))
(-3.914956840413724, datetime.timedelta(days=19831, seconds=52680, microseconds=597896))
(-8.495901138737317, datetime.timedelta(days=19831, seconds=52681, microseconds=97879))
(-10.108844572458873, datetime.timedelta(days=19831, seconds=52681, microseconds=597802))
(-8.190313364322167, datetime.timedelta(days=19831, seconds=52682, microseconds=97916))
(-3.4105368627541592, datetime.timedelta(days=19831, seconds=52682, microseconds=597804))
(2.5606937520517863, datetime.timedelta(days=19831, seconds=52683, microseconds=97826))
(7.637358723265475, datetime.timedelta(days=19831, seconds=52683, microseconds=597976))
(10.045950358435528, datetime.timedelta(days=19831, seconds=52684, microseconds=97870))


There is also :py:attr:`Transmission.event_mode` and
:py:attr:`Transmission.poll_mode` for creating non-periodical subscriptions.

Authentication
--------------

Username and Password can be passed in the url given to :py:meth:`Process.connect`,
for example `msr://username:passwort@localhost`.
If `username` contains an at sign (`@`), it has to be escaped using `%40`.

If login is mandatory and no credentials were supplied,
:py:exc:`LoginRequired` is thrown when calling :py:meth:`Process.connect`.
:py:exc:`LoginFailed` is raised in case login has not been successful.

TLS Encryption
--------------

When using the `msrs` scheme, TLS will be used to encrypt the communication.
If the Certificate Authory (CA) which signed the public certificate
of the PdServ instance is not installed on your machine,
you can provide your own :py:class:`ssl.SSLContext`.

>>> import ssl
>>> ctx = ssl.SSLContext(ssl.PROTOCOL_TLS_CLIENT)
>>> ctx.load_verify_locations("/path/to/ca.pem")
>>> ctx.verify_mode = ssl.CERT_REQUIRED
>>> ctx.check_hostname = True
>>> import pdcom5
>>> process = pdcom5.Process()
>>> await process.connect("msrs://localhost", ctx)

You can also use the :py:class:`ssl.SSLContext` to provide User Certificates,
they can be used for TLS Client Authentication.

How to find the PdCom Version
-----------------------------

In the `pdcom5` module, the variable `full_version` contains the version
of the PdCom5 library. If you have any questions or report a bug,
please include this version information in your request. ::

    $ python3 -c "import pdcom5; print(pdcom5.full_version)"
    5.1.0.8.gea62937+

Indices and tables
------------------


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   pdcom5_Process
   pdcom5_Variable
   pdcom5_Subscription
   pdcom5_Subscriber
   pdcom5_Exceptions
   pdcom5_Misc

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
