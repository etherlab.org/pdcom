PdCom5 Subscriber
=================

.. py:currentmodule:: pdcom5

.. autoclass:: pdcom5.Transmission(period)

   Specifies how a variable is transferred to the client.

   :param datetime.timedelta period: Periodic transmission.

   .. attribute:: event_mode
      :annotation: Event based Transmission
      :type: Transmission

   .. attribute:: poll_mode
      :annotation: Poll based Transmission
      :type: Transmission

.. autoclass:: pdcom5.SubscriberBase
   :members:

.. autoclass:: pdcom5.Subscriber
   :members:
